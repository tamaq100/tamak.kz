<?php

function prepare_menu($menu) {
}
function print_menu($menu, $level = 1, $has_parent_active = false) {
	$lis = '';
	// Переменаая обозначает, является ли элемент или хотя бы один из его детей активными
	$has_active = false;

	foreach ($menu as $item) {
		// Является ли именно этот элемент активным
		$isActive = (isset($item['href']) && strstr($_SERVER['REQUEST_URI'].'/', $item['href'].'/'));

		// Если элемент является активным, тогда его родители тоже должны быть активными
		if ($isActive) {
			$has_active = true;
		}

		// Ищем детей
		$childs = '';
		// Проверяем, являются ли дети активными
		$has_child_active = false;
		if (isset($item['childrens'])) {
			$childs_menu = print_menu($item['childrens'], $level + 1, $isActive);

			if ($childs_menu['has_active']) $has_child_active = $childs_menu['has_active'];

			$childs = $childs_menu['html'];
		}
		if (isset($item['href'])) {
			$lis .= '<li>'.
					'<a class="'.( $has_child_active || $isActive ? 'active' : '') .'" href="'. $item['href'] .'">'. __($item['title']) .'</a>'.
					$childs.
				'</li>';
		} else {
			$lis .= '<li class="group-title">'.
					 __($item['title']) .
					$childs.
					'</li>';
		}
	}
	$ret = '<ul class="level-'.$level.'" '.($has_active || $has_parent_active ? 'style="display: block"' : '').'>'.$lis.'</ul>';

	if ($level == 1) {
		return $ret;
	} else {
		return array('has_active' => $has_active, 'html' => $ret);
	}

}  ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?= $title ?> | Dimension | Система управления сайтом</title>
	<meta http-equiv="author" content="4design">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="stylesheet" type="text/css" href="/admin/vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/admin/vendor/datatables/css/dataTables.bootstrap.css">
	<link rel="stylesheet" type="text/css" href="/admin/vendor/datatables/css/rowReorder.dataTables.min.css">
    <link rel="stylesheet" href="//use.fontawesome.com/fc28cc8f34.css"/>
	<!-- Datepicker -->
	<link rel="stylesheet" type="text/css" href="/admin/vendor/bootstrap-datetimepicker.min.css">
	<!-- DateRangepicker -->
	<link rel="stylesheet" type="text/css" href="/admin/vendor/daterangepicker/daterangepicker.css">
	
	<link rel="stylesheet" type="text/css" href="/admin/vendor/select2.min.css">
	<link rel="stylesheet" type="text/css" href="/admin/css/main.css">

	<!-- old -->
	<!-- <script type="text/javascript" src="/admin/vendor/jquery-1.12.4.min.js"></script> -->
	<!-- old -->

	<!-- new -->
	<script type="text/javascript" src="/admin/vendor/jquery-3.3.1.min.js"></script>
	<!-- new -->

	<script type="text/javascript" src="/admin/vendor/datatables/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="/admin/vendor/datatables/js/jquery.dataTables.rowReordering.js"></script>
	<script type="text/javascript" src="/admin/vendor/datatables/js/dataTables.bootstrap.js"></script>
	<script type="text/javascript" src="/admin/vendor/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
	<!-- <script type="text/javascript" src="/admin/vendor/jquery-"></script> -->
 
	<!-- Datepicker -->
	<!-- old -->
	<!-- <script type="text/javascript" src="/admin/vendor/moment.js"></script> -->
	<!-- oldend -->
	<script type="text/javascript" src="/admin/vendor/moment.new.min.js"></script>

	<script type="text/javascript" src="/admin/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="/admin/vendor/bootstrap-datetimepicker.min.js"></script>
	<!-- DateRangepicker -->
	<script type="text/javascript" src="/admin/vendor/daterangepicker/daterangepicker.js"></script>
	<!-- Pluplpad gallery -->
	<script type="text/javascript" src="/admin/vendor/plupload.full.min.js"></script>
	<!-- <script type="text/javascript" src="/vendor/jquery.chained.min.js"></script> -->
	<script type="text/javascript" src="/admin/vendor/select2.min.js"></script>
	<script type="text/javascript" src="/admin/js/all.min.js?v=1"></script>
	<link rel="icon" href="/favicon.ico?v=1" />
</head>
<body>
	<div class="header">
		<div>
			Здравствуйте, <?=$_SESSION['u_name']; ?><br>
			<a href="/admin/auth/logout">Logout</a>
		</div>
		<a href="/admin/"><img src="/admin/img/logo.png" width="757" height="41" alt="" style=""></a>
	</div>
	<div id="docbody">
		<table border="0" width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<td width="230" valign="top">
					<div class="domainname">
						Домен:<br />
                        <b><a href="/" target="_blank"><?=$_SERVER['SERVER_NAME'];?></a></b>
					</div>
					<div class="leftline"></div>
					<div class="leftblock">
						Управление сайтом
						<?= print_menu($scriptlist) ?>
					</div>
					<div class="leftline" ></div>
				</td>

				<td valign="top" style="padding-left:70px;">
				<h1><?= $title ?></h1>
				<h5><?= $desс ?></h5>
				<?= $content ?>
				</td>
			</tr>
		</table>
	</div>

	<div class="footer">
		<div style="float: left; width: 50%;">&copy; 2005 - <?=date('Y');?> <a href="https://4design.kz/" target="_blank">4Design Web Studio</a></div>
		<div style="float: left; width: 50%; text-align: right;">Powered by Dimension CMS 5.0</div>
		<div class="clear"></div>
	</div>
</body>
</html>