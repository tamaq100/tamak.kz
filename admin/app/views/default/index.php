<?php foreach ($scriptlist as $val): ?>
	<div class="icon">
		<a href="<?= $val['href'] ?>">
			<i class="fa fa-<?= (isset($val['icon']) && $val['icon'])?$val['icon']:'plus-square'; ?>" style="color: <?= (isset($val['icon_color']) && $val['icon_color'])?$val['icon_color']:'gray'; ?>;"></i>
		</a><br />
		<a href="<?= $val['href'] ?>"><?= __($val['title']) ?></a>
	</div>
<?php endforeach ?>
