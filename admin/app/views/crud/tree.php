<?php if (! $items): ?>
	<p>Записей не обнаружено</p>
<?php else: ?>

	<?php
	$treeId = uniqid();
	function print_tree($tree, $level = 1, $items) {
		global $model;
		if (! $tree) return;
		
		$ret = '';

		foreach ($tree as $item) {
			$li_class = (isset($items[$item->id])) ? 'with_sub_menu' : '';
			$data_children = (isset($items[$item->id])) ? ' data-children="'.$item->id.'"' : '';
			$li_class .= ' level-'.$level;

			$id = $item->id;

			$ret .= 
				'<tr'.($li_class != '' ? ' class="'.$li_class.'"' : '') . $data_children . '>'.
					'<td>
						<div class="tree-icon item-title"></div>
						<a class="item-title" href="/admin/'. strtolower($item->getClassName()) .'/show/'. $item->id .'">'. $item->title .'</a>
					</td>'.
					'<td width="10$">
						<div class="btn-group">
							<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Действие <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li><a href="/admin/'. strtolower($item->getClassName()) . '/edit/'. $id . '">Изменить</a></li>
								<li><a href="/admin/'. strtolower($item->getClassName()) . '/show/'. $id . '">Просмотреть</a></li>
								<li><a href="/admin/'. strtolower($item->getClassName()) . '/create_as_submenu/'. $id . '">Добавить подменю</a></li>
								<li>
									<a href="#" onclick="delete_form_submit(\'#delete_form_'. $id .'\')">Удалить</a>
									<form id="delete_form_'. $id .'" action="/admin/'. strtolower($item->getClassName()) .'/delete/'. $id .'" method="POST">
										<input type="hidden" name="id" value="'. $id .'">
									</form>
								</li>
							</ul>
						</div>
					</td>'.
				'</tr>';
			if (isset($items[$id])) {
				$ret .= '<tr class="parent-id-' . $id . '"><td colspan="2" class="inside-td">';
				$ret .= '<table class="table table-hover table-tree inside-table">';
				$ret .= '<tbody>';
				$ret .= print_tree($items[$id], $level + 1, $items);
				$ret .= '</tbody>';
				$ret .= '</table>';
				$ret .= '</td></tr>';
			} 

		}

		return $ret;
	} ?>

	<table class="table table-hover table-tree">
		<tbody>
			<?= print_tree(	$items[0], 1, $items); ?>
		</tbody>
	</table>

<?php endif ?>
<a href="/admin/<?= strtolower($model) ?>/create"><button type="button" class="btn btn-primary">Добавить запись</button></a>
