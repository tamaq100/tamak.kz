<?
		// Поля, которые есть абсолютно во всех моделях
		$base_keys = array('id', 'created', 'updated', 'active');
	?>
	<?php foreach ($fields as $field => $type): ?>
		<div class="form-group row">
			<div class="col-sm-2">
				<?php if (in_array($field, $base_keys)): ?>
					<span><?= __('crud.fields.'.$field) ?></span>
				<?php else: ?>
					<span><?= __(strtolower($entity->getClassName()).'.fields.'.$field) ?></span>
				<?php endif ?>
			</div>
			<div class="col-sm-10">
				<?php 
					$vars = array(
						'entity'		=> $entity,
						'field'			=> $field,
						'type'			=> $type,
					);
					CrudController::include_read_form_group($vars);
				?>
			</div>
		</div>
	<?php endforeach ?>

<a href="/admin/<?= strtolower($entity->getClassName()) ?>/edit/<?= $entity->id ?>"><button class="btn btn-primary">Изменить</button></a>
<a href="/admin/<?= strtolower($entity->getClassName()) ?>"><button class="btn btn-success">К списку</button></a>

