<? $columns = array() ?>
<form id="main_form" action="/admin/<?= strtolower($model) ?>/mass_update" method="POST">
    <table class="table table-hover table-bordered" id="crudtable" width="100%" cellpadding="5" cellspacing="0" style="display: none;">
        <thead>
            <tr class="search-row">
                <?php foreach ($fields as $field => $type): ?>
                    <th>
                        <?php if (! in_array($field, ['actions']) && ! in_array($type, ['hidden', 'checkbox_for_action'])): ?>
                            <?php 
                                $vars = [
                                    'field'     => $field,
                                    'model'     => $model,
                                    'entity'    => new $model,
                                ];
                             ?>
                            <?php if (in_array($type, ['date', 'datetime', 'only_date'])): ?>
                                <?= filterGroup('date_range', $vars) ?>
                            <?php else: ?>
                                <?= filterGroup($type, $vars) ?>
                            <?php endif ?>
                        <?php endif; ?>
                    </th>
                <?php endforeach ?>
                <th>
                    <?= filterGroup('reset', $vars) ?>
                </th>
            </tr>
            <tr>
                <?
                    // Поля, которые есть абсолютно во всех моделях
                    $base_keys = array('id', 'idx', 'created', 'updated', 'active', 'actions', 'ru_title', 'en_title', 'kz_title');
                    $fields['actions'] = '';

                    $columns = array_map(function($i) { return array('name' => $i, 'data' => $i); }, array_keys($fields));
                ?>
                <?php foreach ($fields as $key => $item): ?>
                    <?php
                        $title = in_array($key, $base_keys)
                            ? __('crud.fields.'.$key)
                            : __(strtolower($model).'.fields.'.$key);
                    ?>

                    <th class="table_key_<?= $key ?>"><nobr><?= $title ?></nobr></th>
                <?php endforeach ?>
            </tr>
        </thead>
    </table>

<?
$jsParams = array(
    'language' => array(
        'lengthMenu'        => 'Показывать по _MENU_ записей на странице',
        'zeroRecords'       => 'Ничего не найдено',
        'info'              => 'Страница _PAGE_ из _PAGES_',
        'infoEmpty'         => 'Нет подходящих записей',
        'infoFiltered'      => '(отфильтровано из _MAX_ записей)',  
        'searchPlaceholder' => 'Введите запрос',
        "paginate" => array(
            'previous'      => 'Предыдущая',
            'next'          => 'Следующая',
        )
    ),
    'lengthMenu' => array(array(30, 60, 120, -1), array(30, 60, 120, 'Все')),
    'autoWidth'  => false,
    "processing" => true,
    "serverSide" => true,
    'columns'    => $columns,
    "ajax_url"   => '/admin/' . strtolower($model) . '/index_ajax',
);

if (! $read_only && isset($model::$fields['idx'])) {
    $jsParams['draggable'] = '/admin/' . strtolower($model) . '/update_order';
    $jsParams['rowReorder'] = true;
}
$jsParams = json_encode($jsParams); ?>

<div id="jsParams" style="display: none;"><?= $jsParams ?></div>

<?php if (! $read_only): ?>
    <?php if (! $edit_only && ! $uncreatable): ?>
        <a href="/admin/<?= strtolower($model) ?>/create"><button type="button" class="btn btn-primary">Добавить запись</button></a>
    <?php endif ?>
    <?php if (isset($fields['active'])): ?>
        <button class="btn btn-success hidden" type="submit">Сохранить изменения</button>
    <?php endif ?>
<?php endif ?>

</form>

