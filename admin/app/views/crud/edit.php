<?
	$action = $entity->id ? 'update/'. $entity->id : 'save';
	$action = '/admin/'.strtolower($entity->getClassName()).'/'.$action;

	$after_forms = '';
?>
<form class="crud-form form-horizontal" action="<?= $action ?>" method="POST" enctype="multipart/form-data">
	<?
		// Поля, которые есть абсолютно во всех моделях
		$base_keys = array('id', 'created', 'updated', 'active');
	?>
	<?php foreach ($fields as $field => $type): ?>
		<?php if ($type == 'hidden'): ?>
			<input type="hidden" name="<?= $field ?>" value="<?= $entity->$field ?>">
			<?php continue ?>
		<?php endif ?>
		<div class="form-group">
			<label class="col-sm-2 control-label">
				<?php if (in_array($field, $base_keys)): ?>
					<?= __('crud.fields.'.$field) ?>
				<?php else: ?>
					<?= __(strtolower($entity->getClassName()).'.fields.'.$field) ?>
				<?php endif ?>
			</label>
			<div class="col-sm-10">
			<?php 
				$vars = array(
					'entity'		=> $entity,
					'after_forms'	=> $after_forms,
					'field'			=> $field,
					'type'			=> $type,
				);
				CrudController::include_edit_form_group($vars);
			?>
			</div>
		</div>
	<?php endforeach ?>

	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<button id="saveEntityBtn" type="button"
				data-entity-id="<?= $entity->id ?>" 
				data-entity-controller="<?= $_GET['controller'] ?>" 
				class="btn btn-primary">Сохранить</button>
			<button class="btn btn-warning" id="save_n_continue" type="button"
				data-entity-id="<?= $entity->id ?>" 
				data-entity-controller="<?= $_GET['controller'] ?>">
				Сохранить и продолжить</button>
<!--			<button class="btn btn-info" id="save_n_create" type="button"-->
<!--				data-entity-id="--><?//= $entity->id ?><!--" -->
<!--				data-entity-controller="--><?//= $_GET['controller'] ?><!--">-->
<!--				Сохранить и создать</button>-->
			<button class="btn btn-danger" id="delete_btn" type="button" onclick="delete_form_submit('#delete_form')">Удалить</button>
			<button class="btn btn-info" id="delete_btn" type="button" onclick="window.history.back()">Назад</button>
			<?php if (isset($additional_buttons)): ?>
				<?= $additional_buttons ?>
			<?php endif ?>
		</div>
	</div>
</form>
<form id="delete_form" action="/admin/<?= strtolower($entity->getClassName()) ?>/delete/<?= $entity->id ?>" method="POST">
	<input type="hidden" name="id" value="<?= $entity->id ?>">
</form>

<?= $after_forms ?>