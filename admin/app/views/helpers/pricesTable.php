<? 
	$pricesModel = $entity->getClassName() . 'Price';
	$prices = $pricesModel::findBy([strtolower($entity->getClassName()) . '_id' => $entity->id]);
?>
<?php if ($prices): ?>
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>Кол-во человек</th>
				<th>Цена в USD</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($prices as $price): ?>
				<tr>
					<td>
						<?= $price->humansCount ?>
					</td>
					<td>
						<?= $price->price ?>
					</td>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>
<?php endif ?>
