<?php 
	$viza_countries = Need_viza_country::findBy(['from_country_id' => $entity->id]);
?>
<?php if ($viza_countries): ?>
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>Страна</th>
				<th>Нужна виза</th>
				<th>Примечание</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($viza_countries as $viza_country): ?>
				<tr>
					<td>
						<a href="/admin/country/edit/<?= $viza_country->getDefaultValue('to_country_id') ?>">
							<?= $viza_country->to_country_object ? $viza_country->to_country_object->title : ''  ?>
						</a>
					</td>
					<td>
						<?= $viza_country->need_viza ?>
					</td>
					<td>
						<?= $viza_country->note ?>
					</td>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>
<?php endif ?>
