<?php
	$earliest = $entity->select()->where('1=1')->orderBy('`created` ASC')->one();
	$latest   = $entity->select()->where('1=1')->orderBy('`created` DESC')->one();

	if (! $earliest) $earliest_date = 0;
	else $earliest_date = $earliest->$field;

	if (! $latest) $latest_date = 0;
	else $latest_date = $latest->$field;
?>

 <div class="input-group">
	<input class="daterange-filter form-control data-table-filter" name="<?= $field ?>"  data-start="<?= $earliest_date ?>" data-end="<?= $latest_date ?>">
    <div class="input-group-addon data-table-filter">
		<span class="fa fa-trash small clean-btn"></span>
    </div>
</div>