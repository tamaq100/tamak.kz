<?php 
	// Почему-то где-то у нас на relation полях в конце есть _id, а где-то нет, проще это один раз проверить чем везде менять
	$model = ucfirst($field);
	if ($model == 'Rubric') {
		$relation_entities = $model::baseFindAll();
	} else {
		$relation_entities = $model::findAll();
	}

 ?>

<?php if (count($relation_entities)): ?>
	
	<select class="form-control data-table-filter form-control-xs <?= count($relation_entities) > 7?'js-select2':'' ?>" name="<?= $field ?>">
	<option value="0">--</option>
	<?php foreach ($relation_entities as $key => $relation_entity): ?>
		<option value="<?= $relation_entity->id ?>">
			<?= $relation_entity->getNameForInput() ?>
		</option>
	<?php endforeach ?>
	</select>
<?php else: ?>
	<?php $controller = strtolower($entity->getRelationEntityModel($field)) ?>
	<p class="form-control-static"> --- </p>
<?php endif ?>