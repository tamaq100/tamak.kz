<?php $list = $entity->{'get'.ucfirst($field).'List'}(); ?>

<?php if (count($list)): ?>
	
	<select class="form-control data-table-filter form-control-xs" name="<?= $field ?>">
	<option value="">--</option>
	<?php foreach ($list as $key => $name): ?>
        <!-- Don't show entities with value 0 -->
        <?php if ($key): ?>
            <option value="<?= $key ?>" <?= ($key == $entity->$field ? 'selected' : '') ?>><?= $name ?></option>
        <?php endif ?>
	<?php endforeach ?>
	</select>
<?php else: ?>
	<p class="form-control-static">Записей по данной связи не найдено</p>
<?php endif ?>