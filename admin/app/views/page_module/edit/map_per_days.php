<div class="dinamic-blocks-wrapper">
	<?php if ($entity->json_data): ?>
		<? $koordinates = $entity->json_data->$field ?>
		<?php foreach ($koordinates as $id => $koordinate): ?>
			<div class="dinamic-block">
				<div class="col-xs-5">
					Заголовок
					<input class="form-control" type="text" name="map_per_days[<?= $id ?>][title]" value="<?= $koordinate->title ?>">
				</div>
				<div class="col-xs-5">
					Код карты
					<textarea class="form-control" name="map_per_days[<?= $id ?>][koordinates]"><?= $koordinate->koordinates ?></textarea>
				</div>
				<div class="col-xs-2">
					<button class="btn btn-danger delete-this">Удалить</button>
				</div>
			</div>
		<?php endforeach ?>
	<?php endif ?>
	<div class="hidden dinamic-block">
		<div class="col-xs-5">
			Заголовок
			<input class="form-control" type="text" original-name="map_per_days[uniqid][title]">
		</div>
		<div class="col-xs-5">
			Код карты
			<textarea class="form-control" rows="5" original-name="map_per_days[uniqid][koordinates]"></textarea>
		</div>
		<div class="col-xs-2">
			<button type="button" class="btn btn-danger delete-this">Удалить</button>
		</div>
	</div>
</div>
<button class="btn btn-default add-dinamic-block" type="button">
	Добавить
</button>