<div class="dinamic-blocks-wrapper">
	<?php if ($entity->json_data): ?>
		<? $rooms = $entity->json_data->$field ?>
		<?php foreach ($rooms as $id => $room): ?>
			<div class="dinamic-block">
				<div class="col-xs-5">
					Название
					<input class="form-control" type="text" name="rooms[<?= $id ?>][title]" value="<?= $room->title ?>">
				</div>
				<div class="col-xs-5">
					Цена
					<input class="form-control" type="number" name="rooms[<?= $id ?>][cost]" value="<?= $room->cost ?>">
				</div>
				<div class="col-xs-2">
					<button class="btn btn-danger delete-this">Удалить</button>
				</div>
			</div>
		<?php endforeach ?>
	<?php endif ?>
	<div class="hidden dinamic-block">
		<div class="col-xs-5">
			Название
			<input class="form-control" type="text" original-name="rooms[uniqid][title]">
		</div>
		<div class="col-xs-5">
			Цена
			<input class="form-control" type="number" original-name="rooms[uniqid][cost]">
		</div>
		<div class="col-xs-2">
			<button type="button" class="btn btn-danger delete-this">Удалить</button>
		</div>
	</div>
</div>
<button class="btn btn-default add-dinamic-block" type="button">
	Добавить
</button>