<? 
	$all_coordinates = Coordinate::findAll();
?>
<div class="dinamic-blocks-wrapper">
	<?php if ($entity->json_data): ?>
		<? $koordinates = $entity->json_data->$field ?>
		<?php foreach ($koordinates as $id => $koordinate): ?>
			<div class="dinamic-block">
				<div class="col-xs-10">
					<select name="map_koordinates[<?= $id ?>][point_id]" class="form-control">
						<?php foreach ($all_coordinates as $point_coordinate): ?>
							<option value="<?= $point_coordinate->id ?>"<?= $koordinate->point_id == $point_coordinate->id ? ' selected="selected"' : '' ?>>
								<?= $point_coordinate->ru_title ?>
							</option>
						<?php endforeach ?>
					</select>
				</div>			
				<div class="col-xs-2">
					<button class="btn btn-danger delete-this">Удалить</button>
				</div>
			</div>
		<?php endforeach ?>
	<?php endif ?>
	<div class="hidden dinamic-block">
		<div class="col-xs-10">
			<select original-name="map_koordinates[uniqid][point_id]" class="form-control">
				<?php foreach ($all_coordinates as $point_coordinate): ?>
					<option value="<?= $point_coordinate->id ?>">
						<?= $point_coordinate->ru_title ?>
					</option>
				<?php endforeach ?>
			</select>
		</div>
		<div class="col-xs-2">
			<button type="button" class="btn btn-danger delete-this">Удалить</button>
		</div>
	</div>
</div>
<button class="btn btn-default add-dinamic-block" type="button">
	Добавить
</button>