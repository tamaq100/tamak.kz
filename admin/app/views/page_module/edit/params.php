<div class="dinamic-blocks-wrapper">
	<?php if ($entity->json_data && isset($entity->json_data->$field)): ?>
		<? $params = $entity->json_data->$field ?>
		<?php foreach ($params as $id => $param): ?>
			<div class="dinamic-block">
				<div class="col-xs-5">
					Название
					<input class="form-control" type="text" name="params[<?= $id ?>][title]" value="<?= $param->title ?>">
				</div>
				<div class="col-xs-5">
					Значение
					<input class="form-control" type="text" name="params[<?= $id ?>][value]" value="<?= $param->value ?>">
				</div>
				<div class="col-xs-2">
					<button class="btn btn-danger delete-this">Удалить</button>
				</div>
			</div>
		<?php endforeach ?>
	<?php endif ?>
	<div class="hidden dinamic-block">
		<div class="col-xs-5">
			Название
			<input class="form-control" type="text" original-name="params[uniqid][title]">
		</div>
		<div class="col-xs-5">
			Значение
			<input class="form-control" type="text" original-name="params[uniqid][value]">
		</div>
		<div class="col-xs-2">
			<button type="button" class="btn btn-danger delete-this">Удалить</button>
		</div>
	</div>
</div>
<button class="btn btn-default add-dinamic-block" type="button">
	Добавить
</button>