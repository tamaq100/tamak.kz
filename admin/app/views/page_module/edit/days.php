<div class="dinamic-blocks-wrapper">
	<?php if (isset($entity->json_data->$field)): ?>
		<? $days = $entity->json_data->$field ?>
		<?php foreach ($days as $id => $day): ?>
			<div class="dinamic-block">
				<div class="col-xs-12" style="margin-bottom: 10px;">
					Название
					<input class="form-control" type="text" name="days[<?= $id ?>][title]" value="<?= $day->title ?>">
				</div>
				<div class="col-xs-12" style="margin-bottom: 10px;">
					Описание
					<textarea class="form-control editorarea" name="days[<?= $id ?>][description]" cols="30" rows="5"><?= $day->description ?></textarea>
				</div>
				<div class="col-xs-12" style="margin-bottom: 10px;">
					Изображение
					<?php if (isset($day->image) && $day->image): ?>
						<img src="/userfiles/<?= $day->image ?>" alt="" class="img-responsive">
						<input type="hidden" name="days[<?= $id ?>][image]" value="<?= $day->image ?>">
					<?php endif ?>
					<input class="form-control" type="file" name="days[<?= $id ?>]">
				</div>
				<div class="col-xs-12" style="margin-bottom: 10px;">
					<?php if (isset($day->image) && $day->image): ?>
						<button type="button" class="btn btn-danger delete-this image-remove" data-image="<?= $day->image ?>">Удалить</button>
					<?php else: ?>
						<button type="button" class="btn btn-danger delete-this">Удалить</button>
					<?php endif ?>
				</div>
			</div>
		<?php endforeach ?>
	<?php endif ?>
	<div class="hidden dinamic-block">
		<div class="col-xs-12" style="margin-bottom: 10px;">
			Название
			<input class="form-control" type="text" original-name="days[uniqid][title]">
		</div>
		<div class="col-xs-12" style="margin-bottom: 10px;">
			Описание
			<textarea class="form-control editorarea-clone" original-name="days[uniqid][description]" cols="30" rows="5"></textarea>
		</div>
		<div class="col-xs-12" style="margin-bottom: 10px;">
			Изображение
			<input class="form-control" type="file" original-name="days[uniqid]">
		</div>
		<div class="col-xs-12" style="margin-bottom: 10px;">
			<button class="btn btn-danger delete-this">Удалить</button>
		</div>
	</div>
</div>
<button class="btn btn-default add-dinamic-block" type="button">
	Добавить
</button>