<?
	$gallery = Gallery_item::findBy(array('gallery_id' => 1, 'type' => $_GET['controller'] . $key), 'idx');
?>
<div id="pluploadForm<?= $key ?>" class="pluploadFormImageGallery" data-field="<?= $key ?>">
	<div id="img-uploader<?= $key ?>">
		<p>Ваш браузер не поддерживает html5.</p>
	</div>
	<div id="image_container<?= $key ?>">
		<a id="image_pickfiles<?= $key ?>" href="javascript:;">Добавить фото</a>
	</div>
	<div id="gallery<?= $key ?>">
		<ul id="sortableGallery<?= $key ?>" style="">
			<?php if (! empty($gallery)): ?>
				<?php foreach ($gallery as $image): ?>
					<li id="item-<?= $image->id ?>">
						<div class="panel panel-default">
							<div class="panel-body">
								<div class="row">
									<div class="col-xs-2 text-center">
										<a target="_blank" href="/images/00/<?= $image->media ?>.jpg">
											<img class="img-thumbnail" src="/images/h/norm/130/<?= $image->media ?>.jpg">
										</a>
									</div>
									<div class="col-xs-10">
										<textarea class="descr form-control"
											name="descr[<?= $image->id ?>]"><?= $image->name ?></textarea>
											<br>
										<div class="img-panel">
											<a class="image-del btn btn-danger btn-xs" data-id="<?= $image->id ?>"
												href="#">удалить</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</li>
				<?php endforeach; ?>
			<?php else: ?>
				Картинок нет
			<?php endif; ?>
		</ul>
	</div>
</div>
