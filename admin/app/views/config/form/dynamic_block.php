<div class="row">
	<div class="dinamic-blocks-wrapper">
		<div class="col-xs-12">
			<button class="btn btn-default add-dinamic-block" style="margin-bottom: 20px;" type="button">
				Добавить
			</button>
		</div>
		<?php if ($values[$key]): ?>
			<?php if (is_string($values[$key])): ?>
				<?php $blocks = unserialize($values[$key]); ?>
			<?php else: ?>
				<? $blocks = $values[$key] ?>
			<?php endif ?>
			<?php foreach ($blocks as $id => $param): ?>
				<div class="dinamic-block">
					<div class="row">
						<div class="col-xs-12">
							Заголовок
							<input class="form-control" type="text" name="<?= $key ?>[<?= $id ?>][number]" value="<?= $param['number'] ?>">
						</div>
						<div class="col-xs-12">
							Текст
							<input class="form-control" type="text" name="<?= $key ?>[<?= $id ?>][title]" value="<?= $param['title'] ?>">
						</div>
						<div class="col-xs-3">
							<button class="btn btn-danger delete-this" style="margin-top: 17px;">Удалить</button>
						</div>
					</div>
				</div>
			<?php endforeach ?>
		<?php endif ?>
		<div class="hidden dinamic-block">
			<div class="row">
				<div class="col-xs-12">
					Заголовок
					<input class="form-control" type="text" original-name="<?= $key ?>[uniqid][number]">
				</div>
				<div class="col-xs-12">
					Текст
					<input class="form-control" type="text" original-name="<?= $key ?>[uniqid][title]">
				</div>
				<div class="col-xs-3">
					<button type="button" class="btn btn-danger delete-this" style="margin-top: 17px;">Удалить</button>
				</div>
			</div>
		</div>
	</div>
</div>