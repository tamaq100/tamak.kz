<form action="<?= $action ?>/save" method="POST" enctype="multipart/form-data">
	<?php foreach ($params as $key => $type): ?>
		<div class="form-group row">
			<label class="col-sm-2 control-label">
				<?= __($model.'.fields.'.$key) ?>
			</label>
			<div class="col-sm-10">
				<?php 
					$vars = [
						'key'		=> $key,
						'values'    => $values,
					];

					$dir = SITE_DIR . 'admin/app/views/config/form/';
					if (strpos($key,'_dyn')) {
						if (file_exists($dir .'dynamic_block.php')) {
							echo include_file('config/form/dynamic_block', $vars);
						}
					}else{
						if (file_exists($dir . $type . '.php')) {
							echo include_file('/config/form/' . $type, $vars);
						} else {
							echo $values[$key];
						}
					}
				?>
			</div>
		</div>
	<?php endforeach ?>

	<button type="button" class="btn btn-default" id="saveEntityBtn"
			data-entity-id="1" 
			data-entity-controller="<?= $_GET['controller'] ?>">Сохранить</button>

</form>
