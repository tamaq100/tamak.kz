<?php $spez = Specialization::findOneBy(['id'=>$item->specialization_id]); ?>
<?php if ($spez): ?>
	<?php $spec = Specialty_type::findOneBy(['id' => $spez->specialty_type_id]); ?>
	<?php if (isset($spec->attributes['title'])): ?>
		<?= $spez->attributes['title'] .' ➤ ' . $spec->attributes['title'] . ' ➤ ' . Specialty_type::getEducation_degreeList($spec->education_degree) ?>
	<?php endif ?>
<?php endif ?>