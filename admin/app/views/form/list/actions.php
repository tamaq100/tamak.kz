<div class="btn-group">
	<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		Действие <span class="caret"></span>
	</button>
	<ul class="dropdown-menu">
		<?php if (!$entity->read_only): ?>
			<li>
				<a href="/admin/<?= strtolower($entity->model) ?>/edit/<?= $item->id ?>">Изменить</a>
			</li>
		<?php endif ?>
		<li>
			<a href="/admin/<?= strtolower($entity->model) ?>/show/<?= $item->id ?>">
				Просмотреть
			</a>
		</li>
		<?php if ((!$entity->read_only) && (!$entity->edit_only)): ?>
			<li>
				<a href="#" onclick="delete_form_submit('#delete_form_<?= $item->id ?>')">Удалить</a>
				<form id="delete_form_<?= $item->id ?>" action="/admin/<?= strtolower($entity->model) ?>/delete/<?= $item->id ?>" method="POST">
					<input type="hidden" name="id" value="<?= $item->id ?>">
				</form>
			</li>
		<?php endif ?>
	</ul>
</div>
