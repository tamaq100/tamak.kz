<?php if ($entity->read_only): ?>
	<?= $res_item[$field] = $item->$field; ?>
<?php else: ?>
	<input class="editable_field" type="checkbox" data-name="data[<?= $field ?>][<?= $item->id ?>]"<?= ($item->getDefaultValue($field) ? ' checked' : '')?>>
<?php endif ?>