<?php if ($entity->$field): ?>
	<p class="form-control-static">
		<a href="/admin/report/download/<?= $entity->id ?>">
			<?= $entity->filename ?>
		</a>
	</p>
<?php else: ?>
	<p class="form-control-static">Файл не загружен</p>
<?php endif; ?>
