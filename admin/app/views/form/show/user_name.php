<?php if ($entity->user_id): ?>
	<?php if ($user = User::findOneBy($entity->user_id)): ?>
		<p class="form-control-static">
			<a href="/admin/user/show/<?= $entity->user_id ?>">
				<?= $user->name ?>
			</a>
		</p>
	<?php endif; ?>
<?php else: ?>
	<p class="form-control-static">Аноним</p>
<?php endif; ?>
