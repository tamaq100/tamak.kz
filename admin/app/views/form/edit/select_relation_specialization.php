<?php $relation_entities = $entity->getRelationEntities($field); ?>
<?php if (count($relation_entities)): ?>
	<select class="form-control" name="<?= $field ?>">
	<option value="">--</option>
	<?php foreach ($relation_entities as $key => $relation_entity): ?>
		<option value="<?= $relation_entity->id ?>" <?= ($relation_entity->id == $entity->getDefaultValue($field) ? 'selected' : '')?>>
			<?php if ($relt = Specialty_type::findOneBy(['id' => $relation_entity->specialty_type_id])): ?>
				<?= $relation_entity->getNameForInput() .' ➤ '.$relt->title .' ➤ '. Specialty_type::getEducation_degreeList($relt->id) ?>
			<?php endif ?>
		</option>
		$this->title
	<?php endforeach; ?>
	</select>
<?php else: ?>
	<p class="form-control-static">Записей по данной связи не найдено</p>
<?php endif; ?>
