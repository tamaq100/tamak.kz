<div class="materials-list">';
<div id="new_id" class="form-group material-row row-template hidden">
		<div class="col-xs-6 col-md-8 col-lg-8">
			<input class="form-control" type="text" original_name="<?= $field ?>[new_id][name]" value="" placeholder="Название материала">
			<input type="hidden" original_name="<?= $field ?>[new_id][count]" value="0">
		</div>
		<div class="col-xs-6 col-md-4 col-lg-4">
			<div class="material-remover btn btn-danger">
				<i class="glyphicon glyphicon-remove"></i>
			</div>
		</div>
	</div>
	<?php foreach ($entity->$field as $id => $material): ?>
		<div class="form-group material-row">
			<div class="col-xs-6 col-md-8 col-lg-8">
				<input class="form-control" type="text" name="<?= $field ?>[<?= $id ?>][name]" value="<?= $material['name'] ?>">
				<input type="hidden" name="<?= $field ?>[<?= $id ?>][count]" value="0">
			</div>
			<div class="col-xs-6 col-md-4 col-lg-4">
				<div class="material-remover btn btn-danger">
					<i class="glyphicon glyphicon-remove"></i>
				</div>
				&nbsp;
			</div>
		</div>
	<?php endforeach; ?>
	<div class="btn add-material">Добавить материал</div>
</div>
