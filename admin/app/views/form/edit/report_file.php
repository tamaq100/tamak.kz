<?php if ($entity->$field): ?>
	<p class="form-control-static">
		<a href="/admin/report/download/<?= $entity->id ?>">
			<?= $entity->filename ?>
		</a>
	</p>
<?php else: ?>
	<p class="form-control-static">Файл не загружен</p>
<?php endif; ?>
<? $uniqid = uniqid(); ?>
<input id="<?= $uniqid ?>" class="form-control" type="file" name="<?= $field ?>">
<script>
	$(function() {
		$("#<?= $uniqid ?>").change(function() {
			if (this.files) {
				$("[name=filename]").val(this.files[0].name);
			}
		});
	})
</script>
