<div class="row">
	<div class="dinamic-blocks-wrapper">
		<div class="col-xs-12">
			<button class="btn btn-default add-dinamic-block" style="margin-bottom: 20px;" type="button">
				Добавить
			</button>
		</div>
		<?php if ($entity->get('rooms')): ?>
			<? $rooms = $entity->get('rooms') ?>
			<?php foreach ($rooms as $id => $param): ?>
				<div class="dinamic-block">
					<div class="row">
						<div class="col-xs-2">
							Номер
							<input class="form-control" type="text" name="rooms[<?= $id ?>][number]" value="<?= $param['number'] ?>">
						</div>
						<div class="col-xs-4">
							Название
							<input class="form-control" type="text" name="rooms[<?= $id ?>][title]" value="<?= $param['title'] ?>">
						</div>
						<div class="col-xs-4">
							Площадь (м2)
							<input class="form-control" type="text" name="rooms[<?= $id ?>][value]" value="<?= $param['value'] ?>">
						</div>
						<div class="col-xs-2">
							<button class="btn btn-danger delete-this" style="margin-top: 17px;">Удалить</button>
						</div>
					</div>
				</div>
			<?php endforeach ?>
		<?php endif ?>
		<div class="hidden dinamic-block">
			<div class="row">
				<div class="col-xs-2">
					Название
					<input class="form-control" type="text" original-name="rooms[uniqid][number]">
				</div>
				<div class="col-xs-4">
					Название
					<input class="form-control" type="text" original-name="rooms[uniqid][title]">
				</div>
				<div class="col-xs-4">
					Площадь (м2)
					<input class="form-control" type="text" original-name="rooms[uniqid][value]">
				</div>
				<div class="col-xs-2">
					<button type="button" class="btn btn-danger delete-this" style="margin-top: 17px;">Удалить</button>
				</div>
			</div>
		</div>
	</div>
</div>