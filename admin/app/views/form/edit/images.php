<div class="images">
<div class="existing_items">
<? $images = $entity->$field; ?>
	<?php foreach (array_chunk($images, 3) as $chunk): ?>
		<div class="row">
			<?php foreach ($chunk as $image): ?>
				<div class="col-xs-4 ci_item">
					<img class="img-thumbnail" style="max-width: 100%" src="/images/w/norm/220/<?= $image->media ?>.jpg" alt="<?= $image->title ?>"'<?= ($image->media ? '' : ' style="display: none"')?>>
					<input class="ci_item_title form-control" type="text" data-id="<?= $image->id ?>" value="<?= $image->title ?>" placeholder="Название">
					<button class="btn btn-default image_file" type="button">
						Загрузить
						<input data-id="<?= $image->id ?>" name="<?= $field.'['.$image->id .']' ?>" type="file">
					</button>
					&nbsp;<button class="btn btn-default image_file_delete" type="button">Отменить</button>
				</div>
			<?php endforeach; ?>
		</div>
	<?php endforeach; ?>
</div>
<div class="new_items row">
	<div class="col-xs-4 ci_item ci_item_template hidden">
		<img style="max-width: 100%" class="img-thumbnail" src="" alt="" style="display: none">
		<input class="form-control" type="text" value="" placeholder="Название">
		<button class="btn btn-default image_file" type="button">Загрузить<input type="file"></button>
		&nbsp;<button class="btn btn-default image_file_delete" type="button">Отменить</button>
	</div>
</div>
<div class="image_inputs"></div>
<a id="add_image" href="#">Добавить изображение</a>
</div>
