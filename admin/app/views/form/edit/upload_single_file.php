<?php if ($entity->$field) {
	$delete_file_form_id = uniqid();
	$delete_file_url = '/admin/'. strtolower($entity->getClassName()) .'/delete_file/'. $entity->id;
	$vars = [
		'id' 					=> $delete_file_form_id,
		'action'				=> $delete_file_url,
		'field'					=> $field,
	];
	$after_forms .= include_file('form/after_forms/delete', $vars);
}
?>

<?php if ($entity->$field): ?>
	<? $fileno = $field[strlen($field) - 1]; ?>
	<p class="form-control-static"
>		<a href="/media/press_release_file/<?= $entity->id ?>?fileno=<?= $fileno ?>">
			<?= $entity->$field.'_name' ?>
		</a>
	</p>
<?php else: ?>
	<p class="form-control-static">Файл не загружен</p>';
<?php endif; ?>
<? $uniqid = uniqid(); ?>
<input id="<?= $uniqid ?>" class="form-control" type="file" name="<?= $field ?>">
<script>
	$(function() {
		$("#<?= $uniqid ?>").change(function() {
			if (this.files) {
				$("[name=<?= $field ?>_name]").val(this.files[0].name);
			}
		});
	})
</script>
<?php if ($entity->$field): ?>
	<br>
	<button class="file-remover btn btn-danger" type="button" data-field="<?= $field ?>" data-id="<?= $delete_image_id ?>" data-action="<?= $action ?>">Удалить файл</button>
<?php endif; ?>
