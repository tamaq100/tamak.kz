<?php

/**
 * For model to have this field, there must be a file model with name {model}_file, model must have fields (
        'id'                => 'integer',
        'parent_id'         => 'integer',
        'created'           => 'integer',
        'updated'           => 'integer',
        'title'             => 'string',
        'hash'              => 'file',
        'filesize'          => 'string',
)
see ics-prototype file-model
 */

$model_name = $entity->getClassName();
$model = $model_name . '_file';
$attachements = $model::findBy(array('parent_id' => $entity->id));
 ?>
<!-- <img src="/images/ajax-loader.gif" style="display: none;" /> -->

<div class="uploaderblock-<?= $entity->id ?>">
	<i class="fa fa-paperclip attachimg" data-id="<?= $entity->id ?>" data-model="<?= $model_name ?>"></i>
	
	<div class="progressbar-holder hidden">
		<div class="progress sale-file-progress">
		  <div class="progress-bar progress-bar-striped active" role="progressbar"
		  aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%">
		  </div>
		</div>
	</div>

	<input 
		class="hidden attachfile-<?= $entity->id ?>" 
		id="attachfile-<?= $entity->id ?>" 
		data-parent-id="<?= $entity->id ?>"
		data-model="<?= $model_name ?>" 
		type="file" 
		name="attachfile[]" 
		multiple 
		style="visibility: hidden;"/>
	<div class="form-group">
		<div class="well attachments-list">
			<?php foreach ($attachements as $file): ?>
				<div class="attachment-item">
					<a href="/<?= $model_name ?>/download/<?= $file->id ?>">
						<i class="fa fa-<?= $file->getIcon() ?>"></i>
						<?= stripslashes($file->title) ?>
					</a> (<?= $file->filesize ?>)
					<a href="Javascript:void(0);" onclick="removeAttach(this, <?= $file->id ?>, '<?= $model_name ?>')" class="removefile" title="Удалить"><i class="fa fa-remove"></i></a>
				</div>
			<?php endforeach ?>
		</div>
	</div>
</div>
