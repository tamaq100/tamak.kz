<? $recomended_products = $entity->$field ?: array(); ?>

<select style="width: 100%" title="Выберите товар(ы)" id="main-select2" multiple="multiple" name="<?= $field ?>[]">
	<?php foreach ($recomended_products as $product_id): ?>
		<? $product = Product::findOneBy($product_id); ?>
		<option value="<?= $product->id ?>" selected><?= $product->name ?></option>
	<?php endforeach; ?>
</select>
