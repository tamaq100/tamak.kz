<?php $uploaded = Journal::findBy(array('rubric_id' => $entity->id)) ?>
<?php if (! $entity->id): ?>
	Перед добавлением, нужно сохранить запись
<?php else: ?>
	<div id="pluploadmultiplefilesForm" data-entity="' <?= $entity->getClassName() ?>'">
		<div id="img-uploader">
			<p>Ваш браузер не поддерживает html5.</p>
		</div>
		<div id="image_container">
			<div class="form-group">
				<div class="col-xs-12 youtube-forms">
					<a id="multiplefiles_pickfiles" href="javascript:;" class="btn btn-xs btn-primary"	>Добавить файл</a>
				</div>
			</div>
		</div>
		<div id="gallery">
			<ul id="sortableGallery">
				<?php if ($uploaded): ?>
					<?php foreach ($uploaded as $upload_entity): ?>
						<div class="row-file-template" id="item-<?= $upload_entity->id ?>">
							<div class="panel-collapse collapse fade in panel panel-info">
								<div class="panel-body">
									<div class="row">
										<div class="col-xs-5">
											<span>Название</span>
											<input type="text" name="journal[<?= $upload_entity->id ?>][title]" class="form-control" value="<?= $upload_entity->title ?>">
										</div>
										<div class="col-xs-3">
											<p>Файл</p>
											<?php if ($upload_entity->image): ?>
												<img src="/images/w/norm/100/<?= $upload_entity->image ?>.jpg" alt="">
											<?php endif ?>
											<?php if ($upload_entity->pdf): ?>
												<a href="/userfiles/<?= $upload_entity->pdf ?>">
													<?= $upload_entity->pdf ?>
												</a>
											<?php endif ?>
										</div>
										<div class="col-xs-2">
											
										</div>
										<input type="hidden" name="rubric_id" value="<?= $entity->id ?>">
										<div class="col-xs-2">
											<button type="button" class="multiple-file-del btn btn-danger" data-id="<?= $upload_entity->id ?>">Удалить</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?php endforeach ?>
				<?php endif ?>
			</ul>
		</div>
	</div>
<?php endif ?>
