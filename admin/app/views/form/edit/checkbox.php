<?php $relation_entities = $entity->getRelationEntities($field); ?>
<?php foreach ($relation_entities as $key => $relation_entity): ?>
	<label>
		<input class="form-control" type="checkbox"
		 value="<?= htmlspecialchars($entity->$field) ?>"
		 name="<?= htmlspecialchars($field).?>"
		 <?= ($relation_entity->id == $entity->$field ? ' checked' : '') ?>>
		<?= $relation_entity->getNameForInput() ?>
	</label>
 	
 <?php endforeach ?> 

