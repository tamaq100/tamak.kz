<select class="form-control" name="'.$field.'">
	<?php foreach ($entity->get_places() as $key => $name): ?>
		<option value="<?= $key ?>"<?= ($key == $entity->place ? ' selected' : '') ?>><?= $name ?></option>
	<?php endforeach; ?>
</select>
