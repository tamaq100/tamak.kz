<?php if ($entity->$field): ?>
	<img class="img-thumbnail" src="<?= $entity->$field ?>" alt="<?= $entity->getNameForInput() ?>" style="max-width: 100%">
<?php endif; ?>
