<style type="text/css">
	.btn-file {
	    position: relative;
	    overflow: hidden;
	}
	.btn-file input[type=file] {
	    position: absolute;
	    top: 0;
	    right: 0;
	    min-width: 100%;
	    min-height: 100%;
	    font-size: 100px;
	    text-align: right;
	    filter: alpha(opacity=0);
	    opacity: 0;
	    outline: none;
	    background: white;
	    cursor: inherit;
	    display: block;
	}
</style>
<?php $delete_image_id = uniqid(); ?>
<?php if ($entity->$field): ?>
	<?
		$delete_image_id = uniqid();
		$src = '/images/00/'.$entity->$field.'.jpg';
		$action = '/admin/'. strtolower($entity->getClassName()) .'/delete_image/'. $entity->id;

		// Достаем из модели список размеров для кропера если такой существует
		$list = ($entity->getCropList()) ? $entity->getCropList() : '';
	?>
<?php endif; ?>

<div class="row">
	<!-- col-sm-3 div start -->
	<div class="col-sm-3">
		<div class="attachment-wrap img-thumbnail">
			<?php if (!$entity->$field): ?>
				<? $src = '/admin/img/no_photo.jpg' ?>
			<?php endif; ?>
			<img class="img-responsive attachment-img"  src="<?= $src ?>" alt="<?= $entity->getNameForInput() ?>" style="max-width: 100%">
		</div>		
		<!-- <input class="form-control" type="file" name="<?= $field ?>" style="max-width: 200px;"> -->
	</div>
	<!-- col-sm-3 div end -->

	<!-- col-sm-9 div start -->
	<div class="col-sm-2">
		<label class="btn btn-success btn-file btn-block">
		    Загрузить фотографию <input type="file" class="form-control image-uploader" name="<?= $field ?>" style="display: none;">
		</label>
		<input type="hidden" class="cropped-image-data" name="cropped_image_data">
		<? $isDisabled = ($entity->$field) ? '' : 'disabled' ; ?>
		<br>
		<button class="img-cropper btn btn-warning <?= $isDisabled ?> btn-block" type="button" data-toggle="modal" data-target="#cropper_modal">Сделать евреем</button>
		<? if ($entity->$field): ?>
			<br>
			<button class="file-remover btn btn-danger btn-block" type="button" data-field="<?= $field ?>" data-id="<?= $delete_image_id ?>" data-action="<?= $action ?>">Удалить фотографию</button>
		<? endif; ?>
	</div>
	<!-- col-sm-9 div end -->
	<div class="col-sm-7">
		<div class="file-info-wrap hidden">
			<h5>Информация о файле</h5>
			<table class="table file-info table-condensed">
				<tr">
					<td class="file-info-item-name">Имя:</td>
					<td></td>
				</tr>
				<tr>
					<td class="file-info-item-size">Размер:</td>
					<td></td>
				</tr>
				<tr>
					<td class="file-info-item-type">Тип:</td>
					<td></td>
				</tr>
			</table>			
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="cropper_modal" tabindex="-1" role="dialog" aria-labelledby="cropper_modalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="cropper_modalLabel">Обрезать фотографию</h4>
      </div>
      <div class="modal-body">
      	<div class="image-cropper-wrap">
			<img class="img-responsive attachment-img" id="crop-img" src="<?= $src ?>" alt="<?= $entity->getNameForInput() ?>" style="max-width: 100%">
      	</div>
      	<?php if (isset($list) && $list): ?>
	        <?php if (count($list)): ?>
	        	<select class="form-control" name="<?= $field ?>">
	        	<option value="">--</option>
	        	<?php foreach ($list as $key => $name): ?>
	        		<option value="<?= $name ?>" <?= ($key == $entity->$field ? 'selected' : '')?>><?= $key ?></option>
	        	<?php endforeach; ?>
	        	</select>
	        <?php endif; ?>      		
      	<?php endif ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="cropIt">Обрезать</button>
      </div>
    </div>
  </div>
</div>