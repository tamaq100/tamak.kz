<? $delete_image_id = uniqid(); ?>
<? if ($entity->getDefaultValue($field)): ?>
<?
	$delete_image_id = uniqid();
	$src = '/images/00/'.$entity->$field.'.jpg';
	$action = '/admin/'. strtolower($entity->getClassName()) .'/delete_image/'. $entity->id;
?>
<div class="attachment-wrap">
	<img class="img-thumbnail" src="<?= $src ?>" alt="<?= $entity->getNameForInput() ?>" style="max-width: 100%" id="<?= $delete_image_id ?>">
</div>
<? endif; ?>

<input class="form-control" type="file" name="<?= $field ?>">

<? if ($entity->getDefaultValue($field)): ?>
	<br>
	<button class="file-remover btn btn-danger" type="button" data-field="<?= $field ?>" data-id="<?= $delete_image_id ?>" data-action="<?= $action ?>">Удалить фотографию</button>
<? endif; ?>