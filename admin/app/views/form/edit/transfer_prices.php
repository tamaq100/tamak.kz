<? $autos = Transfer_auto::findAll() ?>
<table class="table table-bordered">
	<tr>
		<th>
			Модель машины
		</th>
		<th>
			Цена
		</th>
	</tr>
	<?php if ($autos): ?>
		<?php foreach ($autos as $auto): ?>
			<?php $relation = Transfer_point_auto_relation::findOneBy(['transfer_auto_id' => $auto->id, 'transfer_point_id' => $entity->id]) ?: new Transfer_point_auto_relation ?>
			<tr>
				<td>
					<?= $auto->title ?>
				</td>
				<td>
					<input type="number" class="form-control" name="autos[<?= $auto->id ?>]" value="<?= $relation->price ?: '' ?>"> 
				</td>
			</tr>
		<?php endforeach ?>
	<?php endif ?>
</table>