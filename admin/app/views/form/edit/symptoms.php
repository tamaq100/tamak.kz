<div class="row">
	<div class="dinamic-blocks-wrapper">
		<div class="col-xs-12">
			<button class="btn btn-default add-dinamic-block" style="margin-bottom: 20px;" type="button">
				Добавить
			</button>
		</div>
		<?php if ($entity->categoriesSymptom): ?>
			<? $categoriesSymptom = $entity->categoriesSymptom ?>
			<?php foreach ($categoriesSymptom as $id => $category): ?>
				<div class="dinamic-block">
					<div class="col-xs-10">
						Симптом
						<input class="form-control" type="text" name="categoriesSymptom[<?= $id ?>][title]" value="<?= $category['title'] ?>">
					</div>
					<div class="col-xs-2">
						<button class="btn btn-danger delete-this" style="margin-top: 17px;">Удалить</button>
					</div>
				</div>
			<?php endforeach ?>
		<?php endif ?>
		<div class="hidden dinamic-block">
			<div class="col-xs10">
				Симптом
				<input class="form-control" type="text" original-name="categoriesSymptom[uniqid][title]">
			</div>
			<div class="col-xs-2">
				<button type="button" class="btn btn-danger delete-this" style="margin-top: 17px;">Удалить</button>
			</div>
		</div>
	</div>
</div>
