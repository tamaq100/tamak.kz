<?php $relation_entities = $entity->getRelationEntities($field); ?>
<?php if (count($relation_entities)): ?>
	<select class="form-control" name="<?= $field ?>">
	<option value="">--</option>
	<?php foreach ($relation_entities as $key => $relation_entity): ?>
		<option value="<?= $relation_entity->id ?>" <?= ($relation_entity->id == $entity->getDefaultValue($field) ? 'selected' : '')?>>
			<?= $relation_entity->getNameForInput() .' ➤ '. Specialty_type::getEducation_degreeList($relation_entity->id) ?>
		</option>
	<?php endforeach; ?>
	</select>
<?php else: ?>
	<p class="form-control-static">Записей по данной связи не найдено</p>
<?php endif; ?>
