<?php if ($entity->id): ?>
	<?php $list = $entity->{'get'.ucfirst($field).'List'}() ?>
	<?php if (count($list)): ?>
		<div class="row">
			<div class="col-xs-7">
				<select class="form-control" data-entity-id="<?= $entity->id ?>" id="page_modules" name="module_type">
				<option value="">--</option>
				<?php foreach ($list as $key => $name): ?>
					<option value="<?= $key ?>" <?= ($key == $entity->$field ? 'selected' : '')?>><?= $name ?></option>
				<?php endforeach; ?>
				</select>
			</div>
			<div class="col-xs-5">
				<button class="btn btn-info" type="button" id="addModule" data-controller="<?= strtolower($entity->getClassName()) ?>">Добавить</button>
				<a href="/admin/page_module/index/<?= $entity->id ?>?parent_model=<?= $entity->getClassName() ?>" class="btn btn-default">
					Список модулей
				</a>
			</div>
		</div>
	<?php else: ?>
		<p class="form-control-static">Записей по данной связи не найдено</p>
	<?php endif; ?>
<?php else: ?>
	<p>Перед добавлением модулей, необходимо сохранить запись</p>
<?php endif ?>
	