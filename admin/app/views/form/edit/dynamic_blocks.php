<div class="row">
	<div class="dinamic-blocks-wrapper">
		<div class="col-xs-12">
			<button class="btn btn-default add-dinamic-block" style="margin-bottom: 20px;" type="button">
				Добавить
			</button>
		</div>
		<?php if ($entity->tabs): ?>
			<? $tabs = $entity->tabs; ?>
			<?php foreach ($tabs as $id => $param): ?>
				<div class="dinamic-block">
					<div class="row">
						<div class="col-xs-12">
							<label for="tabs[<?= $id ?>][name]">Название таба</label>
							<?php if (isset($param['name'])): ?>
								<input class="form-control" type="text" name="tabs[<?= $id ?>][name]" value="<?= $param['name'] ?>">
							<?php else: ?>
								<input class="form-control" type="text" name="tabs[<?= $id ?>][name]" value="">
							<?php endif; ?>
						</div>
						<div class="col-xs-12">
							<label for="tabs[<?= $id ?>][text]">Текст</label>
							<?php if (isset($param['text'])): ?>
							<textarea class="form-control editorarea" name="tabs[<?= $id ?>][text]" cols="30" rows="10"><?= htmlspecialchars( $param['text']) ?></textarea>
							<?php else: ?>
							<textarea class="form-control editorarea" name="tabs[<?= $id ?>][text]" cols="30" rows="10"></textarea>
							<?php endif ?>
						</div>
						<div class="col-xs-12">
						<?
							$gallery = Gallery_item::findBy(array('gallery_id' => $entity->id, 'type' => $_GET['controller'] . $field . $id), 'idx');
						?>
						<div id="pluploadForm<?= $field ?><?= $id ?>" class="pluploadFormImageGallery" data-field="<?= $field ?><?= $id ?>">
							<div id="img-uploader<?= $field ?><?= $id ?>">
								<p>Ваш браузер не поддерживает html5.</p>
							</div>
							<div id="image_container<?= $field ?><?= $id ?>">
								<a id="image_pickfiles<?= $field ?><?= $id ?>" href="javascript:;">Добавить фото</a>
							</div>
						</div>
						</div>
						<div class="col-xs-2">
							<button class="btn btn-danger delete-this" style="margin-top: 17px;">Удалить</button>
						</div>
					</div>
				</div>
			<?php endforeach ?>
		<?php endif ?>
		<div class="hidden dinamic-block">
			<!-- доделать -->
			<div class="row">
				<div class="col-xs-12">
					<label for="tabs[uniqid][name]">Название таба</label>
					<input class="form-control" type="text" original-name="tabs[uniqid][name]">
				</div>
				<div class="col-xs-12">
					<label for="tabs[uniqid][text]">Текст</label>
					<textarea class="form-control editorarea" name="tabs[uniqid][text]" cols="30" rows="10"></textarea>
				</div>
				<div class="col-xs-12">
					<div id="pluploadFormtabs[uniqid]" class="pluploadFormImageGallery" data-field="<?= $field ?><?= $id ?>">
						<div id="img-uploader<?= $field ?><?= $id ?>">
							<p>Ваш браузер не поддерживает html5.</p>
						</div>
						<div id="image_container<?= $field ?><?= $id ?>">
							<a id="image_pickfiles<?= $field ?><?= $id ?>" href="javascript:;">Добавить фото</a>
						</div>
						<div id="gallery<?= $field ?><?= $id ?>">
							<ul id="sortableGallery<?= $field ?><?= $id ?>" style="">
									Картинок нет
							</ul>
						</div>
					</div>	
				</div>
				<div class="col-xs-2">
					<button type="button" class="btn btn-danger delete-this" style="margin-top: 17px;">Удалить</button>
				</div>
			</div>
		</div>
	</div>
</div>