<?php $list = $entity->{'get'.ucfirst($field).'List'}() ?>
<?php if ($list && count($list)): ?>
	<select class="form-control" name="<?= $field ?>">
	<option value="">--</option>
	<?php foreach ($list as $key => $name): ?>
		<option value="<?= $key ?>" <?= ($key == $entity->getDefaultValue($field) ? 'selected' : '')?>><?= $name ?></option>
	<?php endforeach; ?>
	</select>
<?php else: ?>
	<p class="form-control-static">Записей по данной связи не найдено</p>
<?php endif; ?>