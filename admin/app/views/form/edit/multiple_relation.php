

<?php if ($entity->id): ?>
	<?
		// $related_model = ucfirst($field. '_relation');
		$related_model = $entity::$links[$field];
		$related_field = strtolower($entity->getClassName() . '_id');
		$selected_options = $related_model::findBy([$related_field => $entity->id]) ?: array(); 
		$second_field = strtolower($field);

		$relative_model = $entity::$related_entities[$field];
		$options = $relative_model::baseFindAll();
		// die(var_dump($options));
		
	?>
	
	<select id="main-select2" style="width: 100%" title="Выберите категории(ю)" data-request-controller="<?= strtolower($relative_model) ?>" multiple="multiple" name="<?= $field ?>[]">
		<?php foreach ($options as $option): ?>
			<?php $is_selected = $related_model::getCount([$related_field => $entity->id, $second_field => $option->id]); ?>
			<option value="<?= $option->id ?>" <?= $is_selected ? 'selected' : '' ?>>
				<?= $option->title ?>
			</option>
		<?php endforeach; ?>
	</select>
<?php else: ?>
	<p>Перед добавлением необходимо созранить запись</p>
<?php endif ?>