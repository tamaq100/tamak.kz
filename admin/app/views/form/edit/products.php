<?php if (isset($entity->products) && $entity->products): ?>
    <table class="table table-bordered">
        <tr class="active">
            <th colspan="2">
                Название продукта
            </th>
            <th>
                Количество
            </th>
        </tr>
        <? $products = $entity->products; ?>
        <?php foreach($products as $product_id => $count): ?>
            <?php $product = Product::findOneBy($product_id) ?>
            <tr>
                <td colspan="2">
                    <a href="/admin/product/show/<?= $product->id ?>">
                        <?= $product->name ?>
                    </a>
                </td>
                <td>
                    <?= $count ?>
                </td>
            </tr>
        <?php endforeach;?>
    </table>
<?php endif; ?>