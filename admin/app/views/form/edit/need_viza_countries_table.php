<div class="import-xls-submit">
	<div class="table-wrapper">
		<?= include_file('helpers/countriesTable', $vars) ?>
	</div>
	<div class="form-group">
		<div class="col-xs-12">
			<label>Обновить данные</label>
			<input type="file" name="xlsFile">
			<div class="lds-ring preloader" style="display: none;"><div></div><div></div><div></div><div></div></div>
		</div>
	</div>
	<button type="button" class="btn btn-success submit" 
		data-old-title="Загрузить xlsx файл"
		data-url="/admin/<?= strtolower($entity->getClassName()) ?>/importExcel/<?= $entity->id ?>">
		Загрузить xlsx файл
	</button>
	<span class="place-for-error"></span>
</div>