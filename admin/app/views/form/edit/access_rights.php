<?	
	$controller = new AdministratorController;
?>
<?php foreach ($controller->get_scriptlist(true) as $name => $item): ?>
	<?php if (isset($name)): ?>
		<? $checked = in_array($name, $entity->$field); ?>
		<label><input type="checkbox" name="access_rights[]" <?= ($checked ? 'checked' : '') ?> value="<?= $name ?>">&nbsp;<?= __($item['title']) ?></label>
		<br>
	<?php endif; ?>
<?php endforeach; ?>
