<? 
	$countries = Country::findAll();
	$cities    = City::findAll();
?>
<div class="dinamic-blocks-wrapper">
	<?php if ($entity->$field): ?>
		<? $routes = $entity->$field ?>
		<?php foreach ($routes as $id => $route): ?>
			<div class="dinamic-block">
				<div class="form-group">
					<div class="col-xs-6">
						Страна
						<select name="routes[<?= $id ?>][country_id]" id="chained-country-<?= $id ?>" chained-id="<?= $id ?>" class="form-control">
							<option value="0">---</option>
							<?php foreach ($countries as $coutry): ?>
								<option value="<?= $coutry->id ?>"<?= $coutry->id == $route->getDefaultValue("country_id") ? ' selected="selected"' : '' ?>><?= $coutry->title ?></option>
							<?php endforeach ?>
						</select>
					</div>
					<div class="col-xs-6">
						Город
						<select name="routes[<?= $id ?>][city_id]" id="chained-city-<?= $id ?>" chained-id="<?= $id ?>" class="form-control chained">
							<option value="0">---</option>
							<?php foreach ($cities as $city): ?>
								<option data-chained="<?= $city->getDefaultValue('country_id') ?>" value="<?= $city->id ?>"<?= $city->id == $route->getDefaultValue("city_id") ? ' selected="selected"' : '' ?>><?= $city->title ?></option>
							<?php endforeach ?>
						</select>
					</div>
				</div>
				<div class="form-group">
					<div class="col-xs-6">
						<div class="btn btn-danger delete-this-by-ajax" url="/admin/entity_route/delete/<?=  $route->id ?>">Удалить</div>
					</div>
				</div>
			</div>
		<?php endforeach ?>
	<?php endif ?>
	<div class="hidden dinamic-block">
		<div class="form-group">
			<div class="col-xs-6">
				Страна
				<select original-name="routes[uniqid][country_id]" id="chained-country-uniqid" chained-id="uniqid" class="form-control">
					<option value="0">---</option>
					<?php foreach ($countries as $coutry): ?>
						<option value="<?= $coutry->id ?>"><?= $coutry->title ?></option>
					<?php endforeach ?>
				</select>
			</div>
			<div class="col-xs-6">
				Город
				<select original-name="routes[uniqid][city_id]" id="chained-city-uniqid" chained-id="uniqid" class="form-control chained">
					<option value="0">---</option>
					<?php foreach ($cities as $city): ?>
						<option data-chained="<?= $city->getDefaultValue('country_id') ?>" value="<?= $city->id ?>"><?= $city->title ?></option>
					<?php endforeach ?>
				</select>
			</div>
		</div>
		<div class="form-group">
			<div class="col-xs-6">
				<div class="btn btn-danger delete-this">Удалить</div>
			</div>
		</div>
	</div>
</div>
<button class="btn btn-default add-dinamic-block" type="button">
	Добавить
</button>