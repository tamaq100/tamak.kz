<? 
	$pages = [];
	$all_coordinates = Coordinate::findAll();
	if ($entity->getDefaultValue('rubric_id')) {
		$rubric 	= $entity->rubric_object;
		$page_model = $rubric->furl;
		$page_model = ucfirst($page_model);
		if (class_exists($page_model)) {
			$pages = $page_model::findAll();
		}
	}
?>
<div class="dinamic-blocks-wrapper">
	<?php if ($entity->$field): ?>
		<? $koordinates = $entity->$field ?>
		<?php foreach ($koordinates as $id => $koordinate): ?>
			<div class="dinamic-block">
				<div class="form-group">
					<div class="col-xs-12">
						<select name="map_koordinates[<?= $id ?>][point_id]" class="form-control">
							<?php foreach ($all_coordinates as $point_coordinate): ?>
								<option value="<?= $point_coordinate->id ?>"<?= $koordinate->point_id == $point_coordinate->id ? ' selected="selected"' : '' ?>>
									<?= $point_coordinate->ru_title ?>
								</option>
							<?php endforeach ?>
						</select>
					</div>
				</div>
				<div class="form-group">
					<?php if ($pages): ?>
						<div class="col-xs-6">
							<?= __($rubric->furl . '.name') ?>
							<select class="form-control select2" multiple="multiple" name="map_koordinates[<?= $id ?>][parent_id][]">
								<?php foreach ($pages as $page): ?>
									<option value="<?= $page->id ?>" <?= (isset($koordinate->parent_id) && is_array($koordinate->parent_id) && in_array($page->id, $koordinate->parent_id)) ? 'selected="selected"' : '' ?>>
										<?= $page->title ?>
									</option>
								<?php endforeach ?>
							</select>
						</div>
					<?php endif ?>
					<div class="col-xs-2">
						<div class="">&nbsp;</div>
						<button type="button" class="btn btn-danger delete-this">Удалить</button>
					</div>
				</div>
			</div>
		<?php endforeach ?>
	<?php endif ?>
	<div class="hidden dinamic-block">
		<div class="form-group">
			<div class="col-xs-12">
				<select original-name="map_koordinates[uniqid][point_id]" class="form-control">
					<?php foreach ($all_coordinates as $point_coordinate): ?>
						<option value="<?= $point_coordinate->id ?>">
							<?= $point_coordinate->ru_title ?>
						</option>
					<?php endforeach ?>
				</select>
			</div>
		</div>
		<div class="form-group">
			<?php if ($pages): ?>
				<div class="col-xs-6">
					<?= __($rubric->furl . '.name') ?>
					<select class="form-control select2" multiple="multiple" original-name="map_koordinates[uniqid][parent_id][]">
						<?php foreach ($pages as $page): ?>
							<option value="<?= $page->id ?>">
								<?= $page->title ?>
							</option>
						<?php endforeach ?>
					</select>
				</div>
			<?php endif ?>
			<div class="col-xs-6">
				<div class="">&nbsp;</div>
				<button type="button" class="btn btn-danger delete-this">Удалить</button>
			</div>
		</div>
	</div>
</div>
<button class="btn btn-default add-dinamic-block" type="button">
	Добавить
</button>