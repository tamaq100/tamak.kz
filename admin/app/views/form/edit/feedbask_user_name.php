<?php if ($entity->user_id): ?>
	<p class="form-control-static">
		<a href="/admin/user/show/<?= $entity->user_id ?>">
			<?= $entity->user_name ?>
		</a>
	</p>
<?php else: ?>
	<p class="form-control-static">Аноним</p>
<?php endif; ?>
