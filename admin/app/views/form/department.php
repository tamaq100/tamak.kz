<? $professor = $entity->get($field) ?: array(); ?>

<select style="width: 100%" title="Выберите отзыв(ы)" id="review-select" multiple="multiple" name="<?= $field ?>[]">
	<?php foreach ($professor as $department): ?>
		<? $review = Review::findOneBy($department); ?>
		<option value="<?= $review->id ?>" selected><?= $review->get('name') ?></option>
	<?php endforeach; ?>
</select>