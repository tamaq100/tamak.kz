<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Авторизация</title>
</head>
<style>
	body {background: url(/admin/img/head.gif) top left repeat #fff;}
	body, td, input {font-family: Tahoma, Sans-Serif; font-size: 12px;}
	a {color: #345e59;}
	table.frst {width: 100%; height: 100%;}
	div.login {background: #fff; padding: 50px; width: 370px; position: relative;}
	div.login div {text-align: left;}
	div.login input {margin-bottom: 15px; margin-top: 5px; width: 100%;}
	div.login div.msg {color: red; margin-bottom: 15px;}
	div.login .l input {width: 100px; margin: 0;}
	div.login .l, div.login .r {float: left; width: 50%;}
	div.login .r {text-align: right; padding-top: 8px;}
	.c {clear: both; margin: 0;}
	#logo {position: absolute; top: -60px; left: 0px;}
</style>
<body>
<table class="frst">
	<tr>
		<td valign="middle" align="center">
			<form method="post" action="/admin/auth/login_post<?=(isset($_GET['returnpath'])?'?returnpath='.urlencode($_GET['returnpath']):'')?>">
			<div class="login">
				<img src="/admin/img/logo_login.png" width="230" height="41" id="logo" border="0" />
				<?
					if (!isset($loginvar)) $loginvar = 0;
					if ($loginvar==1) {
						print '<div class="msg">Не заполнено поле логин или пароль</div>';
					}elseif ($loginvar==2) {
						print '<div class="msg">Неверный логин или пароль</div>';
					}elseif ($loginvar==3) {
						print '<div class="msg">Вы успешно вышли</div>';
					}
				?>
				<div>Логин:<br />
					 <input type="text" name="u_login" value="" class="text">
				</div>
				<div>Пароль:<br />
					<input type="password" name="u_pass" value="" class="text">
				</div>
				<div class="l">
					<input type="submit" name="sb" value="Войти" class="btn">
				</div>
				<div class="r">
					<a href="mailto:admin@4design.kz?subject=Восстановите пароль для сайта <?=$_SERVER['SERVER_NAME'];?>">Напомнить пароль</a>
				</div>
				<div class="c"></div>
			</div>
			</form>
		</td>
	</tr>
</table>
</body>
</html>