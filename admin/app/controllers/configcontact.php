<?php

/**
* ConfigAuto Controller
*/
class ConfigContactController extends ConfigController {
    
    public $model = 'ConfigContact';

    public $redirect_uri = '/admin/configcontact';

}