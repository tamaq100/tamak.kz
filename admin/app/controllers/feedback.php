<?php

/**
* Feedback Controller
*/
class FeedbackController extends CrudController {
    public $model = 'Feedback_contact';

    // Запрещаем добавление данных
    public $edit_only = true;

    public $list_fields = array(
        'id'            => 'integer',
        'name'          => 'string',
        'telephone'     => 'string',
        'email'         => 'textarea',
    );

    public $edit_fields = array(
        'id'               => 'null',
        'name'             => 'string',
        'telephone'        => 'string',
        'email'            => 'textarea',
        'message'          => 'null',
        'moderated'        => 'bool'
    );
}