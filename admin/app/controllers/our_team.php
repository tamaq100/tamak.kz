<?php
class Our_teamController extends CrudController {

	public $model = 'Our_team';

	public $list_fields = array(
		'idx'			   => 'integer',
		'id'               => 'integer',
        'name'             => 'string',
		'active'		   => 'bool',
	);
	public $edit_fields = array(
	    'id'               => 'null',
	    'active'		   => 'bool',
	    'avatar'		   => 'image',
	    'name'		   	   => 'string',
	    'position'	   	   => 'string',
    );
}