<?php
class FAQController extends CrudController {

	public $model = 'FAQ';

	public $list_fields = array(
		'idx'              => 'integer',
        'id'               => 'integer',
        'question'         => 'string',
        'active'		   => 'bool',
	);
	public $edit_fields = array(
        'id'               => 'null',
        'active'           => 'bool',
        'question'         => 'string',
        'answer'		   => 'text',
    );
}