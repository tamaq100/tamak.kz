<?php
class ReviewController extends CrudController {
	public $title = 'Отзывы';

	public $model = 'Review';

	public $list_fields = array(
        'idx'              => 'integer',
        'id'               => 'integer',
        'name'             => 'string',
        'stars'            => 'integer',
        'active'           => 'bool',
	);
	public $edit_fields = array(
        'id'                  => 'null',
        'created'             => 'null',
        'updated'             => 'null',
        'active'              => 'bool',
        'name'                => 'string',
        'text'                => 'text',
        'stars'               => 'integer',
    );
}