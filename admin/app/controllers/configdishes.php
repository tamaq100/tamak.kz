<?php

/**
* ConfigDelivery Controller
*/
class ConfigDishesController extends ConfigController {
    
    public $model = 'ConfigDishes';

    public $redirect_uri = '/admin/configdishes';

}