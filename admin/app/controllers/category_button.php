<?php
class Category_buttonController extends CrudController {

	public $model = 'Category_button';

	public $list_fields = array(
		'idx'              => 'integer',
		'id'               => 'integer',
        'name'            => 'string',
        'active'		   => 'bool',
	);
	public $edit_fields = array(
        'id'               => 'null',
        'active'           => 'bool',
        'name'            => 'string',
	    'link'		   	   => 'string',
	    'image'			   => 'image',
    );
}