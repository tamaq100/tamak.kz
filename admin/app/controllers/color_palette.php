<?php
class Color_paletteController extends CrudController {

	public $model = 'Color_palette';

	public $list_fields = array(
		'idx'              => 'integer',
		'id'               => 'integer',
        'color'            => 'string',
        'active'		   => 'bool',
	);
	public $edit_fields = array(
        'id'               => 'null',
        'active'           => 'bool',
        'color'            => 'string',
    );
}