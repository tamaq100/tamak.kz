<?php
/**
* AboutUsMissionController model
*/
class AboutUsMissionController extends CrudController {

	public $model = 'AboutUsMission';

	public $list_fields = array(
		'idx'              => 'integer',
        'id'               => 'integer',
        'title'             => 'string',
        'active'		   => 'bool',
	);
	public $edit_fields = array(
        'id'               => 'null',
        'active'           => 'bool',
        'title'			   => 'string',
        'image'			   => 'image',
    );
}