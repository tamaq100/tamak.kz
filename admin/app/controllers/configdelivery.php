<?php

/**
* ConfigDelivery Controller
*/
class ConfigDeliveryController extends ConfigController {
    
    public $model = 'ConfigDelivery';

    public $redirect_uri = '/admin/configdelivery';

}