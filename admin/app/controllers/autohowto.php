<?php
class AutoHowToController extends CrudController {

	public $model = 'AutoHowTo';

	public $list_fields = array(
		'idx'              => 'integer',
		'id'               => 'integer',
        'title'             => 'string',
	);
	public $edit_fields = array(
        'id'               => 'null',
        'title'            => 'string',
    );
}