<?php
class KitchenController extends CrudController {

	public $model = 'Kitchen';

	public $list_fields = array(
		'idx'		=> 'integer',
		'id'		=> 'integer',
		'active'	=> 'bool',
		'name'		=> 'string',
	);
	public $edit_fields = array(
		'id'			=> 'null',
		'active'		=> 'bool',
		'name'			=> 'string',
		'key'		   	=> 'string',
	);
}