<?php

class DefaultController extends BaseController {

	public function indexAction()
	{
		$prepared_scriptlist = [];
		$scriptlist = $this->get_scriptlist();
		foreach ($scriptlist as $script) {
			foreach ($script['childrens'] as $val) {
				$prepared_scriptlist[] = $val;
			}
		}
		$vars = array(
			'scriptlist' => $prepared_scriptlist,
		);
		return $this->render('default/index', $vars);
	}

	public function childsAction($id)
	{
		$scriptlist = $this->get_scriptlist();
		
		if (! isset($this->scriptlist[$id])) return false;

		$vars = array(
			'scriptlist' => $this->scriptlist[$id]['childrens'],
		);

		$this->title = $this->scriptlist[$id]['title'];

		return $this->render('default/index', $vars);
	}
}