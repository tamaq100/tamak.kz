<?php
class Popular_restorauntController extends CrudController {

	public $model = 'Popular_restoraunt';

	public $list_fields = array(
		'idx'			   => 'integer',
		'id'               => 'integer',
		'title'			   => 'string',
		'active'		   => 'bool',
	);
	public $edit_fields = array(
	    'id'               => 'null',
	    'created'		   => 'null',
	    'updated'		   => 'null',
	    'active'		   => 'bool',
	    'title'		   	   => 'string',
	    'citchen'		   => 'text',
	    'min_price'		   => 'string',
	    // 'max_price'		   => 'string',
	    'jobtime_min'	   => 'string',
	    'jobtime_max'	   => 'string',
	    'image'			   => 'image',
    );
}