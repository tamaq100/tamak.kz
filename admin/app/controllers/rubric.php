<?php

/**
* Rubric Controller
*/
class RubricController extends CrudController {
    public $model = 'Rubric';
    public $index_template = 'crud/tree_with_idx';

    public $list_fields = array(
        'idx'              => 'integer',
        'id'               => 'integer',
        'title'            => 'string',
    );
    
    public $edit_fields = array(
        'id'                => 'null',
        'active'            => 'bool',
        'created'           => 'null',
        'updated'           => 'null',
        'parent_id'         => 'select',
        'title'             => 'string',
        'furl'              => 'string',
        'link'              => 'link',
        'type'              => 'select',
        'metaTitle'         => 'string',
        'metaDescription'   => 'string',
        'metaKeywords'      => 'string',
        // 'text'              => 'text',
        // 'gallery'           => 'plupload_gallery',
    );

    /**
     * Экшн для добавления дочерних элементов
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function create_as_submenuAction($id)
    {
        $entity = new $this->model;
        $parent_entity = new $this->model;
        $parent_entity->load($id);

        if (isset($entity->closed_to_users) &&  array_key_exists($_SESSION['u_id'], $parent_entity->closed_to_users)) {
            header('Location: /admin/');
        }

        $entity->parent_id = intval($id);
        $vars = array(
            'entity' => $entity,
            'fields' => $this->edit_fields,
        );

        return $this->render($this->edit_template, $vars);
    }

    /**
     * Просмотр сущности
     */
    public function showAction($id)
    {
        // $this->onlyForNotReadOnly();

        $entity = new $this->model;
        if (! $entity->load($id)) {
            return $this->render(__($this->model.'.404'));
        }

        if (isset($entity->closed_to_users) &&  array_key_exists($_SESSION['u_id'], $entity->closed_to_users)) {
            header('Location: /admin/');
        }

        $this->setShowTitle($entity);

        $vars = array(
            'entity' => $entity,
            'fields' => $this->edit_fields,
        );

        return $this->render($this->show_template, $vars);
    }

    /**
     * Редактирование сущности
     */
    public function editAction($id)
    {
        $fields = $this->edit_fields;
        $this->onlyForNotReadOnly();

        $entity = new $this->model;
        if (! $entity->load($id)) {
            return $this->render(__($this->model.'.404'));
        }

        $this->title = __(strtolower($this->model).'.name').' #'.$entity->id;

        $vars = array(
            'entity' => $entity,
            'fields' => $fields,
        );
        return $this->render($this->edit_template, $vars);
    }

    public function saveAction()
    {
        $this->onlyForNotReadOnly();
        $this->onlyForNotEditOnly();

        $entity = new $this->model;
        $vars = array(
            'entity' => $entity,
            'fields' => $this->edit_fields,
        );

        foreach ($this->edit_fields as $key => $type) {
            if ($type == 'image' || $type == 'report_file') {
                $entity->$key = false;
            } elseif ($type == 'block_rubrics_to_user') {
                $entity->setBlockRubrics($_POST[$key]);
            } elseif (isset($_POST[$key])) {
                $entity->$key = $_POST[$key];
            }
        }

        $entity->save();
        $redirect_uri = $this->getRedirectUrlToShow($entity);
        header('Location: '.$redirect_uri);
        exit;
    }
}
