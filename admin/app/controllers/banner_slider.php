<?php
class Banner_sliderController extends CrudController {
	public $title = 'Главный баннер';

	public $model = 'Banner_slider';

	public $list_fields = array(
		'idx'              => 'integer',
		'id'               => 'integer',
        'title'            => 'string',
        'active'		   => 'bool',
	);
	public $edit_fields = array(
        'id'               => 'null',
        'active'           => 'bool',
        'title'            => 'string',
        'text'			   => 'text',
	    'subtitle'         => 'text',
	    'link'		   	   => 'string',
	    'typecolor'		   => 'color',
	    // 'typecolor'        => 'select',
	    'image'			   => 'image',
    );
}