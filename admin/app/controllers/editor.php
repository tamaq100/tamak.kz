<?php

/**
* Editor Controller for uploading files from editor
*/
class EditorController extends BaseController {

    // Пересмотреть метод на предмет безопасности
    public function uploadAction()
    {
        $funcNum = $_GET['CKEditorFuncNum'];

        $url = '';
        if ($image_id = $this->_saveUpload('upload')) {
            $url = '/userfiles/'.$image_id;
        }
        $message = '';

        return '<script type="text/javascript"> window.parent.CKEDITOR.tools.callFunction("'.$funcNum.'", "'.$url.'", "'.$message.'")</script>';
    }

    /**
     * Базовый метод для сохранения файлов
     */
    public function _saveUpload($field, $id = false)
    {
        $name       = $id && is_array($_FILES[$field]['name'])     ? $_FILES[$field]['name'][$id]     : $_FILES[$field]['name'];
        $tmp_name   = $id && is_array($_FILES[$field]['tmp_name']) ? $_FILES[$field]['tmp_name'][$id] : $_FILES[$field]['tmp_name'];
        $type       = $id && is_array($_FILES[$field]['type'])     ? $_FILES[$field]['type'][$id]     : $_FILES[$field]['type'];
        $size       = $id && is_array($_FILES[$field]['size'])     ? $_FILES[$field]['size'][$id]     : $_FILES[$field]['size'];

        if (is_uploaded_file($tmp_name) and $size) {
            $filename = md5(uniqid(rand()));

            $imagename = "../userfiles/" . $filename . ".".strtolower(end(explode('.', $name)));
            move_uploaded_file($tmp_name, $imagename); 
            chmod($imagename, 0777);

            return $filename . ".".strtolower(end(explode('.', $name)));
        }

        return false;
    }
}
