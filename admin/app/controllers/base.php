<?php
class BaseController
{

	public $title = '';
	public $model = 'default';

	public function get_scriptlist($need_full = false)
	{
		$scriptlist = array(
			'admin' => array(
				'title'	=> 'Для администратора',
				'childrens' =>[
					'administrator' => array(
						'href' => '/admin/administrator',
						'title' => 'administrator.names',
						'icon' => 'user-circle',
						'icon_color' => '#ff0000',
					),
				],
			),
			'global_modules' => array(
				'title' => 'Общие модули',
				'childrens' => [
					'rubric' => array(
						'href' => '/admin/rubric',
						'title' => 'rubric.names',
						'icon' => 'folder-open',
						'icon_color' => '#e4af62',
					),    
					'config' => array(
						'href' => '/admin/config',
						'title' => 'config.names',
						'icon' => 'cog',
						'icon_color' => '#847e7f',
					),
					'feedback' => array(
						'href' => '/admin/feedback',
						'title' => 'feedback.names',
						'icon' => 'comments',
						'icon_color' => '#345e59',
					),
					'city' => array(
						'href' => '/admin/city',
						'title' => 'city.names',
						'icon' => 'globe',
						'icon_color' => '#72a2b5',
					),
					'kitchen' => array(
						'href' => '/admin/kitchen',
						'title' => 'kitchen.names',
						'icon' => 'globe',
						'icon_color' => '#72a2b5',
					),
				],
			),
			'main' => array(
				'title' => 'Главная страница',
				'childrens' =>[
					'banner_slider' => array(
						'href' => '/admin/banner_slider',
						'title' => 'banner_slider.names',
						'icon' => 'window-restore',
						'icon_color' => '#4caf50',
					),
					'category_button' => array(
						'href' => '/admin/category_button',
						'title' => 'category_button.names',
						'icon' => 'hand-pointer-o',
						'icon_color' => '#c54ad2',
					),
//					'company' => array(
//						'href' => '/admin/company',
//						'title' => 'company.names',
//						'icon' => 'list-ol',
//						'icon_color' => '#057b70',
//					),
					'faq' => array(
						'href' => '/admin/faq',
						'title' => 'faq.names',
						'icon' => 'question-circle',
						'icon_color' => '#847e7f',
					),
					// 'feedback_contact' => array(
					// 	'href' => '/admin/feedback_contact',
					// 	'title' => 'feedback_contact.names',
					// 	'icon' => 'comments-o',
					// 	'icon_color' => '#847e7f',
					// ),
					'executor' => array(
						'href' => '/admin/executor',
						'title' => 'executor.names',
						'icon' => 'users',
						'icon_color' => '#a26204',
					),
                    // не используется нигде на данный момент на сайте
//					'our_team' => array(
//						'href' => '/admin/our_team',
//						'title' => 'our_team.names',
//						'icon' => 'users',
//						'icon_color' => '#607d8b',
//					),
//					'popular_food' => array(
//						'href' => '/admin/popular_food',
//						'title' => 'popular_food.names',
//						'icon' => 'cutlery',
//						'icon_color' => '#607d8b',
//					),
					'popular_restoraunt' => array(
						'href' => '/admin/popular_restoraunt',
						'title' => 'popular_restoraunt.names',
						'icon' => 'fire',
						'icon_color' => '#ec5a2a',
					),

					'review' => array(
						'href' => '/admin/review',
						'title' => 'review.names',
						'icon' => 'comments',
						'icon_color' => '#637d44',
					),                    
				],
			),
			'configscustom' => array(
				'title' => 'Настройки страниц',
				'childrens' =>[
					'dishes' => array(
						'href' => '/admin/configdishes',
						'title' => 'configdishes.names',
						'icon' => 'bookmark',
						'icon_color' => '#af5867', 
					),
					'dishesadvantage' => array(
						'href' => '/admin/dishesadvantage',
						'title' => 'dishesadvantage.names',
						'icon' => 'certificate',
						'icon_color' => '#9c1d32', 
					),
					'partner' => array(
						'href' => '/admin/partner',
						'title' => 'partner.names',
						'icon' => 'street-view',
						'icon_color' => '#6c7701', 
					),				
					'auto' => array(
						'href' => '/admin/configauto',
						'title' => 'configauto.names',
						'icon' => 'car',
						'icon_color' => '#f5cb23', 
					),
					'autoadvantages' => array(
						'href' => '/admin/autoadvantages',
						'title' => 'autoadvantages.names',
						'icon' => 'pie-chart',
						'icon_color' => '#673ab7',
						// certificate 
					),
					'autohowto' => array(
						'href' => '/admin/autohowto',
						'title' => 'autohowto.names',
						'icon' => 'tasks',
						'icon_color' => '#3e4456', 
					),
					'configaboutus' => array(
						'href' => '/admin/configaboutus',
						'title' => 'configaboutus.names',
						'icon' => 'tasks',
						'icon_color' => '#ff9800', 
					),
					'aboutusmission' => array(
						'href' => '/admin/aboutusmission',
						'title' => 'aboutusmission.names',
						'icon' => 'grav',
						'icon_color' => '#171616', 
					),
					'aboutusvalues' => array(
						'href' => '/admin/aboutusvalues',
						'title' => 'aboutusvalues.names',
						'icon' => 'diamond',
						'icon_color' => '#154484', 
					),
					'aboutusteam' => array(
						'href' => '/admin/aboutusteam',
						'title' => 'aboutusteam.names',
						'icon' => 'users',
						'icon_color' => '#bb4f74', 
					),
					'configdelivery' => array(
						'href' => '/admin/configdelivery',
						'title' => 'configdelivery.names',
						'icon' => 'rocket',
						'icon_color' => '#00bcd4', 
					),
					'contacts' => array(
						'href' => '/admin/configcontact',
						'title' => 'contacts.names',
						'icon' => 'address-book',
						'icon_color' => '#795548', 
					),
					'configagreement' => array(
						'href' => '/admin/configagreement',
						'title' => 'configagreement.names',
						'icon' => 'handshake-o',
						'icon_color' => '#676766', 
					),
				]
			),
			'media' => array(
				'title' => 'Медиа/Акции/Новости',
				'childrens' =>[
					'news' => array(
						'href' => '/admin/news',
						'title' => 'news.names',
						'icon' => 'newspaper-o',
						'icon_color' => '#9e9e9e', 
					),
					'promo' => array(
						'href' => '/admin/promo',
						'title' => 'promo.names',
						'icon' => 'money',
						'icon_color' => '#bba423',
					),
				]
			),
		);
		return $scriptlist;
	}

	public function getProtectedFilesPath()
	{
		return WORK_DIR . '/../runtime/_protected_files';
	}

	/**
	 * @param $template
	 * @param array $vars
	 * @param string $base_template
	 * @return string
	 */
	public function render($template, $vars = array(), $base_template = 'default/layout')
	{
		$descr = '';

		$content = include_file($template, $vars);

		$title = $this->title ?: __(strtolower($this->model) . '.names');

		if (isset($this->desc)) {
			$descr = $this->desc ? __(strtolower($this->model) . '.desсr') : '';
		}
		$vars = array(
			'content'    => $content,
			'title'      => $title,
			'desс'       => $descr,
			'scriptlist' => $this->get_scriptlist(),

		);
		if ($base_template) {
			return include_file($base_template, $vars);
		} else {
			return $content;
		}
	}

	public function isAjax()
	{
		$is_ajax = !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';

		return $is_ajax;
	}

	public function download_pdfAction($entity_id) {
		$entity = new $this->model;
		$path = './userfiles/';
		if (! $entity->load($entity_id)) abort('404');
		header("X-Sendfile: ./". $path . '/' . $entity->get('manual_pdf'));
		header("Content-Type: application/octet-stream");
		header("Content-Disposition: attachment; filename=\"".$entity->name."\"");
		readfile("./" . $path . "/" . $entity->manual_pdf);
	}

}