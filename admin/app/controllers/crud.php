<?php

class CrudController extends BaseController {

	public $index_template = 'crud/index';
	public $show_template  = 'crud/show';
	public $edit_template  = 'crud/edit';
	public $read_only      = false;
	public $edit_only      = false;
	public $uncreatable    = false;
	
	/**
	 * Отключает действия которые подразумевают за собой изменение данных
	 */
	public function onlyForNotReadOnly()
	{
		if ($this->read_only) {
			abort('502');
		}
	}
	/**
	 * Отключает действия которые подразумевают за собой добавление данных
	 */
	public function onlyForNotEditOnly()
	{
		if ($this->edit_only) {
			abort('502');
		}

	}

	public function getRedirectUrlToIndex($entity)
	{
		return '/admin/'.strtolower($this->model);
	}

	public function getRedirectUrlToShow($entity)
	{
		return '/admin/'.strtolower($this->model).'/show/'.$entity->id;
	}

	public function getRedirectUrlToEdit($entity)
	{
		return '/admin/'.strtolower($this->model).'/edit/'.$entity->id;
	}

	public function getRedirectUrlToCreate($entity)
	{
		return '/admin/'.strtolower($this->model).'/create';
	}

	/**
	 * Страница ленты
	 */
	public function indexAction()
	{
		$model = $this->model;

		$vars = array(
			'model'     	=> $this->model,
			'fields'    	=> $this->list_fields,
			'read_only' 	=> $this->read_only,
			'edit_only' 	=> $this->edit_only,
			'uncreatable'	=> $this->uncreatable,
		);

		if ($this->index_template == 'crud/tree' || $this->index_template == 'crud/tree_with_idx') {
			$vars['items'] = $model::findAll();
		}


		return $this->render($this->index_template, $vars);
	}

	public function index_ajaxAction()
	{
		$model = $this->model;
		$where = [];
		$columns = array_map(function($i) { return $i['name']; }, $_POST['columns']);
		array_pop($columns);

		// Если отправлен запрос на поиск
		$entity = new $model;
		$entity->select()->where('1 = 1');

		if (isJson($_POST['search']['value'])) {
			$entity->generateWhere($this->ajaxSearchByColumn());
		} elseif ($_POST['search']['value']) {
			$entity->andWhere(
				function($entity, $columns = []) {
					$model = $entity->getClassName();
					$group_and_generator = new $model;
					foreach ($columns as $name) {
						$group_and_generator->orWhere([$name, '%'.$_POST['search']['value'].'%', 'LIKE']);
					}
					return $group_and_generator;
				}
			, $columns);
		}

		if (isset($_POST['order'])) {
			$order = current($_POST['order']);
			if (isset($columns[$order['column']])) {
				if ($this->list_fields[$columns[$order['column']]] == 'select_relation') {
					$entity->orderBy($columns[$order['column']].'_id '.$order['dir']);
				} else {
					$entity->orderBy($columns[$order['column']].' '.$order['dir']);
				}
			}
		}

		$count = count($entity->all());

		$entity->offset($_POST['start']);
		$entity->limit($_POST['length'] > 0 ? $_POST['length'] : 10000);

		$items = $entity->all();

		$result = [];
		
		if (! empty($items)) {
			foreach ($items as $item) {
				$res_item = [];
				foreach ($this->list_fields as $field => $type) {
					$vars = [
						'type' 		=> $type,
						'entity'	=> $this,
						'field'		=> $field,
						'item'		=> $item,
					];

					$res_item[$field] = static::include_list_form_group($vars);
				}

				$vars['type']  = 'actions';

				$res_item['actions'] = static::include_list_form_group($vars);
				$res_item['DT_RowId'] = $item->id;

				$result[] = $res_item;
			}
		}

		return json_encode(array(
				'draw' => 0,
				'recordsTotal' => $count,
				'recordsFiltered' => $count,
           		'data' => $result
           	)
		);
	}

	public function ajaxSearchByColumn()
    {
		$fields = $this->list_fields;
    	$where = [];
		$json = $_POST['search']['value'];
    	$columns = json_decode($json);

    	$model = $this->model;
    	$table_name = BaseModel::$TABLE_PREFIX . $model::$table_name;

    	foreach ($columns as $column => $value) {

    		if ($value && isset($fields[$column])) {
    			switch ($fields[$column]) {
    				case 'bool':
    					if ($value == 1) {
		    				$where[$column] = $value;
    					} elseif($value == 2) {
		    				$where[$column] = 0;
    					}
    					break;
    				case 'select_relation':
	    				$where[$column . '_id'] = $value;
    					break;
    				case 'multiple_relations':
	    				$where[$column.'.title'][] = '%' . $value . '%';
	    				$where[$column.'.title'][]= 'like';
    					break;
    				case 'date':
    				case 'datetime':
    				case 'only_date':
    					$date_array = explode(' - ', $value);

	    				$from = strtotime($date_array[0]);
	    				$to = strtotime($date_array[1]);

	    				
	    				$where[$column][] = [$from, $to];
	    				$where[$column][] = 'between';

    					break;

    				default:
	    				$where[$column][] = '%' . $value . '%';
	    				$where[$column][]= 'like';
    					break;
    			}
    		}
    	}
    	// var_dump($where);
    	return $where;
    }


	public function getRedirectUrl($entity)
	{
		if (isset($_GET['and_create']) && $_GET['and_create']) {
			return $this->getRedirectUrlToCreate($entity);
		} elseif (isset($_GET['and_continue']) && $_GET['and_continue']) {
			return $this->getRedirectUrlToEdit($entity);
		} else {
			return $this->getRedirectUrlToShow($entity);
		}
	}
	/**
	 * Изменение сущности
	 */
	public function createAction()
	{
		$this->onlyForNotReadOnly();
		$this->onlyForNotEditOnly();
		$entity = new $this->model;

		$this->title = __(strtolower($this->model).'.name');

		$vars = array(
			'entity' => $entity,
			'fields' => $this->edit_fields,
		);

		return $this->render($this->edit_template, $vars);
	}

	/**
	 * Сохранение сущности
	 */
	public function saveAction()
	{
		$this->onlyForNotReadOnly();
		$this->onlyForNotEditOnly();

		$entity = new $this->model;
		$vars = array(
			'entity' => $entity,
			'fields' => $this->edit_fields,
		);

		$this->save($entity);

		$redirect_uri = $this->getRedirectUrl($entity);
		
		header('Location: '.$redirect_uri);
		exit;
	}

	public function setShowTitle($entity)
	{
		$this->title = __(strtolower($this->model).'.name').' #'.$entity->id;

		return $this;
	}

	/**
	 * Просмотр сущности
	 */
	public function showAction($id)
	{
		// $this->onlyForNotReadOnly();

		$entity = new $this->model;
		if (! $entity->load($id)) {
			return $this->render(__($this->model.'.404'));
		}

		$this->setShowTitle($entity);

		$vars = array(
			'entity' => $entity,
			'fields' => $this->edit_fields,
		);

		return $this->render($this->show_template, $vars);
	}

	/**
	 * Редактирование сущности
	 */
	public function editAction($id)
	{
		$this->onlyForNotReadOnly();

		$entity = new $this->model;
		if (! $entity->load($id)) {
			return $this->render(__($this->model.'.404'));
		}

		$this->title = __(strtolower($this->model).'.name').' #'.$entity->id;

		$vars = array(
			'entity' => $entity,
			'fields' => $this->edit_fields,
		);
		
		return $this->render($this->edit_template, $vars);
	}

	/**
	 * Обновление
	 */
	public function updateAction($id)
	{
		$this->onlyForNotReadOnly();

		$entity = new $this->model;

		if (! $entity->load($id)) {
			return $this->render(__($this->model.'.404'));
		}

		$this->save($entity);
		$redirect_uri = $this->getRedirectUrl($entity);
		header('Location: '.$redirect_uri);
		exit;
	}


	public function save(&$entity)
	{
		$fields = $this->edit_fields;
		foreach ($fields as $key => $type) {
			// Часто приходится обрабатывать данные только при сохранении, функция set из модели не подходит. 
			if (method_exists($entity, 'save' . ucfirst($type))) {
				$data = $entity->{'save' . ucfirst($type)}($key);
				if ($data) {
					$entity->$key = $data;
				}
			} elseif (method_exists($entity, 'save' . ucfirst($key))) {
				$data = $entity->{'save' . ucfirst($key)}();
			} elseif (isset($_POST[$key])) {
				$entity->$key = $_POST[$key];
			}	
		}


        $relation_fields = array_filter($fields, function($a){
        	if($a == 'multiple_relation') return $a;
        }
        );
        if ($relation_fields) {
        	foreach ($relation_fields as $key => $type) {
				$data = (isset($_POST[$key])&&!empty($_POST[$key]))?$_POST[$key]:array();
				$entity->setMultipleRelations($key, $data);
        	}
        }



		$entity->save();
	}


	/**
	 * Удаление
	 */
	public function deleteAction($id)
	{
		$this->onlyForNotReadOnly();

		$entity = new $this->model;

		if (! $entity->load($id)) {
			return $this->render(__($this->model.'.404'));
		}

		$redirect_uri = $this->getRedirectUrlToIndex($entity);

		$entity->delete();

		if ($this->isAjax()) return;

		header('Location: '.$redirect_uri);
		exit;
	}

	public function update_orderAction()
	{
		if (! isset($_POST['id']) || ! isset($_POST['newPosition'])) abort('404');

		$model = $this->model;

		if (! $entity = $model::findOneBy($_POST['id'])) abort('404');

		var_dump($_POST);
		$entity->idx = $_POST['newPosition'];

		$entity->save();

		return;
	}

	public function delete_imageAction($id)
	{
		$this->onlyForNotReadOnly();

		$entity = new $this->model;
		if (! $entity->load($id)) {
			return $this->render(__($this->model.'.404'));
		}

		if (isset($_POST['field']) && isset($entity::$fields[$_POST['field']])) {
			$entity
				->deleteImage($_POST['field'])
				->save();
		}
		return true;
	}

	public function save_idxAction($id)
	{
		foreach ($_POST['idxes'] as $id => $value) {
			$entity = new $this->model;
			$entity->load($id);
			$entity->idx = $value;
			$entity->save();
		}

		$redirect_uri = $this->getRedirectUrlToIndex($entity);
		header('Location:'.$redirect_uri);
		exit;
	}

	public static function include_edit_form_group($vars) {

		$type = $vars['type'];
		$entity = $vars['entity'];
		$dir = SITE_DIR . 'admin/app/views/';
		if (file_exists($dir . strtolower($entity->getClassName()) . '/edit/' . $type . '.php')) {
			$form_group = include_file(strtolower($entity->getClassName()) . '/edit/' . $type, $vars);
		} elseif (file_exists($dir . 'form/edit/' . $type . '.php')) {
			$form_group = include_file('form/edit/' . $type , $vars);
		} elseif (file_exists($dir .strtolower($entity->getClassName()) . '/edit/default.php')) {
			$form_group = include_file(strtolower($entity->getClassName()) . '/edit/default', $vars);
		} else {
			$form_group = include_file('form/edit/default', $vars);
		}	

		echo $form_group;
	}
	public function getAction()
    {
		$result = [];

		$search_string = (isset($_GET['q']) && $_GET['q']) ? $_GET['q'] : '';

		$where = '1=1 AND ('.
			'LOWER(title) LIKE "%' . $search_string . '%" ' .
		')';

		$filter = $where;

		$model = $this->model;

		if (in_array($model, ['Product_category'])) {
			$entities = $model::baseFindBy($filter, 'title ASC');
		} else {
			$entities = $model::findBy($filter, 'title ASC');
		}
		
		foreach ($entities as $entity) {

			$result[] = [
				'id' => $entity->id,
				'text' => $entity->title
			];
		}

		return json_encode($result);

    }
	public static function include_read_form_group($vars)
	{
		$type = $vars['type'];
		$entity = $vars['entity'];
		$dir = SITE_DIR . 'admin/app/views/';

		if (file_exists($dir . strtolower($entity->getClassName()) . '/show/' . $type . '.php')) {
			$form_group = include_file(strtolower($entity->getClassName()) . '/show/' . $type, $vars);
		} elseif (file_exists($dir . 'form/show/' . $type . '.php')) {
			$form_group = include_file('form/show/' . $type , $vars);
		} else {
			$form_group = include_file('form/show/default', $vars);
		}	

		echo $form_group;
	}

	public static function include_list_form_group($vars)
	{
		$type = $vars['type'];

		$controller = $vars['entity'];
		$model_name = $controller->model;
		$model = new $model_name;

		$dir = SITE_DIR . 'admin/app/views/';

		if (file_exists($dir . strtolower($model->getClassName()) . '/list/' . $type . '.php')) {
			$form_group = include_file(strtolower($model->getClassName()) . '/list/' . $type, $vars);
		} elseif (file_exists($dir . 'form/list/' . $type . '.php')) {
			$form_group = include_file('form/list/' . $type , $vars);
		} else {
			$form_group = include_file('form/list/default', $vars);
		}

		return $form_group;
	}

}
