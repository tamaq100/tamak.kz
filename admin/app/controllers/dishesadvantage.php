<?php
class DishesAdvantageController extends CrudController {

	public $model = 'DishesAdvantage';

	public $list_fields = array(
		'idx'			   => 'integer',
		'id'               => 'integer',
        'title'            => 'string',
		'active'		   => 'bool',
	);
	public $edit_fields = array(
	    'id'               => 'null',
	    'active'		   => 'bool',
		'image'		   	   => 'image',
	    'title'		   	   => 'string',
	   	'subtitle'		   => 'string',
    );
}