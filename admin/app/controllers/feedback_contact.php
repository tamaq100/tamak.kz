<?php
class Feedback_contactController extends CrudController {

	public $model = 'Feedback_contact';

	public $list_fields = array(
		'id'               => 'integer',
        'name'             => 'string',
		'telephone'		=> 'null',
		'email'			=> 'null',
		// 'moderated'		   => 'bool',

	);
	public $edit_fields = array(
		'id'			=> 'null',
		'created'		=> 'null',
		'updated'		=> 'null',
		'name'			=> 'null',
		'telephone'		=> 'null',
		'email'			=> 'null',
		'message'		=> 'null',
		'moderated'		=> 'bool',
    );
}