<?php
class Popular_foodController extends CrudController {

	public $model = 'Popular_food';

	public $list_fields = array(
		'idx'			   => 'integer',
		'id'               => 'integer',
		'title'			   => 'string',
		'company'		   => 'string',
		'active'		   => 'bool',
	);
	public $edit_fields = array(
	    'id'               => 'null',
	    'active'		   => 'bool',
	    'urlparse'		   => 'string',
	    // создать View с которого буду обращаться к специфичному классу
	   // и оттуда уже подтягивать данные для этого entity 
	    // entity - это текущая сущность которую можно отправить
	    'title'		   	   => 'string',
	    'company'		   => 'string',
	    'subtitle'		   => 'string',
	    'link'			   => 'string',
	    'price'			   => 'string',
	    'image'			   => 'image',
	    'tags'			   => 'string',
	    'addition'		   => 'string',
    );
}