<?php

/**
* ConfigAboutUs Controller
*/
class ConfigAgreementController extends ConfigController {
    
    public $model = 'ConfigAgreement';

    public $redirect_uri = '/admin/configagreement';

}