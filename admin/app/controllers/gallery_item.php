<?php

/**
* Gallery_item Controller
*/
class Gallery_itemController extends CrudController {
    
    public $model = 'Gallery_item';

    public $list_fields = array(
        'idx'             => 'integer',
        'id'              => 'integer',
        'created'         => 'null',
        'gallery_id'      => 'string',
        'media'           => 'image',
    );

    public $edit_fields = array(
        'created'             => 'null',
        'media'               => 'image',
        'name'                => 'string',
        'gallery_id'          => 'select_relation',
    );


    /**
     * Перегружаем метод для показа  изображений только из нужной галереи
     */
    public function listAction($id)
    {
        if (! $id) {
            return false;
        }
        $model = $this->model;
        $items = $model::findBy(array('gallery_id' => intval($id)));
        $vars = array(
            'items' => $items,
            'model' => $this->model,
            'fields' => $this->list_fields,
        );
        return $this->render($this->index_template, $vars);
    }

    /**
     * Контроллер плуаплоуда, для работы с аяксом plupload-a
     * @return json ответы об ошибках во время загрузки
     */
    public function pluploadAction()
    {
        $gallery = new Gallery_item;

        if (!isset($_REQUEST['id'])) {
            die('There is no id');
        }
        
        $_POST['gallery_id'] = $_REQUEST['id'];
        

        // Чтобы понять что файл не из кэша, как это бывает в iOS
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");

        // ждем 5 минут
        @set_time_limit(5 * 60);

        // $targetDir место где собираются все детали (Интересно но у меня getcwd() показывает корневую папку...)
        $targetDir = getcwd() . '/../runtime/_media/plupload';
        $cleanupTargetDir = true; // Удалить старые файлы
        $maxFileAge = 5 * 3600; // Время доступности файлов в секундах


        // Ежели нет папки, то создать
        if (!file_exists($targetDir)) {
            @mkdir($targetDir);
        }

        // Выбираем имя файла
        if (isset($_REQUEST["name"])) {
            $fileName = $_REQUEST["name"];
        } elseif (!empty($_FILES)) {
            $fileName = $_FILES["file"]["name"];
        } else {
            $fileName = md5(uniqid(rand()));
        }

        // Путь к файлу
        $filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;
        
        // Если файл отправляется кусками, то принимаем параметры
        $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;


        // Удаляем стыре файлы
        if ($cleanupTargetDir) {

            if (!is_dir($targetDir) || !$dir = opendir($targetDir)) {
                // Открываем папку или умераем
                die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory ' . $targetDir . '."}, "id" : "id"}');
            }
            while (($file = readdir($dir)) !== false) {
                $tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;
                // Если временный файл текущий, переходим к следующему
                if ($tmpfilePath == "{$filePath}.part") {
                    continue;
                }
                // Удаляем временный файл если он слишком стар и не является текущим
                if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge)) {
                    @unlink($tmpfilePath); // удаляем не читая возможные ошибки
                }
            }
            closedir($dir);
        }   

        // Открываем временный файл
            // если кусковый режим вкл, то открываем или создаем с с последующей записью в конце файла.
        if (!$out = @fopen("{$filePath}.part", $chunks ? "ab" : "wb")) { 
            // Ошибки отправляются в формате jasonrpc
            die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
        }
        if (!empty($_FILES)) {
            if ($_FILES["file"]["error"] || !is_uploaded_file($_FILES["file"]["tmp_name"])) {
                // Если есть ошибки или файл не загружен
                die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
            }
            // Read binary input stream and append it to temp file
            if (!$in = @fopen($_FILES["file"]["tmp_name"], "rb")) { // Читать бинарно
                die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
            }
        } else {
            if (!$in = @fopen("php://input", "rb")) { // читать бинарно
                die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
            }
        }
        while ($buff = fread($in, 4096)) { // Читать в $buff по 4096 байт или до конца открытого файла
            fwrite($out, $buff); // Записать в файл $out
        }
        @fclose($out); // закрываем оба файла
        @fclose($in);
        // Проверяем загружен ли файл
        if (!$chunks || $chunk == $chunks - 1) {
            // Убираем врменный суффикс .part
            $file_part = "{$filePath}.part";
            rename($file_part, $filePath);
            $imageid = md5(uniqid(rand())); // Называем в стиле дименшона

            $_POST['media'] = $imageid;

            $errors = array();

            foreach ($gallery::$fields as $field => $value) {

                if (!isset($_POST[$field])) {

                    $errors[$field][] = 'Поле должно быть заполнено';
                }

                if (in_array($field, array('id', 'idx', 'active', 'created', 'updated', 'type'))) continue;

                $gallery->$field = $_POST[$field];// Сохраняем в модель с помошью модели

            }

            $gallery->active = 1;
            $gallery->type = $_REQUEST['gallery_type'];
            $gallery->save(); // Сохраняем в базу

            $exploded_file_path = explode('.', $filePath);
            $ext = end($exploded_file_path); // Получаем расширение файла
            $imagename = getcwd() . "/../runtime/_media/" . $imageid . "." . $ext; // сохраняем с новым расширением

            if(!rename($filePath, $imagename)) { // Проверяем
                die(var_dump('Не получилось переместить файл(ы) из временного хранилища'));
            }
            
            chmod($imagename, 0777);

            if (!$errors) { // Вроде ошибок быть не должно

                header('Location: /blog/');
                
            }
        }

        // Возвращаем хороший jsonrpc ответ
        die('{"jsonrpc" : "2.0", "result" : null, "id" : "id"}');
    }

    // запускается только при апдейте поста, то бишь сохраняет сортировку
    public function sort_galleryAction()
    {
        $gallery = new Gallery_item;

        $gallery->sort_items($_POST['item'], $_POST['descr']);
    }

    /**
     * Удаление картинки
     */
    public function remove_imgAction()
    {
        // Создаем экземпляр галерейки
        $img = new Gallery_item;
        $img->load($_REQUEST['id']); // Загружаем из реквеста
        unlink_media($img->media); // Удоляем фаел
        $img->delete(); // Удаляем запись
        return 'media ' . $img->media . ' removed';
    }
}