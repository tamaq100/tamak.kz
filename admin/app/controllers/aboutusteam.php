<?php
class AboutUsTeamController extends CrudController {

	public $model = 'AboutUsTeam';

	public $list_fields = array(
		'idx'              => 'integer',
        'id'               => 'integer',
        'name'             => 'string',
        'active'		   => 'bool',
	);
	public $edit_fields = array(
        'id'               => 'null',
        'active'           => 'bool',
        'name'			   => 'string',
        'position'		   => 'string',
        'image'			   => 'image',
    );
}