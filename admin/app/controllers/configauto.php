<?php

/**
* ConfigAuto Controller
*/
class ConfigAutoController extends ConfigController {
    
    public $model = 'ConfigAuto';

    public $redirect_uri = '/admin/configauto';

}