<?php
class NewsController extends CrudController {

	public $model = 'News';

	public $list_fields = array(
		'idx'			   => 'integer',
		'id'               => 'integer',
        'title'             => 'string',
		'active'		   => 'bool',
		'updated'			=> 'date',
	);
	public $edit_fields = array(
		'id'			=> 'null',
		'created'		=> 'null',
		'updated'		=> 'null',
		'active'		=> 'bool',
		'title'			=> 'string',
		'preview'		=> 'string',
		'image'			=> 'image',
		'link'			=> 'string',
		'text'			=> 'text',
    );
}