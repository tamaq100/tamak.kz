<?php

class AuthController extends BaseController {

	public function loginAction()
	{
		$vars = array(
			'loginvar' => isset($_GET['loginvar']) ? $_GET['loginvar'] : false,
		);

		$this->title = 'Вход';

		return $this->render('auth/login', $vars, '');
	}

	public function login_postAction()
	{
		$returnpath = (isset($_GET['returnpath'])?'&returnpath='.urlencode($_GET['returnpath']):'');
		if (isset($_POST['u_login']) && isset($_POST['u_pass'])) {
			$admin = 0;
			$users = Administrator::findBy(array('login' => $_POST['u_login'], 'password' => $_POST['u_pass']));
			if ($users) {
				$user = current($users);
				$admin = 1;
				$_SESSION['u_id'] = $user->id;
				$_SESSION['super'] = $user->super;
				$_SESSION['u_name'] = $user->name;
			}
			if ($admin <= 0) {
				$GLOBALS['loginvar'] = 2;
				header('Location: /admin/auth/login?loginvar=2'.$returnpath);
				exit;
			}
		} elseif (isset($_POST['u_login']) || isset($_POST['u_pass'])) {
			$GLOBALS['loginvar'] = 1;
			header('Location: /admin/auth/login?loginvar=2'.$returnpath);
			exit;
		} elseif (!isset($_SESSION['u_name'])) {
			header('Location: /admin/auth/login?loginvar=2'.$returnpath);
			exit;
		}
		header('Location: '.(isset($_GET['returnpath'])?$_GET['returnpath']:'/admin/'));
		exit;
	}

	public function logoutAction()
	{
		$_SESSION = array();
		header('Location: /admin/auth/login?loginvar=3');
		exit;
	}
}