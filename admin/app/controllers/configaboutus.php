<?php

/**
* ConfigAboutUs Controller
*/
class ConfigAboutUsController extends ConfigController {
    
    public $model = 'ConfigAboutUs';

    public $redirect_uri = '/admin/configaboutus';

}