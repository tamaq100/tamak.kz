<?php
class CityController extends CrudController {

	public $model = 'City';

	public $list_fields = array(
		'idx'			   => 'integer',
		'id'               => 'integer',
		'active'		   => 'bool',
		'value_ru'		   => 'string',
	);
	public $edit_fields = array(
		'id'			=> 'null',
		'created'		=> 'null',
		'updated'		=> 'null',
	    'active'		=> 'bool',
	    'value_ru'		=> 'string',
	    'lat'		   	=> 'integer',
	    'lng'		   	=> 'integer',
    );
}