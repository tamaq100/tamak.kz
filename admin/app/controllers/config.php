<?php

/**
 * Контроллер настроек.
 * Код переведен со старого Dimension'а
 */
class ConfigController extends CrudController {

	public $model 	 = 'Config';

	public $redirect_uri = '/admin/config';

	public function indexAction()
	{

		$entity = new $this->model;

        $values = $entity::getValues();

		$vars = array(
			'params' => $entity::$params,
			'values' => $values,
			'action' => $this->redirect_uri,
			'model'	 => strtolower($this->model),
		);

		$this->title = 'Настройки';

		return $this->render('config/index', $vars);
	}

	/**
	 * Сохранение сущности
	 */
	public function saveAction()
	{
		$this->onlyForNotReadOnly();
		$this->onlyForNotEditOnly();
		$entity = new $this->model;

		// пробежаться по массиву params
		$configs = [];

		foreach ($entity::$params as $key => $config_type) {
			if ($key == 'images') continue;
			if ($config_type == 'image'){
				if ($_FILES[$key]['name'] == '') continue;
				$value = $_FILES[$key];
			} else {
				$value = $_POST[$key];
			}
			$configs[$key] = [
				'value' => $value, // TODO: проверить если нет такого key в POST
				'type' => $config_type,
			];
		};


		// проверяем есть ли такая запись по названию
		// если нет то создаем, если да то просто обновляем
		foreach ($configs as $key => $value) {

			$entity = new $this->model;

			if ($key == 'images') continue;
			
			$result = $entity->select()
					->where(['title', $key])
					->one();
			if (! $result) {
				$result = new $this->model;
			}
			if ($value['type'] == 'image') {
				$imageLink = $result->saveImage($key);
				$result->type = 'string';
				$result->{'string'} = $imageLink;
			}
			elseif(is_array($value['value']) == 'array'){
				$restlt = serialize($value['value']);
				$result->type = $value['type'];
				$result->{$value['type']} = $restlt;
			}else{
				$result->type = $value['type'];
				$result->{$value['type']} = $value['value'];
			}

			$result->title = $key;
			$result->save();
		}

		$redirect_uri = $this->redirect_uri;
		header('Location: '.$redirect_uri);
		exit;
	}

}
