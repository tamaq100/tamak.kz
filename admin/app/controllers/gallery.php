<?php
class GalleryController extends CrudController {
	
	public $title = 'Галерея';
	public $model = 'Gallery';

	public $list_fields = array(
		'id'            => 'integer', 
		'title'         => 'string',
	);

	public $edit_fields = array(
		'id'              => 'false', 
		'created'         => 'false', 
		'updated'         => 'false', 
		'active'          => 'bool', 
		'title'           => 'string', 
		'media'           => 'image', 
	);

	/**
	 * Обновление
	 */
	 public function updateAction($id)
	 {

	 	$this->onlyForNotReadOnly();

	 	$entity = new $this->model;

	 	if (! $entity->load($id)) {
	 		return $this->render(__($this->model.'.404'));
	 	}

	 	foreach ($this->edit_fields as $key => $type) {

	 		if ($type == 'image') {
	 			$entity->setFieldWithTypeImage($key, $_FILES[$key]);
	 		} elseif (isset($_POST[$key])) {
	 			$entity->$key = $_POST[$key];
	 		}

	 	}


	 	$entity->save();

	 	if (isset($_GET['and_create']) && $_GET['and_create']) {
	 		$redirect_uri = $this->getRedirectUrlToCreate($entity);
	 	} else {
	 		$redirect_uri = $this->getRedirectUrlToShow($entity);
	 	}
	 	header('Location: '.$redirect_uri);
	 	exit;
	 }

	 /**
	  * Метод для сохранения обрезанного изображения
	  */
	 public function saveCroppedImage($field, $id = false)
	 {

	 	$entity = new $this->model;

	     if (!isset($_FILES[$field])) return false;

	     $name       = $id && is_array($_FILES[$field]['name'])     ? $_FILES[$field]['name'][$id]     : $_FILES[$field]['name'];
	     $tmp_name   = $id && is_array($_FILES[$field]['tmp_name']) ? $_FILES[$field]['tmp_name'][$id] : $_FILES[$field]['tmp_name'];
	     $type       = $id && is_array($_FILES[$field]['type'])     ? $_FILES[$field]['type'][$id]     : $_FILES[$field]['type'];
	     $size       = $id && is_array($_FILES[$field]['size'])     ? $_FILES[$field]['size'][$id]     : $_FILES[$field]['size'];

	     if (is_uploaded_file($tmp_name) AND $size AND
	         substr($type, 0, 6) == "image/" AND (strstr($type, "jpeg") OR
	          strstr($type, "gif") OR strstr($type, "png"))) 
	    	{
	         $imageid = md5(uniqid(rand()));
	         $image_field = $field;
	        
	        
	         if (isset($entity->$image_field) && $entity->$image_field){
	             Functions::unlink_media($entity->$image_field);
	         }
	       
	         $imagename = "../runtime/_media/" . $imageid . ".".strtolower(substr($type, -3));
	         move_uploaded_file($tmp_name, $imagename); 
	         chmod($imagename, 0777);
	         return $imageid;
	     }

	     return false;
	 }
}
