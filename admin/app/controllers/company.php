<?php
class CompanyController extends CrudController {

	public $model = 'Company';

	public $list_fields = array(
		'idx'              => 'integer',
		'id'               => 'integer',
        'name'            => 'string',
        'active'		   => 'bool',
	);
	public $edit_fields = array(
        'id'               => 'null',
        'active'           => 'bool',
        'name'             => 'string',
    );
}