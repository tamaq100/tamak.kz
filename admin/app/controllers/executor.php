<?php
class ExecutorController extends CrudController {

	public $model = 'Executor';
	public $read_only      = true;
	public $uncreatable    = true;

	public $list_fields = array(
		'id'               => 'integer',
        'name'             => 'string',
		'is_online'		   => 'bool',
		'rating'			   => 'integer',
	);
	public $edit_fields = array(
		'id'			=> 'null',
		'created'		=> 'null',
		'updated'		=> 'null',
	    'active'		=> 'bool',
	    'avatar'		=> 'image',
	    'name'		   	=> 'string',
	    // 'stars'		   	=> 'integer',
	    // 'status'		=> 'integer',
	    // 'last_coord'	=> 'string',
    );
}