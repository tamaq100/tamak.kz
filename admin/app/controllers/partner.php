<?php
class PartnerController extends CrudController {

	public $model = 'Partner';

	public $list_fields = array(
		'idx'			   => 'integer',
		'id'               => 'integer',
        'title'            => 'string',
		'active'		   => 'bool',
	);
	public $edit_fields = array(
	    'id'               => 'null',
	    'active'		   => 'bool',
		'image'		   	   => 'image',
	    'title'		   	   => 'string',
    );
}