<?php
class PromoController extends CrudController {

	public $model = 'Promo';

	public $list_fields = array(
		'idx'			   => 'integer',
		'id'               => 'integer',
		'title'			   => 'string',
		'active'		   => 'bool',
	);
	public $edit_fields = array(
	    'id'               	=> 'null',
	    'created'		   	=> 'null',
	    'updated'		   	=> 'null',
	    'active'		   	=> 'bool',
	    'title'		   	   	=> 'string',
	    'subtitle'		   	=> 'string',
	    'preview_image'		=> 'image',
	    'image'		   	   	=> 'image',
	    'link'		   	   	=> 'string',
	    'conditions'	   	=> 'text',
	    'company_link'	   	=> 'string',
	    'company'	   	   	=> 'string',
	    'price'		   	   	=> 'string',
	    'more'	   		   	=> 'bool',
    );
}