<?php
class AutoAdvantagesController extends CrudController {

	public $model = 'AutoAdvantages';

	public $list_fields = array(
		'idx'              => 'integer',
        'id'               => 'integer',
        'title'            => 'string',
        'active'		   => 'bool',
	);
	public $edit_fields = array(
        'id'               => 'null',
        'active'           => 'bool',
        'image'            => 'image',
        'title'			   => 'string',
        'subtitle'		   => 'string',
    );
}