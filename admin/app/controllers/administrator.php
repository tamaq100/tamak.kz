<?php

/**
* Administrator Controller
*/
class AdministratorController extends CrudController {
	public $model = 'Administrator';

    public $list_fields = array(
        'id'              => 'integer',
        'name'            => 'string',
    );

    public $edit_fields = array(
        'id'              => 'null',
        'active'          => 'bool',
        'created'         => 'null',
        'updated'         => 'null',
        'name'            => 'string',
        'login'           => 'string',
        'password'        => 'hash',
        // 'access_rights'   => 'access_rights',
    );
}
