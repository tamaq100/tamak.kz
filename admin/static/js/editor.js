$(document).ready(function() {
    initEditors();
});

function initEditors() {
    CKEDITOR_BASEPATH = '/vendor/ckeditor/';
    if ($('.editorarea').length) {
        $.getScript(CKEDITOR_BASEPATH + 'ckeditor.js', function() {
        $.getScript(CKEDITOR_BASEPATH + 'adapters/jquery.js', function() {
            $('.editorarea').each(function(i, item) {
                var toolbar_name = 'Full';
                var roxyFileman = '/admin/fileman/index.html';

                $(item).ckeditor({
                    filebrowserBrowseUrl:roxyFileman,

                    filebrowserImageBrowseUrl:roxyFileman+'?type=image', 
                  
                    removeDialogTabs: 'link:upload;image:Upload',
                    
                });
            });
        });
        });
    }
}
