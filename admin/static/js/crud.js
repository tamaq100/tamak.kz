var delete_form_submit = function(form) {
	if (confirm('Вы действительно хотите удалить запись?')) {
		$(form).submit();
	}
};

var crudtable

function callChangeCrudStages(url, status) {
  var data = $('.editable_field').serialize()

  $.ajax({
    type: 'POST',
    url: url,
    data: data + '&status=' + status,
    dataType: 'json',
    success: function(response) {
      // reload crud table
      if (response.status === 200) {
        console.log('great!')
        // crudtable.clearPipeline()
        // crudtable.ajax.reload()
        location.reload()
      } else {
        console.log('bad!')
      }
    }
  });
}

$(function() {

	// Register an API method that will empty the pipelined data, forcing an Ajax
	// fetch on the next draw (i.e. `table.clearPipeline().draw()`)
	$.fn.dataTable.Api.register( 'clearPipeline()', function () {
	    return this.iterator( 'table', function ( settings ) {
	        settings.clearCache = true;
	    } );
	} );
	 
	var params = $('#jsParams').text();

	if (params) {
		params = JSON.parse(params);
		params["ajax"] = {
			url: params['ajax_url'],
			type: 'POST',
            pages: 5 // number of pages to cache
        };
	}

 	$('.data-table-filter').on( 'keyup change', function () {
      initCrudTableSearch()
    });

  crudtable = $("#crudtable").show().DataTable(params);
  // ЛЯТЬ надо блен пол админки перехуярить
  crudtable.on( 'row-reorder', function ( e, diff, edit ) {
        var result = 'Reorder started on row: '+ edit.triggerRow.data()['DT_RowId']+'\n\r';
 
        for ( var i=0, ien=diff.length ; i<ien ; i++ ) {
            var rowData = crudtable.row( diff[i].node ).data();

        var offset = crudtable.page() * crudtable.page.info().length;

        // $($(diff[i].node).find('td')[0]).html(diff[i].newPosition + 1);
        console.log(offset);

        $.post(params.draggable, {
          id: rowData['id'],
          newPosition: offset + diff[i].newPosition + 1,
        });

            result += rowData['DT_RowId']+' updated to be in position '+
                diff[i].newPosition+' (was '+diff[i].oldPosition+')\n\r';
        }
    } );

	$('.datetime').datetimepicker();
	$('.date').datetimepicker({
		format: 'DD-MM-YYYY',
	});
	$('.date-mini').datetimepicker({
		format: 'DD-MM',
	});

	$('.crud-form input, .crud-form select, .crud-form textarea').first().focus();
    $('#reset-table-filter').unbind();
    $('#reset-table-filter').click(function (e) {
      $('#table .search-row input').val('');
      crudtable.search('').draw(false);

    });
	$('.clean-btn').click(function(e) {
		var input = $(this).parent().parent().find('input');
		input.val('');
		initCrudTableSearch();
	});
});



function initCrudTableSearch() {
  data = {};
  $('.data-table-filter').each(function() {
    data[$(this).attr('name')] = $(this).val();
    data_json = JSON.stringify(data);
  });
  crudtable.search( data_json ).draw(false);
}