$(function() {

	var uploadComplete = 0;

	function getImageConfig(field) {
		return {
			runtimes : "html5,flash,silverlight,html4",
			browse_button : "image_pickfiles" + field, // you can pass an id...
			container: document.getElementById("image_container" + field), // ... or DOM Element itself
			url : "/admin/gallery_item/plupload",
			chunk_size: "200kb",
			width: 1200,
			height: 1200,

			filters : {
				max_file_size : "100mb",
				mime_types: [
					{title : "Image files", extensions : "jpg,jpeg,gif,png"},
				]
			},

			init: {
				PostInit: function() {
					document.getElementById("img-uploader" + field).innerHTML = "";
					uploadComplete++;
				},

				FilesAdded: function(up, files) {
					plupload.each(files, function(file) {
						document.getElementById("img-uploader" + field).innerHTML += "<div id=\"" + file.id + "\">" + file.name + " (" + plupload.formatSize(file.size) + ") <b></b></div>";
					});
				},

				UploadProgress: function(up, file) {
					document.getElementById(file.id)
						.getElementsByTagName("b")[0]
						.innerHTML = "<span>" + file.percent + "%</span>";
				},

				Error: function(up, err) {
					//document.getElementById("image_console").appendChild(document.createTextNode("\nError #" + err.code + ": " + err.message));
				},
				UploadComplete: function(e) {

					uploadComplete--;

					if (! uploadComplete) {
						var form = window['imageUploader' + field].getOption('form');

						form.submit();
					}
				}
			}
		};
	}

	var multiple_fileConfig = {
		runtimes : "html5,flash,silverlight,html4",
		browse_button : "multiplefiles_pickfiles", // you can pass an id...
		container: document.getElementById("image_container"), // ... or DOM Element itself
		url : "/admin/file/plupload",
		chunk_size: "200kb",
		width: 1200,
		height: 1200,

		filters : {
			max_file_size : "100mb",
		},

		init: {
			PostInit: function() {
				document.getElementById("img-uploader").innerHTML = "";
				uploadComplete++;
			},

			FilesAdded: function(up, files) {
				plupload.each(files, function(file) {
					document.getElementById("img-uploader").innerHTML += "<div id=\"" + file.id + "\">" + file.name + " (" + plupload.formatSize(file.size) + ") <b></b></div>";
				});
			},

			UploadProgress: function(up, file) {
				document.getElementById(file.id)
					.getElementsByTagName("b")[0]
					.innerHTML = "<spa	>" + file.percent + "%</span>";
			},

			Error: function(up, err) {
				//document.getElementById("image_console").appendChild(document.createTextNode("\nError #" + err.code + ": " + err.message));
			},
			UploadComplete: function(e) {

				uploadComplete--;

				if (! uploadComplete) {
					var form = multiple_fileUploader.getOption('form');
					form.submit();
				}
			}
		}
	}

	$('.pluploadFormImageGallery').each(function() {
		var field = $(this).data('field');

		window['imageUploader' + field] = new plupload.Uploader(getImageConfig(field));
		window['imageUploader' + field].init();


		window['ul_sortable' + field] = $('#sortableGallery' + field);
		window['ul_sortable' + field].sortable({
			revert: 100,
			placeholder: 'placeholder'
		});

		window['ul_sortable' + field].disableSelection();
	})

	var multiple_fileUploader = new plupload.Uploader(multiple_fileConfig);
	multiple_fileUploader.init();

	var ul_sortable = $('#sortableGallery');

	ul_sortable.sortable({
		revert: 100,
		placeholder: 'placeholder'
	});

	ul_sortable.disableSelection();


	/**
	 * Plupload перехватывает момент сохранения любого материала, 
	 * и проверет имеется ли галерея если нет тосабмитит форму, 
	 * иначе  перед сабмитомп роводит сохранение галерейки
	 */
	$('#saveEntityBtn, #save_n_continue, #save_n_create').click(function() {
		var form = $(this).closest('form'),
			parent_id = $(this).data('entity-id'),
			gallery_type = $(this).data('entity-controller');


		if ($(this).attr('id') == 'save_n_create') {

			var action = form.attr('action'),
			new_action = action + '?and_create=true';

			form.attr('action', new_action);

		} else  if($(this).attr('id') == 'save_n_continue'){

			var action = form.attr('action'),
			new_action = action + '?and_continue=true';

			form.attr('action', new_action);

		}

			if (form.find('.pluploadFormImageGallery').length || form.find('#pluploadmultiplefilesForm').length) {
				if (form.find('.pluploadFormImageGallery').length) {
					$('.pluploadFormImageGallery').each(function() {
						var field = $(this).data('field');

						var sortable_data = window['ul_sortable' + field].sortable('serialize'),
							descriptions = form.serialize();

						$.ajax({
							data: sortable_data + '&' + descriptions,
							type: 'POST',
							url: '/admin/gallery_item/sort_gallery',
						});

						window['imageUploader' + field].setOption('url', '/admin/gallery_item/plupload/' + parent_id + '?gallery_type=' + gallery_type + field);
						window['imageUploader' + field].setOption('form', form);
						window['imageUploader' + field].start();
					});
				}
				if (form.find('#pluploadmultiplefilesForm').length) {
					var sortable_data = ul_sortable.sortable('serialize'),
						descriptions = form.serialize();

					$.ajax({
						data: sortable_data + '&' + descriptions,
						type: 'POST',
						url: '/admin/file/sort_files',
					});

					multiple_fileUploader.setOption('url', '/admin/file/plupload/' + parent_id + '?entity=' + $('#pluploadmultiplefilesForm').attr('data-entity'));
					multiple_fileUploader.setOption('form', form);
					multiple_fileUploader.start();
				}
			} else {
			form.submit();
		}
	})

	$('.file-del').click(function (e) {
		e.preventDefault();
		id = $(this).attr('data-id');
		if (confirm('Вы уверены что хотите удалить картинку - ' + id)) {
			$.ajax({
				type: "POST",
				url: '/admin/lessons_files/remove_file/' + id,
				success: function (response) {
					$('#item-' + id).remove()
				}
			})
		};
	})

	$('.multiple-file-del').click(function (e) {
		e.preventDefault();
		id = $(this).data('id');
		if (confirm('Вы уверены что хотите удалить картинку - ' + id)) {
			$.ajax({
				type: "POST",
				url: '/admin/file/remove_file/' + id,
				success: function (response) {
					$('#item-' + id).remove();
				}
			})
		};
	})

	/**
	 * Удаление картинки
	 */
	$('.image-del').click(function (e) {
		e.preventDefault();
		id = $(this).attr('data-id');
		if (confirm('Вы уверены что хотите удалить картинку - ' + id)) {
			$.ajax({
				type: "POST",
				url: '/admin/gallery_item/remove_img/' + id,
				success: function (response) {
					$('#item-' + id).remove()
				}
			})
		};
	})

	
	$('.clone_option_wrapper').click(function() {
		var tmpl = $('.template').find('.option_select_wrapper'),
			newHTML = tmpl.clone()
			// id1 = uniqid(),
			// id2 = uniqid()
			;

		// console.log(newHTML.html(),
		//             $("#option_values", newHTML),
		// $("#option_types", newHTML));
		// $("#option_values", newHTML).attr('id', id1);
		// $("#option_types", newHTML).attr('id', id2);

		newHTML.appendTo(tmpl.parent().parent());
		// $('#' + id1).chained('#' + id2);
	});

	$('.clone_schedule_wrapper').click(function() {
		var tmpl = $('.template').find('.schedule_select_wrapper'),
			newHTML = tmpl.clone()
			// id1 = uniqid(),
			// id2 = uniqid()
			;

		// console.log(newHTML.html(),
		//             $("#option_values", newHTML),
		// $("#option_types", newHTML));
		// $("#option_values", newHTML).attr('id', id1);
		// $("#option_types", newHTML).attr('id', id2);

		newHTML.appendTo(tmpl.parent().parent());
		$('.datetime').datetimepicker();
		// $('#' + id1).chained('#' + id2);
	});		
});