function uniqid(e,t){if(typeof e==="undefined"){e=""}var n;var r=function(e,t){e=parseInt(e,10).toString(16);if(t<e.length){return e.slice(e.length-t)}if(t>e.length){return Array(1+(t-e.length)).join("0")+e}return e};if(!this.php_js){this.php_js={}}if(!this.php_js.uniqidSeed){this.php_js.uniqidSeed=Math.floor(Math.random()*123456789)}this.php_js.uniqidSeed++;n=e;n+=r(parseInt((new Date).getTime()/1e3,10),8);n+=r(this.php_js.uniqidSeed,5);if(t){n+=(Math.random()*10).toFixed(8).toString()}return n}

$(function() {
	$('.add-dinamic-block').click(function() {
		var id = uniqid();
		html = $(this).closest('.col-sm-10').find('.dinamic-block.hidden').clone();
		html = html.removeClass('hidden');
		html = html.get(0).outerHTML;
		html = html.replace(/original-name/g,  'name');
		html = html.replace(/uniqid/g,  id);
		html = html.replace(/editorarea-clone/g,  'editorarea');

		$(this).closest('.col-sm-10').find('.dinamic-blocks-wrapper').append(html);
		delete_dinamic_block();
		delete_dinamic_product();
		// show_characteristics_values();
	    initEditors();
	})
	delete_dinamic_block();
	delete_dinamic_product();
});
function delete_dinamic_block() {
	$('.delete-this-by-ajax').click(function() {
		$del_btn = $(this);
		$.ajax({
			url: '/admin/project/remove_direction/' + $del_btn.data('id'),
			method: 'GET',
			success: function(data) {
				instance = $del_btn.closest('.dinamic-block').find('.editorarea').attr('name');
				CKEDITOR.instances[instance].destroy();
				$del_btn.closest('.dinamic-block').remove();
			}
		});		
	});
	$('.delete-this').click(function () {
		$del_btn = $(this);
		instance = $del_btn.closest('.dinamic-block').find('.editorarea').attr('name');
		if (instance) {
			CKEDITOR.instances[instance].destroy();
		}
		$del_btn.closest('.dinamic-block').remove();
	})
}
function delete_dinamic_product() {
	$('.delete-product-dinamic').click(function() {
		$del_btn = $(this);
		$.ajax({
			url: '/admin/product_category/remove_direction/' + $del_btn.data('id'),
			method: 'GET',
			success: function(data) {
				$del_btn.closest('.dinamic-block').remove();
			}
		});		
	});
	$('.delete-this').click(function () {
		$del_btn = $(this);
		instance = $del_btn.closest('.dinamic-block').find('.editorarea').attr('name');
		$del_btn.closest('.dinamic-block').remove();
	})
}