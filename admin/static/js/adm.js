function uniqid(e,t){
	if(typeof e === "undefined"){
		e="";
	};
	function r (e,t){
		var eNew = e;
		eNew=parseInt(eNew,10).toString(16);
		if(t<eNew.length){
			return eNew.slice(eNew.length-t);
		}
		if(t>eNew.length){
			return Array(1+(t-eNew.length)).join("0")+eNew;
		}
		var n;
		var retN = e;
		return retN;
	};
	if(!this.php_js){
		this.php_js={};
	};
	if(!this.php_js.uniqidSeed){
		this.php_js.uniqidSeed=Math.floor(Math.random()*123456789);
	};
	this.php_js.uniqidSeed++;
	n=e;
	n+=r(parseInt((new Date).getTime()/1e3,10),8);
	n+=r(this.php_js.uniqidSeed,5);
	if(t){
		n+=(Math.random()*10).toFixed(8).toString();
	};
	return n;
	};
// "use strict";
// function material_observer(block) {
// 	$('.material-remover', block).click(function() {
// 		console.log(block);
// 		if (confirm('Вы уверены что хотите удалить эту запись?')) {
// 			$(this).closest('.row-file-template').remove();

// 			var entity_id = $(this).attr('id');

// 			if (isInt(entity_id)) {
// 				$.ajax({
// 					url: '/admin/journal/ajax_remove/' + $(this).attr('id'),
// 				});
// 			}
// 		}
// 	});

// 	$('.add-additional', block).click(function() {
// 		var id = uniqid();
// 		html = $('.row-file-template.hidden', block).clone();
// 		html = html.removeClass('hidden');
// 		html = html.get(0).outerHTML;
// 		html = html.replace(/new_id/g,  id);
// 		html = html.replace(/replaced-name/g,  'name');

// 		$(html).appendTo('.materials-list');
// 		material_observer($('#'+id));	
// 	});

// }

// function save_n_create_btn() {
// 	$('#save_n_create').click(function() {
// 		var 
// 			form = $(this).closest('form'),
// 			action = form.attr('action'),
// 			new_action = action + '?and_create=true'
// 			;
// 		form.attr('action', new_action);
// 		form.submit();
// 	});
// }

$(function() {
	rating_ajax_form();
	delete_attachment();
	rating_data_show_more();
	setUploader();
	select2_events();
	ajaxImports();
	// material_observer($('.materials-list'));
	toggleTree();
	dateFilter();
	// save_n_create_btn();
	$('.level-1 li').click(function () {
		$(this).find('.level-2').stop().slideToggle();
	})

	importXls();
});

function toggleTree() {
	$('.tree-icon').click(function() {
		var childrens = $(this).closest('.with_sub_menu').data('children');
		$('.parent-id-' + childrens).toggle(50);
		$(this).closest('.with_sub_menu').toggleClass('hide-childrens');
	});
}

function select2_events() {
	$("#main-select2").each(function() {
		controller = $(this).data('request-controller');
		$(this).select2({
			ajax: {
				url: "/admin/" + controller + "/get",
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						q: params.term, // search term
						page: params.page
					};
				},
				processResults: function (data, params) {
					return {
						results: data
					};
				},
				cache: true
			},
		});
	})
}

function dateFilter(){
	//Date range picker for index
    $('input.daterange-filter').each(function() {
    	var 
    		start = $(this).data('start'),
    		end   = $(this).data('end');

    	start = moment(start, 'DD-MM-YYYY HH:mm');
		end   = moment(end, 'DD-MM-YYYY HH:mm');

		$(this).click(function(e) {
			if (!$(this).hasClass('activated')) {
				$(this).addClass('activated')
			    $(this).daterangepicker({
					timeZone: 'Asia/Almaty',
			    	// startDate: start,
			    	// endDate: end,
			    	timePicker: true,
			    	timePicker24Hour: true,
		        	timePickerIncrement: 30,
			        locale: {
			            "format": 'DD-MM-YYYY HH:mm',
				        "applyLabel": "Принять",
				        "cancelLabel": "Отмена",
				        "fromLabel": "С",
				        "toLabel": "До",
				        "customRangeLabel": "Другой",
				        "weekLabel": "Н",
				        "daysOfWeek": [
				            "Вс",
				            "Пн",
				            "Вт",
				            "Ср",
				            "Чт",
				            "Пт",
				            "Сб"
				        ],
				        "monthNames": [
				            "Январь",
				            "Февраль",
				            "Март",
				            "Апрель",
				            "Май",
				            "Июнь",
				            "Июль",
				            "Август",
				            "Сентябрь",
				            "Октябрь",
				            "Ноябрь",
				            "Декабрь"
				        ],
				        "firstDay": 1
			        }
			    });
			}
		})
    });


}
function isInt(n){
    return n % 1 === 0;
}

function ajaxImports() {
	$('#import-btn-users').click(function() {
		var form = $('#log');
		form.html('Данные выгружаются, пожалуйста подождите...');
		$('.import-btn').prop("disabled",true);
		$.ajax({
			url: '/import/users?code=3NgP8t4JsEHP9aCr',
			method: 'GET',
			success: function(data) {
				$('.import-btn').prop("disabled",false);
				form.html('Выгрузка пользователей завершена!');
			}
		});
	});	

	$('#import-btn-history').click(function() {
		var form = $('#log');
		form.html('Данные выгружаются, пожалуйста подождите...');
		$('.import-btn').prop("disabled",true);
		$.ajax({
			url: '/import/users_history?code=3NgP8t4JsEHP9aCr',
			method: 'GET',
			success: function(data) {
				$('.import-btn').prop("disabled",false);
				form.html('Выгрузка кадровой истории сотрудников завершена!');
			}
		});
	});	

	$('#import-btn-departaments').click(function() {
		var form = $('#log');
		form.html('Данные выгружаются, пожалуйста подождите...');
		$('.import-btn').prop("disabled",true);
		$.ajax({
			url: '/import/departaments?code=3NgP8t4JsEHP9aCr',
			method: 'GET',
			success: function(data) {
				$('.import-btn').prop("disabled",false);
				form.html('Выгрузка отделов завершена!');
			}
		});
	});	
}

function rating_data_show_more() {
	$('.show-more-data').click( function() {
		var id = $(this).attr('id');
		$('.more-data-' + id).toggle(100);
	});
}

function rating_ajax_form() {
	$('.rating-ajax-form').submit( function(e) {
		e.preventDefault();
		var form = $(this);
		$.ajax({
			url: form.attr('action'),
			method: form.attr('method'),
			data: form.serialize(),
			dataType: 'json',
			success: function (data) {
				$('.participants').html(data.html);
				rating_data_show_more();
			}
		});
	});
}

function toggleTree() {
	$('.tree-icon').click(function() {
		var childrens = $(this).closest('.with_sub_menu').data('children');
		$('.parent-id-' + childrens).toggle(50);
		$(this).closest('.with_sub_menu').toggleClass('hide-childrens');
	});
}
function setUploader(){
	var 
		type_id = 0,
		model = 'sale',
		document_type = '';
	$('.attachimg').unbind();
	$('.attachimg').click(function(event) {

		console.log('attaching');

		type_id = $(this).data('id');
		model = $(this).data('model');
		if (typeof $('.attachfile-type').val() != 'undefined') {
			document_type = '-' + $('.attachfile-type').val();
		}
		model = model.toLowerCase();
		var item_class = '.attachfile-'+type_id;

		$(item_class).unbind();
		$(item_class).change(function(){
			console.log('attaching changed');
			ajaxAttachUpload(item_class, type_id, model, document_type);
		});
		console.log('attachimg click event: item = ' + item_class)
		console.log('attachimg click event: data = ' + type_id)
		$(item_class).click();
	});
}
function delete_attachment() {
	$(".file-remover").click(function() {
		var button = $(this);
		if (confirm("Вы действительно хотите удалить изображение?")) {
			var data = {'field':button.data('field')};
			$.ajax({
				url: button.data('action'),
				data: data,
				method: 'post',
				success: function() {
					$('#'+button.data('id')).remove();
					button.remove();
				}
			});
		}
	})
}

function importXls() {
	$('.import-xls-submit .submit').click(function (e) {
    	e.preventDefault();
    	var form = $('.import-xls-submit');
    	$('button', form).each(function() {
	        $(this).text('Отправляется');
	        $(this).attr('type', 'button');
	        $(this).addClass('disabled');
	    });

	    form.find('.preloader').show(200);

	    var formData = new FormData();
	    $fileInputElement = form.find('input[type=file]')[0];

	    formData.append('xlsFile', $fileInputElement.files[0]);

	    $btn = $(this);

	    $.ajax({
	        url: $btn.data('url'),
	        method: "POST",
	        data:  formData,
	        contentType: false,
	        cache: false,
	        processData: false,
	        dataType: 'json',
	        success: function(data) {
	        	console.log(data);
	            if (data.html) {
	            	form.find('.table-wrapper').html(data.html);
	            } else if(data.error) {
	            	form.find('.place-for-error').html(data.error);
	            }
	            $('button', form).each(function() {
	                $(this).text($(this).data('old-title'));
	                $(this).attr('type', $(this).data('old-type'));
	                $(this).removeClass('disabled');

	            });
	            form.find('input[type=file]').val('');
		        form.find('.preloader').hide(200);
	        }
	    });
	});
}
function initAllChained() {
	$('select.chained').each(function (e) {
		chainedId = e.attr('chained-id');
		$('#chained-city-' + chainedId).chained('#chained-country-' + chainedId);
	})
}