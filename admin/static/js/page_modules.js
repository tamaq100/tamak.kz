$(function(){
	$('#addModule').click(function(e) {
		var controller = $(this).data('controller');
		e.preventDefault();
		value = $('#page_modules option:selected').val();
		var data = {
			'module_type': value,
		};
		entity_id = $('#page_modules').data('entity-id');
		$.ajax({
			url: '/admin/' + controller + '/addmodule/' + entity_id,
			method: 'POST',
			data: data,
			dataType: 'json',
			success: function(data) {
				if (data.redirect) {
					window.location.href = data.redirect;
				} else {
					form.html(data.html);
				}
			}
		});
	});
});