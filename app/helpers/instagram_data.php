<?php
/**
* Instagram Api model
*/
use InstagramScraper\Instagram;
class Instagram_data
{
	public $log = array();
	
	public static function get_data($accaunt, $count)
	{
		try {
			$cache = SITE_DIR . 'runtime/_cache/instacache.dat';
			if (!file_exists($cache) || filemtime($cache)<strtotime("-6 hours")){
				if ($json = file_get_contents('https://ins.4d.click/get.php?username='.$accaunt)){
					if ($json = json_decode($json)){
						$out = array();
						$i = 0;
						foreach($json as $v){
							if ($i>$count) break;
							$out[] = $v;
							$i++;
						}
						file_put_contents($cache, json_encode($out));
					}
				}
			}
			$media = json_decode(@file_get_contents($cache));
		} catch (Exception $e) {
			Mail::send('eduard@4dclick.kz', 'Error in instagram', $e);
		}

		return $media;
	}

	public static function get_data_from_accaunt($count = 12)
	{
		$return = '';
        // Аккаунт в Инстаграме посты которого, нужно вывести
        if ($accaunt = Config::get('link_instagram')) {

            preg_match('/^(.{3,5}\/\/)?(.+\.[a-z]{2,5}\/)?([^\/]+)?(.+)?$/', $accaunt, $m);
            $accaunt = $m[3];
            // Количество выводимых постов
        	$return = self::get_data($accaunt, $count);
        }

        return $return;

	}

}
