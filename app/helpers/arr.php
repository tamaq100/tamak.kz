<?php

class Arr
{
    public static function avg($arr)
    {
        if (! $arr) return 0;

        return array_sum($arr)/count($arr);
    }

    public static function dot($array, $prepend = '')
    {
        $results = array();

        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $results = array_merge($results, self::dot($value, $prepend.$key.'.'));
            } else {
                $results[$prepend.$key] = $value;
            }
        }

        return $results;
    }

    public static function set(&$array, $key, $value)
    {
        if (is_null($key)) return $array = $value;
        $keys = explode('.', $key);
        while (count($keys) > 1)
        {
            $key = array_shift($keys);
            // If the key doesn't exist at this depth, we will just create an empty array
            // to hold the next value, allowing us to create the arrays to hold final
            // values at the correct depth. Then we'll keep digging into the array.
            if ( ! isset($array[$key]) || ! is_array($array[$key]))
            {
                $array[$key] = array();
            }
            $array =& $array[$key];
        }
        $array[array_shift($keys)] = $value;
        return $array;
    }

    public static function array_map_recursive($callback, $array)
    {
        $func = function ($item) use (&$func, &$callback) {
            return is_array($item) ? array_map($func, $item) : call_user_func($callback, $item);
        };

        return array_map($func, $array);
    }

    public static function echo_arr($arr, $level = 0)
    {
        $current_level = $level;
        $result = 'array('."\n\r";
        foreach ($arr as $key => $value) {
            for ($i=-1; $i < $current_level; $i++) {
                $result .= "\t";
            }
            if (is_array($value)) {
                $value = Arr::echo_arr($value, $level + 1);
                $result .= "'$key' => $value\n\r";
            } else {
                $result .= "'$key' => '$value',\n\r";
            }

        }
        for ($i=-1; $i < $current_level - 1; $i++) {
            $result .= "\t";
        }
        $result .= '),';

        return $result;
    }

}
