<?php

function unlink_media($id)
{
    $medianame = '';
    $extensions = [
        'jpg',
        'png',
        'gif',
        'jpeg',
    ];

    // Добавляем в массив расширений, расширения аперкейсом
    $extensions = array_merge($extensions,  array_map('strtoupper', $extensions));

    foreach ($extensions as $ext) {
        $ext = '.'.$ext;
        if (file_exists(SITE_DIR . 'runtime/_media/' . $id . $ext)) {
            $medianame = $id.$ext;
        }
    }

    if ($medianame) {
        unlink(SITE_DIR . "runtime/_media/" . $medianame);
        $files = scandir_recursive(SITE_DIR . "runtime/_cache/", $medianame);
        foreach ($files as $file) {
            unlink($file);
            unlink_empty_cache_folders($file);
        }
    }
}
// void func, return actual url
function clearGetParamURL($getParam, $url)
{
    $result = actualLinkNonFiltered();

    $resultQueries = parse_url($url);
    parse_str($resultQueries['query'], $params);
    unset($params[$getParam]);

    $resultQueries['query'] = $params;
    return build_url($resultQueries);
}
function getInsertGetParamToURL($getParam, $key, $url)
{
    $result = actualLinkNonFiltered();

    $resultQueries = parse_url($url);
    parse_str($resultQueries['query'], $params);
    $params[$getParam] = $key;

    $resultQueries['query'] = $params;
    return build_url($resultQueries);
}
function build_url(array $elements)
{
    $e = $elements;
    return
        (isset($e['host']) ? (
            (isset($e['scheme']) ? "$e[scheme]://" : '//') .
            (isset($e['user']) ? $e['user'] . (isset($e['pass']) ? ":$e[pass]" : '') . '@' : '') .
            $e['host'] .
            (isset($e['port']) ? ":$e[port]" : '')
        ) : '') .
        (isset($e['path']) ? $e['path'] : '/') .
        (isset($e['query']) ? '?' . (is_array($e['query']) ? http_build_query($e['query'], '', '&') : $e['query']) : '') .
        (isset($e['fragment']) ? "#$e[fragment]" : '')
    ;
}
function actualLinkFiltered()
{ 
    $actual_link = strtok((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]", '?');
    return $actual_link;
}
function actualLinkNonFiltered()
{ 
    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    return $actual_link;
}
function declOfNum($number, $titles)
{
    $cases = array (2, 0, 1, 1, 1, 2);
    return $number." ".$titles[ ($number%100>4 && $number%100<20)? 2 : $cases[min($number%10, 5)] ];
}

function declOfNum2($number, $titles)
{
    $cases = array (2, 0, 1, 1, 1, 2);
    return $titles[ ($number%100>4 && $number%100<20)? 2 : $cases[min($number%10, 5)] ];
}

function scandir_recursive($dir, $needle, $results = [])
{
    $files = scandir($dir);

    foreach($files as $value){
        $path = realpath($dir.DIRECTORY_SEPARATOR.$value);
        if(!is_dir($path)) {
            if ($value == $needle) {
                $results[] = $path;
            }
        } else if($value != "." && $value != "..") {
            $results = scandir_recursive($path, $needle ,$results);
        }
    }

    return $results;
}

function unlink_empty_cache_folders($file)
{
    $folders = explode('/',$file);
    array_pop($folders);
    while ($folder = array_pop($folders)) {
        if ($folder == '_cache') return;;
        $current_folder = implode('/', $folders) . '/' . $folder;
        if (! glob($current_folder . '/*')) {
            rmdir($current_folder);
        }
    }
}

function getmicrotime()
{
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}

/**
 * Рендерим шаблон
 * @param  string $src  путь к шаблону внутри папки в вьюшками
 * @param  array  $vars переменные отправяемые в шаблон
 * @return html         получаем отрисованную html-ку
 */
function include_file($src, $vars = array())
{
    foreach ((array)$vars as $name => $var) {
        $$name = $var;
    }
    ob_start();
    $file = SITE_DIR . ADMIN_FOLDER . 'app/views/'.$src.'.php';
    if (file_exists($file)) {
        require $file;
    }
    $result = ob_get_clean();

    return $result;
}

function abort($code)
{
    if ($code == '404') {
        header('HTTP/1.1 404 Page not found');
        exit;
    } elseif ($code == '502') {
        header('HTTP/1.1 502 Bad Gateway');
        exit;
    }
}

// Оберточная функция для вывода всех ошибок в программе
function error($error)
{
    die(var_dump($error));
}



function include_form_field($file, $vars = [], $params = [], $layout = 'form/fields/layout')
{
    $vars = array_merge($vars, $params);

    if (!file_exists(SITE_DIR . '/app/views/form/fields/' . $file . '.php')) return '';

    if (!isset($vars['required'])) {
        $vars['required'] = false;
    }

    if (isset($vars['placeholder'])) {
        $vars['placeholder'] = __($vars['placeholder']);
    } else {
        $vars['placeholder'] = '';
    }

    $vars['field_file'] = $file;

    $html = include_file($layout, $vars);
    return $html;
}

function echo_memory_usage() 
{
    $mem_usage = memory_get_usage(true);

    if ($mem_usage < 1024)
        return $mem_usage." bytes";
    elseif ($mem_usage < 1048576)
        return round($mem_usage/1024,2)." kilobytes";
    else
        return round($mem_usage/1048576,2)." megabytes";
}

function show_memory_usage($time_start)
{
    $tbody = '';
    $full_time = 0;
    foreach (DB::$log as $id => $query) {
        $full_time += $query['time'];
        $tbody .= '<tr>
            <td>'.$id.'</td>
            <td>'.round($query['time'], 2).'</td>
            <td>'.$query['class_name'].'</td>
            <td class="query_td">'.mb_substr($query['query'], 0, 100).'<div>'.SqlFormatter::format($query['query']).'</div></td>
            <td>'.$query['count'].'</td>
        </tr>';
    }
    echo '
        <div class="queries_popup">
            <div title="Total execution time in microseconds">'.(round((microtime(true) - $time_start)*1000, 3)).' ms</div>
            <div title="Total memory usage">'.echo_memory_usage().'</div>
            <div class="queries">
                '.count(DB::$log).' qrs
                <div class="queries_table_wrapper">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>('.round($full_time, 2).')</th>
                                <th>Class</th>
                                <th>SQL</th>
                            </tr>
                        </thead>
                        <tbody>'.$tbody.'</tbody>
                    </table>
                </div>
                <style>
                    .queries_popup {
                        position: absolute;
                        top: 0;
                        right: 0;
                        background: #fff;
                        padding: 10px
                    }
                    .queries_table_wrapper {
                        background: #fff;
                        position: absolute;
                        top: 30px;
                        right: 10px;
                        padding: 10px;
                        font-size: 8px;
                        overflow: scroll;
                        display: none;
                        border: 1px solid black;
                    }
                    .queries_table_wrapper table {
                        width: 400px;
                    }
                    .queries:hover > div {
                        display: block !important;
                    }
                    .queries td,
                    .queries th {
                        font-size: 10px;
                    }
                    .query_td div {
                        display: none;
                        background: #fff;
                        position: absolute;
                        right: 15px;
                        padding: 10px;
                        font-size: 8px;
                        height: 464px;
                        overflow: scroll;
                        border: 1px solid black;
                        width: 340px;
                    }
                    .query_td:hover div,
                    .query_td div:hover {
                        display: block;
                    }
                </style>
            </div>
        </div>
        ';
}
function siteURL($param = '')
{
    if (!empty($param) && $param == 'prev') {
        $param = (explode('/',$_SERVER['REQUEST_URI']));
        $param = $param[1];
    }
    $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    $domainName = $_SERVER['HTTP_HOST'].'/'.$param;
    return $protocol.$domainName;
}
function forwardGetParams()
{
    $params = $_GET;

    unset($params['controller']);
    unset($params['action']);
    unset($params['real_url']);

    return http_build_query($params);
}

function isJson($string) 
{
    if (! $string) return false;
    json_decode($string);
    return (json_last_error() == JSON_ERROR_NONE);
}

function filterGroup($type, $vars = [])
{
    if (file_exists(SITE_DIR . '/admin/app/views/filter_groups/' . $type . '.php')) {
        echo include_file('filter_groups/' . $type, $vars);
    } else {
        echo include_file('filter_groups/default', $vars);
    }
}
function getMax($array = [], $key = '')
{
    $max = 0;
    foreach( $array as $k => $v )
    {
        $max = max( array( $max, $v[$key] ) );
    }
    return $max;
}