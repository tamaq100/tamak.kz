<?php 
class Map {
	public static function get_link_to_point($point, $category)
	{
		$rubric 	= $category->rubric_object;
		if (! $rubric) return '#';

		$page_model = $rubric->furl;
		$page_model = ucfirst($page_model);

		$ret = '';

		if (isset($point->parent_id)) {
			foreach ($point->parent_id as $parent_id) {
				$parent = $page_model::findOneBy($parent_id);

				if (! $parent) continue;
				
				$ret .= '<a href="/' . $rubric->furl . '/page/' . $parent_id . '">' . $parent->title . '</a><br>';
			}
		}

		return $ret;
	}

	public static function get_events_ids($point)
	{
		return implode(', ', $point->parent_id);
	}
}