<?

/**
 * 
 */
class Api
{
	// public static $https://tamaq.kz/services/list

	public static $apiDomain = 'https://admin.tamaq.kz';
	public static $images_path_dir = '/../runtime/_media/';
	public static $positiveInt = 4;

	public static $GATEWAY = '172.18.0.1';

	//http://tamaq.loc/apio/unloadingUsers?code=wQ4BN5zR
	public static function loadingKitchens()
	{
		if ($ch = curl_init()) {

			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type:application/json;charset=UTF-8'));
			curl_setopt($ch, CURLOPT_URL, self::$apiDomain . '/dictionary/product_areas');
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

			$kitchens = json_decode(curl_exec($ch));
			curl_close($ch);
			foreach ($kitchens as $kitchen) {
				if ($kitchenEntity = Kitchen::findOneBy(['key' => $kitchen->key])) {
					# code...
				} else {
					$kitchenEntity = new Kitchen;
				}
				$kitchenEntity->name = $kitchen->value_ru;
				$kitchenEntity->key  = $kitchen->key;

				$kitchenEntity->save();
			}
			return 1;
		} else {
			return 0;
		}
	}
	public static function loadingCategories()
	{
		if ($ch = curl_init()) {

			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type:application/json;charset=UTF-8'));
			curl_setopt($ch, CURLOPT_URL, self::$apiDomain . '/dictionary/categories');
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

			$data = json_decode(curl_exec($ch));
			curl_close($ch);

			foreach ($data as $category) {
				if ($siteCategory = Category_button::findOneBy(['link' => $category->key])) {
					# code...
				} else {
					$siteCategory = new Category_button;
				}
				$siteCategory->name = $category->value_ru;
				$siteCategory->link = $category->key;
				$siteCategory->save();
			}
			return 1;
		} else {
			return 0;
		}
	}
	public static function unloadingCitiesList()
	{
		set_time_limit(400);

		if ($ch = curl_init()) {
			// lat = широта
			// Широта: 43°15′24″ с.ш. 
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type:application/json;charset=UTF-9'));
			curl_setopt($ch, CURLOPT_URL, self::$apiDomain . '/dictionary/cities');
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

			$data = json_decode(curl_exec($ch));

			curl_close($ch);


			foreach ($data as $cities) {

				if ($city = City::findOneBy(['value_ru' => $cities->value_ru])) { } else {
					continue;
				}
				sleep(2);
				if ($geo = GeocodeYandex::findPlace($cities->value_ru)) {
					$city->lat = $geo['lat'];
					$city->lng = $geo['lng'];
				}
				sleep(1);

				$city->value_ru = $cities->value_ru;
				// $city->value_en = $cities->value_en;
				// $city->value_kz = $cities->value_kz;
				$city->key = $cities->key;
				$city->save();
			}
			return 'Okay';
		} else
			return 'err';
	}
	public static function getGateway()
	{
		return (getenv('ENV') == 'docker_dev') ? self::$GATEWAY : '127.0.0.1';
	}
	public static function unloadingUsersData()
	{

		$GATEWAY = self::getGateway();

		if ($ch = curl_init()) {

			curl_setopt($ch, CURLOPT_TIMEOUT, 120);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type:application/json;charset=UTF-8'));
			curl_setopt($ch, CURLOPT_URL, $GATEWAY . ':3036/userall');
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			$data = curl_exec($ch);
			curl_close($ch);
			$users = json_decode($data);
			echo "Пошла загрузка";
			foreach ($users as $user) {
				if (!isset($user->id)) continue;
				if (!$current_user = User::findOneBy(['uid' => $user->id])) continue;
				if (!$current_data = User_data::findOneBy(['uid' => $current_user->uid])) {
					$current_data = new User_data;
					$current_data->uid = $current_user->uid;
					$current_data->name = $current_user->name;
					$current_data->phone = $current_user->tel;
				}
				$current_data->country_and_town = json_encode(array(
					'country' => isset($user->country->value_ru) ? $user->country->value_ru : '',
					'town'	  => isset($user->town->value_ru) ? $user->town->value_ru : ''
				));
				$current_data->transport = json_encode(array(
					'transport' => isset($user->transport->value_ru) ? $user->transport->value_ru : ''
				));
				$current_data->photos = json_encode(array(
					'photos' => isset($user->photos) ? $user->photos : ''
				));
				$current_data->balance = $user->balance;
				$current_data->bonus_balance = $user->bonus_balance;

				$current_data->phone_callcenter = isset($user->country->details->callcenter_phone) ? $user->country->details->callcenter_phone : '';

				$current_data->avg_state_rate = isset($user->avg_state_rate) ? $user->avg_state_rate : '';
				$current_data->favorite = isset($user->favorite) && empty($user->favorite) ? json_encode($user->favorite) : '';
				$current_data->payments = json_encode(isset($user->payments) ? $user->payments : '');

				$current_data->save();
			}
			return 1;
		} else {

			echo "ошибка";
			return 0;
		}
	}
	public static function unloadingUsersList()
	{
		$GATEWAY = self::getGateway();
		// Специальная функция для того, что бы выгрузить всех пользователей
		if ($ch = curl_init()) {
			curl_setopt($ch, CURLOPT_TIMEOUT, 120);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type:application/json;charset=UTF-8'));
			// Указать IP NodeJS API
			curl_setopt($ch, CURLOPT_URL, $GATEWAY . ':3036/userall');
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

			$data = curl_exec($ch);

			curl_close($ch);
			$users = json_decode($data);
			// Здесь сохраняем юзеров
			echo "Пошла загрузка";
			foreach ($users as $user) {

				if (!isset($user->id) && empty($user->id)) continue;
				if (!isset($user->phone) && empty($user->phone)) continue;

				if (!$current = User::findOneBy(['uid' => $user->id])) {
					echo "\n new User";
					$current = new User;
				}
				// die(var_dump($user));
				$current->uid = $user->id;
				$current->tel =  $user->phone;
				$current->active = 1;
				$current->role = (isset($user->role) && $user->role) ? $user->role : '';
				$current->name = (isset($user->name) && $user->name) ? $user->name : '';
				$current->save();
			}
			return 1;
		} else {
			echo "Ошибка";
			return 0;
		}
	}
	public static function PleaseAdmin()
	{
		$options = array(
			"user_provider_id" => "+79652112274",
			"provider_key" => "PHONE_PASSWORD",
			"password" => "31722387"
		);
		$options = json_encode($options, true);
		// Составляем тело запроса
		$defaults = array(
			CURLOPT_URL => 'https://tamaq.kz/login',
			CURLOPT_HTTPHEADER => array('Content-type:application/json;charset=UTF-8'),
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => $options
		);
		// инициализируем запрос
		$ch = curl_init();
		// вводим в запрос наше тело запроса
		curl_setopt_array($ch, ($defaults));
		// Для очистки буфера после curl_exec
		// посылаем запрос и декодируем данные
		$res = curl_exec($ch);
		// смотрим что нам пришло
		// возможные варианты 200 - значит всё ок
		// и 400 что то не правильно
		$info = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		// заканчиваем наш буфер и очищаем всё что попало в него
		// очищаем запрос

		curl_reset($ch);
		// закрываем запрос
		curl_close($ch);
		return $info;
	}
	// Спиздить всё
	public static function UpAllPopularFood()
	{
		// Получаем список всех продуктов у которых заполнено поле URLPARSE
		$popular_food = Popular_food::findBy('LENGTH(TRIM(urlparse)) > 0');

		foreach ($popular_food as $food) {
			// die(var_dump($food));
			// получаем результат запроса парса
			$result = self::GetProductById($food->urlparse);
			if (!$result) return;
			// Если у нас есть результат
			// die(var_dump($result));
			foreach ($result[0] as $key => $item) {
				if ($key == 'photos') {
					foreach ($item as $photos => $photo) {
						// В норме прикрутить систему анализа ширины и высоты фотографии ёпт
						$thisPhoto = self::DownloadPhotos($photo['path']);
						if ($thisPhoto) {
							$food->image = $thisPhoto;
							break;
						}
					}
				}
				if ($key == 'name') {
					$food->title = $item;
				}
				if ($key == 'description') {
					$food->subtitle = $item;
				}
				if ($key == 'id') {
					$food->link = $item;
				}
				if ($key == 'service') {
					if ($item['id']) {
						$serviceInfo = self::getService($item['id']);
						if ($serviceInfo) {
							$food->company = $serviceInfo;
							$food->service_id = $item['id'];
						}
					}
				}
				if ($key == 'price') {
					$food->price = strval($item);
				}
				if ($key == 'tags') {
					$tagArr = '';
					foreach ($item as $numm => $tagg) {
						if ($tagg != '') {
							$tagArr .= $tagg . ';';
						}
					}
					$food->tags = $tagArr;
				}
			}
			$food->save();
			echo "okay \n";
		}
	}

	public static function DownloadPhotos($path)
	{
		$photo = file_get_contents(self::$apiDomain . $path);
		if ($photo) {
			$path_part = substr($path, 6);
			file_put_contents(WORK_DIR . self::$images_path_dir . $path_part, $photo);
			$path_parts = explode('.', $path_part);
			return $path_parts[0];
		}
		return '';
	}
	public static function findRestoAround($coords, $adress)
	{
		$coords = explode(",", $coords);
		$postdata = json_encode(
			array(
				"from" => "0",
				"count" => '25',
				"latitude" => $coords[0],
				"longitude" => $coords[1],
				"sort" => [
					array(
						"field" => "distance",
						"type"	=> "double",
						"asc"	=> true
					)
				]
			),
			true
		);

		$opts = array(
			'http' =>
			array(
				'method'  => 'POST',
				'header'  => 'Content-type: application/json',
				'content' => $postdata
			)
		);

		$context  = stream_context_create($opts);

		$result = file_get_contents(self::$apiDomain . '/services/list', false, $context);

		$resto = self::RestoTrimmer(json_decode($result), $adress);
		return $resto;
	}
	public static function RestoTrimmer($resto, $city)
	{
		$restos = [];
		foreach ($resto as $index => $obj) {
			// die(var_dump($obj));
			if (count($obj->addresses)) {
				$restos[$index]['name'] = $obj->name;
				$current = $obj->addresses;
				foreach ($current as $number => $department) {
					if ($department->town->key == $city) {
						$restos[$index]['departments'][$number]['id'] = $department->id;
						$restos[$index]['departments'][$number]['latitude'] = $department->latitude;
						$restos[$index]['departments'][$number]['longitude'] = $department->longitude;
					} else {
						continue;
					}
				}
				if (count($obj->categories)) {
					$val = $obj->categories;
					$ArrayCategory = '';
					foreach ($val as $category => $value) {
						if (!$value->value_ru) continue;
						$ArrayCategory .= $value->value_ru . ' | ';
					}
					$restos[$index]['categories'] = $ArrayCategory;
				}
				if (count($obj->photos)) {
					$obj->photos;
					if (isset($obj->photos[0]->path)) {
						$restos[$index]['photo'] = $obj->photos[0]->path;
					}
				}
				if (count($obj->description) && ($obj->description != '')) {
					$restos[$index]['description'] = $obj->description;
				}
			} else {
				continue;
			}
		}

		return json_encode($restos);
	}
	public static function getService($id)
	{
		$service_data = file_get_contents(self::$apiDomain . '/services?ids=' . $id);
		if ($service_data) {
			$service_data = json_decode($service_data, true);
			return $service_data[0]['name'];
		}
		return '';
	}
	public static function GetProductById($id)
	{
		$product = self::sendGetRequest('/products?ids=', ['ids' => $id]);
		if ($product) {
			$product = json_decode($product, true);
			return $product;
		}
		return '';
	}
	public static function TimeHandler($time)
	{
		$time = strval($time);
		$len = strlen($time);
		if ($len == 3) $time = substr_replace($time, ":", 1, 0);
		if ($len == 4) $time = substr_replace($time, ":", 2, 0);
		if ($len == 2) $time = substr_replace($time, ":00", 2, 0);
		return $time;
	}

	public static function sendGetRequest($url, $data)
	{
		$result = file_get_contents(self::$apiDomain . $url . urlencode($data['ids']));
		return $result;
	}

	public static function CreatePopularServices()
	{
		// Т.к мне не сказали по какому принципу мы будет выбирать рестораны
		// будем выводить только самые первые
		$limit = 10;
		// Обращение к API
		$result = self::sendPostRequest('services/list');
		if ($result) {
			$result = json_decode($result, true);
			$CurrentRestos = Popular_restoraunt::findAll();
			// Проходимся по ресторанам 
			foreach ($result as $key => $restoraunt) {
				if ($restoraunt['positive_valuations'] < self::$positiveInt) continue;
				// Если достигнут лимит то выходим;
				if ($limit == 0) break;
				// Если есть такой ресторан уже то возвращаем его
				if (!$entity = Popular_restoraunt::findOneBy(['service_id' => $restoraunt['id']])) {
					$entity = new Popular_restoraunt;
				}

				// Проходимся по ресторану
				foreach ($restoraunt as $property => $val) {
					// die(var_dump($restoraunt));
					if ($property == 'id') $entity->service_id = $val;
					if ($property == 'photos') {
						if (!empty($val)) {
							$thisPhoto = self::DownloadPhotos($val[0]['path']);
							if ($thisPhoto) {
								$entity->image = $thisPhoto;
							}
						}
					}
					// if ($property == 'pricelevel') { Ну ваще хз что за прайс LVL такой
					// 	code...
					// }
					if ($property == 'openfrom') $entity->jobtime_min = self::TimeHandler($val);
					if ($property == 'opento') $entity->jobtime_max = self::TimeHandler($val);
					if ($property == 'name') $entity->title = $val;
					if ($property == 'categories') {
						if (!$val) continue;
						$ArrayCategory = '';
						foreach ($val as $category => $value) {
							if (!$value['value_ru']) continue;
							$ArrayCategory .= $value['value_ru'] . ' | ';
						}
						$entity->citchen = $ArrayCategory;
					}
				}
				$entity->save();
				echo $limit;
				--$limit;
			}
			echo "Ну вроде загрузилось";
		}
	}
	public static function sendPostRequest($url, $data = [], $json)
	{
		if (isset($json)) {
			$options = array(
				'http' => array(
					'header'  => "Content-type: application/json",
					'method'  => 'POST',
					'content' => http_build_query($data)
				)
			);
		} else {
			$options = array(
				'http' => array(
					'header'  => "Content-type: application/x-www-form-urlencoded",
					'method'  => 'POST',
					'content' => http_build_query($data)
				)
			);
		}

		$result = false;

		try {
			$context  = stream_context_create($options);
			$result = file_get_contents(self::$apiDomain . '/' . $url, false, $context);

			if (!$result) {
				throw new Exception('Не удалось обратиться к api по адресу ' . $url . '. Параметры:' . Arr::echo_arr($options));
				return false;
			}
		} catch (Exception $e) {
			Mail::errorMail('Ошибка в функции Api::post', $e);
			return false;
		}

		return $result;
	}

	public static function normalizeData($data)
	{
		return json_decode($data, true);
	}
	public static function findAllCouriers()
	{
		$GATEWAY = self::getGateway();

		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_PORT => "3036",
			CURLOPT_URL => "http://" . $GATEWAY . ":3036/executors",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_POSTFIELDS => "",
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			throw new Exception("Error Processing Request", $err);
		} else {
			return $response;
		}
	}
}
