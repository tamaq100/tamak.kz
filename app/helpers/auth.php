<?php 
class Auth
/**
 *  Для чека авторизованности
 */
{

	// none (Никто)
	// User (пользователь),
	// Executor (курьер),
	// Admin (администратор системы)
	// Provider (провайдер услуг, ресторан),
	// Callcenter (сотрудник колцентра),
	// Operator (оператор системы),
	public static $roles = array(
		'none'		=> 0,
		'user'		=> 1,
		'executor'	=> 2,
		'admin'		=> 3,
		'provider'	=> 4,
		'callcenter'=> 5,
		'operator'	=> 6,
	);
	public static function setSession($role, $uid){
		// Доделать тут н
		$role = strtolower($role);
		$_SESSION['role'] = self::$roles[$role];
		$_SESSION['uid'] = $uid;
		return $_SESSION['role'];
	}
	public static function getSession()
	{

		return isset($_SESSION['role']) ? $_SESSION['role'] : 0;
	}
	public static function ClientCheck($paramID)
	{
		$user = User::findOneBy(array('uid' => $paramID));
		if ($user) {
			self::setSession($user->role, $paramID);
		}else{
			self::setSession($_GET['r'], $paramID);
		} 
		return json_encode($paramID);
	}
}