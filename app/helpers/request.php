<?php 
/**
 * Хелпер для работы с запросами
 */
class Request
{
	// private static $serverAddress = 'http://127.0.0.1:3036';
	/**
	 * Функция для отправки пост запроса на API
	 * @param  $url  
	 * @param  [array] 		$data 
	 */
	public static function post($url, $data = [])
	{
		// $hash = Protection::getHash();

	    $options = array(
	        'http' => array(
	            'header'  => "Content-type: application/x-www-form-urlencoded",
	            'method'  => 'POST',
	            'content' => http_build_query($data)
	        )
	    );

	    $result = false;
	    try {

		    $context  = stream_context_create($options);
		    $result = file_get_contents($url, false, $context);

	    	if (! $result) {
	    		throw new Exception('Не удалось обратиться к api по адресу ' . $url . '. Параметры:' . Arr::echo_arr($options));
	    	}
	    } catch (Exception $e) {
	    	Mail::errorMail('Ошибка в функции Api::post', $e);
	    }

	    return $result;
	}
}