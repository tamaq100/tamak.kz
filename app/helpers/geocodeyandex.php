<?php 
/**
 * 
 */
class GeocodeYandex
{
	public static function findPlace($place){
	    $params = array(
	        'geocode' => $place, // адрес
	        'format'  => 'json',                          // формат ответа
	        'results' => 1,                               // количество выводимых результатов
	    );

	    $response = json_decode(file_get_contents('http://geocode-maps.yandex.ru/1.x/?' . http_build_query($params, '', '&')));

	    if (isset($response->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos)) {
	    	
	    	$geo = explode(" ", $response->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos);
	    	return array('lat' => $geo[0], 'lng'=> $geo[1]);

	    }else{

	    	return false;

	    }
	}

}
