<?php
/**
* Mail model
*/
class Mail
{
	public static $log = array();

	public static function get_http_host()
	{
		if (isset($_SERVER) && isset($_SERVER['HTTP_HOST'])) {
			return 'http://' . $_SERVER['HTTP_HOST'];
		} else {
			return 'http://4dclick.kz';
		}
	}

	public static function send($to, $subject, $message, $attachments = array())
	{
		if (! $to) return false;
		// self::log($to, $subject, $message);

		$to = explode(',', $to);

		if (! $to) return false;
		$message = include_file('mail/layout', array(
			'title'   => $subject,
			'content' => $message,
		));

		$message = str_replace('%http_host%', self::get_http_host(), $message);
		$message = str_replace('%site_name%', $_SERVER['HTTP_HOST'], $message);
		foreach ($to as $mail) {
			self::send_mail($mail, $subject, $message, $attachments);
		}
	}

	private static function send_mail($to, $subject, $message)
	{
		require_once SITE_DIR.'/vendor/PHPMailer/PHPMailerAutoload.php';

		$email = new PHPMailer();

		if (getenv('ENV') == 'docker_dev') {
			$email->isSMTP();                      // Set mailer to use SMTP
			$email->Host     = 'mailcatcher:1025'; // Specify main and backup SMTP servers
			$email->Port     = 1025;               // TCP port to connect to
		}

        if (! empty($attachments)) {
            foreach ($attachments as $attachment_name) {
                if (isset($_FILES[$attachment_name]) && $_FILES[$attachment_name]['error'] == UPLOAD_ERR_OK) {
                    $email->AddAttachment($_FILES[$attachment_name]['tmp_name'],
                        $_FILES[$attachment_name]['name']);
                }
            }
        }


		$email->setLanguage('ru', '/vendor/PHPMailer/language/directory/');

		$email->setFrom('no-reply@' . $_SERVER['HTTP_HOST'], $_SERVER['HTTP_HOST']);
		$email->Subject   = $subject;
		$email->Body      = $message;
		$email->CharSet   = 'UTF-8';
		$email->IsHTML(true);

		$email->AddAddress($to);

		return $email->send();
	}

	private static function log($to, $subject, $message)
	{
		$_SESSION['messages'][] =  array(
			'to'       => $to,
			'subject'  => $subject,
			'message'  => $message,
     	);
	}

	public static function errorMail($subject, $message)
	{
		$to = [
			'eduard.e@4dclick.com',
			'denis.i@4dclick.com',
		];

		$message = include_file('mail/layout', array(
			'title'   => $subject,
			'content' => $message,
		));

		foreach ($to as $mail) {
			self::send_mail($mail, $subject, $message);
		}
	}
}
