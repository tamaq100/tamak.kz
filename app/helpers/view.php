<?php

/**
* 
*/
class View
{

	public static function renderLinearMenuAsA($menu = array()) {

		$ret = '';

		foreach ((array)$menu as $item) {
			$ret .= '<a href="'.$item['link'].'">'.$item['title'].'</a>';
		}

		return $ret;
	}


	public static function renderBreadcrumbsAsA($menu = array()) {
		$ret = '';
		$last = end($menu);
		foreach ((array)$menu as $item) {
			if ($item == $last) {
				$ret .= '<li class="active">'.$item['title'].'</li>';
			} else {
				$ret .= '<li><a href="'.$item['link'].'">'.$item['title'].'</a><i class="fa fa-angle-right"></i>';
			}
		}

		return $ret;
	}

	public static function isAuthorized() {
		return isset($_SESSION['auth']) && isset($_SESSION['auth']['id']);
	}

	public static function buildUrl($get_params = array()) {
	    $protocol = strpos(strtolower($_SERVER['SERVER_PROTOCOL']),'https') === FALSE ? 'http' : 'https';
	    $host     = $_SERVER['HTTP_HOST'];
	    
	    parse_str($_SERVER['QUERY_STRING'], $params);

	    $url = $protocol . '://' . $host . '/' .
	    	(isset($params['real_url']) ? $params['real_url'] : '');

	    unset($params['controller']);
	    unset($params['real_url']);
	    unset($params['action']);
	    unset($params['id']);

	    $params = array_merge($params, $get_params);

	    $params = http_build_query($params);
	    $url .= $params ? '?'.$params : '';

	    return $url;
	}

	public static function getCurrentUser() {
		if (isset($_SESSION['auth']['id'])) {
			return User::findOneBy($_SESSION['auth']['id']);
		} else {
			return false;
		}
	}

	public static function banner_place($banners, $width, $height = false) {
		$ret = '<div class="banners">';
		$i = 0;
		foreach ($banners as $banner) {
			$src = $height
				? '/images/crop/'.$width.'/'.$height.'/'.$banner->media.'.jpg'
				:'/images/w/norm/'.$width.'/'.$banner->media.'.jpg';

			$ret .= '<div class="banner" data-id="'.$i++.'"><a href="/banner/click/'.$banner->id.'"><img src="'.$src.'" alt="'.$banner->title.'"></a></div>';
		}

		$ret .= '</div>';

		return $ret;
	}

	public static function getRusMonthFromNumber($monthNumber, $with_ending = false)
	{

		if (! $with_ending) {
	    	$rus_months = array(1 => "Январь", 2 => "Февраль", 3 => "Март", 4 => "Апрель", 5 => "Май", 6 => "Июнь", 7 => "Июль", 8 => "Август", 9 => "Сентябрь", 10 => "Октябрь", 11 => "Ноябрь", 12 => "Декабрь");
		} else {
	    	$rus_months = array(1 => "Января", 2 => "Февраля", 3 => "Марта", 4 => "Апреля", 5 => "Мая", 6 => "Июня", 7 => "Июля", 8 => "Августа", 9 => "Сентября", 10 => "Октября", 11 => "Ноября", 12 => "Декабря");
		}

	    return $rus_months[$monthNumber];
	}

	public static function getRussianDate($date){
		$date = date('j', $date) . " " . getRusMonthFromNumber(date('n',$date), true) . " " . date('Y', $date);
		return $date;
	}

	public static function getRusmonth($date, $with_ending = false) {
		if (! $with_ending) {
	    	$rus_months = array(1 => "январь", 2 => "февраль", 3 => "март", 4 => "апрель", 5 => "май", 6 => "июнь", 7 => "июль", 8 => "август", 9 => "сентябрь", 10 => "октябрь", 11 => "ноябрь", 12 => "декабрь");
		} else {
	    	$rus_months = array(1 => "января", 2 => "февраля", 3 => "марта", 4 => "апреля", 5 => "мая", 6 => "июня", 7 => "июля", 8 => "августа", 9 => "сентября", 10 => "октября", 11 => "ноября", 12 => "декабря");
		}

		$date = (int)$date;

	    return $rus_months[$date];
	}

	// Функция для очистки телефоного номера от лишних символов
	public static function clear_tel($string)
	{
		return preg_replace('/[^0-9+]/', '', $string);
	}

	// по заданному номеру и слову, возвращает это слово с нужным окончанием
	public static function get_rus_word($word, $num, $words_with_ending = []) {

		$word = strtolower($word);

		if (($num>10) && ($num<20)) {

			$ret = 	$words_with_ending[2];

		} else {
			
			$num = $num % 10;

			switch ($num) {
				case 1:
					$ret = $words_with_ending[0];
					break;
				
				case 2:
				case 3:
				case 4:
					$ret = $words_with_ending[1];
					break;

				case 5: 
				case 6:
				case 7:
				case 8:
				case 9:
				case 0:
					$ret = $words_with_ending[2];
					break;

				default:
					$ret = $words_with_ending[0];
					break;
			}
		}

		return $ret;

	}

	public static function get_youtube_hash($link)
	{
        if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $link, $match)) {
            $result = $match[1];
        }

        return $result;

	}
}