<?php 

/**
 * Recaptcha helper class
 *
 * 
 * Для того чтобы воспользоваться классом,
 * необходимо разместить несколько строчек кода в определенных местах страницы
 *
 * Строчка
 * ** echo Recaptcha::client_side_head();
 * в <head/> блоке
 * 
 * Строчка
 * ** echo Recaptcha::client_side_form();
 * в форме
 * 
 * Строчка
 * ** echo Recaptcha::client_side_before_body_end();
 * перед закрывающим тегом body
 * 
 * Строчка
 * ** $capthca_result = Recaptcha::server_side_check();
 * при валидации формы
 */
class Recaptcha
{
	// ключи для общения с recaptcha сервером
	public static $site_key         = '6LdzMCgTAAAAACw3RHuhEp-k8ghXXgYDgtiUObJ7';
	private static $secret_key      = '6LdzMCgTAAAAAHHc1nFIMjB8MS12m9k6jFybSS6n';
	private static $server_side_url = 'https://www.google.com/recaptcha/api/siteverify';

	public static $success;
	public static $error_codes;
	public static $instansces = array();

	public static function get_success()
	{
		$result = false;
		if (self::$success) {
			$result = true;
		}

		if (isset($_SESSION['recaptcha_ok'])) {
			$result = true;
		}

		return $result;
	}

	public static function client_side_head()
	{
		if (self::get_success()) return;

		return '<script src="//www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=explicit" async defer></script>';
	}

	public static function client_side_before_body_end()
	{
		if (self::get_success()) return;

		$html =
		'<script type="text/javascript">
			var CaptchaCallback = function(){';
		foreach (self::$instansces as $instance_id => $params) {
			$html .= "grecaptcha.render('". $instance_id ."', {'sitekey' : '". self::$site_key ."', 'size': '".$params['size']."'});";
		}
		$html .= '}
		</script>';

		return $html;
	}

	public static function client_side_form($compact = false)
	{
		if (self::get_success()) return;

		$uniqid = uniqid();
		self::$instansces[$uniqid] = array('size' => $compact ? 'compact' : 'normal');
		$html = '<div class="g-recaptcha" id="'.$uniqid.'"></div>';

		if (isset($_POST['g-recaptcha-response'])) {
			$html .=
			'<script>
				if (grecaptcha) {
					grecaptcha.render(\''.$uniqid.'\', {
						\'sitekey\' : \''.self::$site_key .'\'
					});
				};
			</script>';
		}

		return $html;
	}

	public static function server_side_check()
	{
		if (self::get_success()) return true;
		$token = isset($_POST['g-recaptcha-response'])
			? $_POST['g-recaptcha-response']
			: false;

		if (! $token) return false;

		$url = self::$server_side_url;

		$data = array(
			'secret' => self::$secret_key,
			'response' => $token,
			'remoteip' => $_SERVER['REMOTE_ADDR'],
		);

		$response = self::curl_request($url, $data);
		$response = json_decode($response, true);

		self::$success     = $response['success'];
		if (isset($response['error-codes'])) {
			self::$error_codes = $response['error-codes'];
		}

		if (self::$success) {
			$_SESSION['recaptcha_ok'] = true;
		}

		return self::$success;
	}

	public function get_error_codes()
	{
		return self::$error_codes;
	}

	private static function curl_request($url, $data)
	{
		$ch = curl_init(); // create curl handle
		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_POST, true);
		curl_setopt($ch,CURLOPT_POSTFIELDS, http_build_query($data));
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch,CURLOPT_CONNECTTIMEOUT ,3); //timeout in seconds
		curl_setopt($ch,CURLOPT_TIMEOUT, 20); // same for here. Timeout in seconds.
		
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);


		$response = curl_exec($ch);

		curl_close ($ch); //close curl handle

		return $response;
	}
}
