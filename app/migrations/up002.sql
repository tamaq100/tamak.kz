-- ---
-- Globals
-- ---

-- SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
-- SET FOREIGN_KEY_CHECKS=0;

-- ---
-- Table 'banner_sliders'
-- Главный баннер слайдеров, содержит в себе ссылки на различные страницы. Содержит выбор цвета текста
-- ---

DROP TABLE IF EXISTS `ru_banner_sliders`;
		
CREATE TABLE `ru_banner_sliders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idx` INTEGER(11) NULL DEFAULT 0,
  `active` INTEGER(1) NULL DEFAULT NULL,
  `title` VARCHAR(200) NULL DEFAULT NULL,
  `text` VARCHAR(255) NULL DEFAULT NULL,
  `subtitle` VARCHAR(200) NULL DEFAULT NULL,
  `link` VARCHAR(255) NULL DEFAULT NULL,
  `typecolor` INTEGER(2) NULL DEFAULT NULL,
  `image` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) COMMENT 'Главный баннер слайдеров, содержит в себе ссылки на различны'
COLLATE='utf8_general_ci'
ENGINE=MyISAM
;

-- ---
-- Table 'category_buttons'
-- Кнопки
-- ---

DROP TABLE IF EXISTS `ru_category_buttons`;
		
CREATE TABLE `ru_category_buttons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idx` INTEGER(11) NULL DEFAULT NULL,
  `active` INTEGER(1) NULL DEFAULT NULL,
  `name` VARCHAR(255) NULL DEFAULT NULL,
  `image` VARCHAR(255) NULL DEFAULT NULL,
  `link` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) COMMENT 'Кнопки'
COLLATE='utf8_general_ci'
ENGINE=MyISAM
;

-- ---
-- Table 'our_team'
-- 
-- ---

DROP TABLE IF EXISTS `ru_our_team`;
		
CREATE TABLE `ru_our_teams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idx` INTEGER(11) NULL DEFAULT NULL,
  `active` INTEGER(1) NULL DEFAULT NULL,
  `avatar` VARCHAR(255) NULL DEFAULT NULL,
  `name` VARCHAR(255) NULL DEFAULT NULL,
  `position` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM
;

-- ---
-- Table 'FAQ'
-- 
-- ---

DROP TABLE IF EXISTS `ru_FAQ`;
		
CREATE TABLE `ru_FAQ` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idx` INTEGER(11) NULL DEFAULT NULL,
  `active` INTEGER(1) NULL DEFAULT NULL,
  `question` VARCHAR(255) NULL DEFAULT NULL,
  `answer` MEDIUMTEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM
;

-- ---
-- Table 'our_couriers'
-- 
-- ---

DROP TABLE IF EXISTS `ru_our_couriers`;
		
CREATE TABLE `ru_our_couriers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idx` INTEGER(11) NULL DEFAULT NULL,
  `created` INTEGER(11) NULL DEFAULT NULL,
  `updated` INTEGER(11) NULL DEFAULT NULL,
  `active` INTEGER(1) NULL DEFAULT NULL,
  `avatar` VARCHAR(255) NULL DEFAULT NULL,
  `name` VARCHAR(255) NULL DEFAULT NULL,
  `stars` INTEGER(5) NULL DEFAULT NULL,
  `status` INTEGER(11) NULL DEFAULT NULL,
  `last_coord` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM
;

-- ---
-- Table 'feedback_contacts'
-- 
-- ---

DROP TABLE IF EXISTS `ru_feedback_contacts`;
		
CREATE TABLE `ru_feedback_contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` INTEGER(11) NULL DEFAULT NULL,
  `updated` INTEGER(11) NULL DEFAULT NULL,
  `name` VARCHAR(255) NULL DEFAULT NULL,
  `telephone` VARCHAR(255) NULL DEFAULT NULL,
  `email` VARCHAR(255) NULL DEFAULT NULL,
  `message` MEDIUMTEXT NULL DEFAULT NULL,
  `moderated` INTEGER(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM
;

-- ---
-- Table 'news'
-- 
-- ---

DROP TABLE IF EXISTS `ru_news`;
		
CREATE TABLE `ru_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idx` INTEGER(11) NULL DEFAULT NULL,
  `create` INTEGER(11) NULL DEFAULT NULL,
  `update` INTEGER(11) NULL DEFAULT NULL,
  `active` INTEGER(1) NULL DEFAULT NULL,
  `title` VARCHAR(255) NULL DEFAULT NULL,
  `preview` VARCHAR(255) NULL DEFAULT NULL,
  `image` VARCHAR(255) NULL DEFAULT NULL,
  `link` VARCHAR(255) NULL DEFAULT NULL,
  `text` MEDIUMTEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM
;

-- ---
-- Table 'promos'
-- 
-- ---

DROP TABLE IF EXISTS `ru_promos`;
		
CREATE TABLE `ru_promos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idx` INTEGER(11) NULL DEFAULT NULL,
  `created` INTEGER(11) NULL DEFAULT NULL,
  `updated` INTEGER(11) NULL DEFAULT NULL,
  `active` INTEGER(1) NULL DEFAULT NULL,
  `title` VARCHAR(255) NULL DEFAULT NULL,
  `subtitle` VARCHAR(255) NULL DEFAULT NULL,
  `image` VARCHAR(255) NULL DEFAULT NULL,
  `link` VARCHAR(255) NULL DEFAULT NULL,
  `conditions` MEDIUMTEXT NULL DEFAULT NULL,
  `company_id` INTEGER(11) NULL DEFAULT NULL,
  `price` VARCHAR(255) NULL DEFAULT NULL,
  `more` INTEGER(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM
;

-- ---
-- Table 'popular_food'
-- 
-- ---

DROP TABLE IF EXISTS `ru_popular_food`;
		
CREATE TABLE `ru_popular_foods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idx` INTEGER(11) NULL DEFAULT NULL,
  `active` INTEGER(1) NULL DEFAULT NULL,
  `created` INTEGER(11) NULL DEFAULT NULL,
  `updated` INTEGER(11) NULL DEFAULT NULL,
  `title` VARCHAR(255) NULL DEFAULT NULL,
  `company` VARCHAR(255) NULL DEFAULT NULL,
  `subtitle` VARCHAR(255) NULL DEFAULT NULL,
  `link` VARCHAR(255) NULL DEFAULT NULL,
  `price` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM
;

-- ---
-- Table 'companies'
-- 
-- ---

DROP TABLE IF EXISTS `ru_companies`;
		
CREATE TABLE `ru_companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idx` INTEGER(11) NULL DEFAULT NULL,
  `created` INTEGER NULL DEFAULT NULL,
  `updated` INTEGER(11) NULL DEFAULT NULL,
  `active` INTEGER(1) NULL DEFAULT NULL,
  `name` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM
;

-- ---
-- Table 'popular_restoraunts'
-- 
-- ---

DROP TABLE IF EXISTS `ru_popular_restoraunts`;
		
CREATE TABLE `%table_prefix%_popular_restoraunts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idx` INTEGER(11) NULL DEFAULT NULL,
  `created` INTEGER(11) NULL DEFAULT NULL,
  `updated` INTEGER(11) NULL DEFAULT NULL,
  `active` INTEGER(1) NULL DEFAULT NULL,
  `title` VARCHAR(255) NULL DEFAULT NULL,
  `citchen` MEDIUMTEXT NULL DEFAULT NULL,
  `min_price` VARCHAR(255) NULL DEFAULT NULL,
  `max_price` VARCHAR(255) NULL DEFAULT NULL,
  `jobtime_min` VARCHAR(255) NULL DEFAULT NULL,
  `jobtime_max` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM
;

-- ---
-- Table 'reviews'
-- 
-- ---

DROP TABLE IF EXISTS `ru_reviews`;
		
CREATE TABLE `%table_prefix%_reviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idx` INTEGER(11) NULL DEFAULT NULL,
  `created` INTEGER(11) NULL DEFAULT NULL,
  `updated` INTEGER(11) NULL DEFAULT NULL,
  `active` INTEGER(1) NULL DEFAULT NULL,
  `name` VARCHAR(255) NULL DEFAULT NULL,
  `text` MEDIUMTEXT NULL DEFAULT NULL,
  `stars` INTEGER(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM
;

-- ---
-- Foreign Keys 
-- ---


-- ---
-- Table Properties
-- ---

-- ALTER TABLE `banner_sliders` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `category_buttons` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `our_team` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `FAQ` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `our_couriers` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `feedback_contacts` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `news` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `promos` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `popular_food` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `companies` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `popular_restoraunts` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `reviews` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ---
-- Test Data
-- ---