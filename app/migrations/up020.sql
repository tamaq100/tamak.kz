DROP TABLE `ru_our_couriers`;
CREATE TABLE IF NOT EXISTS `ru_executors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` int(11) NULL DEFAULT NULL,
  `updated` int(11) NULL DEFAULT NULL,
  `active` int(1) NULL DEFAULT '0',
  `ids` varchar(255) NULL DEFAULT NULL,
  `latitude` DECIMAL(20,12) NULL DEFAULT NULL,
  `longitude`DECIMAL(20,12) NULL DEFAULT NULL,
  `avatar`  varchar(255)  NULL DEFAULT NULL,
  `name`  varchar(255)  NULL DEFAULT NULL,
  `is_online` int(1) NULL DEFAULT 0,
  `rating`  DECIMAL(5,2) NULL DEFAULT 0,
  PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM
;