DROP TABLE IF EXISTS `ru_color_palettes`;
		
CREATE TABLE `ru_color_palettes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idx` INTEGER(11) NULL DEFAULT 0,
  `active` INTEGER(1) NULL DEFAULT NULL,
  `color` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) COMMENT 'Цветовая палитра'
COLLATE='utf8_general_ci'
ENGINE=MyISAM
;