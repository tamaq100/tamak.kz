DROP TABLE IF EXISTS `ru_citys`;
CREATE TABLE IF NOT EXISTS `ru_citys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idx` int(11) NULL DEFAULT NULL,
  `created` int(11) NULL DEFAULT NULL,
  `updated` int(11) NULL DEFAULT NULL,
  `active` int(1) NULL DEFAULT '0',
  `cityname`  varchar(255)  NULL DEFAULT NULL,
  `lat` varchar(255) NULL DEFAULT NULL,
  `lng`	varchar(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM
;