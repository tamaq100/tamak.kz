DROP TABLE IF EXISTS `ru_users`;
CREATE TABLE IF NOT EXISTS `ru_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid`  varchar(255)  NULL DEFAULT NULL,
  `active` int(1) NULL DEFAULT '1',
  `tel` varchar(255) NULL DEFAULT NULL,
  `role`  varchar(255) NULL DEFAULT NULL,
  `name`  varchar(255)  NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM
;