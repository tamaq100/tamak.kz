ALTER TABLE `ru_popular_foods` ADD COLUMN `service_id` VARCHAR(255) DEFAULT NULL;
ALTER TABLE `ru_popular_foods` ADD COLUMN `tags` VARCHAR(255) DEFAULT NULL;
ALTER TABLE `ru_popular_foods` ADD COLUMN `addition` VARCHAR(255) DEFAULT NULL;
ALTER TABLE `ru_popular_foods` ADD COLUMN `specifics` text DEFAULT NULL;