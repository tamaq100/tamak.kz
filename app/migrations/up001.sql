CREATE TABLE IF NOT EXISTS `ru_administrators` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` int(11) NULL DEFAULT NULL,
  `updated` int(11) NULL DEFAULT NULL,
  `active` int(1) NULL DEFAULT '0',
  `name` varchar(255) NULL DEFAULT NULL,
  `login` varchar(255) NULL DEFAULT NULL,
  `password` varchar(255) NULL DEFAULT NULL,
  `phone` varchar(255) NULL DEFAULT NULL,
  `super` varchar(255) NULL DEFAULT NULL,
  `email` varchar(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) 
COLLATE='utf8_general_ci'
ENGINE=MyISAM
;

CREATE TABLE IF NOT EXISTS `ru_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` int(11) NULL DEFAULT NULL,
  `updated` int(11) NULL DEFAULT NULL,
  `active` int(1) NULL DEFAULT '0',
  `title` varchar(255) NULL DEFAULT NULL,
  `type` varchar(255) NULL DEFAULT NULL,
  `string`varchar(255) NULL DEFAULT NULL,
  `text` text,
  `bool` int(1) NOT NULL DEFAULT 0,
  `integer` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM
;

INSERT INTO `ru_administrators` (`name`, `login`, `password`, `active`, `super`) VALUES ('admin', 'admin', 'admin', 1, 1);
