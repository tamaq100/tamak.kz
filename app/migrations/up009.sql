CREATE TABLE IF NOT EXISTS `ru_about_us_mission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idx` int(11) NULL DEFAULT NULL,
  `created` int(11) NULL DEFAULT NULL,
  `updated` int(11) NULL DEFAULT NULL,
  `active` int(1) NULL DEFAULT '0',
  `title` varchar(255) NULL DEFAULT NULL,
  `image` varchar(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM
;
CREATE TABLE IF NOT EXISTS `ru_about_us_values` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idx` int(11) NULL DEFAULT NULL,
  `created` int(11) NULL DEFAULT NULL,
  `updated` int(11) NULL DEFAULT NULL,
  `active` int(1) NULL DEFAULT '0',
  `title` varchar(255) NULL DEFAULT NULL,
  `subtitle` text NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM
;
CREATE TABLE IF NOT EXISTS `ru_about_us_team` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idx` int(11) NULL DEFAULT NULL,
  `created` int(11) NULL DEFAULT NULL,
  `updated` int(11) NULL DEFAULT NULL,
  `active` int(1) NULL DEFAULT '0',
  `position` varchar(255) NULL DEFAULT NULL,
  `image` varchar(255) NULL DEFAULT NULL,
  `name`varchar(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM
;