DROP TABLE IF EXISTS `ru_users_data`;
CREATE TABLE IF NOT EXISTS `ru_users_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid`  varchar(255)  NULL DEFAULT NULL,
  `active` int(1) NULL DEFAULT '1',
  `name`	varchar(255) NULL DEFAULT NULL,
  `other_data` text NULL DEFAULT NULL,
  `description` text NULL DEFAULT NULL,
  `phone`	varchar(255) NULL DEFAULT NULL,
  `country_and_town` text NULL DEFAULT NULL,
  `transport` text NULL DEFAULT NULL,
  `photos` text NULL DEFAULT NULL,
  `balance` varchar(255) NULL DEFAULT NULL,
  `bonus_balance` varchar(255) NULL DEFAULT NULL,
  `phone_callcenter` varchar(255) NULL DEFAULT NULL,
  `avg_state_rate` varchar(255) NULL DEFAULT NULL,
  `favorite` text NULL DEFAULT NULL,
  `payments` text NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM
;