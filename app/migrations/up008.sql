CREATE TABLE IF NOT EXISTS `ru_autoadvantages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idx` int(11) NULL DEFAULT NULL,
  `created` int(11) NULL DEFAULT NULL,
  `updated` int(11) NULL DEFAULT NULL,
  `active` int(1) NULL DEFAULT '0',
  `title` varchar(255) NULL DEFAULT NULL,
  `image` varchar(255) NULL DEFAULT NULL,
  `subtitle`varchar(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM
;
CREATE TABLE IF NOT EXISTS `ru_auto_howto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idx` int(11) NULL DEFAULT NULL,
  `created` int(11) NULL DEFAULT NULL,
  `updated` int(11) NULL DEFAULT NULL,
  `active` int(1) NULL DEFAULT '0',
  `title` varchar(255) NULL DEFAULT NULL,
  `image` varchar(255) NULL DEFAULT NULL,
  `subtitle`varchar(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM
;