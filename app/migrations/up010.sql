CREATE TABLE IF NOT EXISTS `ru_config_dishes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` int(11) NULL DEFAULT NULL,
  `updated` int(11) NULL DEFAULT NULL,
  `active` int(1) NULL DEFAULT '0',
  `title` varchar(255) NULL DEFAULT NULL,
  `type` varchar(255) NULL DEFAULT NULL,
  `string`varchar(255) NULL DEFAULT NULL,
  `text` text,
  `bool` int(1) NOT NULL DEFAULT 0,
  `integer` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM
;
CREATE TABLE IF NOT EXISTS `ru_dishes_advantage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idx` int(11) NULL DEFAULT NULL,
  `created` int(11) NULL DEFAULT NULL,
  `updated` int(11) NULL DEFAULT NULL,
  `active` int(1) NULL DEFAULT '0',
  `title` varchar(255) NULL DEFAULT NULL,
  `subtitle` text NULL DEFAULT NULL,
  `image` varchar(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM
;
CREATE TABLE IF NOT EXISTS `ru_partners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idx` int(11) NULL DEFAULT NULL,
  `created` int(11) NULL DEFAULT NULL,
  `updated` int(11) NULL DEFAULT NULL,
  `active` int(1) NULL DEFAULT '0',
  `title` varchar(255) NULL DEFAULT NULL,
  `subtitle`varchar(255) NULL DEFAULT NULL,
  `image` varchar(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM
;