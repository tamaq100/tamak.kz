<?php
/**
* FAQ model
*/
class FAQ extends BaseModel
{
	public static $table_name = "FAQ";

	public static $fields = array(
		'id'			=> 'integer',
		'idx'			=> 'integer',
		'active'		=> 'bool',
		'question'		=> 'string',
		'answer'		=> 'text',
	);
}
?>