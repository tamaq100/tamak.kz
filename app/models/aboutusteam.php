<?php
/**
* AboutUsTeam model
*/
class AboutUsTeam extends BaseModel
{
	public static $table_name = "about_us_team";

	public static $fields = array(
        'id'               => 'integer',
        'idx'			   => 'integer',
        'active'           => 'bool',
        'position'		   => 'string',
        'image'			   => 'string',
        'name'			   => 'string',
	);

}
?>