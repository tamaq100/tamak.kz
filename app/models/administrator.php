<?php
/**
* Модель администратора сайта
*/
class Administrator extends BaseModel
{

    public static $table_name = 'administrators';

    public static $fields = array(
        'id'                     => 'integer',
        'active'                 => 'bool',
        'created'                => 'integer',
        'updated'                => 'integer',
        'name'                   => 'string',
        'login'                  => 'string',
        'super'                  => 'integer',
        'password'               => 'hash',
        'access_rights'          => 'array',
    );

    public function getPassword()
    {
        return '******';
    }

    public function setPassword($value)
    {
        if ($value == '******') {
            return;
        }

        $this->attributes['password'] = $value;
    }
}
