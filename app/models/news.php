<?php
/**
* News model
*/
class News extends BaseModel
{
	public static $table_name = "news";

	public static $fields = array(
		'id'			=> 'integer',
		'idx'			=> 'integer',
		'created'		=> 'integer',
		'updated'		=> 'integer',
		'active'		=> 'bool',
		'title'			=> 'string',
		'preview'		=> 'string',
		'image'			=> 'string',
		'link'			=> 'string',
		'text'			=> 'text',

	);
    public function getPrevious()
    {
        return News::findOneBy(' id = (select max(id) from ' . DB::$TABLE_PREFIX . self::$table_name . ' where id < '.$this->id.')');
    }

    public function getNext()
    {
        return News::findOneBy(' id = (select min(id) from ' . DB::$TABLE_PREFIX . self::$table_name . ' where id > '.$this->id.')');
    }
}
?>