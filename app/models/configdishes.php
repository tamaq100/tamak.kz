<?
/**
 * 
 */
class ConfigDishes extends Config
{
    public static $table_name = 'config_dishes';

    public static $params = array(
    	'title_banner'	=> 'string',
        'subtitle_banner'	=> 'text',
        'banner_bg'		=> 'image',
        'banner_hero'	=> 'image',
        'title_service'	=> 'string',
        'text_service'	=> 'text',
        'image_service'	=> 'image',
        'title_cooperation' => 'string',
        'text_cooperation' => 'text',
        'image_cooperation' => 'image',
    );

}