<?php
/**
* User_data model
*/
class User_data extends BaseModel
{
	public static $table_name = "users_data";
	public static $fields = array(

	'id'			=> 'integer',
	'uid'			=> 'string',
	'active'		=> 'integer',
	'name'			=> 'string',
	'other_data'		=> 'text',
	'description'		=> 'text',
	'phone'			=> 'string',
	'country_and_town'	=> 'text',
	'transport'		=> 'text',
	'photos'		=> 'text',
	'balance'		=> 'string',
	'bonus_balance'		=> 'string',
	'phone_callcenter'	=> 'string',
	'avg_state_rate'	=> 'string',
	'favorite'		=> 'text',
	'payments'		=> 'text',

	);
}
?>