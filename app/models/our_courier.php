<?php
/**
* Our_courier model
*/
class Our_courier extends BaseModel
{
	public static $table_name = "our_couriers";

	public static $fields = array(
	    'id'               => 'integer',
	    'idx'			   => 'integer',
	    'created'		   => 'integer',
	    'updated'		   => 'integer',
	    'active'		   => 'bool',
	    'avatar'		   => 'string',
	    'name'		   	   => 'string',
	    'stars'		   	   => 'integer',
	    'status'		   => 'integer',
	    'last_coord'	   => 'string',
	);

    // public static $required_fields = array(
    //     'telephone'            => 'string',
    //     'name'             => 'string',
    //     'email'            => 'string',
    // );
}
?>
