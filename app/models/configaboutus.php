<?

class ConfigAboutus extends Config
{
    public static $table_name = 'config_about_us';

    public static $params = array(
    	'title'			=> 'string',
        'subtitle'      => 'string',
        'image'         => 'image',
//        'advantages_dyn'	=> 'text',
        // 'job_step'		=> 'dynamic_block_step',
    );

}