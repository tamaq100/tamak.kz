<?php
/**
* Promo model
*/
class Promo extends BaseModel
{
	public static $table_name = "promos";

	public static $fields = array(
	    'id'               	=> 'integer',
	    'idx'			   	=> 'integer',
	    'created'		   	=> 'integer',
	    'updated'		   	=> 'integer',
	    'active'		   	=> 'bool',
	    'title'		   	   	=> 'string',
	    'subtitle'		   	=> 'string',
	    'image'		   	   	=> 'string',
	    'link'		   	   	=> 'string',
	    'conditions'	   	=> 'text',
	    'company'	   	   	=> 'string',
	    'company_link'	   	=> 'string',
	    'price'		   	   	=> 'string',
	    'more'	   		   	=> 'bool',
	    'preview_image'		=> 'string',
	);
	public function getCompanyList()
    {
        $list = Company::findAll();
        if (! $list) return '';
        $list = array_map(function($i) {return $i->name;}, $list);
        return $list;
    }

    public function getPriceWithTG()
    {
    	if (!$this->price)
    		return '';

    	return $this->price . ' тг';

    }
}
?>
