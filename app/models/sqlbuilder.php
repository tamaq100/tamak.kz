<?php 
/**
* Класс для посторенния sql запроса
*/
class SqlBuilder extends DB
{
	public static $table_name = '';
	public $query_values = [];
	// Массив параметров запроса, при execute собираем его в строку
	private $query_params = [
		'query_string'	=> '',
		'join'			=> [],
		'where'			=> [],
		'set'			=> [],
		'order'			=> [],
		'offset'		=> '',
		'limit'			=> '',
		'only_active'	=> '',
	];

 	public static $is_count = false;
	public static $custom_query = '';

	// Функция объявляющая запрос как выбор данных, параметр  - нужные столбцы (array, string),
	// дополнительный параметр для имени таблицы, по умолчанию это таблица модели
	public function select($columns = '*', $tablename = false)
	{
		$this->clearQueryParams();

		$columns = $columns?:'*';
		$tablename = $tablename ?: static::$table_name;
		$tablename = static::$TABLE_PREFIX . $tablename;

		$select_string = 'SELECT ' . $columns . ' FROM ' . $tablename;
		$this->query_params['query_string'] = $select_string;

		if (isset(static::$is_admin) && !static::$is_admin) {
			$this->query_params['only_active'] = true;
		}

		return $this;
	}

	// Функция объявляющая запрос как update
	public function update($tablename = false)
	{
		$this->clearQueryParams();
		$tablename = $tablename ?: static::$table_name;
		$tablename = static::$TABLE_PREFIX . $tablename;

		$update_string = 'UPDATE ' . $tablename;
		$this->query_params['query_string'] = $update_string;
		return $this;
	}

	// Функция объявляющая запрос как insert
	public function insert($tablename = false)
	{
		$this->clearQueryParams();
		$tablename = $tablename ?: static::$table_name;
		$tablename = static::$TABLE_PREFIX . $tablename;

		$insert_string = 'INSERT INTO ' . $tablename;
		$this->query_params['query_string'] = $insert_string;
		return $this;
	}

	// Функция объявляющая запрос как delete
	public function drop($columns = '*', $tablename = false)
	{
		$this->clearQueryParams();
		$tablename = $tablename ?: static::$table_name;
		$tablename = static::$TABLE_PREFIX . $tablename;

		$columns = is_array($columns)?static::$TABLE_PREFIX.implode(', '.static::$TABLE_PREFIX, $columns):$tablename.'.*';

		$delete_string = 'DELETE ' . $columns . ' FROM ' . $tablename;
		$this->query_params['query_string'] = $delete_string;
		return $this;
	}

	// Запоос на количество записей
	public function count($tablename = false)
	{
		$this->clearQueryParams();
		$tablename = $tablename ?: static::$table_name;
		$tablename = static::$TABLE_PREFIX . $tablename;
		
		static::$is_count = true;

		$select_string = 'SELECT COUNT(id) FROM ' . $tablename;
		$this->query_params['query_string'] = $select_string;
		return $this;
	}

	/* Функция для присоеденения нескольких таблиц к запросу,
		Параметр - массив вида [
			имя подключаемой таблицы, 
			первое поле для связи,
			второе поле для связи, 
			(необязатлеьный) ['a.c' => 'b.b', 'c.b' => 'b.a'] - дополнительные связи через and, ко всем именам таблицы приписывает prefix],

	*/
	public function join($options = [])
	{
		// Проверяем наличие нужного количества парметров
		if (count($options) < 3) return $this;
		// сбрасыаем ненужные ключи
		$options = array_values($options);

		$table_connection_string = static::$TABLE_PREFIX . $options[0];
		$current_tablename = static::$TABLE_PREFIX . self::$table_name;
		$first_relation_field = $options[1];
		$second_relation_field = $options[2];

		// Разбиваем имя подключаемой таблицы по as
		preg_match('/(\w*)(\sas\s)?(\w*)?/ui', $table_connection_string, $m);
		$joined_tablename = $m[3] ?: $m[1];

		$and_join = '';
		if (isset($options[3]) && is_array($options[3])) {
			foreach ($options[3] as $first_relation => $second_relation) {
				$and_join .= ' and ' . $first_relation . ' = ' . $second_relation;
			}
		}

		$join_string = $current_tablename . '.' . $first_relation_field . ' = ' . $joined_tablename  . '.' . $second_relation_field . $and_join;

		// Может быть несколько связей
		$this->query_params['join'][$table_connection_string] = $join_string;
		return $this;
	}

	// Стандартная функция для генерации where из прошлой версии дименшона
	// Может принимать как массив так и строку, и число (где число - id)
	public function generateWhere($options = [])
	{

		$this->query_params['where'] = [];

        $comp_operators = array(
            '=' => '=',
            '>' => '>',
            '<' => '<',
            '>=' => '>=',
            '<=' => '<=',
            '<>' => '<>',
            '!=' => '!=',
            '!<' => '!<',
            '!>' => '!>',
            'in' => ' IN ',
            'like' => ' LIKE ',
            'between' => ' BETWEEN ',
        );
        $sql = '1=1 ';
        foreach ($options as $name => $param) {
            // Значение
            $value = is_array($param) ? $param[0] : $param;
            // Оператор сравнения
            $comp = is_array($param) && isset($comp_operators[$param[1]]) ? $comp_operators[strtolower($param[1])] : $comp_operators['='];
            // Логический оператор
            $cond = (is_array($param) && isset($param[2])) ? $param[2] : 'AND';

            if ($comp === $comp_operators['in'] ) {
                // Генерируем список значений
                $values = array();
                foreach ($value as $value_item) {
                    $values[] =  $value_item;
                }

                if (! $values) {
                    continue;
                }
                $value = '('.implode(',', $this->addQuotes($values)) .')';
            } elseif ($comp === $comp_operators['between'] ) {
                $value =  ' '. $this->addQuotes($value[0]) . ' AND '. $this->addQuotes($value[1]) . ' ';
            } else {
                $value = $this->addQuotes($value);
            }

            // Если поля нет в модели, то не надо указывать таблицу
            $table = ! isset(static::$fields[$name])
                ? ''
                : static::$TABLE_PREFIX . static::$table_name . '.';
            $sql .= ' ' . $cond . ' (' . $table . $name . $comp . $value . ')';
        }

		$this->query_params['where'][] = $sql;
		return $this;
	}

	public function addQuotes($value)
	{
		if (is_numeric($value)) {
			return $value;
		}

		if (is_array($value)) {
			for ($i=0; $i < count($value); $i++) { 
				$value[$i] = $this->addQuotes($value[$i]);
			}
			return $value;
		}

		if (trim($value, "\\") != $value) {
			return trim($value, "\\");
		}

		return DB::$db->quote($value);
	}

	// Функция для подготовки where, операясь на знак сравнения
	public function prepareWhereValue($value, $operator)
	{
		switch ($operator) {
			case 'between':
				if (! is_array($value)) return $this->error_report('в качестве value должен приходить массив');
				$string_value = implode(' AND ', $this->addQuotes($value));
				break;

			case 'in':
				if (! is_array($value)) return $this->error_report('в качестве value должен приходить массив');
				$string_value = '(' . implode(', ', $this->addQuotes($value)) . ')';
				break;
			
			default:
				$string_value = $this->addQuotes($value);
				break;
		}

		return $string_value;
	}

	// основная функция для создания where, 
	// Параметр - массив вида [столбец, значение, знак сравнения (по умолчанию это =)]
	// можно передавать значение экранированной через // , тогда в качестве значения будет имя другой колнки
	public function where($options = [], $callback_params = [])
	{
		if (is_string($options)) {
			$this->query_params['where'][] = $options;
			return $this;
		}

		if (is_callable($options)) {
			return $this->groupWhere($options, '', $callback_params);
		}
		
		if (count($options) < 2) return $this;
		$this->query_values[$options[0]] = $options[1];

		// Обнуляем where записи
		$this->query_params['where'] = [];
		$operator = ((isset($options[2]) && $options[2]) ? $options[2]: '=');

		$sql = $options[0] . ' ' . $operator . ' :' . $options[0];

		$this->query_params['where'][] = $sql;		
		return $this;
	}

	// добавление записи(ей) с AND в WHERE,
	// Параметр - массив вида [столбец, значение, знак сравнения]
	public function andWhere($options = [], $callback_params = [])
	{
		if (is_string($options)) {
			$this->query_params['where'][] = 'AND' . $options;
			return $this;
		}

		if (is_callable($options)) {
			return $this->groupWhere($options, 'AND', $callback_params);
		}

		if (count($options) < 2) return $this;
		$this->query_values[$options[0]] = $options[1];

		$operator = ((isset($options[2]) && $options[2]) ? $options[2]: '=');

		$sql = 'AND ' . $options[0] . ' ' . $operator . ' :' . $options[0];

		$this->query_params['where'][] = $sql;
		return $this;
	}

	// добавление записи(ей) с OR в WHERE,
	// Параметр - массив вида [столбец, значение, знак сравнения]
	public function orWhere($options = [], $callback_params = [])
	{
		if (is_string($options)) {
			$this->query_params['where'][] = 'OR' . $options;
			return $this;
		}

		if (is_callable($options)) {
			return $this->groupWhere($options, 'OR', $callback_params);
		}

		if (count($options) < 2) return $this;
		$this->query_values[$options[0]] = $options[1];


		$operator = ((isset($options[2]) && $options[2]) ? $options[2]: '=');

		$sql = 'OR ' . $options[0] . ' ' . $operator . ' :' . $options[0];

		$this->query_params['where'][] = $sql;
		return $this;
	}

	// Функция для сбора where  в группу ()
	private function groupWhere($options, $operator = '', $callback_params = [])
	{
		$prepared_callback_params = [];
		$prepared_callback_params[] = new $this;

		array_push($prepared_callback_params, $callback_params);

		$callback_result = call_user_func_array($options, $prepared_callback_params);
		$group_where_array 	= $callback_result->query_params['where'];
		$query_values = $callback_result->query_values;

		// Генерируем вложенную строку, обрезая OR и AND если они есть в начале строки
		$group_where_string = $operator.'(' . ltrim(ltrim(implode(' ', $group_where_array), ' AND '), ' OR ') . ')';

		$this->query_params['where'][] = $group_where_string;
		$this->query_values = $query_values;
		return $this;
	}
	
	// составление SET () части запроса из массива значений ['столбец' => 'значение'], 
	// можно передавать значение экранированноe через // , тогда в качестве значения будет имя другой колонки
	public function set($values = [])
	{
		if (!is_array($values)) {
			throw new Exception("Неправильные входные данные при построении запроса");
		}
		
		foreach ($values as $column => $value) {
			$this->query_values[$column] = $value;
			$this->query_params['set'][] = '`' . $column . '` = :' . $column;
		}
		return $this;
	}
	
	// Функция для объявления offset
	public function offset($value = 0)
	{
		$this->query_params['offset'] = $value;
		return $this;
	}

	// Функция для объявления limit
	public function limit($limit = 1000)
	{
		$this->query_params['limit'] = $limit;
		return $this;
	}

	// Функция для  добавления ORDER by
	public function orderBy($order_by = 'id ASC')
	{
		$this->query_params['order'][] = $order_by;
		return $this;
	}

	// Функция выполняющая собранный запрос и возвращаяющая первый элемент массива результатов
	public function one($row_number = 0)
	{
		$this->createDefaultValuesForWhereClause();

		$query = $this->prepare();
		// var_dump($query);
		$qRS = DB::query($query, $this->query_values);

		if ($qRS->rowCount()) {
			$qRS->setFetchMode(PDO::FETCH_CLASS, get_called_class());
			$result = $qRS->fetch($row_number);
		} else {
			$result = false;
		}
		
		return $result;
	}
	
	// Функция выполняющая собранный запрос и возвращаяющая весь массив результатов
	public function all()
	{
		$this->createDefaultValuesForWhereClause();

		$query = $this->prepare();
		$qRS = DB::query($query, $this->query_values);

		if ($qRS->rowCount()) {
			$qRS->setFetchMode(PDO::FETCH_CLASS, get_called_class());
			$results = $qRS->fetchAll();
		} else {
			return [];
		}

		$new_results = [];
        foreach ($results as $row) {
            $new_results[$row->id] = $row;
        }

		return $new_results;
	}

	public function customQuery($query)
	{
		$this->custom_query = $query;
		return $this;
	}

	// Склеиваем все параметры в единый запрос, возвращаем запрос, полезно для дебага
	public function prepare()
	{		
		// Если запрос сгенерирован вручную, то выполняем его
		if (static::$custom_query) {
			return $query;
		}

		$query_string = $this->query_params['query_string'];

		$join = '';
		if (! empty($this->query_params['join'])) {
			$join = 'LEFT JOIN ';
			foreach ($this->query_params['join'] as $table => $join_string) {
				$join .= $table . ' ON ' . $join_string . ' ';
			}
			rtrim($join);
		}

		$where = '';
		if (! empty($this->query_params['where'])) {
			$where = 'WHERE ' . implode(' ', $this->query_params['where']);
			if (isset($this->query_params['only_active']) && $this->query_params['only_active']) {
				$where .= ' AND active = 1';
			}
		}

		$set = '';
		if (! empty($this->query_params['set'])) {
			$set = 'SET ' . implode(', ', $this->query_params['set']);
		}

		$order_by = (!empty($this->query_params['order']) ? 'ORDER BY ' . implode(', ', $this->query_params['order']) : '');

		$offset = $this->query_params['offset'] ? 'OFFSET ' . $this->query_params['offset'] : '';

		$limit = $this->query_params['limit'] ? 'LIMIT ' . $this->query_params['limit'] : '';

		$query = $query_string . ' ' . $join . ' ' . $set . ' ' . $where . ' ' . $order_by . ' ' . $limit . ' ' . $offset;

		return $query;
	}

	// Функция для выполнения запроса, возвращает true|false
	public function execute()
	{

		$query = $this->prepare();

		$qRS = DB::query($query, $this->query_values);

		// Костыль для вывода запроса  на COUNT
		if (static::$is_count) {
			$rowCount = $qRS->fetchColumn(0);
			return $rowCount;
		}	

		return (bool)$qRS->rowCount();
	}

	// Функция для вывода ошибок
	public function error_report($string = '')
	{
		die(var_dump($string));
	}

	// Функция для вставки деволтных значенийй для order и limit
	public function createDefaultValuesForWhereClause()
	{
		if (! $this->query_params['limit']) {
			$this->query_params['limit'] = '1000';
		}

		// Если есть idx, то сортируем по нему
		if (empty($this->query_params['order'])) {
			if (isset(static::$fields) &&  in_array('idx', array_keys(static::$fields))) {
				$this->query_params['order'][] = 'idx ASC';
			} else {
				$this->query_params['order'][] = 'id ASC';
			}
		}

		return $this;
	}

	// Функция для удаления всех данных из таблицы
	public function emptyTable()
	{
        $SQL = 'TRUNCATE `'.static::$TABLE_PREFIX.static::$table_name.'`';
        $qRS = static::query($SQL);
	}

	// Функция для очистки query_params от предыдущих запросов
	public function clearQueryParams()
	{
		$this->query_params = [
			'query_string'	=> '',
			'join'			=> [],
			'where'			=> [],
			'set'			=> [],
			'order'			=> [],
			'offset'		=> '',
			'limit'			=> '',
		];
		
		$this->query_values = [];
	}
}