<?
/**
 * 
 */
class ConfigAuto extends Config
{
    public static $table_name = 'config_auto';

    public static $params = array(
    	'title'			=> 'string',
        'subtitle'      => 'string',
        'banner'		=> 'image',
        'banner_hero'	=> 'image',
        'auto_isset'	=> 'string',
        'auto_text'		=> 'text',
        // 'advantages_dyn'	=> 'text',
        // 'job_step'		=> 'dynamic_block_step',
    );

}