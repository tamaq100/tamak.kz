<?php
/**
* Banner_slider model
*/
class Banner_slider extends BaseModel
{
	public static $table_name = "banner_sliders";

	public static $fields = array(
	    'id'               => 'integer',
	    'idx'			   => 'integer',
	    'active'		   => 'bool',
	    'title'		   	   => 'string',
	    'text'             => 'string',
	    'subtitle'         => 'string',
	    'link'		   	   => 'string',
	    'typecolor'        => 'string',
	    'image'			   => 'string',
	);
    // public function getTypecolor_List()
    // {
    //     $list = Color_palette::findAll();
    //     if (! $list) return '';
    //     $list = array_map(function($i) {return $i->name;}, $list);
    //     return $list;
    // }
    // public static $required_fields = array(
    //     'telephone'            => 'string',
    //     'name'             => 'string',
    //     'email'            => 'string',
    // );

    public function getColorStyle()
    {
    	$result = '';

    	if ($this->typecolor) {
	    	$result = 'style="color:'.$this->typecolor.'"';
    	}

    	return $result;
    }
}