<?php
/**
* Popular_restoraunt model
*/
class Popular_restoraunt extends BaseModel
{
	public static $table_name = "popular_restoraunts";

	public static $fields = array(
	    'id'               => 'integer',
	    'idx'			   => 'integer',
	    'created'		   => 'integer',
	    'updated'		   => 'integer',
	    'active'		   => 'bool',
	    'title'		   	   => 'string',
	    'citchen'		   => 'text',
	    'min_price'		   => 'string',
	    'max_price'		   => 'string',
	    'jobtime_min'	   => 'string',
	    'jobtime_max'	   => 'string',
	    'image'			   => 'string',
	    'service_id'	   => 'string',
	);
}
?>
