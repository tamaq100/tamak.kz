<?php
/**
* AutoHowTo model
*/
class AutoHowTo extends BaseModel
{
	public static $table_name = "auto_howto";

	public static $fields = array(
        'id'               => 'integer',
        'idx'			   => 'integer',
        'active'           => 'bool',
        'title'			   => 'string',
        'subtitle'		   => 'string',
	);
}
?>