<?php
/**
* Category_button model
*/
class Category_button extends BaseModel
{
	public static $table_name = "category_buttons";

	public static $fields = array(
		'id'			=> 'integer',
		'idx'			=> 'integer',
		'active'		=> 'bool',
		'name'			=> 'string',
		'image'			=> 'string',
		'link'			=> 'string',
	);
}
?>