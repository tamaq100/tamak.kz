<?php
/**
* AutoAdvantages model
*/
class AutoAdvantages extends BaseModel
{
	public static $table_name = "autoadvantages";

	public static $fields = array(
        'id'               => 'integer',
        'idx'			   => 'integer',
        'active'           => 'bool',
        'image'            => 'string',
        'title'			   => 'string',
        'subtitle'		   => 'string',
	);
}
?>