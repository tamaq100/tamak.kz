<?php

/**
 * Базовый класс для всех моделей
 */
class BaseModel extends SqlBuilder {

    /**
     * Флаг админки.
     * Если положителен - доступны неактивные сущности
     * @var boolean
     */
    public static $is_admin = false;

    /**
     * Все поля модели
     * @var array
     */
    public static $fields = [];

    /**
     * Все значения полей модели
     * @var array
     */
    public $attributes = [];

    /**
     * Поля обязательные к заполнению
     * @var array
     */
    public static $required_fields = [];

    // Ну короче кто-то сломал Lang_versions
    // Дабы всё работало норм, нуно писать 'ru'
    // дабы выполнить миграцию пишите 'ru'=> true,
    // P.S: Спасибо что живой
    public static $lang_versions = [
        'ru',
    ];

    function __construct() 
    {
        foreach (array_keys(static::$fields) as $field) {
            if (! isset($this->attributes[$field])) {
                if ($field == 'active') {
                    $this->setFieldValue($field, 1);
                } elseif (in_array($field, ['created', 'updated'])) {
                    $this->setFieldValue($field, time());
                } else {
                    $this->setFieldValue($field, false);
                }
            }
        }
    }

    /**
     * Basic setter
     * @param $field
     * @param $value
     * @return $this
     */
    function __set($field, $value)
    {
        if (method_exists($this, 'set'.ucfirst($field))) return $this->{'set'.ucfirst($field)}($value);

        if(! isset(static::$fields[$field])) return $this;
        $field_type = static::$fields[$field];

        if (method_exists($this, 'setFieldWithType'.ucfirst($field_type))) return $this->{'setFieldWithType'.ucfirst($field_type)}($field, $value);

        // todo throw exception
        // Запрещаем изменять id
        // if ($field == 'id' && $this->id) return $this;
        // 
        switch ($field_type) {
            case 'hash':
            case 'string':
                $this->attributes[$field] = strip_tags($value, '<br>');
                break;
            case 'text':
            case 'image':
            case 'file':
                $this->attributes[$field] = $value;
                break;
            case 'bool':
                $this->attributes[$field] = ($value === 'true' || $value === 1 || $value === '1') ? 1 : 0;
                break;
            case 'null':
                break;
            case 'float':
                $this->attributes[$field] = floatval($value);
                break;

            case 'array':
                if (is_array($value)) {
                    $value = json_encode($value, JSON_HEX_QUOT | JSON_HEX_TAG | JSON_UNESCAPED_UNICODE);
                }

                $this->attributes[$field] = $value;
                break;
            case 'object':
                if (is_array($value)) {
                    $value = json_encode($value, JSON_HEX_QUOT | JSON_HEX_TAG | JSON_UNESCAPED_UNICODE);
            }

                $this->attributes[$field] = $value;
                break;

            default:
                $this->attributes[$field] = intval($value);
                break;
        }

        return $this;
    }

    /**
     * Basic getter
     */
    function __get($field)
    {
        if ($relation_name = $this->needRelationName($field)) {
            return $relation_name;
        }

        if ($relation_name = $this->needRelationObject($field)) {
            return $relation_name;
        }

        if (method_exists($this, 'get'.ucfirst($field))) return $this->{'get'.ucfirst($field)}();

        if(! isset(static::$fields[$field])) return '';

        $field_type = static::$fields[$field];

        if (method_exists($this, 'getFieldWithType'.ucfirst($field_type))) return $this->{'getFieldWithType'.ucfirst($field_type)}($field);

        if (!isset($this->attributes[$field])) return '';


        if(isset($field_type)) {
            switch ($field_type) {
                case 'bool':
                    $lang_key = $this->getDefaultValue($field) ? 'yes' : 'no';
                    return __($lang_key);
                    break;

                case 'text':
                    $result = $this->attributes[$field];

                    $result = stripcslashes($result);
                    return $result;
                    break;

                case 'array':
                    $result = $this->attributes[$field];
                    $result = json_decode($result, true);

                    if (! is_array($result)) {
                        return array();
                    }

                    return $result;
                    break;

                case 'object':
                    $result = $this->attributes[$field];
                    $result = json_decode($result);

                    if (! is_object($result)) {
                        return false;
                    }

                    return $result;
                    break;

                case 'image':
                    $result = $this->attributes[$field];

                    return $result;

                    break;

                default:
                    return $this->attributes[$field];

                    break;
            }
        }
    }

    public function getDefaultValue($field)
    {
        return isset($this->attributes[$field])?$this->attributes[$field]:'';
    }

    public static function getClassName() {
        return get_called_class();
    }

    /**
     * При установке idx поля, нужен хитрый выбор значений
     * Если ничего не передано и сущность сохранена, ставим ей значение ее id
     * Если же ничего не получилось, и у idx все-равно неправильное значение,
     * ищем последнюю сущность и ставим следующее, за ее id, значение
     * Если все равно все пусто, обнуляем значение
     */
    public function setIdx($value)
    {

        $this->setFieldValue('idx', $value);

        if (! $this->idx) {
            $this->setFieldValue('idx', $this->id);
        }


        $model = $this->getClassName();
        if (! $this->idx && $last_entity = $model::findOneBy('idx IS NOT NULL and idx != 0', 'id DESC')) {
            $this->setFieldValue('idx', ++$last_entity->id);
        }

        if (! $this->idx) {
            $this->setFieldValue('idx', 0);
        }

        return $this;
    }

    public function getMessage()
    {
        return str_replace(['\r\n', '\r', '\n', '\n\r'], '<br />', $this->attributes['message']);
    }

    /**
     * @return id
     */
    public function getId()
    {
        return $this->attributes['id']?:false;
    }

    /**
     * @param $id
     * @return $this|bool
     */
    public function load($id)
    {
        if (! $id) return false;
        $result = $this->select()->where(['id', $id])->one();

        if ($result) {
            foreach (array_keys(static::$fields) as $field) {
                $this->$field = $result->getDefaultValue($field);
            }

            return $this;
        }

        return $result;
    }

    public function delete()
    {
        $this->drop()->where(['id', $this->id])->execute();
    }

    /**
     * Главный метод сохранения.
     * Переменная $errors нужна при валидации
     */
    public function save(&$errors = array())
    {

        if (is_array($this->validate($errors)) && (! empty($this->validate($errors)))) {
            return $errors;
        }

        $this->updated = time();
        $values = [];
        foreach (array_keys(static::$fields) as $field) {
            if (in_array($field, array('id'))) continue;
            // invoke furl creating
            if ($field == 'furl') {
                if ($this->$field != '') {
                    $text = $this->$field;
                } else if ($this->title != '') {
                    $text = $this->title;
                } else if (isset($this->name) && ($this->name != '')) {
                    $text = $this->name;
                } else {
                    $text = 'material';
                }

                $class  = $this->getClassName();
                $id     = $this->id;
                $value  = $this->createFurl($text, $class, $id);
                $this->$field = $value; 
            }

            $values[$field] = $this->getDefaultValue($field);
        }

        if($this->id) {
            $this->update()->where(['id', $this->id])->set($values);
        } else {
            $this->insert()->set($values);
        }

        $qRS = $this->execute();
        if(!$this->id) {
            $this->id = static::$db->lastInsertId();
        }
    }

    /**
     * @return array data
     */
    public static function findAll()
    {
        return self::findBy();
    }

    public function massiveSetter($data)
    {
        foreach ($data as $key => $value) {
            // Пропускаем "системные" поля
            if (in_array($key, array('id', 'created', 'updated', 'active'))) continue;

            // Пропускаем поля которые не нужно заполнять
            if (in_array($key, array())) continue;

            $value = strip_tags($value);
            $this->$key = $value;
        }

        return $this;
    }

    public static function findOneBy($where = array(), $orderBy = '')
    {
        $result = self::findBy($where, $orderBy, 1, 0);
        if ($result) {
            $result = current($result);
        }

        return $result;
    }

    public static function findBy($where = array(), $orderBy = '', $limit = 10000, $offset = 0)
    {
        $current_class_name = get_called_class();
        $entity = new $current_class_name;
        $entity->select();
        // die(var_dump( $entity ));
        // die(var_dump( $where ));
        if (empty($where) || !$where) {
            $entity->where('1 = 1');
        } elseif (is_numeric($where)) {
            $entity->where(['id', $where]);
        } elseif (is_string($where)) {
            $entity->where($where);
        } else {
            $entity->generateWhere($where);
        }

        if (! self::$is_admin) {
            $entity->andWhere(['active', 1]);
        }

        if ($orderBy) {
            $entity->orderBy($orderBy);
        } elseif (isset(static::$fields['idx'])) {
            $entity->orderBy('idx ASC');
        }

        $entity->limit($limit);
        $entity->offset($offset);
        $result = $entity->all();
        if (! $result) return false;

        return $result;
    }


    /**
     * Проверка на существование постфикса _id
     */
    public function ifRelationField($field)
    {
        $result = false;
        $postfix = substr($field, -3);
        $method_without_postfix = substr($field, 0, strlen($field) - 3);
        if ($postfix == '_id' && !method_exists($this, 'get'.ucfirst($method_without_postfix))) {
            $result = $method_without_postfix;
        }

        return $result;
    }

    /**
     * Получение всех записей зависимой модели
     */
    public function getRelationEntities($field)
    {
        $relation_entity_name = $this->ifRelationField($field);
        $relation_entity_name = mb_convert_case(strtolower($relation_entity_name), MB_CASE_TITLE, "UTF-8");
        // Если ошибка на этой строчке,
        // вероятнее всего не создан get метод в родительсткой модели
        $relation_entities = $relation_entity_name::findAll();

        return $relation_entities;
    }

    public function getCreated()
    {
        return date('d.m.Y H:i', $this->attributes['created']);
    }

    public function getUpdated()
    {
        return date('d.m.Y H:i', $this->attributes['updated']);
    }

    public function getFieldWithTypeDatetime($field)
    {
        if (! $this->attributes[$field]) {
            return date('d.m.Y H:i');
        }
        return date('d.m.Y H:i', $this->attributes[$field]);
    }

    public function getFieldWithTypeDate($field)
    {
        if (! $this->attributes[$field]) {
            return date('d.m.Y');
        }
        return date('d.m.Y', $this->attributes[$field]);
    }

    public function setFieldWithTypeDate($field, $value)
    {
        // Грязный хак проверки на unixtimestamp
        if ($value == 0 || (int)$value > 1000) {
            $this->attributes[$field] = $value;
        } else {
            $this->attributes[$field] = strtotime($value.' 00:00:00 ');
        }

        return $this;
    }

    public function setFieldWithTypeDatetime($field, $value)
    {
        // Грязный хак проверки на unixtimestamp
        if ($value == 0 || (int)$value > 1000) {
            $this->attributes[$field] = $value;
        } else {
            $this->attributes[$field] = strtotime($value.':00');
        }
    }

    public function getActive()
    {
        $lang_key = $this->attributes['active'] ? 'yes' : 'no';
        return __($lang_key);
    }


    /**
     * Формируем урл сущности с приоритетом 
     * на ссылку если есть
     * @return string относительная ссылка
     */
    public function getEntityUrl()
    {
        if (isset($this->link) && $this->link) {
            return $this->link;
        } 
        if (isset($this->furl) && $this->furl) {
            if (isset($this->parent_id)) {
                return static::prepareFullFurl($this, '/' . $this->furl);
            } else {
                return strtolower($this->getClassName()) . '/' . $this->furl;
            }
        }
        return strtolower($this->getClassName()) . '/show/' . $this->id;
    }

    public static function prepareFullFurl($entity, $ret)
    {
        $Model = get_called_class();
        $parent = $Model::findOneBy($entity->parent_id);
        if ($parent && ! is_array($parent) && ! in_array($entity->parent_id, array(19, 20))) {
            $ret = '/' . $parent->furl . $ret;
            return static::prepareFullFurl($parent, $ret);
        } else {
            return $ret;
        }
    }

    /**
     * Метод сохраняет одно изображение
     *
     * Метод сохранения изображения разделен для того,
     * чтобы была возможность сохранять множественные изображения (name="image[]")
     */
    public function saveImage($field)
    {
        return $this->_saveImage($field);
    }

    /**
     * Базовый метод для сохранения 
     */
    public function _saveImage($field, $id = false)
    {
        if (!isset($_FILES[$field])) return false;

        $name       = $id && is_array($_FILES[$field]['name'])     ? $_FILES[$field]['name'][$id]     : $_FILES[$field]['name'];
        $tmp_name   = $id && is_array($_FILES[$field]['tmp_name']) ? $_FILES[$field]['tmp_name'][$id] : $_FILES[$field]['tmp_name'];
        $type       = $id && is_array($_FILES[$field]['type'])     ? $_FILES[$field]['type'][$id]     : $_FILES[$field]['type'];
        $size       = $id && is_array($_FILES[$field]['size'])     ? $_FILES[$field]['size'][$id]     : $_FILES[$field]['size'];

        if (is_uploaded_file($tmp_name) and
            $size and
            substr($type, 0, 6) == "image/" and
            (strstr($type, "jpeg") or
             strstr($type, "gif") or
             strstr($type, "png"))) {
            $imageid = md5(uniqid(rand()));
            $image_field = isset($this->$field) ? $field : 'media';

            if (isset($this->$image_field) && $this->$image_field){
                unlink_media($this->$image_field);
            }

            $this->$field = $imageid;
            
            $imagename = "../runtime/_media/" . $imageid . ".".strtolower(substr($name, -3));
            move_uploaded_file($tmp_name, $imagename); 
            chmod($imagename, 0777);

            return $imageid;
        }

        return false;
    }

    /**
     * Метод сохраняет один файл
     *
     * Метод сохранения файла разделен для того,
     * чтобы была возможность сохранять множественные файлы (name="file[]")
     */
    public function saveFile($field)
    {
        return $this->_saveFile($field);
    }

    /**
     * Базовый метод для сохранения 
     */
    public function _saveFile($field, $id = false)
    {
        if (!isset($_FILES[$field])) return false;

        $name       = $id && is_array($_FILES[$field]['name'])     ? $_FILES[$field]['name'][$id]     : $_FILES[$field]['name'];
        $tmp_name   = $id && is_array($_FILES[$field]['tmp_name']) ? $_FILES[$field]['tmp_name'][$id] : $_FILES[$field]['tmp_name'];
        $type       = $id && is_array($_FILES[$field]['type'])     ? $_FILES[$field]['type'][$id]     : $_FILES[$field]['type'];
        $size       = $id && is_array($_FILES[$field]['size'])     ? $_FILES[$field]['size'][$id]     : $_FILES[$field]['size'];

        /**
         * Тут мы подлавливаем тех кто хочет загрузить файл
         * проверяем разрешен ли этот файл по:
         * размеру - не более 24мб
         * расширению - ниже массив
         * типу - ниже массив
         */
        $allowedExts = array('doc', 'docx', 'pdf', 'jpg', 'gif', 'png', 'zip', 'rar');
        $extension = explode(".", $name);
        $extension = end($extension);
        $allowedTypes = array(
            'application/pdf',
            'application/msword',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'application/zip',
            'image/jpeg',
            'image/png',
            'image/gif',
            'application/x-rar-compressed',
            'application/octet-stream',
            );
        if ($size > 25165824 ||
            !in_array($type, $allowedTypes) ||
            !in_array($extension, $allowedExts)) {
            return false;
        }        

        if (is_uploaded_file($tmp_name) and $size) {
            $ext = explode('.', $name);
            $ext = end($ext);

            $fileid = md5(uniqid(rand())). ".".$ext;
            if (isset($this->$field) && $this->$field){
                unlink(WORK_DIR . '/userfiles/' . $this->$field);
            }

            $this->$field = $fileid;

            $filename = WORK_DIR . "/userfiles/" . $fileid;
            move_uploaded_file($tmp_name, $filename); 

            chmod($filename, 0777);

            return $fileid;
        }

        return false;
    }

    /**
     * Метод валидации. Пока в зачаточном состоянии, и ничего не делает.
     * Все валидация индивидуальна и происходит в той модели к которой применяется
     * @param  [type] &$errors Массив ошибок. Передается из функции по ссылке
     */
    public function validate(&$errors)
    {
        if (static::$is_admin) return true;

        foreach (static::$fields as $key => $type) {
            // Пропускаем "системные" поля
            if (in_array($key, array('id', 'created', 'updated'))) continue;

            // чекаем обязательные поля
            if  (isset(static::$required_fields[$key])) {
                if (method_exists($this, 'validate'.ucfirst($key)) 
                    && $error = $this->{'validate'.ucfirst($key)}()) {
                    $errors[$key][] = $error;
                } elseif(! trim($this->$key)) {
                    $errors[$key][] = __('validation.empty_field');
                }
            }
        }
    }

    public static function getCount($where = array())
    {
        $SQL = 'SELECT count(*) as cnt FROM ' . static::$TABLE_PREFIX . static::$table_name;

        $SQL .= ' WHERE 1 = 1 ';
        if (is_string($where)) {
            $SQL .= $where;
        } else {
            $where and $SQL .= static::generateWhere($where);
        }

        $db = new DB;
        $qRS = $db::query($SQL);

        $row = $qRS->fetch(PDO::FETCH_COLUMN);
        return $row;
    }

    public function getNameForInput()
    {
        if (isset(static::$fields['title'])) {
            return $this->title;
        }
        if (isset(static::$fields['name'])) {
            return $this->name;
        }

        return 'Field with title not found';
    }


    /**
     * Генерация ЧПУ
     * @param  string  $text       Название материала
     * @param  boolean $model      Модель
     * @param  boolean $id         
     * @return string              ЧПУ
     */
    public static function createFurl($text, $model=false, $id=false){
        if (! $text) {
            return md5(uniqid());
        }
        $text = iconv('UTF-8', 'CP1251', trim($text));
        $repl = array("ё"=>'yo', "х"=>'h', "ь"=>'', "Ь"=>'', "Ъ"=>'', "ъ"=>'', "ц"=>'ts',  "ч"=>'ch', "ш"=>'sh',"щ"=>'shch',  "ю"=>'yu', "я"=>'ya',"Ё"=>'yo', "Х"=>'h',  "Ц"=>'ts',  "Ч"=>'ch', "Ш"=>'sh',"Щ"=>'shch',  "Ю"=>'yu', "Я"=>'ya');
        $repln = array();
        foreach($repl as $k => $v){
            $repln[iconv('UTF-8', 'CP1251', $k)] = $v;
        }
        $text = strtr($text, $repln);
        $text = strtr($text,iconv('UTF-8', 'CP1251',
            "абвгдежзийклмнопрстуфыэАБВГДЕЖЗИЙКЛМНОПРСТУФЫЭ"),
            "abvgdegziyklmnoprstufieabvgdegziyklmnoprstufie");
        $text = strtolower(preg_replace ("/(\W+)/i", "_", $text));
        $ret = iconv('CP1251', 'UTF-8', $text);
        $ret = trim($ret, '_');
        if ($model){
            $entity = $model::findOneBy(array('furl' => $ret));
            if ($entity && $entity->id != $id) {
                return BaseModel::createFurl($ret.rand(1,9), $model, $id);
            }
        }
        return $ret;
    }


    public function deleteImage($field)
    {
        if ($this->$field) {
            unlink_media($this->$field);
        }

        $this->$field = '';

        return $this;
    }

    public function deleteFile($field)
    {
        if ($this->$field) {
            unlink(PUBLIC_DIR . '/userfiles/' . $this->$field);
        }

        $this->$field = '';

        return $this;
    }
   

    public static function getBreadcrumbsStaticly($breadcrumbs)
    {
        $controller     = new BaseController;
        $breadcrumbs    = $controller->getBreadcrumbs($breadcrumbs);
        return $breadcrumbs;
    }

    public static function findAllAndSortByCategory($category_model, $parent_field = 'parent_id')
    {
        $result = [];
        $items = static::findAll();
        $categories = $category_model::findAll();
        foreach ($categories as $category) {
            foreach ($items as $item) {
                if ($item->{$parent_field} == $category->id) {
                    $result[$category->id][$item->id] = $item;
                }
            }
        }
        return $result;
    }

    // Если есть ошибка что второй параметр не вызывается, возможно у тебя есть поле FieldValue и оно попадает под перегрузку
    public function setFieldValue($field, $value)
    {
        $this->attributes[$field] = $value;
        return $this;
    }

    public function needRelationName($field)
    {
        $result = false;
        $postfix = substr($field, -5);
        $method_without_postfix = substr($field, 0, strlen($field) - 5);
        $relation_model = ucfirst($method_without_postfix);

        if ($postfix == '_name' && !method_exists($this, 'get' . ucfirst($field))) {
            if ($relation_id = $this->getDefaultValue($method_without_postfix . '_id')) {
                if ($relation = $relation_model::findOneBy($relation_id)) {
                    return $relation->getNameForInput();
                }
            }
        }

        return false;
    }

    public function needRelationObject($field)
    {
        $result = false;
        $postfix = substr($field, -7);
        $method_without_postfix = substr($field, 0, strlen($field) -7);
        $relation_model = ucfirst($method_without_postfix);

        if ($postfix == '_object' && !method_exists($this, 'get' . ucfirst($field))) {
            if ($relation_id = $this->getDefaultValue($method_without_postfix . '_id')) {
                if ($relation = $relation_model::findOneBy($relation_id)) {
                    return $relation;
                }
            }
        }

        return false;
    }
}