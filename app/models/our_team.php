<?php
/**
* Our_team model
*/
class Our_team extends BaseModel
{
	public static $table_name = "our_teams";

	public static $fields = array(
	    'id'               => 'integer',
	    'idx'			   => 'integer',
	    'active'		   => 'bool',
	    'avatar'		   => 'string',
	    'name'		   	   => 'string',
	    'position'	   	   => 'string',
	);
}
?>
