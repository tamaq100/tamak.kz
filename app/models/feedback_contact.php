<?php
/**
* Feedback_contact model
*/
class Feedback_contact extends BaseModel
{
	public static $table_name = "feedback_contacts";

	public static $fields = array(
		'id'			=> 'integer',
		'created'		=> 'integer',
		'updated'		=> 'integer',
		'name'			=> 'string',
		'telephone'		=> 'string',
		'email'			=> 'string',
		'message'		=> 'text',
		'moderated'		=> 'bool',
	);
    public static $required_fields = array(
        'telephone'        => 'string',
        'name'             => 'string',
        'email'            => 'string',
    );
}
?>