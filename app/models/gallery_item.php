<?php
/**
* Gallery_item model
*/
class Gallery_item extends BaseModel
{
    public static $table_name = 'gallery_items';

    public static $fields = array(
        'id'                  => 'integer',
        'idx'                 => 'integer',
        'active'              => 'bool',
        'created'             => 'integer',
        'updated'             => 'integer',
        'media'               => 'image',
        'name'                => 'string',
        'gallery_id'          => 'integer',
        'type'                => 'string',
    );

    public function getNameForInput()
    {
        return $this->get('name');
    }

    public function sort_items($order_list, $describe_list)
    {
        $order = 1;
        foreach ($order_list as $item) {
            $image = Gallery_item::load($item);
            $image->idx = $order;
            $image->name = $describe_list[$item];
            $image->save();
            $order++;
        }
    }
}
