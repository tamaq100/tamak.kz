<?php
/**
* Модель администратора сайта
*/
class Config extends BaseModel
{

    public static $table_name = 'config';

    public static $fields = array(
        'id'      => 'integer',
        'active'  => 'bool',
        'created' => 'integer',
        'updated' => 'integer',
        'title'   => 'string',
        'type'    => 'string',
        'string'  => 'string',
        'text'    => 'text',
        'bool'    => 'bool',
        'integer' => 'integer',
    );

    public static $params = array(
        'admin_mails'                   => 'string',
        'instagram'                     => 'string',
        'facebook'                      => 'string',
        'twitter'                       => 'string',
        'vk'                            => 'string',
        'metaTitleMain'                 => 'string',
        'metaDescriptionMain'           => 'string',
        'metaKeywordsMain'              => 'string',
        'finder_popular'                => 'string',
        'phone'                         => 'string',
    );

    public static function getValues()
    {
        $values = [];
        $model = get_called_class();
        $entity = new $model;

        foreach (static::$params as $name => $type) {
            if ($type == 'image') {
               $type = 'string';
            }
            if($type == 'plupload_gallery') continue;
            $value = $entity->select($type)
                            ->where(['title', $name])
                            ->one();

            $value = (isset($value->attributes[$type])) ? $value->attributes[$type] : '';

            $values[$name] = $value;    
        }

        return $values;

    }

    public static function get($key)
    {
        $model = get_called_class();
        $entity = new $model;


        $config_type = static::$params[$key];

        $test = 0;
        if ($config_type == 'image') {
            $config_type = 'string';
        }

        $config_row = $entity->select($config_type)
                        ->where(['title', $key])
                        ->one();

        $config_value = (isset($config_row->attributes[$config_type])) ? $config_row->attributes[$config_type] : '';

        return $config_value;
    }

}