<?php
/**
* City model
*/
class City extends BaseModel
{
	public static $table_name = "citys";

	public static $fields = array(
		'id'			=> 'integer',
		'idx'			=> 'string',
		'active'		=> 'bool',
		'created'		=> 'integer',
		'updated'		=> 'integer',
		'key'			=> 'string',
		'value_ru'		=> 'string',
		'value_en'		=> 'string',
		'value_kz'		=> 'string',
		'lat'			=> 'string',
		'lng'			=> 'string',
	);
	
}
?>