<?php

class DB 
{

	public static $log = array();
	public static $db = '';
	public static $TABLE_PREFIX = 'ru_';

	public static function connect() 
	{
		if (getenv('ENV') == 'docker_dev')
		{
				$dsn = 'mysql:dbname=tamaq-kz;host=mysql55;charset=utf8';
				$user = "root";
				$password = "";
				// $dsn        = 'mysql:dbname=tamaq-kz;host=mysql55;charset=utf8';
				// $password   = "";
		} elseif ($_SERVER["REMOTE_ADDR"] == '127.0.0.1') {
				$dsn = 'mysql:dbname=tamaq-kz;host=localhost;charset=utf8';
				$user = "root";
				$password = "";
		} elseif (in_array($_SERVER['SERVER_NAME'], array(
				'tamaq.4dclick.asia',
		))) {
				if ($_SERVER["HTTPS"] != "on") {
						header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
						exit();
				}
				$dsn = 'mysql:dbname=asia4d_tamaq;host=localhost;charset=utf8';
				$user = "asia4d_tamaq";
				$password = "iIlHviznVi";
		} else {
				$dsn = 'mysql:dbname=tamaq_site;host=127.0.0.1;charset=utf8';
				$user = "site";
				$password = "BHVkuqUs";

		}

		try {
			self::$db = new PDO($dsn, $user, $password);
			if ($GLOBALS['enviropment'] == 'dev') {
			}
			self::$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
		} catch (PDOException $e) {
			die('Подключение не удалось: ' . $e->getMessage());
		}
	}

	/**
	 * Враппер для mysql_query функции
	 * Нужен для обработки ошибок и логгирования
	 * @param  string   $sql SQL-запрос
	 * @return resource
	 */
	public static function query($sql, $params = []) 
	{
		if (!$sql) {
			return error($sql);
		}

		$stmt = self::$db->prepare($sql);

		$start = microtime(true);

		$qRS = $stmt->execute($params) or self::error($sql);

		$execution_time = (microtime(true) - $start) * 1000;
		// $count = $qRS ?:$qRS->fetchColumn();
		$count = 1;
		self::log(get_called_class(), $sql, $execution_time, $count);

		return $stmt;
	}

	public static function error($sql) 
	{
		die("<hr size=\"1\"><b>Не удалось выполнить:</b> \"" . SqlFormatter::format($sql) . "\"<br> Код ошибки " . self::$db->errorInfo()[0]);
	}

	private static function log($called_class, $query, $time, $count) 
	{
		self::$log[] = array(
			'time' => $time,
			'class_name' => $called_class,
			'query' => $query,
			'count' => $count,
		);
	}
}