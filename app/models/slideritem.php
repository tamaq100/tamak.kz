<?php
/**
* Gallery_item model
*/
class SliderItem extends BaseModel
{
    public static $table_name = 'slider';

    public static $fields = array(
        'id'                  => 'integer',
        'idx'                 => 'integer',
        'created'             => 'integer',
        'updated'             => 'integer',
        'active'              => 'bool',
        'text'                => 'string',
        'image'               => 'image',
        'image_large'         => 'image',
        'title'               => 'string',
        'background'          => 'image',
    );

}