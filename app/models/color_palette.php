<?php
/**
* Color_palette model
*/
class Color_palette extends BaseModel
{
	public static $table_name = "color_palettes";

	public static $fields = array(
		'id'			=> 'integer',
		'idx'			=> 'integer',
		'active'		=> 'bool',
		'color'			=> 'string',
	);

}
?>