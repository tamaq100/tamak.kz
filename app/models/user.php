<?php
/**
* User model
*/
class User extends BaseModel
{
	public static $table_name = "users";

	public static $fields = array(
        'id'               => 'integer',
        'uid'			   => 'string',
        'tel'			   => 'string',
        'role'			   => 'string',
        'name'			   => 'string',
	);
	public static function logout(){

		unset($_SESSION['uid']);
		unset($_SESSION['role']);

	}
	public static function IsAuth()
	{
		if (isset($_SESSION['uid'])) {
			return($_SESSION['uid']);
			return true;
		}else{
			return false;
		}
	}
	public static function UserAuthSet($number){
		if (!isset($_SESSION['user'])) {
			if ($number) {
				$_SESSION['user'] = $number;
				return '1';
			}
		}
		return '0';
	}
}
?>