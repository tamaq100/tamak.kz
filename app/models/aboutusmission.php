<?php
/**
* AboutUsMission model
*/
class AboutUsMission extends BaseModel
{
	public static $table_name = "about_us_mission";

	public static $fields = array(
        'id'               => 'integer',
        'idx'			   => 'integer',
        'active'           => 'bool',
        'title'			   => 'string',
        'image'			   => 'string',
	);
}
?>