<?php
/**
* DishesAdvantage model
*/
class DishesAdvantage extends BaseModel
{
	public static $table_name = "dishes_advantage";

	public static $fields = array(
        'id'               => 'integer',
        'idx'			   => 'integer',
        'active'           => 'bool',
        'title'			   => 'string',
        'subtitle'		   => 'string',
        'image'			   => 'string',
	);
}
?>