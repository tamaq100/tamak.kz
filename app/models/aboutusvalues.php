<?php
/**
* AboutUsValues model
*/
class AboutUsValues extends BaseModel
{
	public static $table_name = "about_us_values";

	public static $fields = array(
        'id'               => 'integer',
        'idx'			   => 'integer',
        'active'           => 'bool',
        'title'			   => 'string',
        'subtitle'		   => 'string',
	);
}
?>