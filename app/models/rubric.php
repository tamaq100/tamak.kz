<?php
/**
* Rubric model
*/
class Rubric extends BaseModel
{
    public static $table_name = 'rubrics';

    public static $fields = array(
        'id'                => 'integer',
        'idx'               => 'integer',
        'active'            => 'bool',
        'created'           => 'integer',
        'updated'           => 'integer',
        'parent_id'         => 'integer',
        'link'              => 'string',
        'furl'              => 'string',
        'title'             => 'string',
        'text'              => 'text',
        'metaTitle'         => 'string',
        'metaDescription'   => 'string',
        'metaKeywords'      => 'string',
        'type'              => 'string',
    );

    /**
     * Хлебные крошки
     * @return array Массив с хлебными крошками
     */
    public function getBreadcrumbs()
    {

        if ($this->parent_id) {
            $model = $this->getClassName();
            $parent = $model::findOneBy($this->parent_id);
            if ($parent) {
                $result = $parent->getBreadcrumbs();
            }
        }


        $result[] = array(
            'title' => $this->title,
            'link'  => $this->getEntityUrl(),
            'class' => 'isActive',
        );
        return $result;

    }

    /**
     * Переопределяем метод для возврата дерева рубрик
     * @return array Сущности в виде дерева
     */
    public static function findAll()
    {
        $all_rubrics = parent::findAll();
        $result = array();

        if (!$all_rubrics) return $result;
        foreach ($all_rubrics as $rubric) {
            $result[$rubric->parent_id][$rubric->id] = $rubric;
        }

        return $result;
    }

    public static function baseFindAll()
    {
        return parent::findAll();
    }

   
    public static function findBy($where = array(), $orderBy = '', $limit = 10000, $offset = 0)
    {
        $all_rubrics = parent::findBy($where, $orderBy, $limit, $offset);
        $result = array();

        if (!$all_rubrics) {
            return $result;
        }

        foreach ($all_rubrics as $rubric) {
            $result[$rubric->parent_id][$rubric->id] = $rubric;
        }

        return $result;
    }

    public static function baseFindBy($where = array(), $orderBy = '', $limit = 10000, $offset = 0)
    {
        return parent::findBy($where, $orderBy, $limit, $offset);
    }

    /**
     * Генерируем текстовое представление ктегорий в виде дерева
     * @param  [type]  $tree  [description]
     * @param  integer $level [description]
     * @param  [type]  $items [description]
     * @return [type]         [description]
     */
    public function prepareTree($tree, $level = 0, $items) {
        if (!$tree) return array();
        $result = array();

        foreach ($tree as $item) {
            $result[$item->id] = 
                ($level > 1 ? str_repeat('&nbsp;', $level - 1) : '') .
                ($level > 0 ? '└-' : '') .
                ' ' . $item->getNameForInput();

            if (isset($items[$item->id])) {
                $childs = $this->prepareTree($items[$item->id], $level + 1, $items);

                $result += $childs;
            }
        }

        return $result;
    }

    public function getParent_idList()
    {
        // Рубрики отсортированные по parent_id
        $sorted_rubricks = $this->findAll();

        // Создаем линейный список их дерева
        $result = array();
        if (isset($sorted_rubricks[0])) {
            $result = $this->prepareTree($sorted_rubricks[0], 0, $sorted_rubricks);
        }
        // Добавляем первый пустой элемент

        if ($this->id) {
            // Удалям текущий элемент
            unset($result[$this->id]);
        }

        $result = array(0 => '--') + $result;

        return $result;
    }

    public function getParent() {
        $model = $this->getClassName();
        $entity = new $model;
        $entity->load($this->parent_id);

        return $entity->getNameForInput();
    }

    public function delete()
    {
        $model  = $this->getClassName();
        $childs = $model::findBy(array('parent_id' => $this->id));

        if (! empty($childs)) {
            foreach ($childs[$this->id] as $child) {
                $child->delete();
            }
        }

        parent::delete();
    }


    public function getEntityUrl()
    {
        if (isset($this->link) && $this->link) {
            return $this->link;
        } 

        if (isset($this->furl) && $this->furl) {
           

            return $this->furl;
        }

        return  $this->furl;;
    }

    public function isEmptyRubric()
    {
        return !($this->type
            || File::getCount(['parent_id' => $this->id, 'parent_class' => 'Rubric'])
            || $this->text);
    }

    public function getTypeList()
    {
        //лист типов страниц
        $list = [
            // 'specialty'         => 'Специальности',
        ];

        return $list;
    }

    public function getType()
    {
        $list = $this->getTypeList();
        if (isset($list[$this->getDefaultValue('type')])) {
            return $list[$this->getDefaultValue('type')];
        }

        return '';
    }

    public function setType($value)
    {
        if (! $value) {
            return $this;
        }
        $this->setFieldValue('type', $value);
        return $this;
    }
}