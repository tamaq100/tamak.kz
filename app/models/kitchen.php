<?php
/**
* Category_button model
*/
class Kitchen extends BaseModel
{
	public static $table_name = "kitchens";

	public static $fields = array(
		'id'			=> 'integer',
		'idx'			=> 'integer',
		'active'		=> 'bool',
		'image'			=> 'string',
		'name'			=> 'string',
		'key'			=> 'string',
	);

}
?>