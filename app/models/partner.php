<?php
/**
* Partners model
*/
class Partner extends BaseModel
{
	public static $table_name = "partners";

	public static $fields = array(
        'id'               => 'integer',
        'idx'			   => 'integer',
        'active'           => 'bool',
        'title'			   => 'string',
        'subtitle'		   => 'string',
        'image'			   => 'string',
	);
}
?>