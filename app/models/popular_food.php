<?php
/**
* Popular_food model
*/
class Popular_food extends BaseModel
{
	public static $table_name = "popular_foods";

	public static $fields = array(
	    'id'               => 'integer',
	    'idx'			   => 'integer',
	    'created'		   => 'integer',
	    'updated'		   => 'integer',
	    'urlparse'		   => 'string',
	    'active'		   => 'bool',
	    'title'		   	   => 'string',
	    'company'		   => 'string',
	    'subtitle'		   => 'string',
	    'link'			   => 'string',
	    'price'			   => 'string',
	    'image'			   => 'string',
	    'service_id'	   => 'string',
	    'tags'			   => 'string',
	    'addition'		   => 'string',
	    'specifics'		   => 'string',
	);
    public function getCompanyList()
    {
        $list = Company::findAll();
        if (! $list) return '';
        $list = array_map(function($i) {return $i->name;}, $list);
        return $list;
    }
    // public static $required_fields = array(
    //     'telephone'            => 'string',
    //     'name'             => 'string',
    //     'email'            => 'string',
    // );
}
?>
