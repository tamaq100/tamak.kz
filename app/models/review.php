<?php
/**
* Review model
*/
class Review extends BaseModel
{
    public static $table_name = 'reviews';

    public static $fields = array(
        'id'                  => 'integer',
        'idx'                 => 'integer',
        'created'             => 'integer',
        'updated'             => 'integer',
        'active'              => 'bool',
        'name'                => 'string',
        'text'                => 'text',
        'stars'               => 'integer',
    );

}