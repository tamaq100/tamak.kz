<?php
/**
* Company model
*/
class Company extends BaseModel
{
	public static $table_name = "companies";

	public static $fields = array(
		'id'			=> 'integer',
		'idx'			=> 'integer',
		'created'		=> 'integer',
		'updated'		=> 'integer',
		'active'		=> 'bool',
		'name'			=> 'string',
	);
}
?>