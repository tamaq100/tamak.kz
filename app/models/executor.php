<?php
/**
* Executor model
*/
class Executor extends BaseModel
{
	public static $table_name = "executors";
	public static $fields = array(
		'id'			=> 'integer',
		'active'        => 'bool',
		'created'		=> 'integer',
		'updated'		=> 'integer',
		'ids'			=> 'string',
		'latitude'      => 'float',
		'longitude'     => 'float',
		'avatar'		=> 'string',
		'name'	        => 'string',
		'is_online'     => 'bool',
		'free'			=> 'bool',
		'rating'        => 'float',
	);
}
?>