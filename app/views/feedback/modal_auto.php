<?php $field = 'email' ?>
<div class="form-group">
	<?php $field = 'name' ?>
	<label for="name-courier">Имя *</label>
	<input class="form-control <?= isset($errors[$field]) ? ' form-control-alert' : '' ?>" name="<?= $field ?>" required type="text" id="name-courier">
</div>
<?php $field = 'surname' ?>
<div class="form-group">
	<label for="surname-courier">Фамилия *</label>
	<input class="form-control <?= isset($errors[$field]) ? ' form-control-alert' : '' ?>"	name="<?= $field ?>" required type="text" id="surname-courier">
</div>
<?php $field = 'number_form' ?>
<div class="form-group">
	<label for="phone-form">Телефон *</label>
	<input class="form-control <?= isset($errors[$field]) ? ' form-control-alert' : '' ?>" name="number_form" placeholder="+7 XXX XX XX XXX" pattern="[+|8]{1}[0-9(\s))-]{10,}" required type="<?= $field ?>" id="phone-form">
</div>
<button type="submit" class="button orange-button">Отправить</button>