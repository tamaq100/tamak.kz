<?
	if (!isset($entity)) $entity = new Feedback;
	if (!isset($errors)) $errors = [];
?>
<?php $field = 'name' ?>
<div class="form-group">
	<input class="form-control <?= isset($errors[$field]) ? ' form-control-alert' : '' ?>" id="<?= $field ?>" name="<?= $field ?>" type="text" placeholder="Имя *" required="required" value="<?=  htmlspecialchars($entity->$field) ?>" pattern="[A-z,А-я,Ё,ё]{1,}"/>
	<?php if (isset($errors[$field])): ?>
		<span class="control-alert"><?= implode(', ', $errors[$field]) ?></span>
	<?php endif ?>
</div>
<?php $field = 'telephone' ?>
<div class="form-group">
	<input class="form-control <?= isset($errors[$field]) ? ' form-control-alert' : '' ?>" id="<?= $field ?>" name="<?= $field ?>" type="tel" placeholder="Телефон *" required="required" value="<?=  htmlspecialchars($entity->$field) ?>" pattern="[+|8]{1}[0-9(\s))-]{10,}"/>
	<?php if (isset($errors[$field])): ?>
		<span class="control-alert"><?= implode(', ', $errors[$field]) ?></span>
	<?php endif ?>
</div>
<!-- \+?\d{1}\s?\(?\d{3}\)?\s(\d\s?\-?){7} -->
<?php $field = 'email' ?>
<div class="form-group">
	<input class="form-control <?= isset($errors[$field]) ? ' form-control-alert' : '' ?>" id="<?= $field ?>" name="<?= $field ?>" type="email" placeholder="E-mail *" required="required" value="<?=  htmlspecialchars($entity->$field) ?>"/>
	<?php if (isset($errors[$field])): ?>
		<span class="control-alert"><?= implode(', ', $errors[$field]) ?></span>
	<?php endif ?>
</div>
<?php $field = 'city' ?>
<div class="form-group">
	<input class="form-control <?= isset($errors[$field]) ? ' form-control-alert' : '' ?>" id="<?= $field ?>" name="<?= $field ?>" type="city" placeholder="Город" pattern="[A-z,А-я,Ё,ё]{1,}" value="<?=  htmlspecialchars($entity->$field) ?>"/>
	<?php if (isset($errors[$field])): ?>
		<span class="control-alert"><?= implode(', ', $errors[$field]) ?></span>
	<?php endif ?>
</div>
<div class="application-button">
	<div class="button-submit"><button type="submit" class="btn btn-primary">Отправить</button></div>
</div>