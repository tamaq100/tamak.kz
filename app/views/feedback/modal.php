<?
	if (!isset($entity)) $entity = new Feedback;
	if (!isset($errors)) $errors = [];
?>
<div class="modal-content">
	<div class="modal-header">
		<h5 class="modal-title" id="exampleModalLabel">Оставить заявку</h5>
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
	</div>
	<div class="modal-body">
		<?php $field = 'name' ?>
		<div class="form-group">
			<input class="form-control <?= isset($errors[$field]) ? ' form-control-alert' : '' ?>" id="<?= $field ?>" name="<?= $field ?>" type="text" placeholder="Имя *" required="required" value="<?=  htmlspecialchars($entity->$field) ?>"/>
			<?php if (isset($errors[$field])): ?>
				<span class="control-alert"><?= implode(', ', $errors[$field]) ?></span>
			<?php endif ?>
		</div>
		<?php $field = 'telephone' ?>
		<div class="form-group">
			<input class="form-control <?= isset($errors[$field]) ? ' form-control-alert' : '' ?>" id="<?= $field ?>" name="<?= $field ?>" type="text" placeholder="Телефон *" required="required" value="<?=  htmlspecialchars($entity->$field) ?>"/>
			<?php if (isset($errors[$field])): ?>
				<span class="control-alert"><?= implode(', ', $errors[$field]) ?></span>
			<?php endif ?>
		</div>
		<?php $field = 'email' ?>
		<div class="form-group">
			<input class="form-control <?= isset($errors[$field]) ? ' form-control-alert' : '' ?>" id="<?= $field ?>" name="<?= $field ?>" type="email" placeholder="E-mail *" required="required" value="<?=  htmlspecialchars($entity->$field) ?>"/>
			<?php if (isset($errors[$field])): ?>
				<span class="control-alert"><?= implode(', ', $errors[$field]) ?></span>
			<?php endif ?>
		</div>
	</div>
	<div class="modal-footer">
		<!-- <button class="btn btn-secondary" data-dismiss="modal" type="button" data-dismiss="modal" aria-label="Close">Закрыть</button> -->
		<button class="btn btn-primary" type="submit">Отправить</button>
	</div>
</div>