<div class="form-group">
	<?php $field = 'name_restaurant' ?>
	<label>Название заведения *</label>
	<input class="form-control <?= isset($errors[$field]) ? ' form-control-alert' : '' ?>" type="text" required name="name_restaurant">
</div>
<div class="form-group">
	<label>Тип заведения</label>
	<select name="type_restaurant" class="form-control">
		<option value="Ресторан">Ресторан</option>
		<option value="Фастфуд">Фастфуд</option>
		<option value="Кафе">Кафе</option>
		<option value="Столовая">Столовая</option>
	</select>
</div>
<div class="form-group">
	<?php $field = 'city' ?>
	<label>Город *</label>
	<input class="form-control <?= isset($errors[$field]) ? ' form-control-alert' : '' ?>  required type="text" name="city">
</div>
<div class="form-group">
	<?php $field = 'name_form' ?>
	<label>Имя *</label>
	<input class="form-control <?= isset($errors[$field]) ? ' form-control-alert' : '' ?>" required type="text" name="name_form">
</div>
<div class="form-group">
	<label>Должность</label>
	<input class="form-control" type="text" name="position">
</div>
<div class="form-group">
	<?php $field = 'number_form' ?>
	<label>Телефон *</label>
	<input class="form-control <?= isset($errors[$field]) ? ' form-control-alert' : '' ?>" placeholder="+7 XXX XX XX XXX" pattern="[+|8]{1}[0-9(\s))-]{10,}" required type="phone" name="number_form">
</div>
<div class="form-group">
	<?php $field = 'email_form' ?>
	<label>Email *</label>
	<input class="form-control <?= isset($errors[$field]) ? ' form-control-alert' : '' ?>" placeholder=""  required type="email" name="email_form">
</div>
<button type="submit" class="button orange-button">Отправить</button>