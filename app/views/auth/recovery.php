<form class="d-none recovery-ajax ajax-form" method="POST" id="recovery" action="/api/recoverypw">
	<div class="back-chevron-round"><i class="fas fa-chevron-left"></i></div>
	<div class="form-group">
		<p>* введите ваш номер телефона и новый пароль придёт по SMS</p>
		<input placeholder="Номер телефона" data-phone pattern="[+]{1}[0-9(\s))-]{10,}" class="form-control" type="phone" name="user_provider_id"/>
		<input type="text" value="PHONE_PASSWORD" name="provider_key" class="fade">
		<p class="form-error__desc fade">Неверный номер</p>
	</div>
	<div class="share__buy-wrap"><button class="share__buy" href="#">Отправить</button></div>
</form>