<form class="tab-pane" method="POST" id="register" action="/api/registrate">
	<div class="form-group">
		<input placeholder="Номер телефона" data-phone pattern="[+]{1}[0-9(\s))-]{10,}" class="form-control" type="phone" name="user_provider_id" id="number"/>
		<input type="text" value="PHONE_PASSWORD" hidden name="provider_key" class="fade">
		<p class="form-error__desc fade">Неверный номер</p>
	</div>
	<div class="share__buy-wrap"><button class="share__buy" href="#">Зарегистрироваться</button></div>
</form>