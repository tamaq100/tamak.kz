<form class="tab-pane active login-ajax" id="login" method="POST" action="/login">
	<div class="form-group">
		<input inputmode="tel" data-phone class="form-control" type="text" placeholder="Номер телефона" name="user_provider_id" pattern="[+|8]{1}[0-9(\s))-]{10,}" value=""/>
	</div>
	<input type="text" value="PHONE_PASSWORD" name="provider_key" class="fade hidden">
	<div class="form-group">
		<input class="form-control" type="password" autocomplete="password" placeholder="Пароль" name="password" value=""/>
	</div>
	<p class="form-error__desc hidden fade">Неверный номер или пароль</p>
	<div class="form-control__password">
		<a href="#" id="zabyl-pass">Забыли пароль?</a>
	</div>
	<div class="share__buy-wrap">
		<button type="submit" class="share__buy">
			Войти
		</button>
	</div>
<!-- 	<div class="form-control__authorization">Авторизация через соцсети:</div>
	<div class="icon">
		<div class="icon_i">
			<i class="fab fa-facebook-f"></i>
		</div>
		<div class="icon_i">
			<i class="fab fa-vk"></i>
		</div>
	</div> -->
</form>