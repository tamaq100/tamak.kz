<main class="page-content">
	<div class="shares">
		<div class="container">
			<div class="row">
				<ul class="breadcrumb">
					<li class="active"><a href="/">Главная</a></li>
					<li><a href="#"><?= $title ?></a></li>
				</ul>
			</div>
		</div>
		<div class="container news-block">
			<h2 class="shares__title"><?= $title ?></h2>
			<div class="shares__items">
				<?php if ($shares): ?>
					<?php foreach ($shares as $share): ?>
				<div class="shares__item">
					<?php if ($share->preview_image): ?>
						<a class="shares__pic" href="#" style="background-image:url('images/fit/279/225/<?= $share->preview_image ?>.jpg')">
						</a>
					<?php elseif ($share->image): ?>
						<a class="shares__pic" href="#" style="background-image:url('images/fit/279/225/<?= $share->image ?>.jpg')"></a>
					<?php endif ?>
					<div class="shares__text">
						<a class="shares__name" href="/shares/show?furl=<?= $share->id?: 0 ?>">
							<?= $share->title ?>
						</a>
						<p class="shares__desc">
							<?= $share->subtitle ?>
						</p>
					</div>
					<div class="shares__info">
						<a href="<?= $share->company_link ?>">
						<h4 class="shares__company">
								<?= $share->company ?>
						</h4>
						</a>
						<?php if (isset($article->link) && $article->link): ?>
							
						<h4 class="shares__cost">
							<?= $share->priceWithTG ?>
						</h4>
						<?php endif ?>
						<?php if ($share->more === 'Да'): ?>
						<a class="shares__buy" href="/shares/show?furl=<?= $share->id ?>" role="buy">
							Подробнее
						</a>
						<?php else: ?>
						<?php if (isset($article->link) && $article->link): ?>
						<a class="shares__buy" href="<?= $share->link ?>" role="buy">
							Заказать
						</a>
						<?php endif ?>
						<?php endif ?>

					</div>
				</div>
					<?php endforeach ?>
				<?php endif ?>
			</div>
			<?= include_file('default/pagination_news', $pagination);  ?>
		</div>
	</div>
	<div class="welcome-hero" id="welcome_hero"><img src="/img/icons/welcome-hero.png" alt=""/><img class="welcome-hero__text" src="/img/icons/welcome-hero_text.png" alt=""/><a class="welcome-hero_close" href="#" id="welcome_hero_close"></a></div>
</main>