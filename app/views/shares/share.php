<main class="page-content">
	<div class="share">
		<div class="container">
			<div class="row">
				<ul class="breadcrumb">
					<li class="active">
						<a href="<?= siteURL() ?>">Главная</a></li>
					<li><a href="/shares">Акции</a></li>
					<li><?= $article->title ?></li>
				</ul>
			</div>
		</div>
		<div class="container">
			<h2 class="share__title"><?= $article->title ?></h2>
			<?php if ($article->image): ?>
			<div class="share__pic" style="background-image:url('/images/fit/930/370/<?= $article->image ?>.jpg')">
			</div>
			<?php endif ?>
			<div class="share__header">
				<h4 class="share__company"><a href="<?= $article->company_link ?>"><?= $article->company ?></a></h4>
				<h3 class="share__benefit"><?= $article->subtitle ?></h3>
				 <div class="share__buy-wrap">
					<?php if (isset($article->link) && $article->link): ?>
						// <a class="share__buy" href="<?= $article->link ?>" role="buy">Сделай заказ</a>
					<?php endif ?>
					 <span style="cursor: pointer;" class="share__buy" data-trigger="hover" data-toggle="popover" data-placement="bottom" data-content="Приносим свои извинения, наш сервис дорабатывается">Сделать заказ</span>
				</div>
			</div>
			<div class="container share__content">
				<h3>Условия акции</h3>
				<?= $article->conditions ?>
			</div>
			<?php if ($article->link): ?>
			<div class="share__footer">
				<div class="share__buy-wrap share__buy-wrap--footer">
                    <?php if (isset($article->link) && $article->link): ?>
                        // <a class="share__buy" href="<?= $article->link ?>" role="buy">Сделай заказ</a>
                    <?php endif ?>
                    <span style="cursor: pointer;" class="share__buy" data-trigger="hover" data-toggle="popover" data-placement="bottom" data-content="Приносим свои извинения, наш сервис дорабатывается">Сделать заказ</span>
                </div>
			</div>
			<?php endif ?>
		</div>
	</div>
</main>