<div class="container-fluid white-bg mb-30 mt-25" style="padding:0!important;">
    <div class="container">
        <ul class="navbar-cabinet">
        	<?php $actie = ''; ?>
        	<?php foreach ($vars['menu'] as $name => $property): ?>
            <?php if (isset($property['name']['hidden']) && ($property['name']['hidden'] == 1)) continue; ?>
            <li class="<?= $vars['active'] == $name ? 'active' : '' ?>">
            	<a href="/cabinet/<?= $name ?>" class="js-nav-lk" data-goto="<?= $name ?>">
            		<?= $property['name']['ru'] ?>
            	</a>
            </li>
        	<?php endforeach ?>
        </ul>
    </div>
</div>