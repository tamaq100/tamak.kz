<!DOCTYPE html>
<html class="page" lang="en">
	<head>
		<meta charset="UTF-8"/>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimum-scale=1.0, shrink-to-fit=no"/>
		<?php if (isset($meta['title']) && $meta['title']) : ?>
		<title><?= $meta['title'] ?></title>
		<meta name="title" content="<?= $meta['title'] ?>" />
		<meta content="<?= $meta['title'] ?>" property="og:title" />
		<?php else: ?>
		<title>Личный кабинет</title>
		<meta name="title" content="Tamaq" />
		<meta content="Tamaq" property="og:title" />
		<?php endif ?>
		<?php if (isset($meta['description']) && ($meta['description'] != '')): ?>
		<meta name="description" content="<?= $meta['description'] ?>">
		<meta content="<?= $meta['description'] ?>" property="og:description" />
		<?php endif ?>
		<?php if (isset($meta['keywords']) && ($meta['keywords'] != '')): ?>
		<meta name="keywords" content="<?= $meta['keywords'] ?>">
		<?php endif ?>
		<link rel="preload" as="style" href="/css/style.css">
		<link rel="preload" as="image" href="/img/preloaders/rounded.svg">
		<link rel="apple-touch-icon" sizes="57x57" href="/img/favicon/apple-icon-57x57.png"/>
		<link rel="apple-touch-icon" sizes="60x60" href="/img/favicon/apple-icon-60x60.png"/>
		<link rel="apple-touch-icon" sizes="72x72" href="/img/favicon/apple-icon-72x72.png"/>
		<link rel="apple-touch-icon" sizes="76x76" href="/img/favicon/apple-icon-76x76.png"/>
		<link rel="apple-touch-icon" sizes="114x114" href="/img/favicon/apple-icon-114x114.png"/>
		<link rel="apple-touch-icon" sizes="120x120" href="/img/favicon/apple-icon-120x120.png"/>
		<link rel="apple-touch-icon" sizes="144x144" href="/img/favicon/apple-icon-144x144.png"/>
		<link rel="apple-touch-icon" sizes="152x152" href="/img/favicon/apple-icon-152x152.png"/>
		<link rel="apple-touch-icon" sizes="180x180" href="/img/favicon/apple-icon-180x180.png"/>
		<link rel="icon" type="image/png" sizes="192x192" href="/img/favicon/android-icon-192x192.png"/>
		<link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png"/>
		<link rel="icon" type="image/png" sizes="96x96" href="/img/favicon/favicon-96x96.png"/>
		<link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png"/>
		<link rel="manifest" href="/img/favicon/manifest.json"/>
		<meta name="msapplication-TileColor" content="#ffffff"/>
		<meta name="msapplication-TileImage" content="/img/favicon/ms-icon-144x144.png"/>
		<meta name="theme-color" content="#ffffff"/>
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700&amp;amp;subset=cyrillic">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Pacifico">
		<script>
			var fullAjOff = false;
		</script>
		<script src="/vendor/handlebars.js"></script>
		<?php foreach ($styles as $src): ?>
		<link rel="stylesheet" href="<?= $src ?>?v=<?= filemtime(WORK_DIR . $src) ?>"/>
		<?php endforeach ?>
		<?php foreach ($scripts as $src): ?>
		<?php if (preg_match('/jquery/iemU', $src)): ?>
		<script src="<?= $src ?>?v=<?= filemtime(WORK_DIR . $src) ?>"></script>
		<?php endif ?>
		<?php endforeach; ?>
	</head>
	<body>
		<script>
		var heroesData = {
			"hello": {
				"hero": "01",
				"msg": "01"
			},
			"help": {
				"hero": "06",
				"special": true
			},
			"anticipation": {
				"hero": "02",
				"msg": "02"
			},
			"sadness": {
				"hero": "04",
				"msg": "04"
			},
			"forchat": {
				"hero": "05",
				"msg": "05"
			},
			"orderStatus": {
				"hero": "08",
				"msg": "08"
			},
			"statusPending": {
				"hero": "09",
				"msg": "09"
			}
		}
		</script>
		<div class="welcome-hero" id="welcome_hero">
			<img class="welcome-hero__ico" src="/img/heroes/hero01.png" alt=""/>
			<img class="welcome-hero__text" src="/img/icons/welcome-hero_text.png" alt=""/>
			<a class="welcome-hero_close" href="#" id="welcome_hero_close"></a>
		</div>
		<div class="page-inner addTopApp">
			<?= include_file('default/header') ?>
			<main class="page-content page-content--gray">
				<!-- <?= include_file('lk/helpers/breadcrumbs') ?> -->
				<?= include_file('lk/courier/head') ?>
				<?= include_file('lk/helpers/nav', $vars['addons']['menu']) ?>
				<div class="js-wrap-container">
					<div class="js-wrap-preload"></div>
					<?= $content ?>
				</div>
			</main>
			<?= include_file('default/footer') ?>
			<?= include_file('default/modal-fade'); ?>
			<div class="modal fade" id="modal-settings" role="dialog" style="display:none;">
				<div class="modal-dialog modal-dialog--autowidth">
					<form class="modal-body modal-body__settings">
						<div class="modal-settings__title">настройка параметров заказов</div>
						<hr class="modal-settings__hr"/>
						<div class="modal-settings__subtitle mb-15">Режим работы</div>
						<div class="modal-settings__box row mt-4">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label class="restoraunt-filter__checkbox restoraunt-filter__checkbox--radio restoraunt-filter__checkbox--addsubtext">
									<input name="workingMode" value="not_working" type="radio" checked="checked"/>
									<span class="text">Не принимать</span>
									<span class="subtext">Вам не будут поступать заказы</span>
									<span class="checkmark"></span>
								</label>
							</div>
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label class="restoraunt-filter__checkbox restoraunt-filter__checkbox--radio restoraunt-filter__checkbox--addsubtext">
									<input name="workingMode" value="review" type="radio"/>
									<span class="text">Рассматриваю все заказы</span>
									<span class="subtext">
										Вам будут поступать все заказы, и вы сможете принять их или отказаться
									</span>
									<span class="checkmark"></span>
								</label>
							</div>
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label class="restoraunt-filter__checkbox restoraunt-filter__checkbox--radio restoraunt-filter__checkbox--addsubtext">
									<input name="workingMode" value="automatic" type="radio"/>
									<span class="text">Автоставка</span>
									<span class="subtext">
										Вам будут автоматически поступать заказы, соответствующие параметрам автоставки
									</span>
									<span class="checkmark"></span>
								</label>
							</div>
						</div>
						<hr class="modal-settings__hr"/>
						<div class="modal-settings__subtitle mb-10">Тип транспорта</div>
						<!-- <div class="modal-settings__span mb-15">Укажите минимальную сумму за заказ, которую вы хотели бы получить</div> -->
						<div class="modal-settings__span mb-15">Укажите текущий тип транспорта</div>
						<div class="modal-settings__box row mt-4">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label class="restoraunt-filter__checkbox restoraunt-filter__checkbox--radio restoraunt-filter__checkbox--addsubtext">
									<input name="transport" type="radio" value="Pedestrian" checked="checked"/>
									<span class="text">Пеший</span>
									<span class="checkmark"></span>
								</label>
							</div>
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label class="restoraunt-filter__checkbox restoraunt-filter__checkbox--radio restoraunt-filter__checkbox--addsubtext">
									<input name="transport" type="radio" value="Bicycle"/>
									<span class="text">Велосипед</span>
									<span class="checkmark"></span>
								</label>
							</div>
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label class="restoraunt-filter__checkbox restoraunt-filter__checkbox--radio restoraunt-filter__checkbox--addsubtext">
									<input name="transport" type="radio" value="Scooter" />
									<span class="text">Мотороллер</span>
									<span class="checkmark"></span>
								</label>
							</div>
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label class="restoraunt-filter__checkbox restoraunt-filter__checkbox--radio restoraunt-filter__checkbox--addsubtext">
									<input name="transport" type="radio" value="Motorcycle" />
									<span class="text">Мотоцикл</span>
									<span class="checkmark"></span>
								</label>
							</div>
							<div class="col-xs-12 col-sm-4 col-md-4">
								<label class="restoraunt-filter__checkbox restoraunt-filter__checkbox--radio restoraunt-filter__checkbox--addsubtext">
									<input name="transport" type="radio" value="Car"/>
									<span class="text">Автомобиль</span>
									<span class="checkmark"></span>
								</label>
							</div>
						</div>
						<hr class="modal-settings__hr"/>
						<div class="modal-settings__subtitle mb-10">ОПЛАТА РАБОТЫ</div>
						<div class="modal-settings__span mb-15">Укажите минимальное вознаграждение за заказ, которое вы хотели бы получить</div>
						<div class="modal-settings__box row mt-4">
							<div class="col-xs-12 col-sm-4 col-md-4" style="display: flex;flex-wrap: nowrap;align-content: center;align-items: flex-end;">
								<input type="number" name="minReward" class="settings-table__input" placeholder="min"><small style="padding-left: 8px;">тг.</small>
							</div>
						</div>
						<hr class="modal-settings__hr"/>
						<div class="modal-settings__subtitle mb-10">РАДИУС РАБОТЫ</div>
						<div class="modal-settings__span mb-15">Радиус работы от текужего местоположения</div>
						<div class="modal-settings__box row mt-4">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<div style="display: flex;flex-wrap: nowrap;align-content: center;align-items: flex-end;">
									<input type="number" name="workingRadius" class="settings-table__input" placeholder="min"><small style="padding-left: 8px;">км.</small>
								</div>
							</div>
						</div>
						<hr class="modal-settings__hr"/>
						<div class="modal-settings__box mb-15" style="overflow: hidden;">
							<div class="errn alert alert-danger hidden">
								Произошла ошибка при отправке данных.
							</div>
							<button class="button-standard button-standard--orange" style="float: right;">
								Сохранить
							</button>
						</div>
					</form>
					<div class="modal-header">
						<button class="close" type="button" data-dismiss="modal">×</button>
					</div>
				</div>
			</div>
		</div>
		<div id="courier-order-billet" class="order-billet" style="min-height: 50px;">
			<script type="text/x-handlebars-template">
				<div class="order-billet__wrap">
					<span>
						<a class="order-billet-smalltext" style="display: block;font-size: 15px;">
							<i class="fas fa-utensils"></i>
								Ресторан: {{order.service.name}}
						</a>
						<a class="order-billet-smalltext" style="display: block;font-size: 15px;"><i class="fas fa-street-view"></i> Адрес заказчика: {{order.client.address.street}}</a>
					</span>
				</div>
					<a onclick="executorOrderGoTo('{{action}}', '{{order.id}}')" class="order-billet-go" style="min-width: max-content;">
						{{title}}
					</a>
			</script>
			<div class="container">
				<div class="order-billet__container" style="min-height: 40px;">
				</div>
			</div>
		</div>
		<?php foreach ($scripts as $src): ?>
		<?php if (!preg_match('/jquery/iemU', $src)): ?>
		<script src="<?= $src ?>?v=<?= filemtime(WORK_DIR . $src) ?>"></script>
		<?php endif; ?>
		<?php endforeach ?>
	</body>
</html>