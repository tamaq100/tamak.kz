<div class="js-wrap-slide" data-wrap="restolike">
	<div class="container">
	    <div class="row">
	    	{{#each restoraunts}}
	        <div class="col-xs-12">
	            <div class="white__block">
	                <div class="col-xs-12 col-sm-8 col-md-9 col-lg-9 restoraunt-main-b">
	                    <div class="restoraunt__image restoraunt__image--pd">
	                        <div class="restoraunt__image-item" style="background-image:url('<?= Api::$apiDomain ?>/imgs/{{id}}_photo1_140.png');"></div>
	                    </div>
	                    <div class="restoraunt__wrap restoraunt__wrap--ml">
	                        <div class="restoraunt__wrap--flex">
	                            <div class="inner-review__title">Ресторан “{{name}}”</div>
	                            <div class="restoraunt__stars"><span class="restoraunt__stars-resto">Ресторан</span><i class="fas fa-star rate-star active"></i><i class="fas fa-star rate-star active"></i><i class="fas fa-star rate-star active"></i><i class="fas fa-star rate-star"></i><i class="fas fa-star rate-star"></i></div>
	                        </div>
	                        <div class="restoraunt__wrap">
	                            <div class="restoraunt__text restoraunt__text--12 restoraunt__text-pt">Прием заказов: {{openfrom}} - {{opento}}</div>
	                            <div class="restoraunt__text restoraunt__text--13 restoraunt__text-pt">Кухня:
		                            {{#each categories}}
		                            	{{value_ru}}|
		                            {{/each}}
		                        </div>
	                        </div>
	                        <div class="restoraunt__wrap--aflex restoraunt__logopay-container pt-15-10">
	                            <div class="restoraunt__logopay"><img src="/img/cabinet-restoraunts/visa.png"/></div>
	                            <div class="restoraunt__logopay"><img src="/img/cabinet-restoraunts/master.png"/></div>
	                        </div>
	                        <div class="restoraunt__wrap--flex restoraunt__bottom-menu">
	                            <div class="restoraunt__wrap-hlist pt-15-10">
            						<a href="/restoraunts/restoraunt_menu?id={{id}}">
										<span>Меню</span>
									</a>
									<a href="/restoraunts/restoraunt_info?id={{id}}">
										<span>Информация</span>
									</a>
									<a href="/restoraunts/restoraunt_reviews?id={{id}}">
										<span>Отзывы</span>
									</a>
	                            </div>
	                            <div class="restoraunt__wrap pt-15-10">
	                                <div class="restoraunt__like" data-id="{{id}}"><i class="fas fa-heart like--active"></i><span>Убрать из избранных</span></div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	                <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
	                    <div class="restoraunt__wrap pt-15-10">
	                        <div class="restoraunt__text-m ta-l">Минимальная сумма:</div>
	                        {{#if minbill}}
		                        <div class="restoraunt__text-b ta-l">{{minbill}} тг.</div>
	                        {{else}}
		                        <div class="restoraunt__text-b ta-l">-- --</div>
	                        {{/if}}
	                    </div>
	                    <div class="restoraunt__wrap pt-15-10">
	                        <div class="restoraunt__text-m ta-l">Доставка:</div>
	                        <div class="restoraunt__text-b ta-l">Доставка Ресторана</div>
	                    </div>
	                </div>
	                <div class="restoraunt__share restoraunt__share--orange restoraunt__share--right">Акции</div>
	            </div>
	        </div>
	    	{{/each}}
			{{#unless this}}
				<div class="col-xs-12">
					<h6 style="text-align:center;">У вас пока нет избранных ресторанов</h6>
				</div>
	    	{{/unless}}
	    </div>
	</div>
</div>