<div class="js-wrap-slide" data-wrap="notifications">
	<div class="container mb-30">
		<div class="notify-search__container">
			<input class="settings-table__input notify-search__input" placeholder="Поиск уведомления" type="text">
			<div class="notify-search__button"></div>
		</div>
	</div>
	<div class="container">
		<!-- Не забыть собрать -->
		<div class="white__block courier-order">
			<div class="courier-order__block notify-left__container">
				<div class="notify-block__date notify-block__date--orange">01.12.2018 в 12:47</div>
				<div class="notify-left__wrap">
					<div class="notify-block__title">Заказ № 1460294 отменен</div>
					<div class="notify-block__description">Заказ отменен заказчиком 2 344</div>
				</div>
			</div>
			<div class="courier-order__block courier-order__block--buttons">
				<a href="" class="notify-more__container my-8">
					<span>Подробнее</span>
					<i class="notify-arrow notify-arrow--ml"></i>
				</a>
			</div>
		</div>
	</div>
</div>