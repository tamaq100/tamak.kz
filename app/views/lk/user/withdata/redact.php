<form class="container js-wrap-slide" data-wrap="redact">
	<span class="text-success okay_message invisible">Изменения вступили в силу!</span>
	<div class="row">
		<!-- <div class=""style="display: none;"></div> -->
		<div class="col-lg-9 col-xs-12 mt-15 ">
			<table class="clear__table" border="0" style="width:100%">
				<tr>
					<th></th>
					<th></th>
					<th></th>
				</tr>
				<tr>
					<td><span class="settings-table__label">Ваше имя:</span></td>
					<td>
						<input class="settings-table__input" type="text" name="name" value="{{this.userdata.name}}"/>
					</td>
					<td></td>
				</tr>
				<tr>
					<td>
						<span class="settings-table__label">Телефон:</span>
					</td>
					<td>
						<input class="settings-table__input" type="phone" name="phone" value="{{this.userdata.phone}}"/>
					</td>
					<td>
					</td>
				</tr>
				{{!-- {{useraddresses}} --}}
				<tr id="adress">
					<td>
						<span class="settings-table__label">Адрес:</span>
					</td>
					{{#if firstadress}}
					<td>
						<input data-addressid="{{firstadress.id}}" name="" class="settings-table__input" value="{{firstadress.street}}, {{firstadress.house}}" />
					</td>
					<td>
						<span class="settings-table__delete-adress settings-table__delete  d-xs-none" data-addressid="{{firstadress.id}}">Удалить</span>
						<span class="settings-table__delete-adress settings-table__delete  settings-table__delete--small" data-addressid="{{firstadress.id}}">×</span>
					</td>
					{{/if}}
				</tr>
				{{#each this.addresses}}
				<tr>
					<td></td>

					<td>
						<input data-addressid="{{id}}" name="" class="settings-table__input" value="{{street}}, {{house}}" />
					</td>
					<td>
						<span class="settings-table__delete-adress settings-table__delete  d-xs-none" data-addressid="{{id}}">Удалить</span>
						<span class="settings-table__delete-adress settings-table__delete  settings-table__delete--small" data-addressid="{{id}}">×</span>						
					</td>
				</tr>
				{{/each}}
				<tr id="adress_exemplar" style="display:none;">
					<td></td>
					<td>
						<input class="settings-table__input"/>
					</td>
					<td>
						<span class="settings-table__delete-adress settings-table__delete  d-xs-none">Удалить</span>
						<span class="settings-table__delete settings-table__delete--small">×</span>
					</td>
				</tr>
				<tr>
					<td>
					</td>
					<td>
						<span class="settings-table__added">Добавить адрес</span>
					</td>
					<td>
					</td>
				</tr>
{{!--                 <tr>
					<td><span class="settings-table__label">Новый пароль:</span></td>
					<td>
						<input class="settings-table__input"/>
					</td>
					<td></td>
				</tr> --}}
				<tr>
					<td>
						
					</td>
					<td>
						<span class="text-danger error__message invisible">Введены неверные данные</span>
					</td>
					<td>
						
					</td>
				</tr>
				<tr>
					<td></td>
					<td>
						<div class="wrap mt-15">
							<button class="button-standard mi-w150 mb-15 button-standard--orange" type="submit">
								Сохранить
							</button>
							<a class="button-standard mi-w150 mb-15" href="/cabinet">
								Отмена
							</a>
						</div>
					</td>
					<td></td>
				</tr>
			</table>
		</div>
	</div>
</form>