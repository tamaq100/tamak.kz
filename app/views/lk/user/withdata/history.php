<div class="js-wrap-slide" data-wrap="history">
    {{#each this}}
    <div class="container" data-orderID="{{this.id}}">
        <div class="white__block">
            <div class="container orders">
                <div class="row">
                    <div class="col-md-6">
                        <ul class="orders-number-date">
                            <li class="orders__orders-number">№{{this.number}}</li>
                            <li class="orders__orders-date">
                                {{timeOffsetPlus created}}
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <div class="orders-total-status">
                            <ul class="orders-total">
                                <li>Итого:</li>
                                <li class="orders__orders-total">
                                    <span>{{sum2 esum sum}}</span> тг.
                                </li>
                            </ul>
                            <ul class="orders-status">
                                <li>Статус:</li>
                                <li class="orders__orders-status">
                                    {{orderStatus status}}
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row orders-content">
                <div class="col-xs-12 col-md-6">
                    <div class="orders-orders-content">
                        <div class="orders-content-image">
                            <div class="orders-content__image" style="background-image:url('<?= Api::$apiDomain ?>/imgs/{{service.id}}_photo1_140.png'); background-repeat:no-repeat;background-size: cover;"></div>
                        </div>
                        <div class="orders-content-name">
                            <span class="orders-content__orders-name">
                                {{service.name}}
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="orders-content-price">
                        <span class="orders-content__price">
                            <span>{{counterInArray order_products 'amt'}}</span> {{declOfNumDishes order_products 'amt'}} на сумму <span>{{counterInArray order_products 'sum'}}</span> тг.
                        </span>
                    </div>
                </div>

            </div>
            <div class="container orders-details">
                <div class="orders-details_button">
                    <a class="js-nav-lk" data-goto="order{{id}}" href="/cabinet/order?id={{id}}">
                        <div class="btn orders-details-button pull-right">
                            <span>Смотреть детали заказа</span>
                        </div>
                    </a>
                </div>
            </div>

        </div>
    </div>
    {{/each}}
    {{#unless this}}
        <div class="container">
            <h6 style="text-align:center">Список заказов пуст</h6>
        </div>
    {{/unless}}
</div>
{{!-- <div class="container">
    <div class="news-block__pagination">
        <!-- Вперёд -->
        {{#if (Next_nav current pages)}}
            <a href="{{Next_navFull current pages}}">
                <div class="news-block__pagination__next"></div>
            </a>            
        {{/if}}
        <!-- Назад -->
        {{#if (Prev_nav current)}}
            <a href="{{Prev_navFull current}}">
                <div class="news-block__pagination__prev"></div>
            </a>
        {{/if}}
        <!-- передаём страницы текущую страницу и сколько страниц по бокам выводить -->
        {{#times2 pages current '2'}}
        <ul class="pagination">
            {{#if_eq  this.current this.times}}
            <li class="active">
                <a href="{{urlBuilder 'page' this.times}}">
                    {{this.times}}
                </a>
            </li>
            {{else}}
                <li class="">
                    <a href="{{urlBuilder 'page' this.times}}">
                        {{this.times}}
                    </a>
                </li>
            {{/if_eq}}
        </ul>
        {{/times2}}
    </div>
</div> --}}
