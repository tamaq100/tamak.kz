{{#each this}}
<div class="js-wrap-slide" data-wrap="order{{id}}">
    <div class="container order-back mb-15">
        <div class="info_container-main">
            <div class="restoraunt-side__title">№{{number}}</div>
            <div class="info__container">
                <span class="info__label">Время:</span><span class="info__body">{{timeOffsetPlus created}}</span>
            </div>
            <div class="info__container info__container--relative">
                <span class="info__label">Статус:</span>
                <span class="info__body">{{orderStatus status}}</span>
                <span class="info__body info__body--info">i</span>
            </div>
            <a class="info__back" href="/cabinet/history">
                <div class="btn order-back-button">
                    <div class="order-back_image" style="background-repeat:no-repeat;"> </div>
                    <span class="order-back_name">Вернуться к списку заказов</span>
                </div>
            </a>
        </div>
    </div>
    <div class="container">
        <div class="white__block">
            <div class="order" style="overflow: hidden; padding-top: 10px;">
                    <div id="orderDataBag"  data-tamaq_executor="{{#if service.own_executors}}{{service.own_executors}}{{else}}false{{/if}}"></div>
                    <div class="col-xs-12 col-sm-5 col-md-3">
                        <div class="order-order">
                            <div class="order-image">
                                    <div class="order__image" style="background-image:url('<?= Api::$apiDomain ?>/imgs/{{service.id}}_photo1_140.png');">
                                </div>
                            </div>
                            <div class="order-name-type">
                                <p class="order__order-name">{{service.name}}</p><span class="order__order-type">Ресторан</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-4">
                        <ul class="order-amt-count">
                            <li class="order__order-amt">{{counterInArray order_products 'amt'}} {{declOfNumDishes order_products 'amt'}}</li>
                            <li class="order__order-count">{{sum}} тг.</li>
                        </ul>
                        <ul class="order-delivery-cost">
                            <li class="order__order-delivery">доставка</li>
                            <li class="order__order-cost">{{esum}} тг.</li>
                        </ul>
                    </div>
                    {{#if (hasUserValuation status 'rate_restaurant' rates)}}
                        <div class="col-sm-12 col-md-5" style="text-align: right;">
                            <div class="order-geo-status">
                                <div class="button-standard button-standard--orange" id="" style="display: -webkit-flex;
                                display: -moz-flex;
                                display: -ms-flex;
                                display: -o-flex;
                                display: flex;-ms-align-items: center;
                                align-items: center;font-size: 12px;padding: 8px 20px;" onclick="restValuation('{{id}}')">
                                    <i class="far fa-comment-dots" style="font-size: 20px;"></i>&nbsp;  &nbsp;  Оставить отзыв о ресторане
                                </div>
                            </div>
                        </div>
                    {{else}}
                        {{#if (hasUserValuationrev status 'rate_restaurant' rates)}}
                            <div class="col-sm-12 col-md-5" style="text-align: right;">
                                <div class="order-geo-status">
                                        Вы уже поставили свою оценку
                                </div>
                            </div>
                        {{/if}}
                    {{/if}}
            </div>
            {{#each order_products}}
            <div class="row order-content">
                <div class="col-xs-12 col-md-6 col-lg-6">
                    <div class="order-order-content">
                        <div class="order-content-image">
                            {{!-- Внимание нужно сделать хэлпер для вытаскивания нормального изображения --}}
                            <div class="order-content__dishes" style="background-image:url('<?= Api::$apiDomain ?>/imgs/{{product_id}}_photo1_140.png'); background-repeat:no-repeat;background-size: cover;"></div>
                        </div>
                        <div class="order-content-name">
                            <div class="order-content__order-name">{{name}}, {{amt}} шт</div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-5 col-md-3 col-lg-3">
                    <div class="order-content-price">
                        <ul class="order-content_price">
                            <li class="order-content__order-price">{{price}} тг.</li>
                            <li class="order-content__order-portion">Порций: <span class="order-content-amt">{{amt}}</span></li>
                        </ul>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-7 col-md-3 col-lg-3">
                    <ul class="order-content-sum">
                        <li class="order-content__order-sum">Сумма: </li>
                        <li class="order-content__order-count">{{sum}} тг.</li>
                    </ul>
                </div>
            </div>
            {{/each}}
        </div>
    </div>
    <div class="container order-comment-sum">
        <div class="row">
            <div class="col-sm-6 col-md-6">
                {{#if comment_to_executor}}
                <ul class="order_comment">
                    <li class="order_comment__comment">Ваш комментарий к заказу: </li>
                    <li class="order_comment__content">{{comment_to_executor}}</li>
                </ul>
                {{/if}}
            </div>
            <div class="col-sm-6 col-md-6">
                <div class="order_sum">
                    <ul class="order_sum_cost">
                        <li class="order_sum__cost">Предварительная стоимость:</li>
                        <li class="order_sum__cost-value">{{sum}} тг.</li>
                    </ul>
                    <ul class="order_sum_delivery">
                        <li class="order_sum__delivery">Доставка:</li>
                        <li class="order_sum__delivery-value">{{esum}} тг.</li>
                    </ul>
                    <ul class="order_sum_total">
                        <li class="order_sum__total">Итого:</li>
                        <li class="order_sum__total-value">{{sum2 esum sum}} тг.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="container order-geo">
        <div class="white__block" style="padding-bottom:8px;">
            <div style="overflow: hidden;">
                    <div class="col-md-3 col-sm-6">
                        <!--                         <div class="order-geo-image">
                            <div class="order-geo__image" style="background-image:url('../img/courier-pic.png'); background-repeat:no-repeat;"></div>
                        </div> -->
                        <div class="order-geo-courier">
                            <ul class="order-geo__courier" style="margin-top:9px">
                                <li>Ваш курьер:</li>
                                <li>{{executor.name}}</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-5 col-sm-6">
                        <div class="order-geo-status">
                            <ul class="order-geo__status">
                                <li>Статус доставки:</li>
                                <li>{{orderStatus status}}</li>
                            </ul>
                        </div>
                    </div>
                    {{!-- executor_valuation --}}
                    {{#if (hasUserValuation status 'rate_executor' rates)}}
                        <div class="col-sm-12 col-md-4" style="text-align: right;">
                            <div class="order-geo-status"  onclick="orderValuation('{{id}}')">
                                <ul class="order-geo__status" id="rateOrderExec">
                                    <li style="cursor: pointer;" >
                                        <i class="far fa-star" style="font-size: 20px;color: #f3b730;"></i>
                                        &nbsp;
                                        Оценить работу курьера
                                    </li>
                                </ul>
                            </div>
                        </div>
                    {{else}}
                        {{#if (hasUserValuationrev status 'rate_executor' rates)}}
                            <div class="col-sm-12 col-md-4" style="text-align: right;">
                                <div class="order-geo-status">
                                    <ul class="order-geo__status" id="rateOrderExec">
                                        <li style="cursor: pointer;" >
                                            Вы уже поставили свою оценку
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        {{/if}}
                    {{/if}}
                    {{#if (readyCourierChat status)}}
                        <div class="col-sm-12 col-md-4">
                            <div class="order-geo-status">
                                <div class="button-standard button-standard--orange" onclick="toChat('{{id}}')">
                                    Написать курьеру в чат
                                </div>
                            </div>
                        </div>
                    {{/if}}
            </div>
            {{#unless own_executors}}
            <div class="order-geo-map" id="ordermap{{id}}" data-ordermap="{{id}}" data-coords='{ "latitude":{{address.latitude}},"longitude":{{address.longitude}} }'>
                <div class="order-geo__map" style="background-image:url('../img/geo-map.png'); background-repeat:no-repeat;"></div>
            </div>
            {{/unless}}
        </div>
    </div>
    <div class="container order-back">
        <div class="order-back_button">
            <a data-goto="history" class="js-nav-lk" href="/cabinet/history">
                <div class="btn order-back-button pull-right">
                    <div class="order-back_image" style="background-image:url('../img/button_left.png'); background-repeat:no-repeat;"> </div>
                    <span class="order-back_name">Вернуться к списку заказов</span>
                </div>
            </a>
        </div>
    </div>
</div>
{{/each}}