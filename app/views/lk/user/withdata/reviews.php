<div class="js-wrap-slide container" data-wrap="reviews">
	<div class="row">
		{{#each this}}
		<div class="col-xs-12">
			<div class="inner-review__block">
				<div class="col-xs-12 col-md-5 col-lg-4" style="display:flex;">
					{{!-- Нигде нет фотографий --}}
					{{!-- <div class="inner-review__image" style="background-image:url('/img/reviews/japanese.png')"></div> --}}
					<div class="inner-review__wrap inner-review__wrap--ml">
						<div class="inner-review__title">
							{{#if to_user.name}}
								{{to_user.name}}
							{{else}}
								{{service.name}}
							{{/if}}
						</div>
						<div class="inner-review__date">
							{{created}}
						</div>
						{{!-- Нет оценки нет и звёзд--}}
{{!-- 						<div class="inner-review__stars">
							<i class="fas fa-star rate-star active"></i>
							<i class="fas fa-star rate-star active"></i>
							<i class="fas fa-star rate-star active"></i>
							<i class="fas fa-star rate-star"></i>
							<i class="fas fa-star rate-star"></i>
						</div> --}}
					</div>
				</div>
				<div class="col-xs-12 col-md-5 col-lg-6">
					<div class="inner-review__text">
						{{msg_valuation}}
					</div>
				</div>
				<div class="col-xs-12 col-md-2 col-lg-2" style="text-align:right;"><a class="inner-review__link" href="">Удалить отзыв</a></div>
			</div>
		</div>
		{{/each}}
		{{#unless this}}
			<div class="col-xs-12">
				<h6 style="text-align:center">У вас пока нет отзывов</h6>
			</div>
		{{/unless}}
	</div>
</div>