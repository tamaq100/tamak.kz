<div class="js-wrap-slide" data-wrap="bonus">
    <div class="container bonus-list">
        <div class="row">
            <div class="col-xs-9">
                <h6 class="bonus-list__title bonus-list__title--up">История бонусов</h6>
            </div>
            <div class="col-xs-3">
                <div class="bonus-title-count"><span class="bonus-list__bonus-title">Всего бонусов: </span><span class="bonus-list__bonus-count">{{this.bonusBalance}}</span></div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="white__block">
            <table>
                <thead>
                    <tr class="items">
                        <th class="item">Дата</th>
                        <th class="item">№ заказа</th>
                        <th class="item">Начислено/потрачено бонусов</th>
                    </tr>
                </thead>
                <tbody>
                    {{#each this.payments}}
                    <tr class="items">
                        <td class="item">{{timeOffsetPlus created}}</td>
                        <td class="item">{{number}}</td>
                        <td class="item">{{amount}}</td>
                    </tr>
                    {{/each}}
                </tbody>
            </table>
        </div>
    </div>
</div>