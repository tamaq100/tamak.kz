<div class="container">
    <div class="white__block">
        <div class="container orders">
            <div class="row">
                <div class="col-md-6">
                    <ul class="orders-number-date">
                        <li class="orders__orders-number">№00000</li>
                        <li class="orders__orders-date"><span>00.00.0000</span> в <span>00:00</span></li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <div class="orders-total-status">
                        <ul class="orders-total">
                            <li>Итого:</li>
                            <li class="orders__orders-total"><span>00 000</span> тг.</li>
                        </ul>
                        <ul class="orders-status">
                            <li>Статус:</li>
                            <li class="orders__orders-status">В обработке</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row orders-content">
            <div class="col-xs-12 col-md-6">
                <div class="orders-orders-content">
                    <div class="orders-content-image">
                        <div class="orders-content__image" style="background-image:url(''); background-repeat:no-repeat;"></div>
                    </div>
                    <div class="orders-content-name">
                        <span class="orders-content__orders-name">
                            Ресторан "<span>-- -----</span>"
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-6">
                <div class="orders-content-price">
                    <span class="orders-content__price">
                        <span>0</span> блюда на сумму <span>0 000</span> тг.
                    </span>
                </div>
            </div>
        </div>
        <div class="container orders-details">
            <div class="orders-details_button">
                <a href="/html/cabinet-customer-order.html">
                    <div class="btn orders-details-button pull-right">
                        <span>Смотреть детали заказа</span>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>