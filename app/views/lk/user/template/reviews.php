<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="inner-review__block">
                <div class="col-xs-12 col-md-5 col-lg-4" style="display:flex;">
                    <div class="inner-review__image" style="background-image:url('/img/reviews/japanese.png')"></div>
                    <div class="inner-review__wrap inner-review__wrap--ml">
                        <div class="inner-review__title">
                            Ресторан “<span>--- -----</span>“
                        </div>
                        <div class="inner-review__date">
                            <span>00.00.0000</span> 
                            <span>в</span> 
                            <span>00:00</span> 
                        </div>
                        <div class="inner-review__stars">
                            <i class="fas fa-star rate-star active"></i>
                            <i class="fas fa-star rate-star active"></i>
                            <i class="fas fa-star rate-star active"></i>
                            <i class="fas fa-star rate-star"></i>
                            <i class="fas fa-star rate-star"></i>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-5 col-lg-6">
                    <div class="inner-review__text">
                        ...
                    </div>
                </div>
                <div class="col-xs-12 col-md-2 col-lg-2" style="text-align:right;">
                    <a class="inner-review__link" href="">
                        Удалить отзыв
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>