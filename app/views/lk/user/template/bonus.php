<div class="container bonus-list">
    <div class="row">
        <div class="col-xs-9">
            <h6 class="bonus-list__title bonus-list__title--up">История бонусов</h6>
        </div>
        <div class="col-xs-3">
            <div class="bonus-title-count"><span class="bonus-list__bonus-title">Всего бонусов: </span>
                <span class="bonus-list__bonus-count">0000</span>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="white__block">
        <table>
            <thead>
                <tr class="items">
                    <th class="item">Дата</th>
                    <th class="item">№ заказа</th>
                    <th class="item">Начислено/потрачено бонусов</th>
                </tr>
            </thead>
            <tbody>
                <tr class="items">
                    <td class="item">01.12.18 в 12:47</td>
                    <td class="item">019831</td>
                    <td class="item">+ 1200</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>