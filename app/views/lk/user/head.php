<div class="container">
    <div class="user-name__title invisible">---------------------------</div>
</div>
<div class="container">
    <div class="account mt-30">
        <div class="account__info">
            <div class="account__info-block" style="min-width: 120px;"><span class="account__info-block__item">Телефон:</span><span class="account__info-block__number tel invisible">-------</span></div>
            <div class="account__info-block">
                <span class="account__info-block__item">Адрес:</span>
                <span class="account__info-block__text firstAddress invisible">Такой-то такой-то</span>
                <div class="dropdown account-dropdown invisible">
                    <button class="dropdown-toggle" id="dropdownMenu1" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        <span class="caret"></span>
                        Еще <span class="nnn">0</span> адреса
                    </button>
                    <ul class="dropdown-menu drop-menu menu-2 dre" aria-labelledby="dropdownMenu1">
                    </ul>
                </div>
            </div>
            <div class="account__info-block"><span class="account__info-block__item">Бонусов:</span><span class="account__info-block__number bon invisible">----</span>
                <a class="account__info__link js-nav-lk" data-goto="bonus" href="/cabinet/bonus">История бонусов</a>
            </div>
        </div>
        <div class="account__info">
            <button class="btn button account__info__item js-nav-lk" data-goto="redact" href="/cabinet/redact">Редактировать данные</button>
        </div>
    </div>
</div>