<div class="container">
	<div class="row">
		<ul class="breadcrumb">
			<li><a href="<?= siteURL() ?>">Главная</a></li>
			<li><a href="#">Личный кабинет</a></li>
		</ul>
	</div>
</div>
<div class="container">
	<div class="user-name__title invisible">--------- --------</div>
</div>
<div class="container">
	<div class="courier mt-22">
		<div class="courier__info">
			<div class="courier__info-block"><span class="courier__headline">Телефон</span>
			<span class="courier__caption--tel invisible">+77070000000</span>
		</div>
		<div class="courier__info-block">
			<span class="courier__headline">Транспорт</span>
			<span class="courier__caption transport">----</span>
		</div>
		<div class="courier__info-block">
			<span class="courier__headline">Рейтинг</span>
			<div class="courier-review__stars" style="position: relative; transform: translateY(-50%) perspective(1px);display: flex;">
				<i class="fas fa-star rate-star"></i>
				<i class="fas fa-star rate-star"></i>
				<i class="fas fa-star rate-star"></i>
				<i class="fas fa-star rate-star"></i>
				<i class="fas fa-star rate-star"></i>
				<div class="front-rate" style="position:absolute;left:0;top:0;display:flex;flex-wrap:nowrap;flex-shrink:0;overflow:hidden;align-content:center;">
					<i class="fas fa-star rate-star active"></i>
					<i class="fas fa-star rate-star active"></i>
					<i class="fas fa-star rate-star active"></i>
					<i class="fas fa-star rate-star active"></i>
					<i class="fas fa-star rate-star active"></i>
				</div>
			</div>
		</div>
		<div class="courier__info-block"><span class="courier__headline">Баланс</span><span class="courier__caption bon invisible">0000</span></div>
	</div>
	<div class="courier__buttons">
		<button class="courier__btn-new_order white-bg invisible">
		<span class="courier__button-text">2 новых заказа</span>
		</button>
		<div class="courier__button-block">
			<button class="btn button-courier courier__btn-edit js-nav-lk" href="/cabinet/kredact" class="js-nav-lk" data-goto="kredact">Редактировать данные</button>
			<button class="btn button-courier courier__btn-options"  data-target="#modal-settings" data-toggle="modal">Параметры заказов</button>
		</div>
	</div>
</div>
</div>