<div class="js-wrap-slide" data-wrap="chistory">
	{{#if this.newOrders}}
	<div class="container">
		<div class="restoraunt-side__title">НОВЫЕ ЗАКАЗЫ</div>
	</div>
	<div class="container">
		{{#each this.newOrders}}
			<div class="white__block courier-order">
				<div class="courier-order__block">
					{{timeOffsetPlusYMD published_time}}
				</div>
				<div class="courier-order__block courier-order__block--number">
					№{{number}}
				</div>
				<div class="courier-order__block" style="width:160px">
					{{address.street}}
				</div>
				<div class="courier-order__block" style="width:160px">
					{{orderStatus status}}
				</div>
				<div class="courier-order__block courier-order__block--buttons">
					<div class="button-standard button-standard--mini button-standard--orange my-8">
						Принять заказ
					</div>
					<div class="button-standard button-standard--mini my-8">
						отказаться
					</div>
				</div>
			</div>
		{{/each}}
	</div>
	{{/if}}
	<div class="container mt-15 mb-15">
		<div class="restoraunt-side__title">История заказов</div>
	</div>
{{!-- 	<div class="container mb-15">
		<div class="page__sort--ondelimiter">
			<span class="upp">Период:</span>
			<span class="page__sort--delimiter">
				<div class="select-tamaq select-tamaq__select--mgin">
					<select class="select-tamaq__select">
						<option>255</option>
						<option>4584</option>
						<option>777</option>
					</select>
				</div>
				<span>- </span>
				<div class="select-tamaq select-tamaq__select--mgin">
					<select class="select-tamaq__select">
						<option>255</option>
						<option>4584</option>
						<option>777</option>
					</select>
				</div>
			</span>
		</div>
	</div> --}}
	{{#if this.lastOrders}}
	<div class="container">
		{{#each this.lastOrders}}
		<div class="white__block courier-order">
			<div class="courier-order__block">{{timeOffsetPlusYMD published_time}}</div>
			<div class="courier-order__block courier-order__block--number">№{{number}}</div>
			<div class="courier-order__block" style="width:160px">{{address.street}}</div>
			<div class="courier-order__block" style="width:160px">{{orderStatus status}}</div>
			<div class="courier-order__block courier-order__block--buttons">
				<a class="js-nav-lk" data-goto="order{{id}}" href="/cabinet/korder?id={{id}}">
					<div class="button-standard button-standard--mini my-8">детали заказа</div>
				</a>
			</div>
		</div>
		{{/each}}
	</div>
	{{/if}}
</div>
