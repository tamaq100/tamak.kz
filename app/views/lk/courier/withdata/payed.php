<div class="js-wrap-slide" data-wrap="payed">
	<div class="container mb-15">
		<div class="page__sort--ondelimiter"><span class="upp">Период:</span>
			<span class="page__sort">
				<div class="select-tamaq select-tamaq__select--mgin">
					<select class="select-tamaq__select" style="font-size: 14px;">
							<option data-unix="0">----</option>
						{{#days_sort this.payments}}
							{{#each this}}
								<option data-unix="{{this.unix}}">{{timeOffsetPlus this.date}}</option>
							{{/each}}
						{{/days_sort}}
					</select>
				</div>
			</span>
		</div>
	</div>
	<div class="container">
		<div class="white__block">
			<table>
				<thead>
					<tr class="items">
						<th class="item"></th>
						<th class="item"></th>
						<th class="item"></th>
						<th class="item"></th>
					</tr>
				</thead>
				<tbody>
				{{#each this.payments}}
					<tr class="items" data-unix="{{to_nix created}}">
						<td class="item">{{timeOffsetPlusYMD created}}</td>
						<td class="item item--bold">Заказ №{{number}}</td>
						<td class="item">{{details}}</td>
						<td class="item">{{amount}}</td>
					</tr>
				{{/each}}
				</tbody>
			</table>
		</div>
	</div>
</div>