{{#each this}}
<div class="js-wrap-slide" data-wrap="order{{id}}">
	<div class="container order-back mb-15">
		<div class="info_container-main">
			<div class="restoraunt-side__title">№{{number}}</div>
			<div class="info__container">
				<span class="info__label">Время:</span><span class="info__body">{{timeOffsetPlus created}}</span>
			</div>
			<div class="info__container info__container--relative">
				<span class="info__label">Статус:</span>
				<span class="info__body">{{orderStatus status}}</span>
				<span class="info__body info__body--info">i</span>
			</div>
			<a class="info__back" href="/cabinet/khistory">
				<div class="btn order-back-button">
					<div class="order-back_image" style="background-repeat:no-repeat;"> </div>
					<span class="order-back_name">Вернуться к списку заказов</span>
				</div>
			</a>
		</div>
	</div>
	<div class="container">
	  <div class="white__block white__block--big-px">
		<div class="white__row">
		  <div class="white-row__block">
			<div class="order-geo__title">Время:</div>
			<div class="order-comment__desc t-500">{{timeOffsetPlus created}}</div>
		  </div>
		  <div class="white-row__block">
			<div class="order-geo__title">Адрес доставки:</div>
			<div class="order-comment__desc t-500">{{address.street}}</div>
		  </div>
		  <div class="white-row__block">
			<div class="order-geo__title">Сумма заказа:</div>
			<div class="order-comment__desc t-500">{{sum}} тг.</div>
		  </div>
		  <div class="white-row__block">
			<div class="order-geo__title">Сумма доставки:</div>
			<div class="order-comment__desc t-500">{{esum}} тг.</div>
		  </div>
		  <div class="white-row__block">
			<div class="order-geo__title">Статус:</div>
			<div class="order-comment__desc t-500">{{orderStatus status}}</div>
		  </div>
		</div>
		<div class="white__row">
		  <div class="white-row__block">
			<div class="order-geo__title">Комментарий к заказу:</div>
			<div class="order-comment">
				{{#if comment_to_executor}}
					{{comment_to_executor}}
				{{else}}
					Пользователь не оставил комментарий к этому заказу
				{{/if}}
			</div>
		  </div>
		</div>
	  </div>
	</div>
	<div class="container order-geo">
		<div class="white__block" style="padding-bottom: 0;">
			<div class="container">
				<div class="row">
					<div class="col-md-3">
						<!--                         <div class="order-geo-image">
							<div class="order-geo__image" style="background-image:url('../img/courier-pic.png'); background-repeat:no-repeat;"></div>
						</div> -->
					<div class="order-geo__block">
						<div class="order-geo__title">Адрес доставки:</div>
						<div class="order-geo__desc">Ул. Сатыбалдина - пр. Кунанбаева</div>
					</div>
					</div>
					<div class="col-md-3">
						<div class="order-geo-status">
							<ul class="order-geo__status">
								<li>Статус доставки:</li>
								<li>{{orderStatus status}}</li>
							</ul>
						</div>
					</div>
					{{#if (readyCourierChat status)}}
						<div class="col-sm-12 col-md-6">
							<div class="order-geo-status">
								<div class="button-standard button-standard--orange" onclick="toChat('{{id}}')">
									Написать клиенту в чат
								</div>
							</div>
						</div>
					{{/if}}
				</div>
			</div>
			<div class="order-geo-map" id="ordermap{{id}}" data-ordermap="{{id}}" data-coords='{ "latitude":{{address.latitude}},"longitude":{{address.longitude}} }'>
				<div class="order-geo__map" style="background-image:url('../img/geo-map.png'); background-repeat:no-repeat;"></div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="restoraunt-side__title mt-30 mb-15">Детали заказа</div>
	</div>
	<div class="container">
		<div class="white__block">
			<div class="container order">
				<div class="row">
					<div class="col-xs-12 col-sm-5 col-md-3">
						<div class="order-order">
							<div class="order-image">
								{{#if service.photos.[0].path}}
									<div class="order__image" style="background-image:url('<?= Api::$apiDomain ?>{{service.photos.[0].path}}');">
								{{else}}
									<div class="order__image" style="background-image:url('<?= Api::$apiDomain ?>/imgs/{{service.id}}_photo1.png');">
								{{/if}}
								</div>
							</div>
							<div class="order-name-type">
								<p class="order__order-name">{{service.name}}</p><span class="order__order-type">Ресторан</span>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-7 col-md-9">
						<ul class="order-amt-count">
							<li class="order__order-amt">{{counterInArray order_products 'amt'}} {{declOfNumDishes order_products 'amt'}}</li>
							<li class="order__order-count">{{sum}} тг.</li>
						</ul>
						<ul class="order-delivery-cost">
							<li class="order__order-delivery">доставка</li>
							<li class="order__order-cost">{{esum}} тг.</li>
						</ul>
					</div>
				</div>
			</div>
			{{#each order_products}}
			<div class="row order-content">
				<div class="col-xs-12 col-md-6 col-lg-6">
					<div class="order-order-content">
						<div class="order-content-image">
							{{!-- Внимание нужно сделать хэлпер для вытаскивания нормального изображения --}}
							<div class="order-content__dishes" style="background-image:url('https://tamaq.kz{{photos.[0].path}}'); background-repeat:no-repeat;background-size: cover;"></div>
						</div>
						<div class="order-content-name">
							<div class="order-content__order-name">{{name}}, {{amt}} шт</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-5 col-md-3 col-lg-3">
					<div class="order-content-price">
						<ul class="order-content_price">
							<li class="order-content__order-price">{{price}} тг.</li>
							<li class="order-content__order-portion">Порций: <span class="order-content-amt">{{amt}}</span></li>
						</ul>
					</div>
				</div>
				<div class="col-xs-12 col-sm-7 col-md-3 col-lg-3">
					<ul class="order-content-sum">
						<li class="order-content__order-sum">Сумма: </li>
						<li class="order-content__order-count">{{sum}} тг.</li>
					</ul>
				</div>
			</div>
			{{/each}}
		</div>
	</div>
	<div class="container order-back">
		<div class="order-back_button">
			<a data-goto="history" class="js-nav-lk" href="/cabinet/khistory">
				<div class="btn order-back-button pull-right">
					<div class="order-back_image" style="background-image:url('../img/button_left.png'); background-repeat:no-repeat;"> </div>
					<span class="order-back_name">Вернуться к списку заказов</span>
				</div>
			</a>
		</div>
	</div>
</div>
{{/each}}