{{#if chatId}}
<div class="js-wrap-slide" data-wrap="сchat" data-chatid="{{chatId}}">
{{else}}
<div class="js-wrap-slide" data-wrap="сchat" data-chatid="0">
{{/if}}
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
{{!-- <!-- 	            <div class="button-wrap chat__buttons mb-30">
					<a class="button-standard button-standard--orange" href="">Позвонить курьеру</a>
					<a class="button-standard" href="">Позвонить менеджеру</a>
				</div> -->
 --}}			
 					{{#unless chatId}}
	 				{{#if lastChats}}
		 				{{#each lastChats}}
		 					<div class="container">
		 						<div class="white__block">
		 							<div class="row">
		 								<div class="orders-number-date">
											<ul class="orders-number-date">
												<li class="orders__orders-number">№2399</li>
												<li class="orders__orders-date">
												2019-07-09 08:49:35
												</li>
											</ul>									
		 								</div>
		 							</div>
		 						</div>
		 					</div>
		 				{{/each}}
		 			{{else}}
		 				<h3>
			 				У вас нет активных чатов
		 				</h3>
	 				{{/if}}
				{{/unless}}
				{{#if chatId}}
				 <form class="white__block message__area">
					<textarea class="message__field"></textarea>
					<button class="message__button button-standard button-standard--small">Отправить</button>
				</form>
				{{/if}}
				<div class="messages__area mt-30 mb-30">
					{{#if this.data}}
					{{#each this.data}}
						{{#cond "User.getMe().name !== this.user_name"}}
							<div class="messages__wrapper">
								<div class="white__block messages__message">
									<div class="messages__name">Клиент</div>
									<div class="messages__text">{{msg}}</div>
									<div class="messages__date">
										<span>{{split created '0'}}</span>
										<span>{{split created '1'}}</span>
									</div>
								</div>
							</div>
						{{/cond}}
						{{#cond "User.getMe().name === this.user_name"}}
							<div class="messages__wrapper messages__wrapper--left">
								<div class="white__block messages__message">
									<div class="messages__name messages__name--user">Вы</div>
									<div class="messages__text">{{msg}}</div>
									<div class="messages__date messages__date--left">
										<span>{{split created '0'}}</span>
										<span>{{split created '1'}}</span>
									</div>
								</div>
							</div>
						{{/cond}}
					{{/each}}
					{{/if}}
				</div>
			</div>
		</div>
	</div>
</div>
<div class="hidden" id="msgTempl">
	<div class="messages__wrapper">
		<div class="white__block messages__message">
			<div class="messages__name"></div>
			<div class="messages__text"></div>
			<div class="messages__date">
				<span class="one"></span>
				<span class="two"></span>
			</div>
		</div>
	</div>
</div>