<div class="js-wrap-slide" data-wrap="creviews">
	<div class="container">
	  <div class="row">

	  	{{#each this}}
	  	{{#if (typerate_executor typerate.key)}}
	  	
		<div class="col-xs-12">
		  <div class="inner-review__block inner-review__wrap--vam">
			<div class="col-xs-12 col-md-8 col-lg-8 inner-review--sm-specific">
			  <div class="inner-review__text">
			  	{{msg_valuation}}
			  	{{!-- {{typerate.key}} --}}
			  	{{#unless msg_valuation}}
			  		нет текста отзыва
			  	{{/unless}}
			  </div>
			</div>
			<div class="col-xs-12 col-md-4 col-lg-4 inner-review__wrap--right inner-review__wrap--flex-start inner-review--sm-specific2">
			  <div class="inner-review__wrap inner-review__wrap--ib">

				<div class="inner-review__rate">Вам поставили рейтинг:</div>

				<div style="display:flex;margin-top: 9px;flex-direction: column;align-items: end;">
					<span class="inner-review__rate">Скорость доставки</span>
					<div class="inner-review__stars restoraunt__stars" style="margin-top:6px;margin-left:0px; position: relative;display: flex;width:fit-content;max-width: intrinsic; height:14px;">
						<i class="fas fa-star rate-star"></i>
						<i class="fas fa-star rate-star"></i>
						<i class="fas fa-star rate-star"></i>
						<i class="fas fa-star rate-star"></i>
						<i class="fas fa-star rate-star"></i>
						<div class="front-rate" style="position:absolute;left:0;top:0;display:flex;flex-wrap:nowrap;flex-shrink:0;overflow:hidden;align-content:center;width:{{percentagePercentFive speed_rate}}%">
							<i class="fas fa-star rate-star active"></i>
							<i class="fas fa-star rate-star active"></i>
							<i class="fas fa-star rate-star active"></i>
							<i class="fas fa-star rate-star active"></i>
							<i class="fas fa-star rate-star active"></i>
						</div>
					</div>
				</div>
				<div style="display:flex;margin-top: 9px;flex-direction: column;align-items: end;">
					<span class="inner-review__rate">Вежливость</span>
					<div class="inner-review__stars restoraunt__stars" style="margin-top:6px;margin-left:0px; position: relative;display: flex;width:fit-content;max-width: intrinsic; height:14px;">
						<i class="fas fa-star rate-star"></i>
						<i class="fas fa-star rate-star"></i>
						<i class="fas fa-star rate-star"></i>
						<i class="fas fa-star rate-star"></i>
						<i class="fas fa-star rate-star"></i>
						<div class="front-rate" style="position:absolute;left:0;top:0;display:flex;flex-wrap:nowrap;flex-shrink:0;overflow:hidden;align-content:center;width:{{percentagePercentFive civility_rate}}%">
							<i class="fas fa-star rate-star active"></i>
							<i class="fas fa-star rate-star active"></i>
							<i class="fas fa-star rate-star active"></i>
							<i class="fas fa-star rate-star active"></i>
							<i class="fas fa-star rate-star active"></i>
						</div>
					</div>
				</div>

			  </div>
			</div>
		  </div>
		</div>
	  	{{/if}}
		{{/each}}

	  </div>
	</div>
</div>