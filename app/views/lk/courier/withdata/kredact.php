<form class="container js-wrap-slide kredact" data-wrap="kredact">
    <span class="text-success okay_message invisible">Изменения вступили в силу!</span>
    <div class="row">
        <div class="col-lg-9 col-xs-12 mt-15">
            <table class="clear__table" border="0" style="width:100%">
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
                <tr>
                    <td><span class="settings-table__label">Ваше имя:</span></td>
                    <td>
                        <input class="settings-table__input" type="text" name="name" value="{{this.name}}" />
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td><span class="settings-table__label">Телефон:</span>
                    <td>
                        <input class="settings-table__input" type="telephone" name="telephone" value="{{this.phone}}"/>
                         <small style="font-size: 12px;font-weight: 300; max-width: 50px" class="settings-table__label">после смены телефонного номера, придёт смс с кодом подтверждения</small>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        
                    </td>
                    <td>
                        <span class="text-danger error__message invisible">Введены неверные данные</span>
                    </td>
                    <td>
                        
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <div class="wrap mt-15">
                            <button class="button-standard mi-w150 mb-15 button-standard--orange" type="submit">
                                Сохранить
                            </button>
                            <a class="button-standard mi-w150 mb-15" href="/cabinet">
                                Отмена
                            </a>
                        </div>
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</form>