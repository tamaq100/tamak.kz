<!DOCTYPE html>
<html class="page" lang="en">
	<head>
		<meta charset="UTF-8"/>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimum-scale=1.0, shrink-to-fit=no"/>
		<?php if (isset($meta['title']) && $meta['title']) : ?>
		<title><?= $meta['title'] ?></title>
		<meta name="title" content="<?= $meta['title'] ?>" />
		<meta content="<?= $meta['title'] ?>" property="og:title" />
		<?php else: ?>
		<?php endif ?>
		<?php if (isset($meta['description']) && ($meta['description'] != '')): ?>
		<meta name="description" content="<?= $meta['description'] ?>">
		<meta content="<?= $meta['description'] ?>" property="og:description" />
		<?php endif ?>
		<?php if (isset($meta['keywords']) && ($meta['keywords'] != '')): ?>
		<meta name="keywords" content="<?= $meta['keywords'] ?>">
		<?php endif ?>
		<link rel="preload" as="image" href="/img/preloaders/rounded.svg">
		<link rel="apple-touch-icon" sizes="57x57" href="/img/favicon/apple-icon-57x57.png"/>
		<link rel="apple-touch-icon" sizes="60x60" href="/img/favicon/apple-icon-60x60.png"/>
		<link rel="apple-touch-icon" sizes="72x72" href="/img/favicon/apple-icon-72x72.png"/>
		<link rel="apple-touch-icon" sizes="76x76" href="/img/favicon/apple-icon-76x76.png"/>
		<link rel="apple-touch-icon" sizes="114x114" href="/img/favicon/apple-icon-114x114.png"/>
		<link rel="apple-touch-icon" sizes="120x120" href="/img/favicon/apple-icon-120x120.png"/>
		<link rel="apple-touch-icon" sizes="144x144" href="/img/favicon/apple-icon-144x144.png"/>
		<link rel="apple-touch-icon" sizes="152x152" href="/img/favicon/apple-icon-152x152.png"/>
		<link rel="apple-touch-icon" sizes="180x180" href="/img/favicon/apple-icon-180x180.png"/>
		<link rel="icon" type="image/png" sizes="192x192" href="/img/favicon/android-icon-192x192.png"/>
		<link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png"/>
		<link rel="icon" type="image/png" sizes="96x96" href="/img/favicon/favicon-96x96.png"/>
		<link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png"/>
		<link rel="manifest" href="../img/favicon/manifest.json"/>
		<meta name="msapplication-TileColor" content="#ffffff"/>
		<meta name="msapplication-TileImage" content="../img/favicon/ms-icon-144x144.png"/>
		<meta name="theme-color" content="#ffffff"/>
		<script>
		var fullAjOff = false;
		</script>
		<?php foreach ($styles as $src): ?>
		<link rel="stylesheet" href="<?= $src ?>?v=<?= filemtime(WORK_DIR . $src) ?>"/>
		<?php endforeach ?>
		<?php foreach ($scripts as $src): ?>
		<script src="/vendor/handlebars.js"></script>
		<?php if (preg_match('/jquery/iemU', $src)): ?>
		<script src="<?= $src ?>?v=<?= filemtime(WORK_DIR . $src) ?>"></script>
		<?php endif ?>
		<?php endforeach; ?>
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700&amp;amp;subset=cyrillic">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Pacifico">
	</head>
	<body>
		<script>
		var heroesData = {
			"hello": {
				"hero": "01",
				"msg": "01"
			},
			"help": {
				"hero": "06",
				"special": true
			},
			"anticipation": {
				"hero": "02",
				"msg": "02"
			},
			"sadness": {
				"hero": "04",
				"msg": "04"
			},
			"forchat": {
				"hero": "05",
				"msg": "05"
			},
			"orderStatus": {
				"hero": "08",
				"msg": "08"
			},
			"statusPending": {
				"hero": "09",
				"msg": "09"
			}
		}
		</script>
		<!-- Костыль -->
		<div class="get-order-widget" style="display: none;"></div>
		<!-- Костыль -->
		<div class="welcome-hero" id="welcome_hero">
			<img class="welcome-hero__ico" src="/img/heroes/hero01.png" alt=""/>
			<img class="welcome-hero__text" src="/img/icons/welcome-hero_text.png" alt=""/>
			<a class="welcome-hero_close" href="#" id="welcome_hero_close"></a>
		</div>
		<div class="page-inner addTopApp">
			<?= include_file('default/header') ?>
			<main class="page-content page-content--gray">
				<?= include_file('lk/helpers/breadcrumbs') ?>
				<?= include_file('lk/user/head') ?>
				<?= include_file('lk/helpers/nav', $vars['addons']['menu']) ?>
				<div class="js-wrap-container">
					<div class="js-wrap-preload"></div>
					<?= $content ?>
				</div>
			</main>
			<?= include_file('default/footer') ?>
			<?= include_file('default/modal-fade'); ?>
		</div>
		<div id="order-billet" class="order-billet">
			<div class="container">
			<div class="order-billet__container" style="min-height: 81px;">
				<div class="order-billet__wrap">
					<div class="order-billet-food" style="display: inline-block;"></div>
					<span>
					<div class="order-billet-smalltext" style="display: inline-block;">Ваш заказ на сумму:</div>
					<div class="order-billet-cost" id="basketSumCost" style="display: inline-block;">2400 тг.</div>
					</span>
				</div>
				<a href="/basket" class="order-billet-go">
					<span>Перейти в корзину</span>
				</a>
			</div>
			</div>
		</div>
		<?php foreach ($scripts as $src): ?>
		<?php if (!preg_match('/jquery/iemU', $src)): ?>
		<script src="<?= $src ?>?v=<?= filemtime(WORK_DIR . $src) ?>"></script>
		<?php endif; ?>
		<?php endforeach ?>
	</body>
</html>