<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head style="font-family:'Helvetica Neue',Helvetica,Helvetica,Arial,sans-serif;font-size:100%;line-height:1.6em;margin:0;padding:0">
    <meta name="viewport" content="width=device-width" style="font-family:'Helvetica Neue',Helvetica,Helvetica,Arial,sans-serif;font-size:100%;line-height:1.6em;margin:0;padding:0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" style="font-family:'Helvetica Neue',Helvetica,Helvetica,Arial,sans-serif;font-size:100%;line-height:1.6em;margin:0;padding:0">
    <title style="font-family:'Helvetica Neue',Helvetica,Helvetica,Arial,sans-serif;font-size:100%;line-height:1.6em;margin:0;padding:0"><?= $title ?></title>
</head>

<body bgcolor="#f6f6f6" style="-webkit-font-smoothing:antialiased;-webkit-text-size-adjust:none;font-family:'Helvetica Neue',Helvetica,Helvetica,Arial,sans-serif;font-size:100%;height:100%;line-height:1.6em;margin:0;padding:0;width:100%!important">
    <style></style>
    <table class="body-wrap" bgcolor="#f6f6f6" style="font-family:'Helvetica Neue',Helvetica,Helvetica,Arial,sans-serif;font-size:100%;line-height:1.6em;margin:0;padding:20px;width:100%">
        <tbody>
            <tr style="font-family:'Helvetica Neue',Helvetica,Helvetica,Arial,sans-serif;font-size:100%;line-height:1.6em;margin:0;padding:0">
                <td style="font-family:'Helvetica Neue',Helvetica,Helvetica,Arial,sans-serif;font-size:100%;line-height:1.6em;margin:0;padding:0"></td>
                <td class="container" bgcolor="#FFFFFF" style="Margin:0 auto!important;border:1px solid #f0f0f0;clear:both!important;display:block!important;font-family:'Helvetica Neue',Helvetica,Helvetica,Arial,sans-serif;font-size:100%;line-height:1.6em;margin:0;max-width:600px!important;padding:20px">
                    <div class="content" style="display:block;font-family:'Helvetica Neue',Helvetica,Helvetica,Arial,sans-serif;font-size:100%;line-height:1.6em;margin:0 auto;max-width:600px;padding:0">
                        <table style="font-family:'Helvetica Neue',Helvetica,Helvetica,Arial,sans-serif;font-size:100%;line-height:1.6em;margin:0;padding:0;width:100%">
                            <tbody>
                                <tr style="font-family:'Helvetica Neue',Helvetica,Helvetica,Arial,sans-serif;font-size:100%;line-height:1.6em;margin:0;padding:0">
                                    <td style="font-family:'Helvetica Neue',Helvetica,Helvetica,Arial,sans-serif;font-size:100%;line-height:1.6em;margin:0;padding:0">
                                        <p style="font-family:'Helvetica Neue',Helvetica,Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.6em;margin:0;margin-bottom:10px;padding:0"><?= $content ?></p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </td>
                <td style="font-family:'Helvetica Neue',Helvetica,Helvetica,Arial,sans-serif;font-size:100%;line-height:1.6em;margin:0;padding:0"></td>
            </tr>
        </tbody>
    </table>
    <table class="footer-wrap" style="clear:both!important;font-family:'Helvetica Neue',Helvetica,Helvetica,Arial,sans-serif;font-size:100%;line-height:1.6em;margin:0;padding:0;width:100%">
        <tbody>
            <tr style="font-family:'Helvetica Neue',Helvetica,Helvetica,Arial,sans-serif;font-size:100%;line-height:1.6em;margin:0;padding:0">
                <td style="font-family:'Helvetica Neue',Helvetica,Helvetica,Arial,sans-serif;font-size:100%;line-height:1.6em;margin:0;padding:0"></td>
                <td class="container" style="Margin:0 auto!important;clear:both!important;display:block!important;font-family:'Helvetica Neue',Helvetica,Helvetica,Arial,sans-serif;font-size:100%;line-height:1.6em;margin:0;max-width:600px!important;padding:0">
                    <div class="content" style="display:block;font-family:'Helvetica Neue',Helvetica,Helvetica,Arial,sans-serif;font-size:100%;line-height:1.6em;margin:0 auto;max-width:600px;padding:0">
                        <table style="font-family:'Helvetica Neue',Helvetica,Helvetica,Arial,sans-serif;font-size:100%;line-height:1.6em;margin:0;padding:0;width:100%">
                            <tbody>
                                <tr style="font-family:'Helvetica Neue',Helvetica,Helvetica,Arial,sans-serif;font-size:100%;line-height:1.6em;margin:0;padding:0">
                                    <td align="center" style="font-family:'Helvetica Neue',Helvetica,Helvetica,Arial,sans-serif;font-size:100%;line-height:1.6em;margin:0;padding:0"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </td>
                <td style="font-family:'Helvetica Neue',Helvetica,Helvetica,Arial,sans-serif;font-size:100%;line-height:1.6em;margin:0;padding:0"></td>
            </tr>
        </tbody>
    </table>
</body>

</html>