<main class="page-content">
	<div class="news-block gray-bg">
		<div class="container">
			<div class="row">
				<ul class="breadcrumb">
					<li class="active"><a href="<?= siteURL() ?>">
						Главная
					</a></li>
					<li><a href="<?= actualLinkNonFiltered() ?>"><?= $title ?></a></li>
				</ul>
			</div>
		</div>
		<div class="container">
			<h2 class="news-block__title"><?= $title ?></h2>
			<?php foreach ($news as $article): ?>
			<div class="news-block__content">
				<div class="row">
				<?php  $ActualLink = $article->link ? $article->link : $article->id?>
				<?php if ($article->image): ?>
					<div class="col-md-3 col-xs-12 col-lg-3">
						<a class="news-block__pic__wrap" href="news/show?furl=<?= $ActualLink ?>">
							<img src="/images/fit/190/145/<?= $article->image ?>.jpg" alt=""/>
						</a>
						</div>
					<div class="col-md-9 col-xs-12 col-lg-9">
						<p class="news-block__date">
							<?= substr($article->created ,0, -6) ?>
						</p>
						<h4 class="news-block__subtitle">
							<a href="news/show?furl=<?= $ActualLink ?>">
								<?= $article->title ?>
							</a>
						</h4>
						<?php if ($article->preview): ?>
						<div class="news-block__desc">
							<?= $article->preview ?>
						</div>
						<?php endif ?>
					</div>
				<?php else: ?>
					<div class="col-md-12 col-xs-12">
						<p class="news-block__date"><?= substr($article->created ,0, -6) ?></p>
						<h4 class="news-block__subtitle">
						<a href="news/show?furl=<?= $ActualLink ?>">
							<?= $article->title ?>
						</a>
						</h4>
						<?php if ($article->preview): ?>
						<div class="news-block__desc"><?= $article->preview ?></div>
						<?php endif ?>
					</div>	
				<?php endif ?>
				</div>
			</div>
			<?php endforeach ?>
			<!-- news-block__pagination за сочетание двух разных типов нужно штрафовать печеньками -->
			<?= include_file('default/pagination_news', $pagination);  ?>
		</div>
	</div>
	<div class="welcome-hero" id="welcome_hero"><img src="/img/icons/welcome-hero.png" alt=""/><img class="welcome-hero__text" src="/img/icons/welcome-hero_text.png" alt=""/><a class="welcome-hero_close" href="#" id="welcome_hero_close"></a></div>
</main>