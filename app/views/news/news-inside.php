<main class="page-content">
	<div class="news-inside gray-bg">
		<div class="container">
			<div class="row">
				<ul class="breadcrumb">
					<li class="active">
						<a href="<?= siteURL() ?>">
							Главная
						</a>
					</li>
					<li>
						<a href="/news">Новости</a>
					</li>
					<li>
						<a href="#">
							<?= $article->title ?>
						</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="container">
			<h2 class="news-inside__title"><?= $article->title ?></h2>
			<div class="news-inside__content">
				<div class="row">
					<?php if ($article->image): ?>
					<div class="col-md-6">
						<div class="news-inside__img" style="background-image: url('/images/fit/415/320/<?= $article->image ?>.jpg');"></div>
					</div>
					<div class="col-md-6">
					<?php else: ?>
					<div class="col-md-12">
					<?php endif ?>
						<div class="news-inside__desc"><?= $article->preview ?></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="news-inside__text"><?= $article->text ?></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="news-inside__follow">поделиться</div>
						<div class="social-buttons light-social-buttons">
<!-- 							<a class="social-button" href="#">
								<i class="fab fa-instagram"></i>
							</a> -->
							<a class="social-button" href="http://www.facebook.com/sharer.php?u=<?= $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] ?>" target="blank">
								<i class="fab fa-facebook"></i>
							</a>
							<a class="social-button" href="https://vk.com/share.php?url=<?= $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] ?>&amp;text=<?= $article->title ?>&amp;hashtags=<?= $_SERVER['HTTP_HOST'] ?>" target="blank">
								<i class="fab fa-vk"></i>
							</a>
						</div>
					</div>
				</div>
			</div>
			<?php if ($next = $article->next): ?>
				<a href="/news/show?furl=<?= $next->id ?>">
					<button class="btn button dish-order-button news-inside__button disable-md">назад</button>
					<button class="btn button dish-order-button news-inside__button disable-xs">предыдущая новость</button>
				</a>
			<?php endif ?>
			<?php if ($previous = $article->previous): ?>
				<a href="/news/show?furl=<?= $previous->id ?>">
					<button class="btn button dish-order-button btn-right news-inside__button disable-md">далее</button>
					<button class="btn button dish-order-button btn-right news-inside__button disable-xs">следующая новость</button>
				</a>
			<?php endif ?>
		</div>
	</div>
	<div class="welcome-hero" id="welcome_hero"><img src="/img/icons/welcome-hero.png" alt=""/><img class="welcome-hero__text" src="/img/icons/welcome-hero_text.png" alt=""/><a class="welcome-hero_close" href="#" id="welcome_hero_close"></a></div>
</main>