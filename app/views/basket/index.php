<main class="page-content page-content--gray">
	<div class="container">
		<div class="row">
			<ul class="breadcrumb">
				<li>
					<a href="<?= siteURL() ?>">Главная</a>
				</li>
				<li>
					<a href="<?= actualLinkNonFiltered() ?>">Корзина</a>
				</li>
			</ul>
		</div>
	</div>
	<!-- preload -->
	<div class="container js-wrap-container-rest js-wrap-container-rest preload"  style="margin-top:20px;padding-top: 5px;">
		<div class="js-wrap-preload"></div>
		<div class="row" id="basket_container">
			<script id="basket_menu" type="text/x-handlebars-template">
				{{#if (user_is_auth)}}
				{{else}}
				<form class="white__block white__block--big-px js-menu-rest" id="userdata" style="display:flex; justify-content: space-between; flex-wrap:wrap;margin-top:0;">
					<div class="wrap-dib">
						<div class="restoraunt-filter__title restoraunt-filter__title--small">
							Данные
						</div>
						<div class="micro-nav micro-nav--small">
							<a class="pr-11 micro-nav__text micro-nav__text--small micro-nav__text--active upp" href="" >
								Без регистрации
							</a>
							<a class="pr-10 micro-nav__text micro-nav__text--small upp" data-target="#main-modal" data-toggle="modal" href="">
								регистрация
							</a>
							<a class="pr-10 micro-nav__text micro-nav__text--small upp" data-target="#main-modal" data-toggle="modal" href="">
								вход
							</a>
						</div>
						<input class="basket-input" type="text" name="name" placeholder="* Имя Фамилия"/>
						<input class="basket-input" type="text" name="number" placeholder="Телефон"/>
					</div>
				</form>
				{{/if}}
				<form class="white__block white__block--big-px js-menu-rest" id="pays" style="display:flex; justify-content: space-between; flex-wrap:wrap;margin-top:0px;">
					<div class="wrap-dib">
						<div class="restoraunt-filter__title">Способы оплаты</div>
						<label class="restoraunt-filter__checkbox restoraunt-filter__checkbox--radio">
							{{#if (isCash)}}
								<input type="radio" name="type_pay" value="cash" checked="true"/>
							{{else}}
								<input type="radio" name="type_pay" value="cash"/>
							{{/if}}
							<span class="text">Оплата наличными курьеру</span>
							<span class="checkmark"></span>
						</label>
						<label class="restoraunt-filter__checkbox restoraunt-filter__checkbox--radio">
							{{#if (isCard)}}
								<input type="radio" name="type_pay" value="card" checked="true"/>
							{{else}}
								<input type="radio" name="type_pay" value="card"/>
							{{/if}}
							<span class="text">Оплата картой онлайн</span>
							<span class="checkmark"></span>
						</label>
					</div>
					<div class="wrap-dib">
						<span class="type_pay type_pay--cash hidden" data-paytype="cash">
							<label>Сдача с</label>
							<input class="basket-input" type="text">
						</span>
{{!-- 						<span class="type_pay type_pay--card hidden" data-paytype="card">
							<input class="basket-input" type="">
						</span> --}}
					</div>
					{{#if (user_is_auth)}}
{{!-- 					<div class="wrap-dib">
						<label class="restoraunt-filter__checkbox restoraunt-filter__checkbox--radio">
							<input type="radio" name="pay_bonus"/>
							<span class="text">Оплата бонусами</span>
							<span class="checkmark"></span>
						</label>
					</div> --}}
					{{/if}}
				</form>
				<form class="white__block white__block--big-px js-menu-rest" style="display:flex; justify-content: space-between; flex-wrap:wrap;padding:0;min-height:500px">
					<div class="wrap-dib" style="width: 100%;padding:0px 30px">
						<div class="restoraunt-filter__title" style="padding-top:30px;" id="addrTotal">Адрес* <span style="font-size: 14px;color: #484848;"></span></div>
					</div>
						<div class="map" id="map" style="width: 100%;min-height: 200px;max-height:350px;">
						</div>
					<div class="wrap-dib" style="width: 100%;padding:0px 30px">
					{{#if (user_is_auth)}}
						{{#if (useraddressesOption)}}
							<div class="form-group">
								<span class="text ">Выбрать адрес</span>
								{{useraddressesOptionsss 'adressSelect'}}
							</div>
						{{/if}}
					{{/if}}
						<div class="form-group">
							<span class="text">Новый адрес</span>
							<textarea type="text" class="basket-input" name="newAddr">
							<input type="hidden" name="position">
						</div>
					</div>
				</form>
			</script>
			<div class="col-xs-12 col-sm-12 col-md-4 specific-col-lg-23" id="basket_menu_container">
			</div>

			<div class="col-xs-12 col-sm-12 col-md-8 specific-col-lg-77" id="basket_content_container">
				<div class="preload"></div>
				<div class="restoraunt-side__title">Корзина</div>
			<script id="basket_content" type="text/x-handlebars-template">
				{{#if restorauntData}}
				<div class="restoraunt-side__description pt-15-10">В корзине <span>0</span> блюда</div>
				<div class="white__block basket-order mb-25">
					<div class="row order">
						<div id="orderDataBag"  data-tamaq_executor="{{#if service.own_executors}}{{service.own_executors}}{{else}}false{{/if}}" hidden></div>
						<div class="col-md-5 col-lg-4">
							<div class="order-order">
								<div class="order-image">
									{{#if this.restorauntData.[0].photos.1.path }}
									<div class="order__image" style="background-image:url('<?= Api::$apiDomain ?>/{{this.restorauntData.[0].photos.1.path}}'); background-repeat:no-repeat;">
									</div>
									{{ else }}
									<div class="order__image" style="background-image:url('<?= Api::$apiDomain ?>/imgs/{{restorauntData.[0].id}}_photo1_140.png'); background-repeat:no-repeat;">
									</div>
									{{/if}}
								</div>
								<div class="order-name-type">
									<p class="order__order-name">{{this.restorauntData.[0].name}}</p>
									{{!-- <span class="order__order-type">Ресторан</span> --}}
								</div>
							</div>
						</div>
						<div class="col-md-7 col-lg-5">
							<ul class="order-amt-count">
								<li class="order__order-amt"><span>0</span> блюда</li>
								<li class="order__order-count"><span>---</span> тг.</li>
							</ul>
							<ul class="order-delivery-cost">
								<li class="order__order-delivery">доставка</li>
								<li class="order__order-cost"><span>---</span> тг.</li>
							</ul>
						</div>
{{!-- 						<div class="col-md-3 col-lg-3">
							<a class="tdn">
								<div class="order__delete">Убрать заказ из этого ресторана</div>
							</a>
						</div> --}}
						</div>
						{{#each order_products}}
						<div class="row order-content" data-productId="{{id}}">
							<div class="col-xs-12 col-sm-6 col-md-4">
								<div class="order-order-content">
									<div class="order-content-image">
										{{#if product.photos}}
										<div class="order-content__dishes" style="background-image:url('<?= Api::$apiDomain ?>/{{product.photos.1.path}}'); background-repeat:no-repeat;background-size: cover;"></div>
										{{/if}}
										{{#if photos}}
										<div class="order-content__dishes" style="background-image:url('<?= Api::$apiDomain ?>/{{photos.1.path}}'); background-repeat:no-repeat;background-size: cover;"></div>
										{{/if}}
									</div>
									{{#if product.name}}
										<div class="order-content-name order-content__order-name">{{product.name}},
										<span class="product-count">
											{{amt}}
										</span> шт</div>
									{{else}}
										<div class="order-content-name order-content__order-name">{{name}}, <span class="product-count">0</span> шт</div>
									{{/if}}
								</div>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-5">
								<ul class="order-content-price" style="padding:0;">
									<li class="order-content__order-price">
										<span>0</span> тг.
									</li>
									<li class="order-content__order-portion">Порций:
										<span class="basket__button-counter--elements">
										<span class="basket__button-counter--elem basket__button-counter--minus"></span>
										<input class="basket__button-counter product-portions" value="0"/>
										<span class="basket__button-counter--elem basket__button-counter--plus"></span>
										</span>
									</li>
								</ul>
							</div>
							<div class="col-xs-12 col-md-3">
								<ul class="order-content-sum">
									<li class="order-content__order-sum">Сумма: <span class="order-content__order-count"><span class="product-price">0</span> тг.</span><div class="order__delete order__delete--small" data-productId="{{id}}"></div></li>
								</ul>
							</div>
						</div>
						{{/each}}
					</div>
					<div class="order-comment-sum">
						<div class="col-xs-12 col-sm-6">
							{{!-- <ul class="order_comment"> --}}
								{{!-- <li class="order_comment__comment">Ваш комментарий к заказу: </li> --}}
								{{!-- <li class="order_comment__content"> </li> --}}
							{{!-- </ul> --}}
							<ul style="padding-left:0px">
								<li>
									<label class="restoraunt-filter__checkbox restoraunt-filter__checkbox--radio">
									{{#if (toTime)}}
										<input type="radio" name="type_to_time" value="toTime" checked="true"/>
									{{else}}
										<input type="radio" name="type_to_time" value="toTime"/>
									{{/if}}
										<span class="text">Ко времени*</span>
										<span class="checkmark"></span>
									</label>
								</li>
								<li>
									<label class="restoraunt-filter__checkbox restoraunt-filter__checkbox--radio">
									{{#if (toFast)}}
										<input type="radio" name="type_to_time" value="toFast" checked="true"/>
									{{else}}
										<input type="radio" name="type_to_time" value="toFast" />
									{{/if}}
										<span class="text">Как можно скорее*</span>
										<span class="checkmark"></span>
									</label>
								</li>
								<li>
									<div class="form-group mb-25">
										<label class="type_to_time type_to_time--toTime">Введите время</label>
										<input class="basket-input" id="datetimepicker" value="<?= date('Y-m-d H:i:s', time()) ?>" type="text" >
									</div>
								</li>
							</ul>
						</div>
						<div class="col-xs-12 col-sm-6" style="overflow:hidden">
							<div class="order_sum">
								<ul class="order_sum_cost">
									<li class="order_sum__cost ">Предварительная стоимость:</li>
									<li class="order_sum__cost-value prefatory">0 тг.</li>
								</ul>
								<ul class="order_sum_delivery hidden">
									<li class="order_sum__delivery">Доставка:</li>
									<li class="order_sum__delivery-value">0 тг.</li>
								</ul>
								<ul class="order_sum_total hidden">
									<li class="order_sum__total">Итого:</li>
									<li class="order_sum__total-value">0 тг.</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="basket__area">
						<textarea class="basket__area-text" placeholder="Ваш комментарий к заказу"></textarea>
						<a class="button-standard button-standard--orange basket__button js-order js-order-accept" disabled="disabled" style="margin-top: 10px">
						{{#if (basketOrderedStat)}}
							{{#if (isCard)}}
								заказать <small data-c="card">(оплата картой)</small><small data-c="cash" class="hidden">(оплата наличными)</small>
							{{else}}
								заказать <small class="hidden" data-c="card">(оплата картой)</small><small data-c="cash">(оплата наличными)</small>
							{{/if}}
						{{else}}
							рассчитать доставку
						{{/if}}
						</a>
						<a class="button-standard basket__button js-order js-order-close"style="margin-top: 10px">Отменить заказ</a>
					</div>
					<div class="mt-30 mb-30"></div>
				{{else}}
					<div class="restoraunt-side__description pt-15-10">Корзина пуста</div>
				{{/if}}
			</script>
			</div>
		</div>
	</div>
</main>