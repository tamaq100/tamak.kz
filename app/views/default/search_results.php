<div class="catalog-page search-results">
  <div class="section-pink">
    <div class="container">
      <div class="row">
          <?= include_file('default/breadcrumbs_col_sm_6', $vars) ?>
        </div>
    </div>
  </div>
  <div class="catalog-items">
    <?= include_file($view_folder . '/catalog_items', $vars) ?>
  </div>
</div>