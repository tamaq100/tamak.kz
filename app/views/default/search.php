<div class="catalog-page page-content--gray" style="min-height: calc(35.1vw - 2vh)">
	<div class="container">
		<div class="row">
			<ul class="breadcrumb">
				<li>
					<a href="<?= siteURL() ?>">Главная</a>
				</li>
				<li>
					<a href="<?= actualLinkNonFiltered() ?>">Поиск</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="catalog-items">
		<div class="container">
			<div class="row">
				<div class="col-xs-12"><h3>Результаты по запросу: <?= $_GET['value'] ?></h3></div>
			</div>
			<div class="row" id="search-big-results">				
				<script id="search-big-norm" type="text/x-handlebars-template">
					{{#if this.isset}}
						{{#if services}}
							<div class="col-xs-12">
							</div>
							{{#each services}}
							  <div class="white__block--box-shadow">
								<div class="white__block white__block--bb-n">
								  <div class="col-xs-12 col-sm-8 col-md-9 col-lg-9 restoraunt-main-b restoraunt-main-list">
									<div class="restoraunt__image restoraunt__image--pd">
									  <div class="restoraunt__image-item" style="background-image:url('<?= Api::$apiDomain ?>/imgs/{{id}}_photo1_140.png');"></div>
									</div>
									<div class="restoraunt__wrap restoraunt__wrap--ml restoraunt__wrap--ml-list">
									  <div class="restoraunt__wrap--flex restoraunt__wrap--flex-wrap  restoraunt__wrap--flex-aic">

										<div class="inner-review__title inner-review__title--uppercase">
											<a class="inner-review__title--anchor" href="/restoraunts/restoraunt_menu?id={{id}}">
												{{name}}
											</a>
										</div>
										<div class="restoraunt__stars" style="position: relative;display: flex;width:fit-content;max-width: intrinsic; height:14px;">
											<i class="fas fa-star rate-star"></i>
											<i class="fas fa-star rate-star"></i>
											<i class="fas fa-star rate-star"></i>
											<i class="fas fa-star rate-star"></i>
											<i class="fas fa-star rate-star"></i>
											<div class="front-rate" style="position:absolute;left:0;top:0;display:flex;flex-wrap:nowrap;flex-shrink:0;overflow:hidden;align-content:center;width:{{percentagePercent positive_valuations negative_valuations}}%">
												<i class="fas fa-star rate-star active"></i>
												<i class="fas fa-star rate-star active"></i>
												<i class="fas fa-star rate-star active"></i>
												<i class="fas fa-star rate-star active"></i>
												<i class="fas fa-star rate-star active"></i>
											</div>
											<!-- <span style="transform:translateY(-27%);font-weight:500">({{sum2 positive_valuations negative_valuations}})</span> -->
										</div>

									  </div>
									  <div class="restoraunt__wrap">
										<div class="restoraunt__text restoraunt__text--12 restoraunt__text-pt">Прием заказов: {{openfrom}} - {{opento}}</div>
										<div class="restoraunt__text restoraunt__text--12 restoraunt__text-pt">Блюда: 
										{{product_tags}}{{!-- {{#each categories}}{{value_ru}}|{{/each}} --}}
										</div>
									  </div>
									  <div class="restoraunt__wrap--aflex restoraunt__logopay-container pt-15-10">
										<div class="restoraunt__logopay"><img src="/img/cabinet-restoraunts/visa.png" alt="Visa pay"/></div>
										<div class="restoraunt__logopay"><img src="/img/cabinet-restoraunts/master.png" alt="MasterCard pay"/></div>
									  </div>
									  <div class="restoraunt__wrap--flex restoraunt__bottom-menu">
											<div class="restoraunt__wrap-hlist pt-15-10">
												<a href="restoraunts/restoraunt_menu?id={{id}}">
													<span>Меню</span>
												</a>
												<a href="restoraunts/restoraunt_info?id={{id}}">
													<span>Информация</span>
												</a>
												<a href="restoraunts/restoraunt_reviews?id={{id}}">
													<span>Отзывы</span>
												</a>
											</div>
										<div class="restoraunt__wrap pt-15-10">
											{{#if (user_is_auth)}}
												{{#if (is_favorite id)}}
													<div class="restoraunt__like" data-id="{{id}}">
													   <i class="fas fa-heart like--active"></i>
															<span>Убрать из избранных</span>
													</div>
												{{else}}
													<div class="restoraunt__like" data-id="{{id}}">
														<i class="fas fa-heart" ></i>
														<span>В избранное</span>
													</div>
												{{/if}}
											{{/if}}
										</div>
									  </div>
									</div>
								  </div>
								  <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
									<div class="restoraunt__wrap pt-15-10">
									  <div class="restoraunt__text-m ta-l">Минимальная сумма:</div>
									  <div class="restoraunt__text-b ta-l">
										{{#if minbill}}{{minbill}} тг.{{else}}----{{/if}}
									  </div>
									</div>
									<div class="restoraunt__wrap pt-15-10">
									  <div class="restoraunt__text-m ta-l">Доставка:</div>
									  <div class="restoraunt__text-b ta-l">Доставка Ресторана</div>
									</div>
								  </div>
								  {{!-- <div class="restoraunt__share restoraunt__share--orange">Акции</div> --}}
								</div>
								<div class="white__block-wrapper--light">
								  <div class="col-xs-12">
									<div class="dishes-list py-0 slider" data-slider="dishesSliderOnMain">
									{{#each products}}
									  <div class="dish dish--mr20">
										<a class="dish-pic" aria-label="{{name}}" href="/restoraunts/restoraunt_menu?id={{service.id}}" style="background-image:url('<?= Api::$apiDomain ?>/imgs/{{id}}_photo1_140.png');background-size:cover;background-position:center;" >
											{{!-- <img src="" alt="{{name}}"/> --}}
										</a>
										<div class="dish-info">
											<a class="dish-title" aria-label="Информация о {{name}}" href="/restoraunts/restoraunt_menu?id={{service.id}}">{{name}}</a>
										  <div class="dish-desc">{{description}}</div>
										  <div class="dish-bottom-info"><span class="dish-price">{{#if price}}{{price}} тг{{/if}}</span>
											<a aria-label="Заказать {{name}}" href="/restoraunts/restoraunt_menu?id={{service.id}}" style="display: flex;align-items: center;" class="btn button dish-order-button">
												заказать
											</a>
										  </div>
										</div>
									  </div>
									{{/each}}
									</div>
								  </div>
								</div>
							  </div>
							{{/each}}
						{{/if}}
					{{else}}
						<div class="col-xs-12">
							Нет результатов
						</div>
					{{/if}}
				</script>
				<script id="search-big-template" type="text/x-handlebars-template">
					{{#if this.isset}}
						{{#if services}}
							<div class="col-xs-12">
								<h4><b>Рестораны</b></h4>
							</div>
							{{#each services}}
								<div class="col-xs-6">
									<a class="tdn" href="/restoraunts/restoraunt_menu?id={{id}}">
										<div class="searchbig-result">
											{{name}}
										</div>
									</a>
								</div>
							{{/each}}
						{{/if}}
						{{#if products}}
							<div class="col-xs-12">
									<h4><b>Блюда</b></h4>
							</div>
							{{#each this.products}}
								<div class="col-xs-6">
									<a class="tdn" href="/restoraunts/restoraunt_menu?id={{service.id}}">
										<div class="searchbig-result">
											{{name}}
										</div>
									</a>
								</div>
							{{/each}}
						{{/if}}
					{{else}}
						<div class="col-xs-12">
							Нет результатов
						</div>
					{{/if}}
				</script>
			</div>
		</div>
		<script type="text/javascript">
			var productsData;
			var restoList = [];
			document.addEventListener("DOMContentLoaded", () => {
				var querySearch = '<?= $query ?>';
				$.when(
					Requests.asyncPost("services/list", generateSearchParams([{
						"field": "name",
						"type": "String",
						"value": querySearch
					}])),
					Requests.asyncPost("services/products/list", generateSearchParams([{
						"field": "name",
						"type": "String",
						"value": querySearch
					}]))
				).then(function(services, products) {
					function ids(ids){
						var result = '';
						ids.forEach(function(elem,index){
							if(index !== (ids.length-1)){
								result += elem + ',';
							}else{
								result += elem;
							}
						});
						return result;
					}
					var isset = false;
					if ( services[2].responseJSON.length || products[2].responseJSON.length ) {
						isset = true;
					}
					var pseudoRestoraunts = [];
					productsData = products[2].responseJSON;
					productsListResto = {};

					productsData.forEach(function(elem, index){
						restoList.includes(elem.service.id) ? [] : restoList.push(elem.service.id);
						productsListResto[elem.service.id] ? productsListResto[elem.service.id].push(elem) : productsListResto[elem.service.id] = [elem];
					});
					services[2].responseJSON.length ? pseudoRestoraunts = services[2].responseJSON : [];

					var data = {
						"services": services[2].responseJSON,
						"products": products[2].responseJSON,
						"isset": isset
					}
					var restoIds = ids(restoList);
					if(restoIds.length > 0) {
						Requests.asyncGeting('services?ids='+restoIds).done(function(restoraunts){
							console.log(data);
							restoraunts.forEach(function(restoraunt){
								restoraunt.products = productsListResto[restoraunt.id];
							});
							var data = {
								"services": restoraunts,
								"isset": true
							}
							console.log(data.services);
							$("#search-big-results").append(
								RenderTabs.render(
								$("#search-big-norm")
									.html(), data
								)
							)
							dishesSliderOnMain($('.dishes-list'));
						});
					} else {
						var data = {
							"services": [],
							"isset": false
						}

						$("#search-big-results").append(
							RenderTabs.render(
							$("#search-big-norm")
								.html(), data
							)
						);
						dishesSliderOnMain($('.dishes-list'));
					}
				});
			});
		</script>
	</div>
</div>