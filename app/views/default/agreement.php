<main class="page-content">
	<div class="agreement gray-bg">
		<div class="container">
			<div class="row">
				<ul class="breadcrumb">
					<li class="active"><a href="<?= siteURL() ?>">Главная</a></li>
					<li><a href="<?= actualLinkNonFiltered() ?>"><?= $title ?></a></li>
				</ul>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2 class="agreement__title"><?= $agreement_title  ?></h2>
					<div class="agreement__content">
						<?= $agreement_text ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>