<div class="modal fade" id="modalPay" role="dialog">
	<div class="modal-dialog" style="top:33%;">
		<div class="modal-content">
			<div class="modal-body" style="text-align: center; padding: 40px 30px 0px 30px;">
				<h2>
					для оплаты пройдите по <a href="" target="_blank">ссылке</a>
				</h2>
			</div>
		</div>
		<div class="modal-header">
		</div>
	</div>
</div>
<div class="modal fade" id="main-modal" role="dialog" style="height: 100vh;width: 100vw;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<ul class="nav nav-tabs">
					<li class="active">
						<a href="#login" data-toggle="tab">вход</a>
					</li>
					<li>
						<a href="#register" data-toggle="tab">регистрация</a>
					</li>
				</ul>
				<button class="close" type="button" data-dismiss="modal">×</button>
			</div>
			<div class="modal-body">
				<div class="tab-content">
					<?= include_file('auth/login'); ?>
					<?= include_file('auth/register'); ?>
					<?= include_file('auth/recovery');  ?>
				</div>
			</div>
			<div class="modal-footer"></div>
		</div>
	</div>
</div>
<div class="modal fade" id="phone_approve" role="dialog" style="height: 100vh;width: 100vw;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="min-height: 40px;">
				<button class="close" type="button" data-dismiss="modal">×</button>
			</div>
			<div class="modal-body" style="padding: 15px 40px;">
				<div class="tab-content">
					<h6>Ваш аккаунт ещё не подтверждён</h6>
					<hr>
					<p>Для того, что-бы получить возможность совершать заказы, пожалуйста подтвердите свой аккаунт</p>
					<div style="text-align: center;">
					<hr>
						<button id="askapprove_button" style="margin: 0;display: inline-block;" class="button-standard button-standard--orange">Отправить</button>
						<small class="alert-danger invisible" style="display: inline-block;">Слишком частые запросы, попробуйте отправить позднее</small>
					</div>
					<hr>
					<div>
						<form id="askapprove_form" style="display: none;text-align: center;" action="">
							<small style="vertical-align: middle;display: block;margin: 0 auto;padding-bottom: 15px">Код подтверждения придёт по смс на ваш номер в течении 60 секунд</small>
							<input type="hidden" value="" name="user_provider_id">
							<input type="hidden" value="PHONE_PASSWORD" name="provider_key">
							<input type="text" class="settings-table__input" name="password" placeholder="Код из смс" value="">
							<small class="alert-danger invisible">Вы ввели неверный код, попробуйте ввести его снова</small>
							<button id="send_approve" style="margin: 15px auto;display: block;" class="button-standard button-standard--orange">Подтвердить</button>
							<button class="reload_askapprove button-standard " style="margin: 0 auto;display: none;" class="button-standard">Повторить отправку</button>
						</form>
					</div>
				</div>
			</div>
			<div class="modal-footer" style="min-height: 40px;"></div>
		</div>
	</div>
</div>
<div class="modal fade" id="find-me-modal" role="dialog">
	<div class="modal-dialog">
		<div class="modal-map" id="mapModal"></div>
		<div class="modal-header">
			<button class="close" type="button" data-dismiss="modal">×</button>
		</div>
		<form class="modal-map-panel" data-panel="panel">
			<div class="modal-panel-window">
				<div class="modal-panel-select hide fade">
					<label class="modal-map-street">
						<span>Укажите адрес</span>
						<input class="form-control" id="InputAdress" required="required" type="text" name="custom-street"/>
					</label>
				</div>
				<div class="modal-adress">
					<div class="modal-map-desc">Адрес доставки:</div>
					<div class="modal-map-title">------</div>
				</div>
			</div>
			<div class="modal-panel-control">
				<div class="control-okay control-button">Всё верно</div>
				<div class="control-no control-button">Ввести вручную</div>
			</div>
		</form>
	</div>
</div>
<div class="modal fade" id="cabinet-modal" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" type="button" data-dismiss="modal">×</button>
			</div>
			<div class="modal-body">
				<a href="/auth/logout">ВЫХОД</a>
			</div>
			<div class="modal-footer"></div>
		</div>
	</div>
</div>
<div class="modal fade" id="dishes-modal" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" type="button" data-dismiss="modal">×</button>
			</div>
			<div class="modal-body">
				<form action="/dishes/submit" class="ajax-form" method="POST">
					<?= include_file('feedback/modal_dishes'); ?>
				</form>
			</div>
			<div class="modal-footer"></div>
		</div>
	</div>
</div>

<div class="modal fade" id="auto-modal" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" type="button" data-dismiss="modal">×</button>
			</div>
			<div class="modal-body">
				<form class="ajax-form" action="/auto/submit" method="POST">
					<?= include_file('feedback/modal_auto'); ?>
				</form>
			</div>
			<div class="modal-footer"></div>
		</div>
	</div>
</div>
<div class="modal fade" id="findRestoModal" role="dialog">
	<div class="modal-dialog">
		<div class="modal-map" id="mapResto"></div>
		<div class="modal-header">
			<button class="close" type="button" data-dismiss="modal">×</button>
		</div>
	</div>
</div>
<div class="modal fade" id="helpModal" role="dialog">
	<div class="modal-dialog" style="top:33%;">
		<div class="modal-content">
			<div class="modal-body" style="text-align: center; padding: 40px 30px 0px 30px;">
				<h5>
					Дорогой друг, желаешь пройти краткое обучение на нашем сайте?
				</h5>
				<button class="button-standard button-standard--orange" onclick="helpActions({'action':'ok'})" style="width: calc(50% - 20px);">Да</button>
				<button class="button-standard" style="width: calc(50% - 20px);" onclick="helpActions({'action':'dismis'})">Нет</button>
			</div>
		</div>
		<div class="modal-header">
			<button class="close" type="button" data-dismiss="modal">×</button>
		</div>
	</div>
</div>

<div class="modal fade" id="modalBasketDraft" tabindex="-1" aria-hidden="true" data-backdrop="static" data-keyboard="false" role="dialog" style="height: 100vh;width: 100vw;">
	<div class="modal-dialog" style="top: 50%;
    transform: translateY(-50%) !important;">
		<div class="modal-content" style="padding-bottom: 0;">
			<div class="modal-header">
				<!-- <button class="close" type="button" data-dismiss="modal">×</button> -->
			</div>
			<div class="modal-body" style="padding: 40px">
				<div class="tab-content">
					<div class="tab-pane active" data-pane="1">
						<h5 style="font-size: 23px; margin-bottom: 30px;">
							Подождите идёт поиск курьера
						</h5>
						<div class="preload" style="height: 200px">
							<div class="js-wrap-preload active"></div>
						</div>
					</div>
					<div class="tab-pane" data-pane="2">
						<h5 style="font-size: 23px; margin-bottom: 18px;">
							Выберите курьера
						</h5>
						<div class="pane-courier">
						</div>
					</div>

					<div class="tab-pane" data-pane="3">
						<h5 style="font-size: 23px; margin-bottom: 30px;">
							Спасибо за ваш выбор! Скоро курьер доставит Ваш заказ.
						</h5>
					</div>

					<script id="ourCourier" type="text/x-handlebars-template">
						<div class="modal-courier__wtitle" style="margin-bottom:10px;">Выбрать другого курьера можно не более 2-х раз</div>
						<div class="modal-courier__container modal-courier__container--flex" style="align-items: center;margin-bottom: 40px;">
							<div class="modal-courier__picture" style="background-image: url('<?= Api::$apiDomain ?>/imgs/{{this.executor.id}}_avatar.png');background-position: center;background-size: contain;flex-basis:65px;background-repeat: no-repeat;"></div>
							<div class="modal-courier__wrapp" style="flex-basis:55%">
								<div class="modal-courier__wtitle">Ваш курьер:</div>
								<div class="modal-courier__wdesc">{{this.executor.name}}</div>
								<div class="modal-courier__wtitle">({{sum2 this.executor.positive_valuations this.executor.negative_valuations}} {{declOfNumOVAVSUM this.executor.positive_valuations this.executor.negative_valuations}})</div>
							</div>
						</div>
						<div class="specific-modal__container">
						{{!-- // TODO --}}
							<button class="button-standard" onclick="searchNewCourier('{{id}}')">
								Найти другого
							</button>
							<button class="button-standard button-standard--orange" onclick="selectThisCourier('{{id}}')">
								Выбрать
							</button>
						</div>
					</script>

				</div>
			</div>
			<div class="modal-footer"></div>
		</div>
	</div>
</div>
<div class="modal fade" id="ExecRequest" role="dialog">

	<div class="modal-dialog" style="top: 50%;transform: translateY(-50%)!important;margin-top: 0px">
		<div class="modal-content">
			<script type="text/x-handlebars-template">
				<h5 style="font-size: 23px; margin-bottom: 18px;">Новый заказ</h5>
				<div class="modal-courier__container modal-courier__container--flex" style="align-items: center;margin-bottom: 18px;">
					<div class="modal-courier__wrapp">
						<div class="modal-courier__wtitle" style="margin-bottom: 10px;">Прибудьте в ресторан в {{order.to_time}} что бы забрать заказ</div>
						<div class="modal-courier__wdesc">
							<i class="fas fa-utensils"></i> {{service.name}}
						</div>
					</div>
				</div>
				<div class="modal-courier__wrapp modal-courier__container" style="margin-bottom: 18px;">
					<div class="modal-courier__wtitle" style="margin-bottom: 10px;">Деньги</div>
					<div class="modal-courier__wdesc"><i class="fas fa-coins"></i> За доставку {{eearn}} KZT</div>
				</div>
				<div class="modal-courier__wrapp modal-courier__container" style="margin-bottom: 18px;">
					<div class="modal-courier__wtitle" style="margin-bottom: 10px;">Адреса</div>
					<div class="modal-courier__wdesc" style="margin-bottom: 6px;"><i class="fas fa-utensils"></i> {{service_address.street}}{{#if service_address.house}}, {{service_address.house}}{{/if}}</div>
					<div class="modal-courier__wdesc"><i class="fas fa-user-circle"></i> Адрес заказчика: {{address.street}}{{#if address.house}}, {{address.house}}{{/if}}
					</div>
				</div>
				<div class="modal-courier__wrapp modal-courier__container modal-courier__container--flex modal-courier__container--btns modal-courier__wrapp--flex-row" style="margin-bottom: 18px;">
					<button class="button-standard" onclick="orderDecline()">Отклонить</button>
					<button class="button-standard button-standard--orange" onclick="ordersOffer('{{this.id}}')">Подтвердить</button>
				</div>
			</script>
			<div class="modal-body" style="text-align: center; padding: 40px 30px 0px 30px;">
			</div>
		</div>
		<div class="modal-header">
			<button class="close" type="button" data-dismiss="modal">×</button>
		</div>
	</div>

</div>
<div class="modal fade" id="rateCourierModal" role="dialog">

	<div class="modal-dialog" style="top:33%;">
		<div class="modal-content">
			<script type="text/x-handlebars-template">
				<h5 style="font-size: 23px; margin-bottom: 30px;">
					ОЦЕНИТЕ РАБОТУ ВАШЕГО КУРЬЕРА
				</h5>
				<div class="modal-courier__container modal-courier__container--flex" style="align-items: center;margin-bottom: 40px;">
					<div class="modal-courier__picture" style="background-image: url('<?= Api::$apiDomain ?>/imgs/{{executor.id}}_avatar.png');background-size: contain;">
					</div>
					<div class="modal-courier__wrapp" style="">
						<div class="modal-courier__wtitle">Ваш курьер:</div>
						<div class="modal-courier__wdesc">
							{{executor.name}}
						</div>
					</div>
				</div>
				<div class="modal-courier__container modal-courier__container--flex modal-courier__container--btns" style="margin-bottom: 40px;">
					<div class="modal-courier__bigbtn active">
						<div class="modal-courier__ico modal-courier__ico--good"></div>
						Положительно
						<input type="hidden" name="valuation" value="true">
					</div>
					<div class="modal-courier__bigbtn">
						<div class="modal-courier__ico modal-courier__ico--bad"></div>
						Отрицательно
						<input type="hidden" name="valuation" value="false">
					</div>
				</div>
				<div class="modal-courier__container" style="margin-bottom: 40px;">
					<div class="modal-courier__row modal-courier__row--flex modal-courier__row--rates">
						<div class="modal-courier__set">Скорость доставки:</div>
						<div class="modal-courier__stars js-rate">
							<input type="hidden" name="speed_rate" value="100">
							<div class="courier-review__stars" style="position: relative;display: flex;">
								<i class="fas fa-star rate-star" data-percent="20"></i>
								<i class="fas fa-star rate-star" data-percent="40"></i>
								<i class="fas fa-star rate-star" data-percent="60"></i>
								<i class="fas fa-star rate-star" data-percent="80"></i>
								<i class="fas fa-star rate-star" data-percent="100"></i>
								<div class="front-rate" style="position:absolute;left:0;top:0;display:flex;flex-wrap:nowrap;flex-shrink:0;overflow:hidden;align-content:center; width:100%">
									<i class="fas fa-star rate-star active" data-percent="20"></i>
									<i class="fas fa-star rate-star active" data-percent="40"></i>
									<i class="fas fa-star rate-star active" data-percent="60"></i>
									<i class="fas fa-star rate-star active" data-percent="80"></i>
									<i class="fas fa-star rate-star active" data-percent="100"></i>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-courier__row modal-courier__row--flex modal-courier__row--rates">
						<div class="modal-courier__set">Вежливость:</div>
						<div class="modal-courier__stars js-rate">
							<input type="hidden" name="civility_rate" value="100">
							<div class="courier-review__stars" style="position: relative;display: flex;">
								<i class="fas fa-star rate-star" data-percent="20"></i>
								<i class="fas fa-star rate-star" data-percent="40"></i>
								<i class="fas fa-star rate-star" data-percent="60"></i>
								<i class="fas fa-star rate-star" data-percent="80"></i>
								<i class="fas fa-star rate-star" data-percent="100"></i>
								<div class="front-rate" style="position:absolute;left:0;top:0;display:flex;flex-wrap:nowrap;flex-shrink:0;overflow:hidden;align-content:center; width:100%">
									<i class="fas fa-star rate-star active" data-percent="20"></i>
									<i class="fas fa-star rate-star active" data-percent="40"></i>
									<i class="fas fa-star rate-star active" data-percent="60"></i>
									<i class="fas fa-star rate-star active" data-percent="80"></i>
									<i class="fas fa-star rate-star active" data-percent="100"></i>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-courier__row modal-courier__row--flex modal-courier__row--rates">
						<div class="modal-courier__set">Состояние доставки:</div>
						<div class="modal-courier__stars js-rate">
							<input type="hidden" name="state_rate" value="100">
							<div class="courier-review__stars" style="position: relative;display: flex;">
								<i class="fas fa-star rate-star" data-percent="20"></i>
								<i class="fas fa-star rate-star" data-percent="40"></i>
								<i class="fas fa-star rate-star" data-percent="60"></i>
								<i class="fas fa-star rate-star" data-percent="80"></i>
								<i class="fas fa-star rate-star" data-percent="100"></i>
								<div class="front-rate" style="position:absolute;left:0;top:0;display:flex;flex-wrap:nowrap;flex-shrink:0;overflow:hidden;align-content:center; width:100%">
									<i class="fas fa-star rate-star active" data-percent="20"></i>
									<i class="fas fa-star rate-star active" data-percent="40"></i>
									<i class="fas fa-star rate-star active" data-percent="60"></i>
									<i class="fas fa-star rate-star active" data-percent="80"></i>
									<i class="fas fa-star rate-star active" data-percent="100"></i>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-courier__container" style="margin-bottom: 20px;">
				</div>
				<div class="modal-courier__container" style="margin-bottom: 30px">
					<textarea class="basket__area-text"></textarea>
				</div>
				<div class="modal-courier__container">
					<button class="button-standard button-standard--orange" onclick="rateToCourier('{{id}}')" style="width: calc(50% - 20px);">Готово</button>
				</div>
			</script>
			<div class="modal-body" style="text-align: center; padding: 40px 30px 0px 30px;">
			</div>
		</div>
		<div class="modal-header">
			<button class="close" type="button" data-dismiss="modal">×</button>
		</div>
	</div>

</div>
<div class="modal fade" id="rateRestoModal" role="dialog">

	<div class="modal-dialog" style="top:33%;">
		<div class="modal-content">
			<script type="text/x-handlebars-template">
				<h5 style="font-size: 23px; margin-bottom: 30px;">
					Отзыв о ресторане
				</h5>
				<div class="modal-courier__container modal-courier__container--flex" style="align-items: center;margin-bottom: 40px;">
					<div class="modal-courier__picture" style="background-image: url('<?= Api::$apiDomain ?>/imgs/{{service.id}}_photo1_140.png');background-size: contain;">
					</div>
					<div class="modal-courier__wrapp" style="">
						<div class="modal-courier__wdesc">
							{{service.name}}
						</div>
					</div>
				</div>
				<div class="modal-courier__container modal-courier__container--flex modal-courier__container--btns" style="margin-bottom: 40px;">
					<div class="modal-courier__bigbtn active">
						<div class="modal-courier__ico modal-courier__ico--good"></div>
						Положительно
						<input type="hidden" name="valuation" value="true">
					</div>
					<div class="modal-courier__bigbtn">
						<div class="modal-courier__ico modal-courier__ico--bad"></div>
						Отрицательно
						<input type="hidden" name="valuation" value="false">
					</div>
				</div>
				<div class="modal-courier__container" style="margin-bottom: 30px">
					<textarea class="basket__area-text"></textarea>
				</div>
				<div class="modal-courier__container">
					<button class="button-standard button-standard--orange" onclick="rateToRestourant('{{id}}')" style="width: calc(50% - 20px);">Готово</button>
				</div>
			</script>
			<div class="modal-body" style="text-align: center; padding: 40px 30px 0px 30px;">
			</div>
		</div>
		<div class="modal-header">
			<button class="close" type="button" data-dismiss="modal">×</button>
		</div>
	</div>

</div>