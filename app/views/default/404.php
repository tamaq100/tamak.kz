<div class="not-found" style="background-image:url('/img/404.png');">
	<div class="pattern-top"></div>
	<div class="container page-404">
		<div class="description">
			<h4>
				<?= __('404.text') ?>
			</h4>
		</div>
	</div>
	<div class="pattern-bottom"></div>
</div>
