<?php if ($modules): ?>
	<?php $firstModule = current($modules) ?>
	<div class="catalog-inner-page">
	    <div class="inner-content">
			<div class="pattern-top"></div>
			<?= include_file('default/breadcrumbs', $vars) ?>
			<?php if (in_array($firstModule->getDefaultValue('type'), ['text_block_two_column', 'text_block_one_column'])): ?>
				<div class="container">
					<div class="title"><?= $page->title ?></div>
					<? $module_vars = [
						'module'  => $firstModule,
						'page'    => $page,
					] ?>
					<?= include_file('page_modules/' . $firstModule->getDefaultValue('type'), $module_vars); ?>
				</div>
				<div class="pattern-bottom"></div>
				<?php unset($modules[$firstModule->id]) ?>
			<?php elseif(in_array($firstModule->getDefaultValue('type'), ['big_slider'])): ?>
				<? $module_vars = [
					'module'  => $firstModule,
					'page'    => $page,
				] ?>
				<?= include_file('page_modules/' . $firstModule->getDefaultValue('type'), $module_vars); ?>
				<?php unset($modules[$firstModule->id]) ?>
			<?php else: ?>
				<div class="pattern-bottom absolute"></div>
			<?php endif ?>
		</div>
		<?php if ($modules): ?>
			<?php foreach ($modules as $module): ?>
				<?php if (file_exists(SITE_DIR . '/app/views/page_modules/' . $module->getDefaultValue('type') . '.php')): ?>
					<? $module_vars = [
						'module'  => $module,
						'page'    => $page,
						'currencies' => Currency::findAll(),
						'map_final'	=> ($module == end($modules) && $module->getDefaultValue('type') == 'map_koordinates') ,
					] ?>
					<?= include_file('page_modules/' . $module->getDefaultValue('type'), $module_vars); ?>
				<?php endif ?>
			<?php endforeach ?>
		<?php endif ?>
	</div>
<?php endif ?>