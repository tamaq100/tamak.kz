<div class="categories-page <?= $wrapper_class ?>">
  <div class="slider-wrap main-slider">
    <?php if ($head_banners): ?>
      <div class="pattern-top"></div>
      <div class="container">
        <div class="arrow-wrap-prev">
          <div class="arrows-slider arrow-prev"></div>
        </div>
        <div class="arrow-wrap-next">
          <div class="arrows-slider arrow-next"></div>
        </div>
      </div>
      <div class="slider" data-slider="main-slider">
        <?php foreach ($head_banners as $banner): ?>
          <div class="slider-item">
            <div class="slider-item-pic" style="background-image:url('/images/00/<?= $banner->media ?>.jpg');">
              <div class="container">
                <div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-7 col-md-offset-3 col-lg-7 col-lg-offset-3">
                    <div class="slider-item-title">
                      <?= $banner->text ?>
                    </div>
                    <a class="slider-item-button" href="#" data-toggle="modal"  data-target="#request_modal"><?= __('make_a_request') ?></a>
                  </div>
                </div>
              </div>
            </div>
          </div> 
        <?php endforeach ?>
      </div>
      <div class="qoute-right-bottom"></div>
    <?php endif ?>
  </div>
  <?= include_file('search/form', $vars); ?>
  <div class="container">
    <?= include_file('default/breadcrumbs', $vars) ?>
    <div class="category">
      <div class="row">
        <?php if ($categories): ?>
          <?php foreach ($categories as $category): ?>
            <div class="col-xs-12 col-md-6">
              <a class="category-item" href="/<?= ltrim($rubric->link, '/') ?>/category/<?= $category->id ?>">
                <div class="category-item-name"><?= $category->title ?></div>
                <div class="category-item-pic-wrap">
                  <div class="category-item-pic" style="background-image: url('/images/w/norm/580/<?= $category->image ?>.jpg');"></div>
                </div>
              </a>
            </div>
          <?php endforeach ?>
        <?php endif ?>
      </div>
    </div>
  </div>
</div>