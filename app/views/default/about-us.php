<main class="page-content">
	<div class="about-us gray-bg">
		<div class="container">
			<div class="row">
				<ul class="breadcrumb">
					<li class="active"><a href="<?= siteURL() ?>">Главная</a></li>
					<li><a href="<?= actualLinkNonFiltered() ?>"><?= $title ?></a></li>
				</ul>
			</div>
		</div>
		<div class="container">
			<h2 class="about-us__title"><?= $title ?></h2>
            <?php if (ConfigAboutUs::get('image')): ?>
                <div class="about-us__item" style="background-image: url('/images/00/<?= ConfigAboutUs::get('image') ?>.jpg');">
             <?php else: ?>
                <div class="about-us__item">
             <?php endif; ?>
				<div class="row">
					<div class="col-md-12">
						<?php if (ConfigAboutUs::get('title')): ?>
							<div class="about-us__item__title"><?= ConfigAboutUs::get('title') ?></div>
						<?php endif ?>
						<?php if (ConfigAboutUs::get('subtitle')): ?>
						<div class="about-us__item__desc">
							<?= ConfigAboutUs::get('subtitle') ?>
						</div>
						<?php endif ?>
					</div>
				</div>
			</div>
			<?php if ($mission): ?>
			<div class="about-us__mission">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12 col-lg-2">
						<h3 class="about-us__mission__title">миссия:</h3>
					</div>
					<div class="col-md-12 col-lg-10">
						<?php foreach ($mission as $itmMis): ?>
						<div class="col-md-4 col-sm-12 col-xs-12">
							<div class="about-us__mission__item">
								<img src="/images/00/<?= $itmMis->image ?>.jpg" alt=""/>
								<div class="about-us__mission__desc"><?= $itmMis->title ?></div>
							</div>
						</div>
						<?php endforeach ?>
					</div>
				</div>
			</div>
			<?php endif ?>
			<?php if ($values): ?>
			<div class="about-us__worth">
				<div class="row">
					<div class="col-lg-3 col-xs-12 col-sm-12 col-md-4">
						<h3 class="about-us__worth__title">ОСНОВНЫЕ ЦЕННОСТИ:</h3>
					</div>
					<div class="col-lg-9 col-xs-12 col-sm-12 col-md-8">
						<div class="about-us__worth__item">
							<ol class="list-unstyled">
								<!-- респект за то, что сделали цифры через Ol-> li -->
								<?php foreach ($values as $gold): ?>
								<li>
									<?php if ($gold->title): ?>
									<h6 class="about-us__worth__subtitle"><?= $gold->title ?></h6>
									<?php endif ?>
									<?php if ($gold->subtitle): ?>
									<?= $gold->subtitle ?>
									<?php endif ?>
								</li>
								<?php endforeach ?>
							</ol>
						</div>
					</div>
				</div>
			</div>
			<?php endif ?>
			<?php if ($team): ?>
			<div class="about-us__team">
				<div class="row">
					<div class="col-md-12">
						<h3 class="about-us__team__title">наша команда</h3>
					</div>
				</div>
			<?php $teamChunk = array_chunk($team, 4);
				$teamChunk_count = count($teamChunk);
			?>
			<?php for ($i= 0; $i < $teamChunk_count; $i++):?>
				<div class="row">
				<?php foreach ($teamChunk[$i] as $person): ?>
						<div class="col-md-6 col-xs-12 col-sm-6 col-lg-3">
							<div class="about-us__team__item">
								<?php if ($person->image): ?>
								<div class="about-us__team__img" style="background-image: url('/images/fit/210/190/<?= $person->image ?>.jpg');">
								</div>
								<?php endif ?>
								<?php if ($person->name): ?>
								<div class="about-us__team__name"><?= $person->name ?></div>
								<?php endif ?>
								<?php if ($person->position): ?>
								<div class="about-us__team__position"><?=  $person->position ?></div>
								<?php endif ?>
							</div>
						</div>
				<?php endforeach ?>
				</div>
			<?php endfor ?>
			</div>
			<?php endif ?>
		</div>
	</div>
</main>