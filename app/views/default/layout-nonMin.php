<!DOCTYPE html>
<html class="page" lang="en">
  <head>
    <meta charset="UTF-8"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimum-scale=1.0, shrink-to-fit=no"/>
<?php if (isset($meta['title']) && $meta['title']) : ?>
    <title><?= $meta['title'] ?></title>
    <meta name="title" content="<?= $meta['title'] ?>" />
    <meta content="<?= $meta['title'] ?>" property="og:title" />
<?php else: ?>
<?php endif ?>
<?php if ($meta['description'] != '' ): ?>
    <meta name="description" content="<?= $meta['description'] ?>">
    <meta content="<?= $meta['description'] ?>" property="og:description" />
<?php endif ?>
<?php if ($meta['keywords'] != ''): ?>
    <meta name="keywords" content="<?= $meta['keywords'] ?>">
<?php endif ?>
    <style>.main-slider-wrap{overflow: hidden;}</style>
    <link rel="preload" href="<?= $styles[0] ?>?v=<?= filemtime(WORK_DIR . $styles[0]) ?>" as="stylesheet">
    <link rel="apple-touch-icon" sizes="57x57" href="/img/favicon/apple-icon-57x57.png"/>
    <link rel="apple-touch-icon" sizes="60x60" href="/img/favicon/apple-icon-60x60.png"/>
    <link rel="apple-touch-icon" sizes="72x72" href="/img/favicon/apple-icon-72x72.png"/>
    <link rel="apple-touch-icon" sizes="76x76" href="/img/favicon/apple-icon-76x76.png"/>
    <link rel="apple-touch-icon" sizes="114x114" href="/img/favicon/apple-icon-114x114.png"/>
    <link rel="apple-touch-icon" sizes="120x120" href="/img/favicon/apple-icon-120x120.png"/>
    <link rel="apple-touch-icon" sizes="144x144" href="/img/favicon/apple-icon-144x144.png"/>
    <link rel="apple-touch-icon" sizes="152x152" href="/img/favicon/apple-icon-152x152.png"/>
    <link rel="apple-touch-icon" sizes="180x180" href="/img/favicon/apple-icon-180x180.png"/>
    <link rel="icon" type="image/png" sizes="192x192" href="/img/favicon/android-icon-192x192.png"/>
    <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png"/>
    <link rel="icon" type="image/png" sizes="96x96" href="/img/favicon/favicon-96x96.png"/>
    <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png"/>
    <link rel="manifest" href="/img/favicon/manifest.json"/>
    <link rel="preload" href="/vendor/slick/fonts/slick.woff" as="font">
    <link rel="preload" href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700&amp;amp;subset=cyrillic" as="stylesheet">
    <meta name="msapplication-TileColor" content="#ffffff"/>
    <meta name="msapplication-TileImage" content="/img/favicon/ms-icon-144x144.png"/>
    <meta name="theme-color" content="#ffffff"/>
    <link rel="preconnect" href="https://tamaq.kz"/>
    <!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700&amp;amp;subset=cyrillic"> -->
    <!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Pacifico&amp;amp;subset=cyrillic"> -->
<?php foreach ($styles as $src): ?>
    <link rel="stylesheet" href="<?= $src ?>?v=<?= filemtime(WORK_DIR . $src) ?>"/>
<?php endforeach ?>
    <script>function srcRepair($this,$img){$this.src = $img;}var fullAjOff = true;</script>
  </head>
  <body>
    <script>
    var heroesData={hello:{hero:"01",msg:"01"},help:{hero:"06",special:!0},anticipation:{hero:"02",msg:"02"},sadness:{hero:"04",msg:"04"},forchat:{hero:"05",msg:"05"},orderStatus:{hero:"08",msg:"08"},statusPending:{hero:"09",msg:"09"}};
    </script>
    <div class="mobile-app disable">
        <div class="mobile-app-container">
            <a data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="Приносим свои извинения, наш сервис дорабатывается" class="mobile-app-link android" >
                <img class="mobile-app-image" src="https://play.google.com/intl/en_us/badges/images/generic/ru_badge_web_generic.png" alt="Доступно в Google Play"/>
            </a>
            <a data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="Приносим свои извинения, наш сервис дорабатывается" class="mobile-app-link apple" >
                <img class="mobile-app-image mobile-app-apple" src="/img/mobile-icon/Download_on_the_App_Store.svg" alt="Доступно в Google Play"/>
            </a>
            <div class="mobile-app-close"><i class="fas fa-chevron-up"></i></div>
        </div>
    </div>
    <div class="welcome-hero" id="welcome_hero">
        <img class="welcome-hero__ico" src="/img/heroes/hero01.png" alt="hero-logo"/>
        <img class="welcome-hero__text" src="/img/icons/welcome-hero_text.png" alt="welcome text"/>
        <a class="welcome-hero_close" href="#" id="welcome_hero_close"></a>
    </div>
    <div class="page-inner addTopApp">
        <?= include_file('default/header') ?>
        <?= $content ?>
        <?= include_file('default/footer') ?>
        <?= include_file('default/modal-fade'); ?>
    </div>
    <div id="order-billet" class="order-billet">
        <div class="container">
        <div class="order-billet__container" style="min-height: 81px;">
            <div class="order-billet__wrap">
                <div class="order-billet-food" style="display: inline-block;"></div>
                <span>
                <div class="order-billet-smalltext" style="display: inline-block;">Ваш заказ на сумму:</div>
                <div class="order-billet-cost" id="basketSumCost" style="display: inline-block;">2400 тг.</div>
                </span>
            </div>
            <a href="/basket" class="order-billet-go">
                <span>Перейти в корзину</span>
            </a>
        </div>
        </div>
    </div>
    <div id="courier-order-billet" class="order-billet">
        <div class="container">
        <div class="order-billet__container" style="min-height: 81px;">
            <div class="order-billet__wrap">
                <div class="order-billet-food" style="display: inline-block;">
                </div>
                <span>
                <div class="order-billet-smalltext" style="display: inline-block;">Заказ {{id}}</div>
                </span>
            </div>
            <a href="/basket" class="order-billet-go">
                <span>Перейти к заказу</span>
            </a>
        </div>
        </div>
    </div>
<?php foreach ($styles2 as $src): ?>
    <link rel="stylesheet" href="<?= $src ?>?v=<?= filemtime(WORK_DIR . $src) ?>"/>
<?php endforeach ?>
<script src="/vendor/driver/dist/driver.min.js" async></script>
<?php foreach ($scripts as $src): ?>
<?php if (preg_match('/jquery/iemU', $src)): ?>
    <script src="<?= $src ?>?v=<?= filemtime(WORK_DIR . $src) ?>"></script>
<?php endif ?>
<?php endforeach; ?>
<script src="/vendor/handlebars.js"></script>
<?php foreach ($scripts as $src): ?>
<?php if (!preg_match('/jquery/iemU', $src)): ?>
<script src="<?= $src ?>?v=<?= filemtime(WORK_DIR . $src) ?>"></script>
<?php endif; ?>
<?php endforeach ?>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700&amp;amp;subset=cyrillic">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Pacifico:400,600&amp;amp;subset=cyrillic">
  </body>
</html>