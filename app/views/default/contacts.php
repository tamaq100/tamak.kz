<main class="page-content">
	<div class="contacts gray-bg">
		<div class="container">
			<div class="row">
				<ul class="breadcrumb">
					<li class="active"><a href="<?= siteURL() ?>">Главная</a></li>
					<li><a href="<?= actualLinkNonFiltered() ?>"><?= $title ?></a></li>
				</ul>
			</div>
		</div>
		<div id="coords" class="hide" data-coords="<?= $coords ?>">
		</div>
		<div class="container">
			<h2 class="contacts__title">контакты</h2>
			<?php if ($cards): ?>
			<div class="row">
					<!-- Сори напутал данные number это title и наоборот -->
				<?php foreach ($cards as $key => $value): ?>
				<div class="col-md-4">
					<div class="contacts__item">
						<?php if ($value['number'] != ''): ?>
						<h4 class="contacts__subtitle"><?= $value['number'] ?></h4>
						<?php endif ?>
						<?php if ($value['title'] != ''): ?>
						<div class="contacts__number"><?= $value['title'] ?> </div>
						<?php endif ?>
					</div>
				</div>
				<?php endforeach ?>
			</div>
			<?php endif ?>
			<div class="row">
				<div class="col-md-12">
					<div class="contacts__map" id="map"></div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<form class="contacts__form ajax-form" action="/feedback/send" method="POST">
						<h3 class="contacts__form__title">Обратная связь</h3>
						<div class="row">
							<div class="col-md-6 col-xs-12 col-sm-6">
								<div class="contacts__form__item">
									<div class="form-group">
										<input class="form-control" type="text" placeholder="Ваше имя" name="name"/>
									</div>
									<div class="form-group">
										<input class="form-control" type="tel" placeholder="Телефон" name="telephone"/>
									</div>
									<div class="form-group">
										<input class="form-control" type="email" placeholder="E-mail" name="email"/>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-xs-12 col-sm-6">
								<div class="contacts__form__item">
									<div class="form-group">
										<textarea class="form-control" type="text" placeholder="Сообщение" name="message" rows="5"></textarea>
										<button class="btn button dish-order-button" type="submit">Отправить</button>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</main>
<script async src="https://api-maps.yandex.ru/2.1/?apikey=93d87125-10a2-4bf7-be4c-03dcb928c540&lang=ru_KZ&onload=map&onerror=failmap" type="text/javascript"></script>