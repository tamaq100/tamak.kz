<?php if ($count && $count > $limit): ?>
	<div class="news-block__pagination">
			<?php if ($page < ceil($count/$limit)): ?>
				<a href="?page=<?= ($page + 5 < ceil($count/$limit)) ? $page + 5 : ceil($count/$limit) ?>">
					<div class="news-block__pagination__next"></div>
				</a>
			<?php endif ?>
			<!-- если у нас дальше первой страницы то появляется назад стрелочка -->
			<?php if ($page > 1): ?>
				<a href="?page=<?= ($page - 5 > 1) ? $page - 5 : 1?>">
					<div class="news-block__pagination__prev"></div>
				</a>
			<?php endif ?>
			<!-- объявляем странную переменную -->
			<?php $start = $page > 3 ? $page - 2 : 1; ?>
			<ul class="pagination">
			<?php for ($i = $start; $i <= ceil($count/$limit); $i++):  ?>
					<li class="<?= $page == $i ? ' active ' : '' ?>">
						<a href="?page=<?= $i ?>">
							<?= $i ?>
						</a>
					</li>
			<?php endfor ?>
			</ul>
	</div>
<?php endif ?>