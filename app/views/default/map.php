<div class="footer-map add-negative-margin">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
			<div class="title"><?= __('default_map') ?></div>
			</div>
		</div>
	</div>
	<div id="map"></div>
	<script>
		function initMap() {
			var map = new google.maps.Map(document.getElementById('map'), {});
			setMarkers(map);

			function setMarkers(map) {
				var bounds = new google.maps.LatLngBounds();
				var beaches = [
				<?php $i = 1 ?>
				<?php foreach ($map_koordinates as $koordinates): ?>
					['<?= $koordinates->title ?>', <?= $koordinates->koordinates ?>, <?= $i++ ?>],
				<?php endforeach ?>
				];
				for (var i = 0; i < beaches.length; i++) {
					var beach = beaches[i];
					var marker = new google.maps.Marker({
						position: {
							lat: beach[1],
							lng: beach[2]
						},
						map: map,
						icon: '/img/icon/marker-map.png',
						title: beach[0],
						zIndex: beach[3]
					});
					bounds.extend(marker.position);
				}

				var listener = google.maps.event.addListener(map, "idle", function () {
					if ($(window).width() > 767){
						map.setZoom(11);
					} else {
						map.setZoom(9);
					}
					google.maps.event.removeListener(listener);
				});

				map.fitBounds(bounds);
			}
		}
	</script>
</div>