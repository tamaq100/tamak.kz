<main class="page-content">
	<?php if ($banner = ConfigAuto::get('banner')): ?>
	<div class="page-banner page-banner-auto" style="background-image:url('/images/00/<?= $banner ?>.jpg'); ">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="page-banner-auto-inner">
						<div class="page-banner-info">
							<?php if ($title = ConfigAuto::get('title')): ?>
							<h1 class="page-banner-title"><?= $title ?></h1>
							<?php endif ?>
							<?php if ($subtitle = ConfigAuto::get('subtitle')): ?>
							<p class="page-banner-text">
								<?= $subtitle ?>
							</p>
							<?php endif ?>
							<button class="button orange-button" data-toggle="modal" data-target="#auto-modal">подать заявку</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php endif ?>
	<?php if ($advantages): ?>
	<div class="benefits paddings">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="title inner-title centered">преимущества работы с нами</div>
				</div>
			</div>
			<div class="row">
				<?php foreach ($advantages as $advantage): ?>
				<div class="col-xs-4">
					<div class="benefit-item">
						<div class="benefit-item-pic">
							<?php if ($advantage->image): ?>
								<img src="/images/00/<?= $advantage->image ?>.jpg" alt=""/>
							<?php endif ?>
						</div>
						<?php if ($advantage->title): ?>
						<div class="benefit-item-title"><?= $advantage->title ?></div>
						<?php endif ?>
						<?php if ($advantage->subtitle): ?>
						<div class="benefit-item-text"><?= $advantage->subtitle ?></div>
						<?php endif ?>
					</div>
				</div>
				<?php endforeach ?>
			</div>
		</div>
	</div>
	<?php endif ?>
	<?php if ($howto): ?>
	<div class="getting-started gray-bg paddings">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="title inner-title centered">чтобы начать работать</div>
					<div class="getting-started-steps">
						<?php foreach ($howto as $step): ?>
						<div class="step">
							<div class="step-number"><span><?= $step->idx ?></span></div>
							<div class="step-text"><?= $step->title ?></div>
						</div>
						<?php endforeach ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php endif ?>
	<?php if (($auto_isset = ConfigAuto::get('auto_isset')) && ($auto_text = ConfigAuto::get('auto_text'))): ?>
	<div class="offer-block paddings">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<div class="inner-title"><?= $auto_isset ?></div>
					<?= $auto_text ?>
				</div>
				<div class="col-xs-6 hidden-xs">
					<div class="offer-block-pic">
						<img class="hidden-xs" src="/img/icons/do-u-have-a-car.png" alt=""/></div>
				</div>
			</div>
		</div>
	</div>
	<?php endif ?>
	<?php if ($faq): ?>
	<div class="faq paddings gray-bg">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<h3 class="inner-title centered title">часто задаваемые вопросы</h3>
					<?php foreach ($faq as $itemm): ?>
						<?php if ($itemm->question): ?>
							<div class="faq-item-wrap">
								<div class="faq-item"><span class="faq-icon"></span>
									<div class="faq-title"><?= $itemm->question ?></div>
								</div>
								<div class="faq-full-text">
									<?= $itemm->answer ?>
								</div>
							</div>
						<?php endif ?>
					<?php endforeach ?>
				</div>
			</div>
		</div>
	</div>
	<?php endif ?>
	<?php if ($executors): ?>
	<div class="our-couriers paddings">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<h3 class="title centered inner-title">наши курьеры</h3>
					<div class="couriers-list page-couriers-list">
						<div class="couriers-slider-up couriers-slider-control" id="couriers-slider-prev"></div>
						<div class="couriers-slider-down couriers-slider-control" id="couriers-slider-next"></div>
						<div class="row js-slider-our-couriers">
							<?php $executors_chunks = array_chunk($executors, 3); ?>
							<?php foreach ($executors_chunks as $executors_chunk): ?>
								<div class="col-xs-12 col-sm-12 col-md-6">
									<?php foreach ($executors_chunk as $executor): ?>
										<div class="courier">
											<?php if ($executor->avatar): ?>
												<div class="courier-pic" style="background-image: url('https://tamaq.kz<?= $executor->avatar ?>');">
												</div>
											<?php endif ?>
											<span class="courier-name"><?= $executor->name ?></span>
											<div class="courier-rate">
											<?php for ($i=0; $i < 5; $i++) :?>
												<i class="fas fa-star rate-star"></i>
											<?php endfor ?>
												<?php $rating = $executor->rating; ?>
												<?php $percent = 100 * $rating / 5 ?>
												<div class="front-stars" style="width: <?= $percent ?>%;">
													<?php for ($i=0; $i < 5; $i++) :?>
														<i class="fas fa-star rate-star"></i>
													<?php endfor ?>
												</div>
											</div>
										</div>
									<?php endforeach ?>
								</div>
							<?php endforeach ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php endif ?>
	<div class="apply paddings">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<h3 class="title inner-title">начните зарабатывать!</h3>
					<p>
						Заполните заявку на сотрудничество,<br>
						и наши менеджеры свяжутся с вами
					</p>
					<button class="button orange-button" data-toggle="modal" data-target="#auto-modal">подать заявку</button>
				</div>
			</div>
		</div>
	</div>
</main>