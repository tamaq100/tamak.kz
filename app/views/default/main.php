<script>
	const fStepPopover = 'first-step-popover-class';
	var helpSteps =
	[{
		element: '.defaultLogin',
		popover: {
			className: fStepPopover,
			title: ' ',
			description: 'Хотите копить бонусы, а потом тратить их на еду? Тогда регистрируйтесь скорее!',
			position: 'left'
		}
	}, {
		element: '.search-wrap',
		popover: {
			className: fStepPopover,
			title: ' ',
			description: 'В поисковой строке вы быстро найдете любимое заведение либо интересующее Вас блюдо',
			position: 'left'
		}
	}, {
		element: '#findme',
		popover: {
			className: fStepPopover,
			title: ' ',
			description: 'Введите свой адрес/либо нажмите на кнопку «найти меня», которая автоматически определит ваше местоположение',
			position: 'left'
		}
	}, {
		element: '#restoAround',
		popover: {
			className: fStepPopover,
			title: ' ',
			description: 'Благодаря кнопке «найти ближайшие», наша система автоматически найдет БЛИЖАЙШИЕ К ВАМ рестораны и отобразит их на карте',
			position: 'left'
		}
	}, {
		element: '.main-categories-wrap',
		popover: {
			className: fStepPopover,
			title: ' ',
			description: 'Помимо популярных категорий блюд, в этом списке вы найдете новые и полезные услуги. Например: ДОСТАВКА СВЕЖЕЗАМОРОЖЕННОЙ ЕДЫ, БЛЮД ДЛЯ СЫРОЕДОВ, УСЛУГИ КЕЙТЕРИНГА, ШЕФ-ПОВАР ПО ВЫЗОВУ и тд',
			position: 'top-center'
		}
	}, {
		element: '.f-links',
		popover: {
			className: fStepPopover,
			title: ' ',
			description: 'Мы в поиске партнеров! Всю подробную информацию Вы узнаете, кликнув сюда',
			position: 'top-center'
		}
	}];
</script>
<main class="page-content">
	<?php if ($banner_slider): ?>
	<div class="main-slider-wrap">
		<a class="main-slider-control main-slider-control-left" href="#" id="main-slider-control-left"></a>
		<div class="main-slider slider" data-slider="mainSlider">
			<?php foreach ($banner_slider as $banner): ?>
				<?php if ($banner->image): ?>
					<div class="main-slider-item" style="background-image: url('/images/00/<?= $banner->image ?>.jpg');background-repeat: no-repeat;background-size: cover;background-position: center ;">
						<div class="container">
							<div class="row">
								<div class="col-xs-12 col-md-6 plashka">
									<div class="main-slider-item-category" <?= $banner->colorStyle ?>><?= $banner->title ?></div>
									<h2 class="main-slider-item-title" <?= $banner->colorStyle ?>><?= $banner->subtitle ?></h2>
									<p class="main-slider-item-text" <?= $banner->colorStyle ?>><?= $banner->text ?><p>
									<?php if ($banner->link): ?>
										<a class="main-slider-item-link" href="<?= $banner->link ?>">подробнее</a>
									<?php endif ?>
								</div>
							</div>
						</div>
					</div>
				<?php endif ?>
			<?php endforeach ?>
		</div>
		<a class="main-slider-control main-slider-control-right" href="#" id="main-slider-control-right"></a>
	</div>
	<?php endif ?>
	<div class="get-order-widget" id="adressBar">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<form class="get-order-form posr" action="#">
						<div class="get-order-dropdown-wrap" data-toggle="popover" data-content="Выберете свой город из списка" data-trigger="hover" data-placement="top">
							<span class="get-order-dropdown-label">Ваш город:</span>
							<div class="dropdown get-order-dropdown poss">
								<button title="выбор города" class="dropdown-toggle city_list_current" id="dropdownMenu1" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
									<span class="city">
										Алматы
									</span>
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu drop-menu menu-2 dre kostyl" aria-labelledby="dropdownMenu1">
								<?php if ($citys): ?>
								<?php foreach ($citys as $city): ?>
									<li>
										<a class="city_list" data-city="<?= $city->id ?>" data-lat="<?= $city->lat ?>" data-lng="<?= $city->lng ?>">
											<?= $city->value_ru ?>
										</a>
									</li>
								<?php endforeach ?>
								<?php endif ?>
								</ul>
							</div>
						</div>

						<div class="input-wrapper" 
							style="display: inline-block;
									position: relative;
									max-width: 374px;
									width: 100%;"
						>
							<input class="form-control" id="adressInput" type="text" placeholder="Введите адрес доставки"/>
							<div id="addressList" class="fake-input search-geo" style="position: absolute; display: none;">
								<ul></ul>
							</div>
						</div>

						<div class="get-order-buttons">
							<!-- <button type="button" data-trigger="hover" class="button orange-button" data-toggle="popover" data-placement="bottom" data-content="Приносим свои извинения, наш сервис дорабатывается">Найти меня</button> -->
							<div style="display: inline-block;" data-content="Кнопка «Найти меня» определит ваше текущее местоположение "data-placement="auto bottom" data-toggle="popover" data-trigger="hover" >
								<button title="Найти меня на карте" type="button" class="button orange-button" data-toggle="modal" data-target="#find-me-modal"  id="findme">найти меня</button>
							</div>
							<!-- <div style="display: inline-block;" data-toggle="popover" data-content="Кнопка «найти ближайшие» покажет, какие ближайшие к вам рестораны являются партнерами Тамак" data-trigger="hover" data-placement="auto bottom"> -->
								<!-- <button type="button" class="button" data-toggle="modal" id="restoAround" data-target="#findRestoModal">найти ближайшие</button> -->
							<!-- </div> -->
							<!-- <button title="Найти ближайшие рестораны на карте" type="button" data-trigger="hover" class="button" data-toggle="popover" data-placement="bottom" data-content="Приносим свои извинения, наш сервис дорабатывается">найти ближайшие</button> -->
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="search-bar__container" id="searchDoneBar" style="display: none">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 search-bar__wrapper" style="color: #000;">
					<div class="search-bar__block search-bar__text" style="color: #fff;">
						<span class="search-bar__addr" style="font-weight: 700; font-size: 15px;">Ваш адрес: </span>
						<span class="search-bar_text" style="font-size: 18px;font-weight: 300;">Кунаева</span> <i style="font-size: 18px;margin-left: 5px;" class="fas fa-map-marker-alt"></i>
					</div>
					<div class="search-bar__block search-bar__button">
						<button title="Изменить выбор города" class="button-standard button-standard--white">Изменить</button>
					</div>
					<div class="get-order-buttons">
						<a class="button-standard button-standard--white" href="<?= siteURL() . 'restoraunts?type=all_restaurants' ?>">найти ближайшие</a>
						<!-- <button type="button" data-trigger="hover" class="button-standard button-standard--white" data-toggle="popover" data-placement="bottom" data-content="Найти ближайшие рестораны на карте. Приносим свои извинения, наш сервис дорабатывается">найти ближайшие</button> -->
					</div>
				</div>
			</div>
		</div>
	</div>
<?php if ($category_buttons): ?>
	<div class="main-categories-wrap">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="main-categories" data-toggle="popover" data-content="В этом блоке вы сможете найти нужное блюдо и рестораны, которые приготовят их для вас" data-trigger="hover" data-placement="auto bottom">
						<?php $chunkButtons = array_chunk($category_buttons,6); ?>
						<?php $countBtnChunk = count($chunkButtons); ?>
						<?php foreach ($chunkButtons[0] as $category_button): ?>
							<div class="category">
								<a class="category-pic" href="restoraunts?type=<?= $category_button->link ?>">
									<?php if ($category_button->image): ?>
									<img  onerror="this.onerror=null;srcRepair(this, '/img/about-us/pizza.png');" src="/images/resize/75/75/<?= $category_button->image ?>.jpg" alt="<?= $category_button->name ?>"/>
									<?php endif ?>
								</a>
								<p class="category-title"><?= $category_button->name ?></p>
							</div>
						<?php endforeach ?>
						<div id="hidden-main-categories">
						<?php if ($countBtnChunk > 1): ?>
						<?php for($i= 1; $i <= $countBtnChunk; $i++): ?>

							<?php foreach ($chunkButtons[$i] as $category_button): ?>
							<div class="category">
								<a class="category-pic" href="restoraunts?type=<?= $category_button->link ?>">
									<img loading="lazy" onerror="this.onerror=null;srcRepair(this, '/img/about-us/pizza.png');" src="/images/resize/75/75/<?= $category_button->image ?>.jpg" alt="<?= $category_button->name ?>"/>
								</a>
								<p class="category-title"><?= $category_button->name ?></p>
							</div>
							<?php endforeach ?>
						<?php endfor ?>
						<?php endif ?>
						</div>
					</div>
					<?php if ($countBtnChunk > 1): ?>
					<a id="category-toggle" href="#" data-toggle="popover" data-content="Нажмите на «все категории» и увидите весь список категорий блюд!" data-trigger="hover" data-placement="auto bottom">
						<span class="mr-1">Все категории</span>
						<span class="category-toggle-arrow"></span>
					</a>
					<?php endif ?>
				</div>
			</div>
		</div>
	</div>
<?php endif ?>
<div class="dishes-list gray-bg dishes-list__popular">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<a class="slider-control slider-control-right" href="#" id="popular-slider-control-right"></a>
				<div id="restoraunt_items_container">
				</div>
				<script id="restoraunt_items" type="text/x-handlebars-template">
				<h3 class="title">популярные блюда</h3>
				<div class="dishes-list__popular__widget slider" data-slider="dishesSliderOnMain">
					{{#each this}}
						<div class="dish">
							<a class="dish-pic" href="/restoraunts/restoraunt_menu?id={{service.id}}">
								{{#if photos.0.path}}
								<img loading="lazy" style="height:100%!important" src="<?= Api::$apiDomain ?>/imgs/{{id}}_photo1_265x225.png" alt="{{name}}"/>
								{{/if}}
							</a>
							<div class="dish-info">
								<a class="dish-title" href="/restoraunts/restoraunt_menu?id={{service.id}}">
									{{name}}
								</a>
								{{#if description}}
									<div class="dish-desc">
										{{description}}
									</div>
								{{/if}}
								<div class="dish-bottom-info">
								{{#if price}}
									<span class="dish-price">
										{{price}} тг
									</span>
								{{/if}}
								<a href="/restoraunts/restoraunt_menu?id={{service.id}}">
									<button class="btn button dish-order-button">
										подробнее
									</button>
								</a>
								</div>
							</div>
						</div>
					{{/each}}
				</div>
				<a class="slider-control slider-control-left" href="#" id="popular-slider-control-left"></a>
				</script>
			</div>
		</div>
	</div>
</div>
<?php if ($promos): ?>
	<div class="promos gray-bg">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<h3 class="title">акции</h3>
					<div class="promos__widget">
					<script id="promotional_dishes_template" type="text/x-handlebars-template">
					<a class="slider-control slider-control-left" href="#" id="promo-slider-control-left"></a>
					<div class="slick-in-modile slick-promo">
						{{#each this}}
							<div class="col-md-12 col-lg-6">
							<div class="promo-item">
								<a class="promo-item-pic" href="#">
									{{#if photos.0.path}}
										<img loading="lazy" style="height:100%!important" src="<?= Api::$apiDomain ?>/imgs/{{id}}_photo1_265x225.png" alt="{{name}}"/>
									{{/if}}
								</a>
								<div class="promo-item-info">
									<p class="promo-item-title">
										{{name}}
									</p>
									<a href="/restoraunts/restoraunt_menu?id={{service.id}}">
										<div class="restaurant-title">{{ service.name }}</div>
									</a>
									{{#if description}}
										<div class="promo-item-desc">{{ description }}</div>
									{{/if}}
									<div class="promo-bottom-info">
										{{#if price}}
											<span class="promo-price">{{ price }} тг</span>
										{{/if}}
										<a href="/restoraunts/restoraunt_menu?id={{service.id}}">
											<button title="подробнее" class="btn button promo-order-button">подробнее</button>
										</a>
									</div>
								</div>
							</div>
						</div>
						{{/each}}
					</div>
					<a class="slider-control slider-control-right" href="#" id="promo-slider-control-right"></a>
					</script>
						<div class="row" id="promotional_dishes_container">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif ?>
<?php if (false): ?>
	<div id="executors" class="couriers-wrap gray-bg couriers__widget" style="padding-bottom: 20px; ">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<h3 class="title">наши курьеры</h3>
					<div class="couriers">
						<div class="couriers-map">
							<div id="mapExecutors" style="min-height:360px; background: #ddd;"></div>
						</div>
						<div class="couriers-list">
							<?= include_file('executors/list', $executors->executorsSearch()) ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif ?> 
<script id="restaurants" type="text/x-handlebars-template">
	<div class="col-xs-12">
		<h3 class="title">популярные рестораны</h3>
		<div class="restaurants__widget">
			<a class="slider-control slider-control-left" href="#" id="resto-slider-control-left"></a>
			<style>
				.slick-in-modile .slick-slide {outline: none;}
			</style>
			<div class="row slick-in-modile">
			{{#each this}}
				<div class="col-xs-12 col-sm-6">
					<div class="restaurant">
						<div class="restaurant-pic">
							<a href="/restoraunts/restoraunt_menu?id={{id}}">
								<div loading="lazy" class="restoraunt__image-item" style="background-image:url('<?= Api::$apiDomain ?>/imgs/{{id}}_photo1_158.png');" alt="{{name}}"></div>
							</a>
						</div>
						<div class="restaurant-info">
							<a href="/restoraunts/restoraunt_menu?id={{id}}">
							<div class="restaurant-title">
								{{name}}
							</div>
							</a>
							<div class="restaurant-menu">
								Кухни: {{prod_removeShares product_tags}}
							</div>
							<div class="restaurant-bottom-info">
								<div class="restaurant-minsum">
									<p>Минимальная сумма:</p><span class="number">
									{{#if minbill}}
										{{minbill}} тг.
									{{else}}
										--
									{{/if}}
									</span>
								</div>
								<div class="restaurant-workhours">
										<p>Время работы:</p><span class="hours">
										{{normTime openfrom}} - {{normTime opento}}</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			{{/each}}
			</div>
			<a class="slider-control slider-control-right" href="#" id="resto-slider-control-right"></a>
		</div>
	</div>
</script>
<div class="restaurants gray-bg">
	<div class="container">
		<div class="row">
		</div>
	</div>
</div>
<?php if ($reviews): ?>
	<div class="reviews gray-bg">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<h3 class="title">отзывы</h3>
					<div class="reviews-slider slider" data-slider="restaurantsSliderOnMain">
						<?php foreach ($reviews as $review): ?>
							<?php if ($review->name && $review->text): ?>
						<div class="review">
							<div class="row">
								<div class="col-xs-12 col-sm-3">
									<div class="review-info">
										<div class="review-title"><?= $review->name ?></div>
										<div class="review-date"><?= $review->updated ?></div>
									</div>
								</div>
								<div class="col-xs-12 col-sm-9">
									<div class="review-block">
										<div class="review-rate">
											<?php $stars = $review->stars; ?>
											<?php for ($i=0; $i < 5; $i++) :?>
												<?php if ($stars > 0): ?>
												<i class="fas fa-star rate-star active"></i>
												<?php --$stars; ?>
												<?php else: ?>
												<i class="fas fa-star rate-star"></i>
												<?php endif ?>
										<?php endfor ?>
										</div>
										<div class="review-text">
											<?= $review->text ?>
										</div>
									</div>
								</div>
							</div>
						</div>
							<?php endif ?>
						<?php endforeach ?>
					</div>
					<div class="reviews-slider-controls">
						<div class="reviews-slider-control reviews-slider-up" id="reviews-slider-up"></div>
						<div class="reviews-slider-control reviews-slider-down" id="reviews-slider-down"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif ?>
	<div class="banner gray-bg">
		<img class="img-responsive banner__center" loading="lazy" src="/img/app.png" alt="app banner"/>
		<div class="item-banner">
			<div class="items-banner">
				<div class="links-banner" >
					<a href="#" data-trigger="hover" data-toggle="popover" data-placement="bottom" data-content="Приносим свои извинения, наш сервис дорабатывается"></a>
				</div>
				<div class="links-banner">
					<a href="#" data-trigger="hover" data-toggle="popover" data-placement="bottom" data-content="Приносим свои извинения, наш сервис дорабатывается"></a>
				</div>
			</div>
		</div>
	</div>
    <link rel="dns-prefetch" href="https://api-maps.yandex.ru/"/>
</main>