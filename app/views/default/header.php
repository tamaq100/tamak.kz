<header role="banner">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-3">
        <div class="logo-wrap"><a class="logo" href="/"><img src="/img/logo_1.png" alt="Logotype"/></a></div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-5">
        <form class="search-wrap" action="#" method="GET">
          <div class="input-group">
            <button class="search-button" title="Поиск" type="submit"></button>
            <!-- {{!-- result --}} -->
            <input  class="form-control search-input" type="text" placeholder="Ресторан или блюдо" data-toggle="popover" data-content="Поисковая строка поможет найти Вам любимое блюдо либо ресторан" data-trigger="hover" data-placement="top auto" autocomplete="off" id="input-search" />
            <!-- hidden  -->
            <div class="search-results hidden">
              <ul>
                <script id="search-results" type="text/x-handlebars-template">
                {{#if this.isset}}
                  {{#if this.services}}
                    <li>
                      <span><b>Рестораны</b></span>
                    </li>
                  {{#each this.services}}
                    <li>
                      <a href="/restoraunts/restoraunt_menu?id={{id}}">{{name}}</a>
                    </li>
                  {{/each}}
                  {{/if}}
                  {{#if this.products}}
                    <li>
                      <span><b>Блюда</b></span>
                    </li>
                    {{#each this.products}}
                      <li>
                        <a href="/restoraunts/restoraunt_menu?id={{service.id}}">{{name}}</a>
                      </li>
                    {{/each}}
                  {{/if}}
                  {{#if this.more}}
                    <li>
                      <a href="/search?value={{search}}">
                        <span><b>Больше результатов ({{all_count}})</b></span>
                      </a>
                    </li>                    
                  {{/if}}
                {{else}}
                    <li>
                      Нет результатов
                    </li>
                {{/if}}
                </script>
              </ul>
            </div>
          </div>
        <p class="search-title">Популярные запросы: 
          <?php 
            $queries = [];
            if ($finder_popular = Config::get('finder_popular')) {
              $queries = explode(',', $finder_popular);
            }
            $queries = array_map(function ($item)
            {
              return trim($item);
            }, $queries);
            $queries = implode('</b>, <b class="popular-query">', $queries);
           ?>
          <span>
                <b class="popular-query"><?= $queries  ?></b>
            </span>
        </form>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-3">
        <div class="h-info">
<!--           <div class="h-button  <?= User::isAuth()? 'hidden': '' ?> defaultLogin" >
               <button class="btn auth-button loginbtn" data-toggle="popover" data-placement="bottom" data-content="Приносим свои извинения, наш сервис дорабатывается">
                вход / регистрация
              </button>
              <div class="button-bonus" data-toggle="popover" data-content="Для того, чтобы получать бонусы от каждой покупки, зарегистрируйтесь или войдите в свой личный кабинет" data-trigger="hover" data-placement="auto">
                <a class="button-bonus__present" href="#"></a>
              </div>
          </div> -->
          <div class="h-button <?= User::isAuth()? 'hidden': '' ?> defaultLogin">
               <button class="btn auth-button loginbtn" data-toggle="modal" data-target="#main-modal">
                вход / регистрация
              </button>
              <div class="button-bonus" data-toggle="popover" data-content="Для того, чтобы получать бонусы от каждой покупки, зарегистрируйтесь или войдите в свой личный кабинет" data-trigger="hover" data-placement="auto">
                <a class="button-bonus__present" href="#"></a>
              </div>
          </div>
          <div class="h-button <?= User::isAuth()? '': 'hidden' ?> authLogin ">
            <span class="dropdown">
              <button class="btn auth-button lk-button smooth dropdown-toggle" type="button" id="lk-button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <span class="uname">-------------</span><i class="fas fa-chevron-down lk-button__chevron"></i>
              </button>
              <div class="dropdown-menu lk-button__container" aria-labelledby="lk-button">
                <a class="dropdown-item lk-button__item" href="/cabinet">
                  <div>
                    Личный кабинет
                  </div>
                </a>
                <a class="dropdown-item lk-button__item" onclick="User.exit()">
                  <div>
                    Выход
                  </div>
                </a>
              </div>            
            </span>
          </div>
          <div class="h-phone">
          <p class="h-phone-text">Позвонить нам:</p>
          <a href="tel:<?= $CGp = Config::get('phone')?: '' ?>">
            <?= $CGp ?>
          </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>