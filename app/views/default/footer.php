<a class="button-up" href="#" id="button-up">Наверх</a>
<footer role="contentinfo">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-9 col-md-9 col-lg-10 none-padding">
				<div class="col-xs-12 col-sm-6 col-md-6">
					<ul class="f-nav">
						<li class="f-nav-item">
							<a class="f-nav-item-link" href="/aboutus">о нас</a>
						</li>
						<li class="f-nav-item">
							<a class="f-nav-item-link" href="/news">новости</a>
						</li>
						<li class="f-nav-item">
							<a class="f-nav-item-link" href="/shares">акции</a>
						</li>
						<li class="f-nav-item">
							<a class="f-nav-item-link" href="/contacts">контакты</a>
						</li>
						<li class="f-nav-item">
							<a class="f-nav-item-link" href="/delivery">Доставка и оплата</a>
						</li>
						<li class="f-nav-item">
							<a class="f-nav-item-link" href="/agreement">пользовательское соглашение</a>
						</li>
					</ul>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 none-padding">
					<div class="f-links">
						<div class="f-link">
							<a class="f-link-pic" href="/auto">
								<img src="/img/icons/car.png" alt="car"/></a>
							<div class="f-link-info">
								<div class="f-link-title"><a href="/auto">У вас есть транспорт?</a></div><a class="f-link-subtitle" href="/auto">Зарабатывайте</a>
							</div>
						</div>
						<div class="f-link">
							<a class="f-link-pic" href="/dishes">
								<img src="/img/icons/pizza.png" alt="pizza"/></a>
							<div class="f-link-info">
								<div class="f-link-title"><a href="/dishes">вы вкусно готовите?</a></div>
								<a class="f-link-subtitle" href="/dishes">Добавьте свои блюда</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-3 col-md-3 col-lg-2">
				<div class="social-buttons-wrap">
					<p class="social-buttons-title">мы в соцсетях:</p>
					<div class="social-buttons light-social-buttons">
						<a target="_blank" class="social-button" href="<?= Config::get('instagram')?: ''  ?>">
							<i class="fab fa-instagram"></i>
						</a>
						<a target="_blank" class="social-button" href="<?= Config::get('facebook')?: '' ?>">
							<i class="fab fa-facebook"></i>
						</a>
						<?php if(Config::get('vk')): ?>
						<a target="_blank" class="social-button" href="<?= Config::get('vk')?: '' ?>">
							<i class="fab fa-vk"></i>
						</a>
						<?php endif ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>