<main class="page-content">
	<?php if ($bannerImg = ConfigDishes::get('banner_bg')): ?>
	<div class="page-banner page-banner-auto" style="background:url(/images/00/<?=	$bannerImg ?>.jpg)">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="page-banner-auto-inner">
						<div class="page-banner-info">
							<?php if ($titleBan = ConfigDishes::get('title_banner')): ?>
							<h1 class="page-banner-title"><?= $titleBan ?></h1>
							<?php endif ?>
							<?php if ($subBan = ConfigDishes::get('subtitle_banner')): ?>
							<p class="page-banner-text">
								<?= $subBan ?>
							</p>
							<?php endif ?>
							<button class="button orange-button" data-toggle="modal" data-target="#dishes-modal">подать заявку</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php endif ?>
	<?php if ($advantages): ?>
	<div class="benefits paddings">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="title inner-title centered">преимущества работы с нами</div>
				</div>
			</div>
			<div class="row">
				<?php foreach ($advantages as $advantage): ?>
				<div class="col-xs-12 col-sm-4">
					<div class="benefit-item">
						<?php if ($advantage->image): ?>
						<div class="benefit-item-pic">
							<img src="/images/00/<?= $advantage->image ?>.jpg" alt=""/>
						</div>
						<?php endif ?>
						<?php if ($advantage->title): ?>
						<div class="benefit-item-title"><?= $advantage->title ?></div>
						<?php endif ?>
						<?php if ($advantage->subtitle): ?>
						<div class="benefit-item-text"><?= $advantage->subtitle ?></div>
						<?php endif ?>
					</div>
				</div>
				<?php endforeach ?>
			</div>
		</div>
	</div>
	<?php endif ?>
	<div class="infoblock infoblock__left gray-bg paddings">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<div class="infoblock-pic">
					<?php if ($imageServ = ConfigDishes::get('image_service')): ?>
						<img src="/images/00/<?= $imageServ ?>.jpg" alt=""/>
					<?php endif ?>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6">
					<div class="infoblock-content">
						<?php if ($titleServ = ConfigDishes::get('title_service')): ?>
						<div class="infoblock-title title inner-title"><?= $titleServ ?></div>
						<?php endif ?>
						<?php if ($textServ = ConfigDishes::get('text_service')): ?>
						<div class="infoblock-text">
							<?= $textServ ?>
						</div>
						<?php endif ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="infoblock infoblock__right paddings">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-sm-push-6">
					<div class="infoblock-pic">
						<?php if ($imageCoop = ConfigDishes::get('image_cooperation')): ?>
							<img src="/images/00/<?= $imageCoop ?>.jpg" alt=""/>
						<?php endif ?>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-sm-pull-6">
					<div class="infoblock-content">
						<?php if ($titleCoop = ConfigDishes::get('title_cooperation')): ?>
						<div class="infoblock-title title inner-title">
							<?= $titleCoop ?>
						</div>
						<?php endif ?>
						<?php if ($textCoop = ConfigDishes::get('text_cooperation')): ?>
							<div class="infoblock-text">
								<?= $textCoop ?>
							</div>
						<?php endif ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php if ($partners): ?>
	<div class="partners-wrap paddings gray-bg">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="title inner-title centered">с нами работают</div>
					<div class="partners slider" data-slider="dishPartners">
						<?php foreach ($partners as $partner): ?>
						<div class="partner"><img src="/images/fit/117/117/<?= $partner->image ?>.jpg" alt=""/></div>
						<?php endforeach ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php endif ?>
	<div class="apply paddings decor-bg-light">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<h3 class="title inner-title">начните зарабатывать!</h3>
					<p>
						Заполните заявку на сотрудничество,<br>
						и наши менеджеры свяжутся с вами
					</p>
					<button class="button orange-button" data-toggle="modal" data-target="#dishes-modal">подать заявку</button>
				</div>
			</div>
		</div>
	</div>
</main>