<div class="couriers-general-info">
	<div class="all-couriers">
		<span class="all-couriers-label">Курьеров в системе:</span>
		<span class="all-couriers-count"><?= $executors ? count($executors) : '' ?></span>
	</div>
	<div class="free-couriers">
		<span class="free-couriers-label">Свободны:</span><span class="free-couriers-count">
			<?= $executorsActive ?:'0' ?>
			</span>
		</div>
</div>
<div class="slider">
	<?php foreach ($executorsTop3 as $executor): ?>
		<div class="courier">
			<div class="courier-pic" style="background-image: url('<?= Api::$apiDomain ?>/imgs/<?= $executor['id'] ?>_avatar_140.png');">
			</div>
			<span class="courier-name"><?= $executor['name'] ?></span>
			<div class="courier-rate">
			<?php for ($i=0; $i < 5; $i++) :?>
				<i class="fas fa-star rate-star"></i>
			<?php endfor ?>
				<?php $rating = $executor['positive_valuations'] - $executor['negative_valuations']?: 0; ?>
				<?php $percent = 100 * $rating / 5 ?>
				<div class="front-stars" style="width: <?= $percent ?>%;">
					<?php for ($i=0; $i < 5; $i++) :?>
						<i class="fas fa-star rate-star"></i>
					<?php endfor ?>
				</div>
			</div>
		</div>
	<?php endforeach ?>
</div>