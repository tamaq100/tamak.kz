<script class="rest-slide-data" data-slide="reviews" type="text/x-handlebars-template">
	<section class="slide-data" data-slide="reviews">
	{{#unless id }}
	<div class="container mt-30">
	  <div class="restoraunt-side__title">Оставить отзыв</div>
	</div>

	{{#if (user_is_auth)}}
	{{!-- <form class="container restoraunt-reviews__container pb-30">
	  <div class="white__block restoraunt-reviews__textarea mb-0" style="display:flex">
		<textarea style="height:auto !important;"></textarea>
	  </div>
	  <div class="restoraunt-reviews__rating-wrapper">
		<div class="restoraunt-reviews__descw">
		  <div class="restoraunt__stars-resto">Поставьте рейтинг заведению:</div>
		  <div class="restoraunt__stars-rating js-rating">
		  		<input id="valuation" name="valuation" type="hidden" value="">
				<i class="far fa-thumbs-down cup thumb thumb-red" style="margin-left: 10px;" data-check="false"></i>
				<i class="far fa-thumbs-up cup thumb thumb-orange" data-check="true"></i>
		  </div>
		</div>
		<div class="button-standard btn mt-15">оставить отзыв</div>
	  </div>
	</form> --}}
	{{else}}
	<div class="container mb-30">
		<div class="restoraunt-reviews__into">
			<a href="">Войдите</a><span>или</span><a href="">Зарегестрируйтесь</a><span>,чтобы оставить отзыв</span>
		</div>
	</div>
	{{/if}}
	{{/unless}}
	{{#if id}}
	<div class="container mb-15 mt-30">
	  <div class="restoraunt-side__title">{{reviews.length}} {{ declOfNumOVAV }}</div>
	</div>
	<div class="container mb-15">
	  <div class="micro-nav">
		  <a class="pr-10 micro-nav__text micro-nav__text--active" onclick="reviewsNav('all', this)" >Все отзывы</a>
		  <a class="pr-10 micro-nav__text" onclick="reviewsNav('positive', this)" >Положительные</a>
		  <a class="pr-10 micro-nav__text" onclick="reviewsNav('negative', this)" >Отрицательные</a>
	  </div>
	</div>
	<div class="container">
	  <div class="row">
	  {{#each this.reviews}}
		<div class="col-xs-12 review" data-valuation="{{valuation}}">
		  <div class="inner-review__block resto-review">
			<div class="col-xs-12 col-md-5 col-lg-4" style="display:flex;">
			  <div class="inner-review__wrap">
				<div class="inner-review__title">{{user.name}}</div>
				<div class="inner-review__date">{{created}}</div>
			  </div>
			</div>
			<div class="col-xs-12 col-md-5 col-lg-6">
			  <div class="inner-review__text">{{msg_valuation}}</div>
			</div>
			<div class="col-xs-12 col-md-2 col-lg-2" style="text-align:right;">
				{{#if valuation}}
					<i class="far fa-thumbs-up" style="font-size: 30px; color: #ff9800;"></i>
				{{else}}
					<i class="far fa-thumbs-down" style="font-size: 30px; color: #ff5722;"></i>
				{{/if}}
			</div>
		  </div>
		</div>
	  {{/each}}
	  </div>
	</div>
	{{/if}}
	</section>
</script>