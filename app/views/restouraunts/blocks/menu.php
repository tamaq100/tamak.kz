<script class="rest-slide-data" data-slide="menu" type="text/x-handlebars-template">
<div class="container" style="margin-top:50px;">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-2">
			<div class="restoraunt-sidebar__wrapper">
			{{#each tags}}
				<a class="restoraunt-sidebar__title" data-key="{{this}}" data-type="categorie">{{this}}</a>
			{{/each}}
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-10">
		<section class="product-slides__container" style="margin-top:0px;">
			<div class="js-wrap-preload"></div>
		</section>
		</div>
	</div>
</div>
</script>
<script class="rest-slide-categories" data-slide="category" type="text/x-handlebars-template">

<div class="product-slides__slide" data-slide="baseCategory">
	<div class="oh-x mb-30">
{{#each this}}
		{{#if (isDublicate category)}}
		{{else}}
		{{#if slide}}
			<div class="restoraunt-side__title">{{category}}</div>
			<div class="restoraunt-big__slider restoraunt-big__slider--arrows" style="margin-bottom:60px">
				{{#each data}}
					<div class="restoraunt-big__slide" data-prodId="{{id}}">
					<div class="restoraunt-big__slide-image" style="background-image:url(<?= Api::$apiDomain ?>/imgs/{{id}}_photo1_265x225.png)">
					</div>
					<div class="restoraunt-big__slide-wrapper restoraunt-big__slide-wrapper--flex">
						<div class="restoraunt-big__slide-wrapper2">
							<div class="restoraunt-big__slide-title">
								{{name}}
							</div>
							<div class="restoraunt-big__slide-desc">
								{{description}}
							</div>
						</div>
						<div class="restoraunt-big__slide-wrapper--flex--row restoraunt-big__slide-wrapper--flex--sm-col mt-30">
							<div class="restoraunt-big__slide-price">
								{{price}}тг
							</div>
							<a>
								<div class="restoraunt-big__slide-button js-addToBasket" data-prodId="{{id}}" data-price="{{price}}">
								Заказать
								</div>
							</a>
						</div>
					</div>
					</div>
				{{/each}}
				{{#unless data}}
					<div style="font-weight: 400;color: #545454;font-size: 16px;" class="mb-30 mt-30">Нет товаров</div>
				{{/unless}}
			</div>
		{{else}}
			<div class="restoraunt-side__title mb-30 mt-30">{{category}}</div>
				<div class="restoraunt-menu dishes-list py-0 slider" data-slider="dishesSliderOnMain">
			{{#each data}}
				<div class="dish dish--mr20" style="vertical-align: top;" data-prodId="{{id}}">
					<div class="dish-pic" href="#" style="background-image:url(<?= Api::$apiDomain ?>/imgs/{{id}}_photo1_320x190.png)">
					</div>
					<div class="dish-info">
						<div class="dish-title" href="#">
							{{name}}
						</div>
						<div class="dish-desc">
							{{description}}
						</div>
						<div class="dish-bottom-info">
							<span class="dish-price">
								{{price}} тг
							</span>
							<button class="btn button dish-order-button js-addToBasket"  data-prodId="{{id}}" data-price="{{price}}">
								заказать
							</button>
						</div>
					</div>
				</div>
			{{/each}}
			</div>
		{{/if}}
		{{/if}}
{{/each}}
		</div>
</div>
</script>

<script class="rest-slide-category" data-slide="category" type="text/x-handlebars-template">

<div class="product-slides__slide" data-slide="{{this.slideKey}}">


		<div class="restoraunt-side__title mb-30" >{{this.slideKey}}</div>

	<div class="oh-x mb-30">
		{{#if this.products.onlyKey}}
			<div class="restoraunt-menu dishes-list py-0 slider" data-slider="dishesSliderOnMain">
			{{#each this.products.onlyKey}}
				<div class="dish dish--mr20" style="vertical-align: top;" data-prodId="{{id}}">
					<a class="dish-pic" href="#" style="background-image:url(<?= Api::$apiDomain ?>/imgs/{{id}}_photo1_320x190.png)">
					</a>
					<div class="dish-info">
					<a class="dish-title" href="#">{{name}}</a>
						<div class="dish-desc">{{description}}</div>
						<div class="dish-bottom-info">
							<span class="dish-price">{{price}} тг</span>
						<button class="btn button dish-order-button js-addToBasket"  data-prodId="{{id}}" data-price="{{price}}">заказать</button>
						</div>
					</div>
				</div>
			{{/each}}
				</div>
		{{else}}
			<h1 class="restoraunt-sidebar__title">Нет товаров</h1>
		{{/if}}
		{{#if this.products.popular}}
		<div class="restoraunt-side__title mb-30 mt-30">Популярное</div>
			<div class="restoraunt-menu dishes-list py-0 slider" data-slider="dishesSliderOnMain">
			{{#each this.products.popular}}
				<div class="dish dish--mr20" style="vertical-align: top;" data-prodId="{{id}}">
					<a class="dish-pic" href="#" style="background-image:url(<?= Api::$apiDomain ?>/imgs/{{id}}_photo1_320x190.png)">
					</a>
					<div class="dish-info">
					<a class="dish-title" href="#">{{name}}</a>
						<div class="dish-desc">{{description}}</div>
						<div class="dish-bottom-info">
							<span class="dish-price">{{price}} тг</span>
						<button class="btn button dish-order-button js-addToBasket"  data-prodId="{{id}}" data-price="{{price}}">заказать</button>
						</div>
					</div>
				</div>
			{{/each}}
			</div>
		{{/if}}
</div>
</div>
</script>