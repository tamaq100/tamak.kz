<script class="rest-slide-data" data-slide="info" type="text/x-handlebars-template">
<div class="container" style="margin-top:50px;">
	<div class="row">
	<div class="col-xs-12 mb-30">
		<div class="restoraunt-side__title">О заведении</div>
		<table class="restoraunt-table__container mb-30" style="width:100%;">
		<thead>
			<tr>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
			</tr>
			<tr>
				<td class="restoraunt-table__title">
					Кухни
				</td>
				<td class="restoraunt-table__text">
					{{prod_removeShares product_tags}}
				</td>
			</tr>
			<tr>
				<td class="restoraunt-table__title">
					Блюда
				</td>
				<td class="restoraunt-table__text">
				{{#each (removeAllCategory categories 'all_restaurants')}}
					{{value_ru}},
				{{/each}}
				</td>
			</tr>
			<tr>
				<td class="restoraunt-table__title">
					Время приема заказов
				</td>
				<td class="restoraunt-table__text">
					{{openfrom}} - {{opento}}
				</td>
			</tr>
		</thead>
		</table>
		<div class="restoraunt-side__title mb-15">
			Условия доставки
		</div>
		<p class="restoraunt-info__text mb-30">
			{{#if own_executors}}
				Доставка Ресторана
			{{else}}
				Доставка Tamaq
			{{/if}}
		</p>
		<div class="restoraunt-side__title mb-15">
			Адрес
		</div>
		{{#each addresses}}
			<p class="restoraunt-info__text mb-30">
			{{#if town.value_ru}}{{town.value_ru}}, {{/if}}{{street}} {{#if house}}, д.{{house}}{{/if}}
			</p>
		{{/each}}
		<div class="mb-30" id="map" style="width:100%; height: 360px;background-image:url('../img/map_1.jpg');background-size:cover;">
		</div>
		<div class="white__block white__block-wrapper--light mb-30" style="display:flex;flex-direction:column;justify-content: center;align-items: center;">
			<a class="simple__button simple__button--orange simple__button--big" href="/restoraunts/restoraunt_menu?id={{this.id}}">Сделать заказ</a>
		</div>
	</div>
	</div>
</div>
</script>