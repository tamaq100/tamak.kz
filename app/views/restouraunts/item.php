<section class="menu-main">
<script id="restoraunt_menue" type="text/x-handlebars-template">
	<div class="container-fluid white-bg">
	  <div class="container">
		<div class="row mb-30 mt-30">
		  <div class="col-xs-12 col-sm-8 col-md-9 col-lg-9 restoraunt-main-b restoraunt-main-list">
			<div class="restoraunt__image restoraunt__image--pd">
				<div class="restoraunt__image-item" style="background-image:url('<?= Api::$apiDomain ?>/imgs/{{id}}_photo1_140.png');"></div>
			</div>
			<div class="restoraunt__wrap restoraunt__wrap--ml restoraunt__wrap--ml-list avaible">
			  <div class="restoraunt__wrap--flex restoraunt__wrap--flex-wrap">
				<div class="inner-review__title inner-review__title--uppercase">{{this.name}}</div>
				<div class="restoraunt__stars" style="display:flex;align-items:center">
					<span class="restoraunt__stars-resto">{{business.value_ru}}</span>
					<div class="restoraunt__stars" style="position: relative; display: flex;width:fit-content;max-width: intrinsic;height:14px;">
						<i class="fas fa-star rate-star"></i>
						<i class="fas fa-star rate-star"></i>
						<i class="fas fa-star rate-star"></i>
						<i class="fas fa-star rate-star"></i>
						<i class="fas fa-star rate-star"></i>
						<div class="front-rate" style="position:absolute;left:0;top:0;display:flex;flex-wrap:nowrap;flex-shrink:0;overflow:hidden;align-content:center;width:{{percentagePercent this.positive_valuations this.negative_valuations}}%">
							<i class="fas fa-star rate-star active"></i>
							<i class="fas fa-star rate-star active"></i>
							<i class="fas fa-star rate-star active"></i>
							<i class="fas fa-star rate-star active"></i>
							<i class="fas fa-star rate-star active"></i>
						</div>
						{{!-- <span style="transform:translateY(-27%);font-weight:500">({{sum2 positive_valuations negative_valuations}})</span> --}}
					</div>
				</div>
			  </div>
			  <div class="restoraunt__wrap">
				<div class="restoraunt__text restoraunt__text--12 restoraunt__text-pt">
					Прием заказов: {{openfrom}} - {{opento}}
				</div>
				<div class="restoraunt__text restoraunt__text--13 restoraunt__text-pt">
					Кухни:
					{{prod_removeShares product_tags}}
				</div>
{{!-- 				<div class="restoraunt__text restoraunt__text--13 restoraunt__text-pt">
					Блюда:
					{{#each (removeAllCategory categories 'all_restaurants')}}
						{{value_ru}} |
					{{/each}}
				</div> --}}
			  </div>
			  <div class="restoraunt__wrap--aflex restoraunt__logopay-container pt-15-10">
				<div class="restoraunt__logopay">
					<img src="/img/cabinet-restoraunts/visa.png"/>
				</div>
				<div class="restoraunt__logopay">
					<img src="/img/cabinet-restoraunts/master.png"/>
				</div>
			  </div>
			  <div class="restoraunt__wrap--flex restoraunt__bottom-menu">
				<div class="restoraunt__wrap-hlist restoraunt__wrap-hlist--bold pt-15-10 pr-10">
					<a data-nav="/restoraunts/restoraunt_menu" href="/restoraunts/restoraunt_menu?id={{id}}">
						<span>Меню</span>
					</a>
					<a data-nav="/restoraunts/restoraunt_info" href="/restoraunts/restoraunt_info?id={{id}}">
						<span>Информация</span>
					</a>
					<a data-nav="/restoraunts/restoraunt_reviews" href="/restoraunts/restoraunt_reviews?id={{id}}">
						<span>Отзывы</span>
					</a>
				</div>
				<div class="restoraunt__wrap pt-15-10">
					{{#if (user_is_auth)}}
						{{#if (is_favorite id)}}
							<div class="restoraunt__like" data-id="{{id}}">
							   <i class="fas fa-heart like--active"></i>
									<span>Убрать из избранных</span>
							</div>
						{{else}}
							<div class="restoraunt__like" data-id="{{id}}">
								<i class="fas fa-heart" ></i>
								<span>В избранное</span>
							</div>
						{{/if}}
					{{/if}}
				</div>
			  </div>
			</div>
		  </div>
		  <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
			<div class="restoraunt__wrap pt-15-10">
				<div class="restoraunt__text-m ta-l">
					Минимальная сумма:
				</div>
				{{#if minbill}}
					<div class="restoraunt__text-b ta-l">{{minbill}} тг.</div>
				{{else}}
					<div class="restoraunt__text-b ta-l">-- --</div>
				{{/if}}
			</div>
			<div class="restoraunt__wrap pt-15-10">
				<div class="restoraunt__text-m ta-l">
					Доставка:
				</div>
				<div class="restoraunt__text-b ta-l">
				{{#if own_executors}}
					Доставка Ресторана
				{{else}}
					Доставка Tamaq
				{{/if}}
				</div>
			</div>
		  </div>
		</div>
	  </div>
	</div>
</script>
</section>