<script>
var sortE = {
	"distanceASC": {
		'field': 'distance',
		'type': 'Double',
		'asc': true
	},
	"minbillASC": {
		"field": "minbill",
		"type": "Double",
		"asc": true
	},
	"distanceDSC": {
		'field': 'distance',
		'type': 'Double',
		'asc': false
	},
	"minbillDSC": {
		"field": "minbill",
		"type": "Double",
		"asc": false
	}
}
var filters = {
	"with_stocks":{
		data: [{
			"field": "product_tags",
			"type":"String",
			"value":"Акции"
		}
		],
		type: 'like_ands'
	},
	"work_now": {
		data: [{
			"field": "opento",
			"type": "Integer",
			"value": new Date().getHours() * 60
		}],
		type: 'ge_ands'
	},
	/*"good_rating": {
		data: [{
			"field": "positive_valuations",
			"type": "Integer",
			"value": 0
		}],
		type: 'ne_ands'
	},*/
	"own_executor": {
		data: [{
			"field": "own_executors",
			"type": "Boolean",
			"value": true
		}],
		type: 'eq_ands'
	},
	"own_executorf": {
		data: [{
			"field": "own_executors",
			"type": "Boolean",
			"value": false
		}],
		type: 'eq_ands'
	}
};
</script>
<div class="welcome-hero" id="welcome_hero">
    <img class="welcome-hero__ico" src="/img/heroes/hero01.png" alt=""/>
    <img class="welcome-hero__text" src="/img/icons/welcome-hero_text.png" alt=""/>
    <a class="welcome-hero_close" href="#" id="welcome_hero_close"></a>
</div>
<main class="page-content page-content--gray news-block js-wrap-container-rest preload">
	<div class="js-wrap-preload"></div>
	<div class="container">
		<div class="row">
			<ul class="breadcrumb">
				<li>
					<a href="<?=siteURL()?>">Главная</a>
				</li>
				<li>
					<a href="<?=actualLinkNonFiltered()?>"><?=$typePage->name?></a>
				</li>
			</ul>
		</div>
	</div>
	<div class="container">
		<div class="page__wrap page__wrap--flex page__wrap--flex-alignA">
			<h1 class="page__title"><?=$typePage->name?><span class="page__title-and" id="resto_count">(---)</span>
			</h1>
			<div class="page__wrap mb-md-15">
				<div class="page__sort">Сортировать по:
					<span class="dropdown drop-order-dropdown filter-drop">
						<button class="dropdown-toggle dropdown-toggle--caret-left" id="sortBy" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
							<span class="caret"></span><span class="textF">-- --</span></i></button>
						<ul class="dropdown-menu dropdown-menu--right drop-menu menu-2 dre" aria-labelledby="sortBy">
							<li>
								<a data-filter="distanceASC">Ближе</a>
							</li>
							<li>
								<a data-filter="distanceDSC">Дальше</a>
							</li>
<!-- 						<li>
								<a data-filter="minbillDSC">Сначала дороже</a>
							</li>
							<li>
								<a data-filter="minbillASC">Сначала дешевле</a>
							</li> -->
						</ul>
					</span>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-4 col-md-4 specific-col-lg-23">
				<form class="white__block white__block--big-px js-menu-rest" style="display:flex; justify-content: space-between; flex-wrap:wrap">
					<div class="wrap-dib mb-15">
						<div class="restoraunt-filter__title">
							<div class="restoraunt-filter__title">
								<a data-type="type" class="text text--gray text--hover-orange <?= isset($_GET['filter']) && $_GET['filter']?'':'active' ?>" href="<?= getInsertGetParamToURL('filter', '', actualLinkNonFiltered()) ?>">
									Все Критерии
								</a>
							</div>
						</div>
						<label class="restoraunt-filter__checkbox">
							<input class="js-filter-ands" type="checkbox" name="with_stocks"/>
							<span class="text">C акциями</span>
							<span class="checkmark"></span>
						</label>
						<label class="restoraunt-filter__checkbox">
							<input class="js-filter-ands" type="checkbox" name="work_now"/>
							<span class="text">Работают сейчас</span>
							<span class="checkmark"></span>
						</label>
						<label class="restoraunt-filter__checkbox">
							<input class="js-filter-ands" type="checkbox" name="own_executor"/>
							<span class="text">Доставка ресторана</span>
							<span class="checkmark"></span>
						</label>
						<label class="restoraunt-filter__checkbox">
							<input class="js-filter-ands" type="checkbox" name="own_executorf"/>
							<span class="text">Доставка Tamaq</span>
							<span class="checkmark"></span>
						</label>
					</div>
					<?php if (isset($category_buttons) && $category_buttons): ?>
						<div class="wrap-dib">
							<div class="restoraunt-filter__title">
								<a data-type="type" class="text text--gray text--hover-orange <?= $_GET['type'] === 'all_restaurants' ? 'active':'' ?>" href="<?= getInsertGetParamToURL('type', 'all_restaurants', actualLinkNonFiltered()) ?>">
									Все Категории
								</a>
							</div>
							<?php foreach ($category_buttons as $btn): ?>
							<?php if (!($btn->name === 'Все заведения')): ?>
							<div class="" style="padding-bottom: 13px;">
								<?php if ($btn->link === $_GET['type']): ?>
								<a data-type="type" data-value="<?= isset($kitchen->key) ?: '' ?>" class="text text--gray text--hover-orange active" href="<?= getInsertGetParamToURL('type', $btn->link, actualLinkNonFiltered()) ?>">
								<?php else: ?>
								<a data-type="type" data-value="<?= isset($kitchen->key)?:'' ?>" class="text text--gray text--hover-orange" href="<?= getInsertGetParamToURL('type', $btn->link, actualLinkNonFiltered()) ?>">
								<?php endif?>
									<span class="text" >
										<?=$btn->name?>
									</span>
								</a>
							</div>
							<?php endif?>
							<?php endforeach?>
						</div>
					<?php endif?>
					<?php if (isset($kitchens) && $kitchens): ?>
						<div class="wrap-dib">
							<div class="restoraunt-filter__title">
								<a data-type="type" class="text text--gray text--hover-orange <?= isset($_GET['area'])?'':'active' ?>" href="<?= getInsertGetParamToURL('area', '', actualLinkNonFiltered()) ?>">
									Все Кухни
								</a>
							</div>
							<?php foreach ($kitchens as $kitchen): ?>
							<div class="" style="padding-bottom: 13px;">
								<?php if (isset($_GET['area']) && ($kitchen->key === $_GET['area'])): ?>
								<a data-type="area" data-value="<?= $kitchen->key ?>" class="text text--gray text--hover-orange active" href="<?= getInsertGetParamToURL('area', $kitchen->key, actualLinkNonFiltered()) ?>">
								<?php else: ?>
								<a data-type="area" data-value="<?= $kitchen->key ?>" class="text text--gray text--hover-orange" href="<?= getInsertGetParamToURL('area', $kitchen->key, actualLinkNonFiltered()) ?>">
								<?php endif?>
									<span class="text" >
										<?=$kitchen->name?>
									</span>
								</a>
							</div>
							<?php endforeach?>
						</div>
					<?php endif?>
				</form>
			</div>
			<div class="col-xs-12 col-sm-8 col-md-8 specific-col-lg-77" id="restoraunts_container" style="min-height: 250px">
				<script id="restoraunt_items" type="text/x-handlebars-template">
				{{#each this}}
				<div class="white__block rest-item" >
					<div class="col-xs-12 col-sm-8 col-md-9 col-lg-9 restoraunt-main-b restoraunt-main-list">
						<div class="restoraunt__image restoraunt__image--pd">
							<a href="/restoraunts/restoraunt_menu?id={{id}}">
								<div class="restoraunt__image-item" style="background-image:url('<?= Api::$apiDomain ?>/imgs/{{id}}_photo1_140.png');" alt="{{name}}"></div>
							</a>
						</div>
						<div class="restoraunt__wrap restoraunt__wrap--ml restoraunt__wrap--ml-list">
							<div class="restoraunt__wrap--flex restoraunt__wrap--flex-wrap restoraunt__wrap--flex-aic">
									<div class="inner-review__title inner-review__title--uppercase">
										<a class="inner-review__title--anchor" href="/restoraunts/restoraunt_menu?id={{id}}">
											{{name}}
										</a>
									</div>
								<div class="restoraunt__stars" style="display:flex;align-items:center">
									<span class="restoraunt__stars-resto">{{business.value_ru}}</span>
									<!-- <div class="restoraunt__stars" style="position: relative; display: flex;width:fit-content;max-width: intrinsic;height:14px;">
										<i class="fas fa-star rate-star"></i>
										<i class="fas fa-star rate-star"></i>
										<i class="fas fa-star rate-star"></i>
										<i class="fas fa-star rate-star"></i>
										<i class="fas fa-star rate-star"></i>
										<div class="front-rate" style="position:absolute;left:0;top:0;display:flex;flex-wrap:nowrap;flex-shrink:0;overflow:hidden;align-content:center;width:{{percentagePercent this.positive_valuations this.negative_valuations}}%">
											<i class="fas fa-star rate-star active"></i>
											<i class="fas fa-star rate-star active"></i>
											<i class="fas fa-star rate-star active"></i>
											<i class="fas fa-star rate-star active"></i>
											<i class="fas fa-star rate-star active"></i>
										</div>
										{{!-- <span style="transform:translateY(-27%);font-weight:500">({{sum2 positive_valuations negative_valuations}})</span> --}}
									</div> -->
								</div>
							</div>
							<div class="restoraunt__wrap">
								<div class="restoraunt__text restoraunt__text--12 restoraunt__text-pt">Прием заказов: {{normTime openfrom}} - {{normTime opento}}</div>
								<div class="restoraunt__text restoraunt__text--12 restoraunt__text-pt">Кухни:
										{{prod_removeShares product_tags}}
								</div>
							</div>
							<div class="restoraunt__wrap--aflex restoraunt__logopay-container pt-15-10">
								<div class="restoraunt__logopay"><img src="/img/cabinet-restoraunts/visa.png"/></div>
								<div class="restoraunt__logopay"><img src="/img/cabinet-restoraunts/master.png"/></div>
							</div>
							<div class="restoraunt__wrap--flex restoraunt__bottom-menu">
								<div class="restoraunt__wrap-hlist pt-15-10">
									<a href="restoraunts/restoraunt_menu?id={{id}}">
										<span>Меню</span>
									</a>
									<a href="restoraunts/restoraunt_info?id={{id}}">
										<span>Информация</span>
									</a>
									<a href="restoraunts/restoraunt_reviews?id={{id}}">
										<span>Отзывы</span>
									</a>
								</div>
								<div class="restoraunt__wrap pt-15-10">
									{{#if (user_is_auth)}}
										{{#if (is_favorite id)}}
											<div class="restoraunt__like" data-id="{{id}}">
											<i class="fas fa-heart like--active"></i>
													<span>Убрать из избранных</span>
											</div>
										{{else}}
											<div class="restoraunt__like" data-id="{{id}}">
												<i class="fas fa-heart" ></i>
												<span>В избранное</span>
											</div>
										{{/if}}
									{{/if}}
								</div>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
						<div class="restoraunt__wrap pt-15-10">
							<div class="restoraunt__text-m ta-l">Минимальная сумма:</div>
							{{#if minbill}}
								<div class="restoraunt__text-b">{{minbill}} тг.</div>
							{{else}}
								<div class="restoraunt__text-b">-- --</div>
							{{/if}}
						</div>
						<div class="restoraunt__wrap pt-15-10">
							<div class="restoraunt__text-m ta-l">Доставка:</div>

							<div class="restoraunt__text-b ta-l">
							{{#if own_executors}}
								Доставка Ресторана
							{{else}}
								Доставка Tamaq
							{{/if}}
							</div>
						</div>
					</div>
					{{#if (prod_issetShares product_tags)}}
						<div class="restoraunt__share restoraunt__share--orange">Акции</div>
					{{/if}}
				</div>
				{{/each}}
				</script>
			</div>
		</div>
	</div>
	<script id="restoraunt_nav" type="text/x-handlebars-template">
	<div class="container">
		<div class="news-block__pagination">
			<!-- Вперёд -->
			{{#if (Next_nav current pages)}}
				<a href="{{Next_navFull current pages}}">
					<div class="news-block__pagination__next"></div>
				</a>
			{{/if}}
			<!-- Назад -->
			{{#if (Prev_nav current)}}
				<a href="{{Prev_navFull current}}">
					<div class="news-block__pagination__prev"></div>
				</a>
			{{/if}}
			<!-- передаём страницы текущую страницу и сколько страниц по бокам выводить -->
			{{#times2 pages current '2'}}
			<ul class="pagination">
				{{#if_eq  this.current this.times}}
				<li class="active">
					<a href="{{urlBuilder 'page' this.times}}">
						{{this.times}}
					</a>
				</li>
				{{else}}
					<li class="">
						<a href="{{urlBuilder 'page' this.times}}">
							{{this.times}}
						</a>
					</li>
				{{/if_eq}}
			</ul>
			{{/times2}}
		</div>
	</div>
	</script>
</main>