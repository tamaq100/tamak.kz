<div class="welcome-hero" id="welcome_hero">
    <img class="welcome-hero__ico" src="/img/heroes/hero01.png" alt=""/>
    <img class="welcome-hero__text" src="/img/icons/welcome-hero_text.png" alt=""/>
    <a class="welcome-hero_close" href="#" id="welcome_hero_close"></a>
</div>
<main class="page-content page-content--gray news-block">
	<section class="js-wrap-container-rest">
	<div class="js-wrap-preload"></div>

	<?php include 'helpers/breadcrumb.php' ?>
	<?php include 'item.php' ?>
	<section class="resto-slides">
		<div class="js-wrap-preload"></div>
	</section>
	<section data-block="slide-active">
	<?php include 'blocks/'.$menu_item ?>
	</section>
	
	</section>
</main>