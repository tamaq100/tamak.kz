<?
/**
 * 
 */
class DeliveryController extends BaseController
{
	public function indexAction()
	{
        if (!$rubric = Rubric::findOneBy(['furl' => 'delivery'])) do404();

        $this->meta = $rubric->meta;

        $this->title = $rubric->title;
        $this->meta['title'] = Config::get('metaTitleMain');
        $this->meta['keywords'] = Config::get('metaKeywordsMain');
        $this->meta['description'] = Config::get('metaDescriptionMain');

        $vars = [
            'title'     => $this->title,
            'agreement_title' => ConfigDelivery::get('title'),
            'agreement_text' => ConfigDelivery::get('text'),
            'breadcrumbs' => $this->getBreadcrumbs(array(
                array(
                    'title' => $this->title,
                ),
            )),
        ];
        return $this->render('default/agreement', $vars);
	}

}