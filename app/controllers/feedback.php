<?php

/** 
* Feedback page controller
*/
class FeedbackController extends BaseController
{
	public function sendAction()
	{
	    $entity = new Feedback_contact;

	    $errors = array();
	    
	    $entity->massiveSetter($_POST);

	    $entity->save($errors);

	    if (isset($_GET['modal']) && $_GET['modal'] == 1) {
		    if ($errors) return json_encode(array(
		        'html' => include_file('feedback/modal', array('entity' => $entity, 'errors' => $errors))
		    ));
	    } else {
		    if ($errors) return json_encode(array(
		        'html' => include_file('feedback/form', array('entity' => $entity, 'errors' => $errors))
		    ));
	    }

	    $mails = Config::get('admin_mails');
	    
	    $to = $mails;
	    $subject = __('mails.admin.after_feedback_send.subject');

	    $vars = [
	    	'entity'		 => $entity,
	    	'errors'         => $errors,
	    ];
	    
	    $message = include_file('feedback/mail', $vars);
	    Mail::send($to, $subject, $message);

	    if (isset($_GET['modal']) && $_GET['modal'] == 1) {
		    return json_encode(array(
		        'html' => include_file('feedback/modal_success')
		    ));
		} else {
		    return json_encode(array(
		        'html' => include_file('feedback/success')
		    ));
		}
	}
}
?>