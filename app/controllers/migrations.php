<?php

/**
 * Контроллер личного кабинета
 */
class MigrationsController {
    public $up_code = 'dd2aed2e36fd9008';

    /**
     * Миграции БД
     */
    public function upAction()
    {

        if (! (isset($_GET['code']) && $_GET['code'] == $this->up_code)) abort();

        $migrations_table_exists = false;
        $asd = [];
        $db = new DB;
        $qRS = $db::query('SHOW TABLES');
        $result = $qRS->fetchAll(PDO::FETCH_COLUMN);

        if (in_array('migrations', $result)) {
            $migrations_table_exists = true;
        }

        if (!$migrations_table_exists) {
            $db::query('CREATE TABLE `migrations` (
                `name` VARCHAR(255) NULL,
                `created_at` INT NULL
            )
            COLLATE=\'utf8_general_ci\'
            ENGINE=InnoDB;');
        }

        $used_migrations = array();

        $qRS = $db::query("select * from migrations");
        $used_migrations = $qRS->fetchAll(PDO::FETCH_COLUMN);

        // Чтобы пропустить миграцию необходимо
        // раскомментировать и вставить на место ключа нужный файл
        // $used_migrations['up010.sql'] = true;

        // Чтобы повторно запустить миграцию необходимо
        // раскомментировать и вставить на место ключа нужный файл
        // unset($used_migrations['up021.sql']);
        foreach (scandir(SITE_DIR . '/app/migrations') as $file) {
            if (! strstr($file, 'up')) continue;

            if (in_array($file, $used_migrations)) continue;
            // Парсинг файла
            $SQL = file_get_contents(SITE_DIR . '/app/migrations/'.$file);
            // Для корректных замен вставляем символы новой строки в начало файла
            $SQL = "\n".$SQL;
            $SQL = "\n".$SQL;
            // Убираем комментарии
            $SQL = preg_replace('/\/\*.+\n/iU', '', $SQL);
            $SQL = preg_replace('/--.*\n/iU', '', $SQL);
            // Убираем директивы SET
            $SQL = preg_replace('/\nSET .*\n/iU', '', $SQL);
            $SQL = preg_replace('/\nSET time_zone .*\n/iU', '', $SQL);

            // Выполняем каждый запрос по отдельности
            foreach (explode(";\n", $SQL) as $query) {
                $query = trim($query);
                if (! $query) continue;
                $this->run_migration($query, $db);
            }
            $db::query('INSERT INTO migrations SET name="'.$file.'", created_at="'.time().'"');

            echo 'Migration '.$file.' executed<br>';
        }

        die('OK');
    }

    // Функция для запуска миграций с разными префиксами таблиц
    public function run_migration($query, $connection)
    {
        if (strpos($query, '%table_prefix%')) {
            foreach (BaseModel::$lang_versions as $key => $value) {
            	$lang_query = str_replace('%table_prefix%', $key, $query);
            	$connection::query($lang_query);
            }
    	} else {
        	$connection::query($query);
    	}
    }

}