<?php

/**
 * Родительский Контроллер для рубрики
 */
class RubricController extends BaseController {

    public $model = 'Rubric';

    public function indexAction($furl)
    {
        if (! $furl && isset($_GET['href'])) {
            $furl = $_GET['href'];
        }
        if (! isset($furl) ) {
            die('502');
        }
        $model = $this->model;
        $rubrics = $model::findAll();
        $root_id = 19;
        if (isset($_SESSION['lang']) && $_SESSION['lang'] == 'en') {
            $root_id = 20;
        }
        $rubrics = $rubrics[$root_id]; 
        // Ищем главную рубрику
        $main_rubric = $model::findOneBy(array('furl' => $_GET['controller']));

        if (! $main_rubric) {
            $main_rubric = $model::findOneBy(array('furl' => $furl));
        }
        if (! $main_rubric || empty($main_rubric)) {
            do404();
        }

        // Первая хлебная крошка
        $breadcrumbs = array(array(
            'title' => $main_rubric->title,
            'href'  => $main_rubric->href,
        ));

        // if (isset($_GET['furl']) && $_GET['furl'])  {
        //     // Складываем массив furl'ов из адреса 
        //     $current_category_furl = isset($furl) ? addslashes($furl) : false;
        //     $current_category_furl = strtolower($current_category_furl);
        //     $category_furl_array = explode('/', $current_category_furl);

        //     array_map(
        //         function($element) {
        //             $model = $this->model;  
        //             if(! $model::findOneBy(array('furl' => $element)))
        //             {
        //                 do404();
        //             }
        //         }, $category_furl_array);

        //     // Ищем рубрику по последнему furl в адресе 
        //     $current_rubric = $model::findOneBy(array('furl' => end($category_furl_array)));
        //     if (! $current_rubric) die('502');
        //     $breadcrumbs = $this->getBreadcrumbsByFurl($breadcrumbs, $category_furl_array, 0);
        // } else {
        //     //  Рубрика если furl пуст. Для индивидуальных настроек достаточно просто переопределить эту функцию
        //     $current_rubric = $this->getRubricIfFurlIsEmpty($main_rubric);
        //     $link_furl =  '/' . $current_rubric->furl;

        //     if ($current_rubric == $main_rubric) {
        //         $breadcrumbs = array(array(
        //             'title' => __('default.names'),
        //             'link'  => '/',
        //         ));
        //     }
        //     $breadcrumbs[] = array(
        //         'title' => $current_rubric->title,
        //         'link'  => $link_furl,
        //     );
        // }

		// $parent_rubric = $model::findOneBy(array('id' => $current_rubric->parent_id));
		
		// Ищем рубрики, принадлежащие тому же родителю
		// $closest_rubrics = $model::findBy(array('parent_id' => $current_rubric->parent_id));

        $this->title = $current_rubric->title;

		$content = $model::getContent($current_rubric->furl);

		$vars = array(
			'content'				=> $content,
			'closest_rubrics'		=> $closest_rubrics[$current_rubric->parent_id],
			'parent_rubric'			=> $parent_rubric,
			'breadcrumbs'			=> $breadcrumbs,
		);

		return $this->render('default/rubric', $vars);
    }

    public function getRubricIfFurlIsEmpty($main_rubric)
    {
        $model = $this->model;
    	$current_rubric = $model::findOneBy(array('parent_id' => $main_rubric->id));
        if (! $current_rubric) {
            $current_rubric = $main_rubric;
        }

		return $current_rubric;

    }
        
    public function getBreadcrumbsByFurl($breadcrumbs, $category_furl_array, $i, $link_furl = '/about')
    {
        $model = $this->model;
        if (isset($category_furl_array[$i]) && $category_furl_array[$i]) {
            
            $current_rubric_furl = $category_furl_array[$i];
            $current_rubric = $model::findOneBy(array('furl' => $current_rubric_furl));

            $link_furl .=  '/' . $current_rubric->furl;

            $breadcrumbs[] = array(
                'title' => $current_rubric->title,
                'link'  => $link_furl,
            );

            $i++;

            return $this->getBreadcrumbsByFurl($breadcrumbs, $category_furl_array, $i, $link_furl);
        } 

        return $breadcrumbs;
    }
}