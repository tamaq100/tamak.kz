<?
/**
 * 
 */
class ContactsController extends BaseController
{
	public function indexAction()
	{
		// $couriers = Our_courier::findAll();
		// $faq = FAQ::FindAll();
		// $cont = 
        if (!$rubric = Rubric::findOneBy(['furl' => 'contacts'])) do404();

        $this->meta = $rubric->meta;

        $this->title = $rubric->title;
        $this->meta['title'] = Config::get('metaTitleMain');
        $this->meta['keywords'] = Config::get('metaKeywordsMain');
        $this->meta['description'] = Config::get('metaDescriptionMain');
        $cards = unserialize(ConfigContact::get('contact_dyn'));
        $vars = [
            'title'     	  => $this->title,
            'coords' 		  => ConfigContact::get('map_coords'),
            'cards' 	      => $cards,
            'breadcrumbs' => $this->getBreadcrumbs(array(
                array(
                    'title' => $this->title,
                ),
            )),
        ];
        return $this->render('default/contacts', $vars);
	}

}