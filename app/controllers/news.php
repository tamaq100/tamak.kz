<?
/**
 * 
 */
class NewsController extends BaseController
{
	public function indexAction()
	{
        if (!$rubric = Rubric::findOneBy(['furl' => 'news'])) do404();

        $this->meta = $rubric->meta;

        $this->title = $rubric->title;
        $this->meta['title'] = Config::get('metaTitleMain');
        $this->meta['keywords'] = Config::get('metaKeywordsMain');
        $this->meta['description'] = Config::get('metaDescriptionMain');

        $page = isset($_GET['page']) && intval($_GET['page']) > 0
            ? intval($_GET['page'])
            : 1;
        $limit = 4;

        $offset = ($page - 1) * $limit;

        $where = [
            // 'active' => 1,
        ];

        $vars = [
            'title'     => $this->title,
            'news'   => News::findBy($where, '', $limit, $offset),
            'pagination' => array(
                'page'   => $page,
                'limit'  => $limit,
                'count'  => News::getCount($where),
            ),
            // 'breadcrumbs' => $this->getBreadcrumbs(array(
            //     array(
            //         'title' => $this->title,
            //     ),
            // )),
        ];
        return $this->render('news/news', $vars);
	}
    public function showAction()
    {
        if (!isset($_GET['furl']) || $_GET['furl']) {
            // abort('404');
        }
        if (intval($_GET['furl'])) {
            $article = News::findOneBy(['id'=> $_GET['furl']]);
        }
        else{
            $article = News::findOneBy(['link' => $_GET['furl']]);
        }
        // $this->meta = $rubric->meta;
        // $this->title = $rubric->title;
        $this->meta['title'] = Config::get('metaTitleMain');
        $this->meta['keywords'] = Config::get('metaKeywordsMain');
        $this->meta['description'] = Config::get('metaDescriptionMain');

        $vars = [
            'title'     => $this->title,
            'article'      => $article,
            'breadcrumbs' => $this->getBreadcrumbs(array(
                array(
                    'title' => $this->title,
                ),
            )),
        ];
        return $this->render('news/news-inside', $vars);
    }

}