<?
/**
 * 
 */
class AgreementController extends BaseController
{
	public function indexAction()
	{
		// $couriers = Our_courier::findAll();
		// $faq = FAQ::FindAll();
		// $cont = 
        if (!$rubric = Rubric::findOneBy(['furl' => 'agreement'])) do404();

        $this->meta = $rubric->meta;

        $this->title = $rubric->title;
        $this->meta['title'] = Config::get('metaTitleMain');
        $this->meta['keywords'] = Config::get('metaKeywordsMain');
        $this->meta['description'] = Config::get('metaDescriptionMain');

        $vars = [
            'title'     => $this->title,
            'agreement_title' => ConfigAgreement::get('title'),
            'agreement_text' => ConfigAgreement::get('text'),
            'breadcrumbs' => $this->getBreadcrumbs(array(
                array(
                    'title' => $this->title,
                ),
            )),
        ];
        return $this->render('default/agreement', $vars);
	}

}