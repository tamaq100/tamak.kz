<?
/**
 * 
 */
class BasketController extends BaseController
{
	public function indexAction()
	{
		if (!$rubric = Rubric::findOneBy(['furl' => 'basket'])) do404();


		$this->meta = $rubric->meta;
		$this->title = $rubric->title;
		$this->meta['title'] = Config::get('metaTitleMain');
		$this->meta['keywords'] = Config::get('metaKeywordsMain');
		$this->meta['description'] = Config::get('metaDescriptionMain');

		$vars = [
			'title'     => $this->title,
			'breadcrumbs' => $this->getBreadcrumbs(array(
				array(
					'title' => $this->title,
				),
			)),
		];

		return $this->render('basket/index', $vars);
	}
}