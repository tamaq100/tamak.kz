<?
/**
 * 
 */
class AboutUsController extends BaseController
{
	public function indexAction()
	{
		// $couriers = Our_courier::findAll();
		// $faq = FAQ::FindAll();
		// $cont = 
        if (!$rubric = Rubric::findOneBy(['furl' => 'AboutUs'])) do404();

        $this->meta = $rubric->meta;

        $this->title = $rubric->title;
        $this->meta['title'] = Config::get('metaTitleMain');
        $this->meta['keywords'] = Config::get('metaKeywordsMain');
        $this->meta['description'] = Config::get('metaDescriptionMain');

        $vars = [
            'title'     => $this->title,
            'mission'	=> AboutUsMission::findAll(),
            'values'	=> AboutUsValues::findAll(),
            'team'		=> AboutUsTeam::findAll(),
        	// 'couriers'	=> $couriers,
        	// 'faq'		=> $faq,
         //    'howto'     => AutoHowTo::findAll(),
         //    'advantages' => AutoAdvantages::findAll(),
            'breadcrumbs' => $this->getBreadcrumbs(array(
                array(
                    'title' => $this->title,
                ),
            )),
        ];
        return $this->render('default/about-us', $vars);
	}

}