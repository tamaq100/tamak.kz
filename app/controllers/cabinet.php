<?
/**
 * PersonalArea
 */
class CabinetController extends BaseController
{

	function __construct()
	{
		$userAuth = Auth::getSession();
		// die(var_dump($userAuth));
		switch ($userAuth) {
			case 0:
				header('location: '.siteURL());
				exit;
				break;
			case 1:
				self::allowURL($this::$USER_MENU);
				break;
			case 2:
				self::allowURL($this::$COURIER_MENU);
				break;
			case 3:
				self::allowURL($this::$USER_MENU);
				break;
			default:
				header( 'Refresh: 0; url=/' );
				break;
		}
	}
	public static $USER_MENU = array(
		'bonus'	=>	array(
			'name'	=>	array(
				'ru'	=>	'Бонусы',
				'hidden' => 1,
			),
		),
		'redact'	=> array(
			'name' => array(
				'ru' => 'Редактирование данных',
				'hidden' => 1,
			), 
		),
		'history'	=>	array(
			'name'	=>	array(
				'ru'	=>	'история заказов',
			),
		),
		'reviews'   => array(
			'name'  => array(
				'ru' => 'мои отзывы', 
			), 
		),
		'restolike' => array(
			'name' => array(
				'ru' => 'избранные рестораны', 
			),
		),
		'chat'  => array(
			'name' => array(
				'ru' => 'чат с курьером', 
			),
		),
		'notifications' => array(
			'name' => array(
				'ru' => 'уведомления',
			), 
		),
		'order' => array(
			'name' => array(
				'ru' => 'заказ',
				'hidden' => 1, 
			), 
		),
	);
	public static $COURIER_MENU = array(
		'khistory'	=>	array(
			'name'	=>	array(
				'ru'	=>	'история заказов',
			),
		),
		'payed' => array(
			'name' => array(
				'ru' => 'история оплаты', 
			),
		),
		'creviews'   => array(
			'name'  => array(
				'ru' => 'отзывы', 
			), 
		),
		'kchat'  => array(
			'name' => array(
				'ru' => 'чат с клиентом', 
			),
		),
		'kredact' => array(
			'name' => array(
				'ru' => 'Редактирование данных',
				'hidden' => 1, 
			), 
		),
		'korder' => array(
			'name' => array(
				'ru' => 'заказ',
				'hidden' => 1, 
			), 
		),
	);
	protected static function allowURL($pagesArray)
	{
		$req_uri = explode('/', trim($_SERVER['REQUEST_URI'], '/'));
		$page_path = strtok(end($req_uri),'?');
		$allowOrDenied = isset($pagesArray[$page_path]) ? true : false;
		$userAuth = Auth::getSession();
		if ($userAuth === 2) {
			if (!$allowOrDenied) header('location: '. siteURL(). 'cabinet/khistory');
		}else{
			if (!$allowOrDenied) header('location: '. siteURL(). 'cabinet/history');
		}

	}
	protected static function emptyAJ($client = 'user', $func_name = 'empty')
	{
		return json_encode(array("html" => include_file('lk/'.$client.'/template/'.$func_name)));
	}
	protected static function fullAJ($client = 'user', $func_name = 'empty'){
		return json_encode(array(
			"html" => include_file(
				'lk/'.$client.'/withdata/'.$func_name,
				['myname' => $func_name]
			)
		));
	}
	public function indexAction()
	{
		header( 'Refresh: 0; url=/cabinet/history' );
	}
	public function notificationsAction()
	{
		$func_name = 'notifications';
		$ajax = self::ajaxModule('user', 'notifications');
		if (!empty($ajax)) return $ajax;
		$vars = [
			'myname'    => $func_name,
			'title'     => 'Личный кабинет',
			'addons'	=>  array(
				'menu' => array(
					'menu' => $this::$USER_MENU,
					'active' =>  $func_name,
				)
			),
		];
		
		return $this->render('lk/empty', $vars,'lk/user-layout');
	}
	
	public function bonusAction()
	{
		$func_name = 'bonus';
		$ajax = self::ajaxModule('user', 'bonus');
		if (!empty($ajax)) return $ajax;
		$vars = [
			'myname'    => $func_name,
			'title'     => 'Личный кабинет',
			'addons'	=>  array(
				'menu' => array(
					'menu' => $this::$USER_MENU,
					'active' =>  $func_name,
				)
			),
		];
		// return full-page from browser
		return $this->render('lk/empty', $vars,'lk/user-layout');
	}
	public function kredactAction()
	{
		$func_name = 'kredact';
		$ajax = self::ajaxModule('courier', 'kredact');
		if (!empty($ajax)) return $ajax;
		$vars = [
			'myname'    => $func_name,
			'title'     => 'Личный кабинет',
			'addons'	=>  array(
				'menu' => array(
					'menu' => $this::$COURIER_MENU,
					'active' =>  $func_name,
				)
			),
		];
		// return full-page from browser
		return $this->render('lk/empty', $vars,'lk/courier-layout');
	}
	public function redactAction()
	{
		$func_name = 'redact';
		$ajax = self::ajaxModule('user', 'redact');
		if (!empty($ajax)) return $ajax;
		$vars = [
			'myname'    => $func_name,
			'title'     => 'Личный кабинет',
			'addons'	=>  array(
				'menu' => array(
					'menu' => $this::$USER_MENU,
					'active' =>  $func_name,
				)
			),
		];
		// return full-page from browser
		return $this->render('lk/empty', $vars,'lk/user-layout');
	}
	public function historyAction()
	{
		$func_name = 'history';
		// without-data on ajax request
		// return empty template
		$ajax = self::ajaxModule('user', 'history');

		if (!empty($ajax)) return $ajax;

		$user = User::findOneBy(['uid' => $_SESSION['uid']]);

		// $userData = User_data::findOneBy(['uid' => $user->uid]);
		// $payments = json_decode($userData->payments);

		$vars = [
			'myname'    => $func_name,
			'title'     => 'Личный кабинет',
			'user'		=> $user,
			// 'user_data'	=> $userData,
			// 'payments' => 	$payments,
			'addons'	=>  array(
				'menu' => array(
					'menu' => $this::$USER_MENU,
					'active' =>  $func_name,
				)
			),
		];
		// return full-page from browser
		return $this->render('lk/empty', $vars,'lk/user-layout');
	}
	public function khistoryAction()
	{
		$func_name = 'khistory';
		// without-data on ajax request
		$ajax = self::ajaxModule('courier', 'khistory');
		if (!empty($ajax)) return $ajax;
		$vars = [
			'myname'    => $func_name,
			'title'     => 'Личный кабинет',
			'addons'	=>  array(
				'menu' => array(
					'menu' => $this::$COURIER_MENU,
					'active' =>  $func_name,
				)
			),
		];
		return $this->render('lk/empty', $vars,'lk/courier-layout');
	}
	public function payedAction()
	{
		$func_name = 'payed';
		$ajax = self::ajaxModule('courier', 'payed');
		if (!empty($ajax)) return $ajax;
		$vars = [
			'myname'    => $func_name,
			'title'     => 'Личный кабинет',
			'addons'	=>  array(
				'menu' => array(
					'menu' => $this::$COURIER_MENU,
					'active' =>  $func_name,
				)
			),
		];
		return $this->render('lk/empty', $vars,'lk/courier-layout');
	}
	public function creviewsAction()
	{
		$func_name = 'creviews';
		$ajax = self::ajaxModule('courier', 'creviews');
		if (!empty($ajax)) return $ajax;
		$vars = [
			'myname'    => $func_name,
			'title'     => 'Личный кабинет',
			'addons'	=>  array(
				'menu' => array(
					'menu' => $this::$COURIER_MENU,
					'active' =>  $func_name,
				)
			),
		];
		return $this->render('lk/empty', $vars,'lk/courier-layout');
	}
	public function kchatAction()
	{
		$func_name = 'kchat';
		$ajax = self::ajaxModule('courier', $func_name);
		if (!empty($ajax)) return $ajax;
		$vars = [
			'myname'    => $func_name,
			'title'     => 'Личный кабинет',
			'addons'	=>  array(
				'menu' => array(
					'menu' => $this::$COURIER_MENU,
					'active' =>  $func_name,
				)
			),
		];
		return $this->render('lk/empty', $vars,'lk/courier-layout');	
	}
	public function korderAction()
	{
		$func_name = 'korder';
		$ajax = self::ajaxModule('courier',$func_name);
		if (!empty($ajax)) return $ajax;
		$vars = [
			'myname'    => $func_name,
			'title'     => 'Личный кабинет',
			'addons'	=>  array(
				'menu' => array(
					'menu' => $this::$COURIER_MENU,
					'active' =>  $func_name,
				)
			),
		];
		return $this->render('lk/empty', $vars,'lk/user-layout');
	}

	public function reviewsAction()
	{
		$func_name = 'reviews';
		$ajax = self::ajaxModule('user',$func_name);
		if (!empty($ajax)) return $ajax;
		$vars = [
			'myname'    => $func_name,
			'title'     => 'Личный кабинет',
			'addons'	=>  array(
				'menu' => array(
					'menu' => $this::$USER_MENU,
					'active' =>  $func_name,
				)
			),
		];
		return $this->render('lk/empty', $vars,'lk/user-layout');
	}
	public function restolikeAction()
	{
		$func_name = 'restolike';
		$ajax = self::ajaxModule('user',$func_name);
		if (!empty($ajax)) return $ajax;
		$vars = [
			'myname'    => $func_name,
			'title'     => 'Личный кабинет',
			'addons'	=>  array(
				'menu' => array(
					'menu' => $this::$USER_MENU,
					'active' =>  $func_name,
				)
			),
		];
		return $this->render('lk/empty', $vars,'lk/user-layout');
	}
	public function chatAction()
	{
		$func_name = 'chat';
		$ajax = self::ajaxModule('user',$func_name);
		if (!empty($ajax)) return $ajax;
		$vars = [
			'myname'    => $func_name,
			'title'     => 'Личный кабинет',
			'addons'	=>  array(
				'menu' => array(
					'menu' => $this::$USER_MENU,
					'active' =>  $func_name,
				)
			),
		];
		return $this->render('lk/empty', $vars,'lk/user-layout');
	}
	public function orderAction()
	{
		$func_name = 'order';
		$ajax = self::ajaxModule('user',$func_name);
		if (!empty($ajax)) return $ajax;
		$vars = [
			'myname'    => $func_name,
			'title'     => 'Личный кабинет',
			'addons'	=>  array(
				'menu' => array(
					'menu' => $this::$USER_MENU,
					'active' =>  $func_name,
				)
			),
		];
		return $this->render('lk/empty', $vars,'lk/user-layout');
	}
	protected static function ajaxModule($user = 'user', $func_name = 'empty'){
		// return partial template without on ajax request
		if (isset($_GET['empty-aj'])) return self::emptyAJ($user, $func_name);
		// return partial page with data on ajax request
		if (isset($_GET['full-aj'])) return self::fullAJ($user, $func_name);
	}
}