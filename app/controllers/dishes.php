<?
/**
 * 
 */
class DishesController extends BaseController
{
	public function indexAction()
	{
        if (!$rubric = Rubric::findOneBy(['furl' => 'dishes'])) do404();

        $this->meta = $rubric->meta;

        $this->title = $rubric->title;
        $this->meta['title'] = Config::get('metaTitleMain');
        $this->meta['keywords'] = Config::get('metaKeywordsMain');
        $this->meta['description'] = Config::get('metaDescriptionMain');

        $vars = [
            'title'     => $this->title,
            'advantages'  => DishesAdvantage::findAll(),
            'partners'	  => Partner::findAll(),
            'breadcrumbs' => $this->getBreadcrumbs(array(
                array(
                    'title' => $this->title,
                ),
            )),
        ];
        return $this->render('default/dishes', $vars);
	}

    public function submitAction()
    {
        $fields = [
            'name_restaurant'       => 'Название заведения',
            'type_restaurant'       => 'Тип заведения',
            'city'                  => 'Город',
            'name_form'             => 'Имя',
            'position'              => 'Должность',
            'number_form'           => 'Телефон',
            'email_form'            => 'Email',
        ];

        // build mail to tamaq100@gmail.com
        $subject = 'Заявка на сотрудничество c сайта tamaq.kz';
        $html = '<table>
            <tbody>';

        // get data
        foreach ($fields as $field => $label) {
            // validate data or send error message
            if (!isset($_POST[$field]) || ($_POST[$field] === '')) {

                $value = '';
                $errors = [
                    $field => $field,
                ];
                return json_encode(array(
                'html' => include_file('feedback/modal_dishes', array('errors' => $errors))
                ));
            } else {

                $value = $_POST[$field];
                
            }


            $html .= '
                <tr><td>';
            $html .= $label;

            $html .= '</td><td>';
            $html .= $value;
            $html .= '</td>
                </tr>';

        }

        $html .= '
            </tbody>
        </table>';

        // send mail
        // $to = 'abylay.t@4dclick.com';
        $to = 'tamaq100@gmail.com';
        // $to = 'sporretimur@gmail.com';
        
        Mail::send($to, $subject, $html);
        // return success message
        $successMessage = 'Ваша заявка успешно отправлена и находится на рассмотрении, наши менеджеры скоро свяжутся с вами';

        $response = [
            'html' => $successMessage
        ];


        return json_encode($response);
    }

}