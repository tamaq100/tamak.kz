<?
/**
 * 
 */
class UsersController extends BaseController
{
	public $up_code = '06rE3EBwXzWpVDXiIsx83iBOTwOPgurg';
	private $server_node = 'http://bg.tamaq.4dclick.asia/';
	

	public function listExecutorsAction()
	{
		if (! (isset($_GET['code']) && $_GET['code'] == $this->up_code)) abort(404);
		$path = $this->server_node .'user/list';
		$postData = array(
			'key' => $this->up_code
		);

		$executors = json_decode(Request::post($path, $postData));

		if (!$executors) return;

		foreach ($executors as $executor) {
			if (!$entity = Executor::findOneBy(['ids' => $executor->id])) {
			  $entity = new Executor;
			  $entity->ids = $executor->id;

			}
			// $userInfo = self::getUserInfo($executor->id);

			if (isset($executor->name)) {
				$entity->name = $executor->name;
			}

			if (isset($executor->avg_civility_rate) && isset($executor->avg_speed_rate) && isset($executor->avg_state_rate)) {
				$entity->rating = ((float)$executor->avg_civility_rate + (float)$executor->avg_speed_rate + (float)$executor->avg_state_rate) / 3; // среднее значение рейтингов
			}

			if (!isset($executor->executing_order) && isset($executor->workingMode) && $executor->workingMode != 'not_working') {
				$entity->free = 1;
			} else {
				$entity->free = 0;
			}

			if (isset($executor->is_online)) { // уменьшенная фотография
				$entity->is_online = $executor->is_online;
			}
			if (isset($executor->photos[1])) { // уменьшенная фотография
				$entity->avatar = $executor->photos[1]->path;
			}

			if (isset($executor->last_location)) {
				$entity->latitude = $executor->last_location->latitude;
				$entity->longitude = $executor->last_location->longitude;
			}

			$entity->save();
		}
		return 'Оk';

	}


	private function executorsGetAndHand()
	{
		try {
			return json_decode(Api::findAllCouriers(), true);
		} catch(Exception $e){
			do404();
		}

	}
	private function seachTopCouriers($executors)
	{
		$limit = 3;
		$top_executors = [];
		foreach ($executors as $key => $executor) {
			if ($limit === 0) break;
			if ($percentil = $executor['positive_valuations'] - $executor['negative_valuations'] || $limit) {
				$top_executors[] = $executor;
				--$limit;
			}
		}
		return $top_executors;
	}
	public function executorsSearch() {
		$executors = $this->executorsGetAndHand();
		$top_executors = $this->seachTopCouriers($executors);
		$executors_list = [];
		$executors_active = [];

		foreach ($executors as $key => $executor) {
			isset($executor['is_online'])  && ($executor['is_online'] === true) ? $executors_list[] = $executor:'';
			$executor['ask_for_executing'] === true ? $executors_active[] = $executor:'';
		}

		$vars = array(
			'executors'  => $executors,
			'executorsTop3' => $top_executors,
			'executorsActive' => count($executors_active),
		);
		return $vars;
	}
	public function executorsAction()
	{
		$vars = $this->executorsSearch();
		return json_encode(array(
			'html' => include_file('executors/list', $vars),
			'executors'  => $vars['executors'],
		));
	}
}