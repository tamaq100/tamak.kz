<?php
/**
 * Контроллер с функционалом для которого отдельный контроллер будет излишним
 */
class DefaultController extends BaseController {

    public function indexAction($id)
    {   
        if (isset($_GET['custom-street']) && $_GET['custom-street']) {
            header("Location: ".actualLinkFiltered().'search?value='.$_GET['custom-street']);
        }
        $vars = array(
            'banner_slider'     => Banner_slider::findAll(),
            'category_buttons'  => Category_button::findAll(),
            'promos'            => Promo::findAll(),
            'restaurants'       => Popular_restoraunt::findAll(),
            'reviews'           => Review::findAll(),
            'companys'          => Company::findAll(),
            'citys'             => City::findAll(),
        );
        
        $this->title = 'Главная';
        
        $this->meta['title'] = Config::get('metaTitleMain');
        $this->meta['keywords'] = Config::get('metaKeywordsMain');
        $this->meta['description'] = Config::get('metaDescriptionMain');

        return $this->render('default/main', $vars);
    }

    public function searchAction(){

        if (!$rubric = Rubric::findOneBy(['furl' => 'poisk'])) do404();
        $query = '';
        if (isset($_GET['value'])) {
            $query = $_GET['value'];
        }
        $vars = [
            'title'     => $this->title,
            'breadcrumbs' => $this->getBreadcrumbs(array(
                array(
                    'title' => $this->title,
                ),
            )),
            "query" => $query
        ];
        $this->meta = $rubric->meta;
        $this->title = $rubric->title;

        $this->meta['title'] = Config::get('metaTitleMain');
        $this->meta['keywords'] = Config::get('metaKeywordsMain');
        $this->meta['description'] = Config::get('metaDescriptionMain');

        return $this->render('default/search', $vars);
    }

    public function error_404Action()
    {
        $this->title = __('page_not_found');

        $vars = [
            'breadcrumbs'   => $this->getBreadcrumbs(array(
                array(
                    'title' => $this->title,
                ),
            )),
        ];

        return $this->render('default/404', $vars);
    }
}