<?
/**
 * 
 */
class RestorauntsController extends BaseController
{
	public function indexAction()
	{

		if (!$rubric = Rubric::findOneBy(['furl' => 'restoraunts'])) do404();
		if (empty($_GET["type"])) { header("Location: ".actualLinkFiltered().'?type=all_restaurants'); }

		$category_btn = Category_button::findOneBy(['link' => $_GET["type"]]);

		if (!$category_btn) { header("Location: ".actualLinkFiltered().'?type=all_restaurants'); }

		$this->meta = $rubric->meta;
		$this->title = $rubric->title;
		$this->meta['title'] = Config::get('metaTitleMain') ?: "";
		$this->meta['keywords'] = Config::get('metaKeywordsMain') ?: "";
		$this->meta['description'] = Config::get('metaDescriptionMain') ?: "";

		$vars = [
			'title'     => $this->title ?: "",
			'typePage'  => $category_btn,
            'category_buttons'  => Category_button::findAll(),
            'kitchens'  => Kitchen::findAll(),
			'breadcrumbs' => $this->getBreadcrumbs(array(
				array(
					'title' => $this->title,
				),
			)),
		];

		return $this->render('restouraunts/restouraunts', $vars);
	}

	public function restoraunt_menuAction()
	{
		if (!$rubric = Rubric::findOneBy(['furl' => 'restoraunt_menu'])) do404();

		$this->meta = $rubric->meta;
		$this->title = $rubric->title;
		$this->meta['title'] = Config::get('metaTitleMain');
		$this->meta['keywords'] = Config::get('metaKeywordsMain');
		$this->meta['description'] = Config::get('metaDescriptionMain');

		$vars = [
			'title'     => $this->title,
			'breadcrumbs' => $this->getBreadcrumbs(array(
				array(
					'title' => $this->title,
				),
			)),
			'menu_item' => 'menu.php',
		];

		return $this->render('restouraunts/restoraunt-layout',$vars);
	}
	public function restoraunt_infoAction()
	{
		if (!$rubric = Rubric::findOneBy(['furl' => 'restoraunt_info'])) do404();

		$this->meta = $rubric->meta;
		$this->title = $rubric->title;
		$this->meta['title'] = Config::get('metaTitleMain');
		$this->meta['keywords'] = Config::get('metaKeywordsMain');
		$this->meta['description'] = Config::get('metaDescriptionMain');

		$vars = [
			'title'     => $this->title,
			'breadcrumbs' => $this->getBreadcrumbs(array(
				array(
					'title' => $this->title,
				),
			)),
			'menu_item' => 'info.php',
		];
				
		return $this->render('restouraunts/restoraunt-layout',$vars);
	}
	public function restoraunt_reviewsAction()
	{
		if (!$rubric = Rubric::findOneBy(['furl' => 'restoraunt_reviews'])) do404();

		$this->meta = $rubric->meta;
		$this->title = $rubric->title;
		$this->meta['title'] = Config::get('metaTitleMain');
		$this->meta['keywords'] = Config::get('metaKeywordsMain');
		$this->meta['description'] = Config::get('metaDescriptionMain');

		$vars = [
			'title'     => $this->title,
			'breadcrumbs' => $this->getBreadcrumbs(array(
				array(
					'title' => $this->title,
				),
			)),
			'menu_item' => 'reviews.php',
		];
				
		return $this->render('restouraunts/restoraunt-layout', $vars);
	}
}