<?
/**
 * 
 */
class AutoController extends BaseController
{
	public function indexAction()
	{
		$faq = FAQ::FindAll();
		// $cont = 

        if (!$rubric = Rubric::findOneBy(['furl' => 'auto'])) do404();

        $this->meta = $rubric->meta;

        $this->title = $rubric->title;
        $this->meta['title'] = Config::get('metaTitleMain');
        $this->meta['keywords'] = Config::get('metaKeywordsMain');
        $this->meta['description'] = Config::get('metaDescriptionMain');
        
        $vars = [
            'title'     => $this->title,
            'executors'  => Executor::findBy([], 'rating desc'),
        	'faq'		=> $faq,
            'howto'     => AutoHowTo::findAll(),
            'advantages' => AutoAdvantages::findAll(),
            // 'breadcrumbs' => $this->getBreadcrumbs(array(
            //     array(
            //         'title' => $this->title,
            //     ),
            // )),
        ];
        return $this->render('default/auto', $vars);
	}
    public function submitAction()
    {
        $fields = [
            'name'                  => 'Имя',
            'surname'               => 'Фамилия',
            'number_form'           => 'Телефон',
        ];

        // build mail to tamaq100@gmail.com
        $subject = 'Заявка на сотрудничество курьера c сайта tamaq.kz';
        $html = '<table>
            <tbody>';

        // get data
        foreach ($fields as $field => $label) {
            // validate data or send error message
            if (!isset($_POST[$field]) || ($_POST[$field] === '')) {

                $value = '';
                $errors = [
                    $field => $field,
                ];
                return json_encode(array(
                'html' => include_file('feedback/modal_auto', array('errors' => $errors))
                ));
            } else {

                $value = $_POST[$field];
                
            }


            $html .= '
                <tr><td>';
            $html .= $label;

            $html .= '</td><td>';
            $html .= $value;
            $html .= '</td>
                </tr>';

        }

        $html .= '
            </tbody>
        </table>';

        // send mail
        // $to = 'abylay.t@4dclick.com';
        // $to = 'sporretimur@gmail.com';
        $to = 'tamaq100@gmail.com';
        Mail::send($to, $subject, $html);
        // return success message
        $successMessage = 'Ваша заявка успешно отправлена и находится на рассмотрении, наши менеджеры скоро свяжутся с вами';

        $response = [
            'html' => $successMessage
        ];


        return json_encode($response);
    }
}