<?php

/**
 * Контроллер личного кабинета

  // Adm
  // +79652112274
  // 31722387
  // Manager
  // +56773156699
  // 96170464
 */
class ApioController
{

	public $key_code_foods = 'qV9E3f9b';

	public $key_code_services = 'wQ4BN5zR';
	// взаимодействие
	public function upallPopularFoodAction()
	{
		if (!(isset($_GET['code']) && $_GET['code'] == $this->key_code_foods)) abort('');
		Api::UpAllPopularFood();
	}
	public function CreatePopularServicesAction()
	{
		if (!(isset($_GET['code']) && $_GET['code'] == $this->key_code_services)) abort('');
		Api::CreatePopularServices();
	}
	public function RestoAroundAction()
	{
		$coord = $_GET['coord'];
		$adress = explode(",", $_GET['adress']);
		$adress = Translit::transliterate($adress[0]);

		$result = Api::findRestoAround($coord, $adress);
		return $result;
	}
	/*
		Необходимо переодически запускать по крону,
		загружает всех курьеров NodeJS
	*/
	public function testRAction()
	{
		die(var_dump(Rubric::findAll()));
	}
	public function getCouriersAction()
	{
		// if (! (isset($_GET['code']) && $_GET['code'] == $this->key_code_services)) abort();
		$result = Api::findAllCouriers();
		return $result;
	}
	/*
		Необходимо переодически запускать по крону,
		загружает юзеров с помощью NodeJS
	*/
	public function unloadingUsersAction()
	{
		// if (! (isset($_GET['code']) && $_GET['code'] == $this->key_code_services)) abort();
		return Api::unloadingUsersList();
	}
	/*
		Необходимо переодически запускать по крону,
		загружает данные юзеров с помощью NodeJS
	*/
	public function unloadingUsersDataAction()
	{
		// if (! (isset($_GET['code']) && $_GET['code'] == $this->key_code_services)) abort();
		return Api::unloadingUsersData();
	}
	/*
		Необходимо переодически запускать по крону,
		загружает категории напрямую из API
	*/
	public function loadingCategoriesAction()
	{
		// if (! (isset($_GET['code']) && $_GET['code'] == $this->key_code_services)) abort();
		return Api::loadingCategories();
	}
	public function loadingKitchenAction()
	{
		// if (! (isset($_GET['code']) && $_GET['code'] == $this->key_code_services)) abort();
		return Api::loadingKitchens();
	}
	/*
		Необходимо переодически запускать по крону,
		загружает список городов напрямую из API
	*/
	public function unloadingCitiesAction()
	{

		return Api::unloadingCitiesList();
	}
	public function TestAction()
	{
		return print_r(GeocodeYandex::findPlace('Алматы'));
	}
	public function lifeCheckAction()
	{
		$GATEWAY = Api::getGateway();
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_PORT => "3036",
			CURLOPT_URL => 'http://' . $GATEWAY . '/lifecheck',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_POSTFIELDS => "",
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			echo "cURL Error #:" . $err;
		} else {
			echo $response;
		}
	}
}
