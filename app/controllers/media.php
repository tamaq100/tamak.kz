<?php 
/**
* Контроллер для работы с картинками
*/
use Intervention\Image\ImageManager;
class MediaController 
{
    private $image = '';
    private $w = NULL;
    private $h = NULL;
    private $cache_imagepath = '';

    public function createImage()
    {
        // Принимаем имя картинки
        if (!isset($_GET['imagename']) || !($imagename = $_GET['imagename'])) return abort('404');

        $image_manager = new ImageManager(array('driver' => 'gd'));
        // Высота
        $this->h = (isset($_GET['h']) && $_GET['h']) ? $_GET['h'] : NULL;
        // Ширина
        $this->w = (isset($_GET['w']) && $_GET['w']) ? $_GET['w'] : NULL;

        // Ищем ее в медии
        $imagepath = str_replace('//', '/', SITE_DIR . "/runtime/_media/" . $imagename);

        // Определяем расширение изображения
        $extensions = [
            'jpg',
            'png',
            'gif',
            'jpeg',
        ];

        // Добавляем в массив расширений, расширения аперкейсом
        $extensions = array_merge($extensions,  array_map('strtoupper', $extensions));

        foreach ($extensions as $ext) {
            $ext = '.'.$ext;
            if (file_exists($imagepath . $ext)) {
                $imagepath .= $ext;
        
                $cache_dir = $this->getCacheDirName();
                $cache_imagepath = $cache_dir.$imagename.$ext;
            }
        }

        if (!isset($cache_imagepath)) return abort('404');

        // Если изображение есть в кэше, то показываем его
        if (file_exists($cache_imagepath)) {
            $ext = strtolower($ext);
            if ($ext=='jpg') $ext = 'jpeg';
            header('Cache-Control: max-age=37739520, public');
            header('Content-type: image/'.substr($ext, 1));
            readfile($cache_imagepath);
            exit;
        }

        // создаем изображение
        $this->cache_imagepath = $cache_imagepath;

        $image = $image_manager->make($imagepath);

        $this->image = $image;
    }

    public function indexAction()
    {
        $this->createImage();

        if (intval($this->w) != 0) {
            $percent = $this->w / 100;
            $this->image->resize($this->image->width() * $percent, $this->image->height() * $percent);
        }

        return $this->showImage();
    }

    public function cropAction()
    {
        $this->createImage();
        $this->image->crop($this->w, $this->h);
        return $this->showImage();
    }

    public function crop_topAction()
    {
        $this->createImage();
        $x = $this->image->width() / 2 - $this->w / 2;

        $this->image->crop($this->w, $this->h, $x, 0);
        return $this->showImage();
    }

    public function fitAction()
    {
        $this->createImage();
        $this->image->fit($this->w, $this->h);
        return $this->showImage();
    }
    
    public function fit_topAction()
    {
        $this->createImage();
        $this->image->fit($this->w, $this->h, function ($constraint) {
           $constraint->aspectRatio();
        }, 'top');
        return $this->showImage();
    }

    public function resize_stretchAction()
    {
        $this->createImage();
        $this->image->resize($this->w, $this->h);
        return $this->showImage();
    }

    public function resizeAction()
    {
        $this->createImage();
        $this->image->resize($this->w, $this->h, function ($constraint) {
           $constraint->aspectRatio();
        });
        return $this->showImage();
    }

    public function resize_canvasAction()
    {
        $this->createImage();
        $this->image->resizeCanvas($this->w, $this->h);
        return $this->showImage();
    }
    
    public function showImage()
    {
        // Сохранеяем изображение в кэш
        
        if (! is_dir($this->getCacheDirName())) {
            mkdir($this->getCacheDirName(), 0777, true);
        }

        $this->image->interlace(true);
        $this->image->save($this->cache_imagepath);

        // Показываем изображение
        return $this->image->response();
    }

    public function getCacheDirName()
    {
        $folder = ['size'];
        if ($this->w) {
            $folder[] = 'w' . $this->w;
        }
        if ($this->h) {
            $folder[] = 'h' . $this->h;
        }

        $folder = implode('_', $folder);

        $cache_dir = SITE_DIR . '/runtime/_cache/images/' . $_GET['action'] . '/' . $folder . '/';
        $cache_dir = str_replace("//", "/", $cache_dir);

        return $cache_dir;
    }
}
