<?php

/**
 * Базовый контроллер.
 * От него должны наследоваться все контроллеры
 */
class BaseController {

	public $title = '';
	public $request_form = '';
	public $render_request_form = false;
	
	public $meta = array(
		'descipion' => '',
		'keywords' => array(),
	);

	public $styles = array(
		'/vendor/bootstrap/bootstrap.min.css',
		'/vendor/bootstrap-select/bootstrap-select.min.css',
		'/vendor/slick/slick-all.min.css',
		'/vendor/fontawesome-5.5.0/css/all.min.css',
		'/css/default.css',
		'/css/style.css',
        '/vendor/datetime-picker/jquery.datetimepicker.min.css',
        '/vendor/driver/dist/driver.min.css',
	);

	public $scripts = array(
		'/vendor/jquery/jquery.min.js',
		'/lazyload.min.js',
		'/vendor/bootstrap/bootstrap.min.js',
		'/vendor/bootstrap-select/bootstrap-select.min.js',
		'/vendor/slick/slick.min.js',
		'/vendor/jquery.inputmask.bundle.min.js',
		'/vendor/jquery.inputmask-multi.min.js',
		'/js/all.min.js',
		'/vendor/datetime-picker/jquery.datetimepicker.full.js',
	);

	function __construct()
	{
		$this->before();
	}

	public function getProtectedFilesPath()
	{
		return WORK_DIR . '/_protected_files';
	}


	public static function generateMenuItem($root_id, $rubrics) {

		if (!isset($rubrics[$root_id]) || !$rubrics[$root_id]) {
			return false;
		}

		return array_map(function($rubric) use($rubrics) {
			$classses = array();
			$link = $rubric->link ? $rubric->link : $rubric->entity_url;
			$childrens = isset($rubrics[$rubric->id])    
				? BaseController::generateMenuItem($rubric->id, $rubrics)
				: array();

			$has_active_children = false;
			foreach ($childrens as $children) {
				if (strstr($children['class'], 'active')) {
					$has_active_children = true;
				}
			}

			if (strstr($_SERVER['REQUEST_URI'], $link) || $has_active_children) {
				$classses[] = 'active';
			}

			return array(
				'link'  => $link,
				'url'   => $rubric->entity_url,
				'title' => $rubric->title,
				'class' => implode(' ', $classses),
				'childrens' => $childrens,
			);
		}, $rubrics[$root_id]);
	}

	public function getBreadcrumbs($breadcrumbs = array())
	{
		array_unshift($breadcrumbs, array(
			'title' => __('default.names'),
			'link'  => '/',
		));
		return $breadcrumbs;
	}

	public function render($template, $vars = array(), $base_template = 'default/layout')
	{
		include_file('helper');
		$content = include_file($template, $vars);

		$menu = array();

		$root_id = 0;

		// $menu = Rubric::findAll();
		// $menu = $menu[$root_id];
		$addons = isset($vars['addons'])? $vars['addons']:'';
		// $footer_menu = Rubric::findBy(['in_footer' => true]);
		// $footer_menu = $footer_menu[$root_id];

		$vars = array(
			'template'        => $template,
			'content'         => $content,
			'title'           => $this->title,
			'meta'            => $this->meta,
			'scripts'         => $this->scripts,
			'styles'          => $this->styles,
			// 'menu'            => $menu,
			// 'footer_menu'     => $footer_menu,
			'addons'		  => $addons,
			'request_form'    => $this->request_form,
			'render_request_form'    => $this->render_request_form,
		);

		return include_file($base_template, $vars);
	}

	/**
	 * Функция проверяющая авторизацию.
	 * Не пускает неавторизованных пользователей на страницы для авторизованных пользователей.
	 */
	protected function needAuth()
	{
		if (! User::isAuthorized()) {

			$redirect_uri = '/auth/login';
			header('Location: '.$redirect_uri);
			exit;
		}
	}

	/**
	 * Метод выполняется перед всеми остальными методами
	 * @return [type] [description]
	 */
	public function before()
	{
	}

	public function furlAction($furl)
	{
		if (!isset($furl) || !$furl) {
			var_dump('no furl');
			return abort('404');
		}

		// Тут безопасность
		$furl   = addslashes($furl);
		$model  = $this->model;
		$entity = $model::findOneBy(array('furl' => $furl));

		if (! $entity) {
			var_dump('no entity');
			return abort('404');
		}

		return $this->showAction($entity->id, true);
	}

	public function isAjax()
	{
		$is_ajax = !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';

		return $is_ajax;
	}
}