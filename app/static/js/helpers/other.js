/*
	проверка на неопределён или нулл
*/
function is_undefined(value) {
	return (value === void 0) || (value === null);
}

function getCallback(callback) {
	if (typeof callback != 'function') return function () {};
	return callback;
}
var urlEvent = new Event('urlChanged');
/*
	Великолепный преобразователь урла, вроде не очень работает
*/

function ChangeUrl(page, url) {
	var obj = {
		Page: page,
		Url: url
	};
	if (typeof history.pushState === "undefined")
		window.location.href = "homePage";

	history.pushState(obj, obj.Page, obj.Url);
	window.dispatchEvent(urlEvent);

}
function latticeFilter (argument) {
	return argument.replace('#', '');
}

function getLastUrl() {
	return location.pathname.match(/([^\/]*)\/*$/)[1];
}

// Авто подгонка высоты блоков
function AutoHeightPure(blocks) {
	var el = blocks.length;
	if (el) {
		var maxHeight = 0;
		for (var i = el - 1; i >= 0; i--) {
			if (blocks[i].offsetHeight > maxHeight) {
				maxHeight = blocks[i].offsetHeight;
			}
		}
		for (var j = el - 1; j >= 0; j--) {
			blocks[j].style.hejght = '';
			blocks[j].style.height = maxHeight + 'px';
		}
	}
}


function getMobileOperatingSystem(callback) {
	var userAgent = navigator.userAgent || navigator.vendor || window.opera;
	callback(userAgent);
	// Windows Phone must come first because its UA also contains "Android"
}
/*
	Получение формдаты и выкидывание в виде массива
*/
function getFormData($form) {
	var unindexed_array = $form.serializeArray();
	var indexed_array = {};

	$.map(unindexed_array, function(n, i) {
		indexed_array[n['name']] = n['value'];
	});

	return indexed_array;
}

/*
	Ужасающая магия, просьба не трогать
*/
function declOfNum(number, titles) {
	cases = [2, 0, 1, 1, 1, 2];
	return titles[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]];
}
/*
	Создаёт объект из сериализованного массива, нужно для строк
*/
function createObject_on_Serialize($array) {
	var newObj = {};

	$array.forEach(function(element) {
		newObj[element.name] = element.value;
	});

	return newObj;
}
/*
	разбивка строки урла, нужно будет убрать тк добавил библиотеку
	URI JS
*/
function getUrlParams(url) {
	var params = {};
	(url + '?').split('?')[1].split('&').forEach(function(pair) {
		pair = (pair + '=').split('=').map(decodeURIComponent);
		if (pair[0].length) {
			params[pair[0]] = pair[1];
		}
	});
	return params;
}
/*
Проверка объекта на пустоту
*/
function isEmpty(object) {
	return JSON.stringify(object) == "{}";
}
function dateTimePickerInit(){
	$.datetimepicker.setLocale('ru');
	var inline = false;
	var $datetimepicker = $('#datetimepicker');
	if (widthScreen < 500) {
		inline = !inline;
		$datetimepicker.closest(".col-xs-12").css({
			'padding': '0px'
		});
		$datetimepicker.closest("ul").css({
			"display": "flex",
			"flex-direction": "column"
		});
	}
	var dC = new Date();
	function getHrs(){
		
		if (dC.getHours() < 10) {
			return '0' + dC.getHours();
		}else {
			return ''+ dC.getHours();
		}
	}
	$datetimepicker.datetimepicker({
		i18n: {
			ru: {
				months: [
					'Январь', 'Февраль', 'Март', 'Апрель',
					'Май', 'Июнь', 'Июль', 'Август',
					'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь',
				],
				dayOfWeek: [
					"Вс", "Пн", "Вт", "Ср",
					"Чт", "Пт", "Сб",
				]
			}
		},
		mask:true,
		inline: inline,
		minDate: 0,
		maxDate: '+1970/01/02',
		minTime: getHrs()+':'+ dC.getMinutes() ,
		timepicker: true,
		format: 'Y-m-d H:i:s'
	});
	window.setTimeout(function() {
		var setting = {
			"width": "calc(100% - 8px)"
		};
		$('.xdsoft_inline .xdsoft_datepicker').css(setting);
		$('.xdsoft_inline .xdsoft_timepicker').css(setting);
	}, 100);
}

function notifyFilterCanMake (notifications, how) {
	how = how?how:'can_make_it';
	return notifications.filter(function(notification){

		if(notification.type ===  how){
			if(notification.read === false){
				return notification;
			}
		}
	});

}

function getUrlDomainAndProtocol() {
	var $uri = new URI();
	return $uri.protocol() + '://' + $uri.domain();
}