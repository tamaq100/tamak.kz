function redactHandlers() {
	addressCRUD();
	$('.js-wrap-slide[data-wrap="redact"]')
		.unbind('submit')
		.on('submit', function(e) {

			e.preventDefault();
			var dUsr = User.getMe();

			$(this)
				.serializeArray()
				.forEach(function(value) {
					if (hasChangesField(value,'name', dUsr)) { sendName(value.value); }
					if (hasChangesField(value,'email', dUsr)) { sendEmail(value.value); }
					if (hasChangesField(value,'telephone', dUsr)) { sendTelephone(); }
				});
		});

	function sendTelephone() {
		// replace(/\+|\-/g,'')
	}
}
// function deleteAdress(id){
// 	var id = id;
// 	function isNotThisID(value) {
// 	  return value.id !== id;
// 	}
// 	var filtered = User.getMyAdresses().filter(isNotThisID);
// 	return filtered;
// }

function addressCRUD(){
	// async put
	window.setTimeout(function() {
		$("body").append(yandexMapScript("mapM"));
	}, 100);
	$('.settings-table__delete-adress').on('click', function(e){
		var $this = $(this);
		// console.log($this.data("addressid"));
		//var passedAdresses = deleteAdress($this.data("addressid"));
		Requests.asyncDel('user_address/'+$this.data("addressid")).done(function(data){
			// console.log(data);
			var tableRow = $this.closest('tr');
			
			if (tableRow.attr('id'))
				tableRow.find("input").val('');
			else
				tableRow.remove();
		});
		// deleteAdress()
	});
	$('.settings-table__added').on('click', function(e){
		$('#find-me-modal').modal('show');
	});
	$('.control-okay.control-button').unbind('click').click(function(e){
		e.preventDefault();
		// myPlacemark.geometry.getCoordinates();
		var baseTown = settingsAdress.getLocalities()[0];
		var baseCountry = settingsAdress.getCountry();

		var newTown = AllCities.find(function(element) {
			if (element.value_ru === baseTown)
				return element;
		});
		var country = AllCountries.find(function(element) {
			if (element.value_ru === baseCountry)
				return element;
		});
		var townKey = typeof newTown.key != 'undefined' ? newTown.key : "";
		var countryKey = typeof country.key != 'undefined' ? country.key : "";

		User.newAdress({
			"town": townKey,
			"country": countryKey,
			"lat": myPlacemark.geometry.getCoordinates()[0],
			"lng": myPlacemark.geometry.getCoordinates()[1],
			"street": getStreet(settingsAdress),
			"house": getHouse(settingsAdress)
		}, false).done(function(data){
			User.refreshAddresses(function(){
				window.location.reload(false);
			});
		});
	});
}


function hasChangesField(value, field, sample){
	return (value[field] === 'name') && !(value.value === sample[field]);
}
function kredactHandler() {

	$('.kredact')
		.unbind('submit')
		.on('submit', function(e) {

			e.preventDefault();
			var dUsr = User.getMe();
			$(this)
				.serializeArray()
				.forEach(function(value) {
					if (hasChangesField(value,'name', dUsr)) { sendName(value.value); }
					if (hasChangesField(value,'telephone', dUsr)) { sendTelephone(); }
				});
		});

	function sendTelephone() {
		// replace(/\+|\-/g,'')
	}
}

function sendEmail(newEmail) {

	Requests.putMeEmail(newEmail, function(call) {

		if (call === 0) {} else {
			User.upload(1);

			$('.js-wrap-slide[data-wrap="redact"] .okay_message')
				.removeClass('invisible');

			window.setTimeout(function() {

				$('.js-wrap-slide[data-wrap="redact"] .okay_message')
					.addClass('invisible');

			}, 2990);
		}
	});
}

function sendName(newName) {

	Requests.putMeName(newName, function(call) {

		if (!(call === 0)) {

			User.upload(1);
			$('.kredact .okay_message')
				.removeClass('invisible');

			window.setTimeout(function() {
				$('.kredact .okay_message')
					.addClass('invisible');
			}, 2990);

		}

	});
}

function courierSettingsBind() {
	var userData = User.getMe();
	$('.settings-table__input[name="minReward"]')
		.val(userData.minReward);
	$('.settings-table__input[name="workingRadius"]')
		.val(userData.workingRadius);
	$('.restoraunt-filter__checkbox input[value="' + userData.transport.key + '"]')
		.prop('checked', true);

	$('.restoraunt-filter__checkbox input[value="' + userData.workingMode + '"]')
		.prop('checked', true);

	$('.modal-body__settings')
		.unbind('submit')
		.on('submit', function(e) {
			e.preventDefault();

			$(this).find('.button-standard.button-standard--orange')
				.text('Отправляется')
				.prop("disabled", true);

			var serializedData = createObject_on_Serialize($(this).serializeArray());
			var $self = $(this);
			serializedData.transport = {
				"key": serializedData.transport
			};
			Requests.putCourierSettings(serializedData, function(data, status) {
				if (status) {
					$self.find('.button-standard.button-standard--orange')
						.text('Отправить')
						.prop("disabled", false);
					return 0;
				}
				$self.find('.errn').removeClass('hidden');
				window.setTimeout(function() {

					$self.find('.button-standard.button-standard--orange')
						.text('Отправить')
						.prop("disabled", false);

					window.setTimeout(function() {
						$self.find('.errn').addClass('hidden');
					}, 2000 * 3);
				}, 700);
			});

		});
}