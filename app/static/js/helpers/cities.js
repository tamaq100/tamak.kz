var AllCities;
var AllCountries;

function getAllCities() {
	// console.log('Get allCities start')
	var servername = Requests.getServerName();
	$.get(servername + '/api/dictionary/cities')
		.done(function(data) {
			// console.log('Get all cities',data);
			AllCities = data;
			localStorage
				.setItem(
					'cities',
					JSON.stringify(AllCities)
				);
		});
	$.get(servername + '/api/dictionary/countries')
		.done(function(data) {
			AllCountries = [];
			data.forEach(function(element) {
				AllCountries.push({
					"type": "countries",
					"key": element.key,
					"value_ru": element.value_ru
				});
			});
			localStorage
				.setItem('countries', JSON.stringify(AllCountries));
		});
}


function upAllCities() {
	console.log("upAllCities start");
	var localDB = LDB();

	AllCities = localDB.get('cities');
	AllCountries = localDB.get('countries');
	var cities_expired = localDB.get('cities_expired');
	if (cities_expired ||
		(JSON.parse(localStorage.getItem('cities_expired')).timeStamp < new Date().getTime())
	) {

		localStorage.setItem(
			'cities_expired',
			JSON.stringify({
				"timeStamp": new Date().getTime() + (24 * 60 * 60 * 1000)
			})
		);

		getAllCities();
	} else if (!AllCities && !AllCities.length) {
		getAllCities();
	}
}