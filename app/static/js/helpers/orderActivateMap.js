var ordersMapInfo = {};
/*
 * save info from order to map info
*/
function orderActivateMap(orderID, orderAdress){
	if (!orderID) {
		return void 0;
	}
	ordersMapInfo[orderID] = {
		"initialized":false,
		"data": orderAdress
	};
	if (window.hasOwnProperty('ymaps')) {
		prepareOrderMap();
	}else{
		$("body").append(yandexMapScript('prepareOrderMap'));
	}
}

/*
 * middleware
*/
function prepareOrderMap(){

	for(orderID in ordersMapInfo){
		if (!ordersMapInfo[orderID].initialized) {
			initOrderMap(orderID);
		}
	}

}
function initOrderMap(orderID) {
	var tempMap = new ymaps.Map('ordermap'+orderID, {
		center: [ordersMapInfo[orderID].data.latitude, ordersMapInfo[orderID].data.longitude],
		zoom: 15,
		behaviors: ['default', 'scrollZoom']
	});

	var myPlacemark = new ymaps.Placemark([ordersMapInfo[orderID].data.latitude, ordersMapInfo[orderID].data.longitude], {
		balloonContent: 'Точка назначения',
	});

	tempMap.geoObjects.add(myPlacemark);

	tempMap.setCenter([ordersMapInfo[orderID].data.latitude, ordersMapInfo[orderID].data.longitude], 15, {
		checkZoomRange: true
	});
	$('#ordermap'+orderID).css('paddingTop', '25px');
	$('#ordermap'+orderID).find('.order-geo__map').remove();
}