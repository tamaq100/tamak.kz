// Pattern Observer
// Router
Router = function() {
    console.log('Router init');
    this._observers = [];
};
Router.prototype = {
    event: function(data) {
        // console.log('Router event');
        var routeGET = getUrlParams(data) || '';
        data = data.split('?')[0];
        for (var i in this._observers) {
            var item = this._observers[i];
            // При проборе эвента, если у нас текущий Url не совпадает
            // с тем, что мы объявили в роуте то тогда проходим дальше
            if (item.url != data) continue;
            // Иначе вызываем наш роут
            if(item.fired === false) {
                console.log('Route fired', item.url);
                item.fired = true;
                window.setTimeout(item.observer.call(item.url, data, routeGET), 10);
            }
        }
    },
    route: function(url, observer) {

        // console.log('Router register route :', {
        //     'url': url,
        //     'observer': typeof observer
        // });

        var ctx = url || null;
        this._observers.push({
            observer: observer,
            url: ctx,
            fired: false
        });

        this.event(window.location.href.toString().split(window.location.host)[1]);
    },
    getAllRoutes: function() {
        return this._observers;
    },
    issetRoute: function(route) {
        // var routeGET = getUrlParams(route);
        route = route.split('?')[0];
        for (var i in this._observers) {
            if (this._observers[i].url === route) return true;
        }
        return false;
    }
};
var Router = new Router();