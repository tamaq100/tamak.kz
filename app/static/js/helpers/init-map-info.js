function initMapInfo() {
	var $map = $('#map');
	var myMapResto = new ymaps.Map('map', {
		center: [CookieLat, CookieLng],
		zoom: 1,
		behaviors: ['default', 'scrollZoom']
	}, {
		searchControlProvider: 'yandex#search'
	});
	ADRESSESRESTO.forEach(function(adress){
		var img = restor.photos[0].path;
		if (typeof img === "undefined") {
			img = false;
		} else {
			img = "https://admin.tamaq.kz" + img;
		}

		var content = (img ? "<img src=\""+ img + "\"" : "");
		content += content;
		var myPlacemark = new ymaps.Placemark([adress.latitude, adress.longitude], {
			balloonContent: content
		});
		myMapResto.geoObjects.add(myPlacemark);
	});
	
	myMapResto.setCenter([ADRESSESRESTO[0].latitude, ADRESSESRESTO[0].longitude], 17, {
		checkZoomRange: true
	});
	console.log(myMapResto);
}