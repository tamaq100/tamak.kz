function LDB() {
     function normalizer(field) {
          return field = typeof field === 'string' ? field : '';
     }
     /**
      * @param  {number} hour=2 times in hours
      * @returns {number} expireTime in ms
      */
     function generateExpireTime(hour) {
          if(hour < 0 || typeof hour != 'number') {
               hour = 0.3;
          }

          return (new Date().getTime() + (1000 * 60 * 60 * hour));
     }
     return {
          get(field, defaultObject) {
               var returned = localStorage.getItem(normalizer(field));
               defaultObject = JSON.stringify(defaultObject ? defaultObject : {} );
               return JSON.parse(returned ? returned : defaultObject);
          },
          removeItem(key) {
               if (typeof key != 'string') return "";
               localStorage.removeItem(key);
          },
          removeItems(list) {
               if (!Array.isArray(list)) list = [];
               var $this = this;
               list.forEach(function(el){
                    $this.removeItem(el);
               });
          },
          set(field, value) {
               localStorage.setItem(
                    field,
                    JSON.stringify(value ? value : '')
               );
          },
          setTemporal(field, value, expireTime) {
               var exp = generateExpireTime(expireTime);
               var data = {
                    'expiredAt': exp,
                    'data': value ? value : ''
               }
               this.set(field, data);

               return data;
          },
          getTemporal(field) {
               var defaultObject = {
                    'expiredAt': -1,
                    'data': []
               };

               return this.get(field, defaultObject);
          },
          /**
           * @param  {number} time in ms
           * @returns {boolean} expired 
           */


          
          /**
           *
           * @param {*} time
           * @returns
           */
          isExpired(time) {
               if(time < 0 || typeof time != 'number') {
                    time = -1;
               }

               return (time - new Date().getTime()) > 0 ? false : true;
          }
     };
}