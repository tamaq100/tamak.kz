var orderStateTable = {
	// Назначен курьер
	"ExecutorSelected": {
		"text": "Еду в ресторан",
		"switch_to": "GoingToProvider"
	},
	// Курьер в пути к ресторану
	"GoingToProvider": {
		"text": "Прибыл в ресторан",
		"switch_to": "ArrivedToProvider"
	},
	// Курьер прибыл в ресторан
	"ArrivedToProvider": {
		"text": "Еду к заказчику",
		"switch_to": "GoingToCustomer"
	},
	// "Курьер на пути к заказчику": 
	"GoingToCustomer": {
		"text": "Отправляюсь к заказчику",
		"switch_to": "ArrivedToCustomer"
	},
	"ArrivedToCustomer": {
		"text": "Заказ Завершен",
		"switch_to": "Done"
	},
	"CancelExecutor": "Отклонён Курьёром",
};

function notificationsMakeExec(notifications) {
	// Ну не ну я хз как это нормальным образом сделать
	showExecRequest(notifications[0]);
	readAllMessages();
}

function executorOrderGoTo(action, orderId) {
	User.upload(true);
	Requests.asyncPut('orders/move', {
		"id": orderId,
		"to_status": action,
		"msg": action
	}).done(function(){
		closeScaffoldOrders();
		readAllMessages();
	});
}

function getStatusInOrder(notification) {
	if (notification.order) {
		notification.id = notification.order.id;
		notification.status = notification.order.status;
		notification.to_time = notification.order.to_time;
		notification.address = notification.order.address;
	}
	return {
		"title": orderStateTable[notification.status].text,
		"action": orderStateTable[notification.status].switch_to,
		"order": {
			"id": notification.id,
			"service": notification.service,
			"client": {
				"customer": notification.customer,
				"address": notification.address
			}
		}
	};
}

function notificationsYourSelectedFunc(notification) {
	// notification
	if (scaffoldShow) return;
	// Организовать таблицу переходов
	var actions = getStatusInOrder(notification);
	showScaffoldOrders(actions);
	// readAllMessages();
}
var scaffoldShow = false;

function showScaffoldOrders(data) {
	// console.log("!!!!!!!!!!!!!!@#!@#!@##@!data");
	// console.log(data);
	var $scaffold = $('#courier-order-billet');
	$scaffold.find('.order-billet__container').append(
		RenderTabs.render(
			$scaffold.find('script[type="text/x-handlebars-template"]').html(),
			data
		)
	);
	scaffoldShow = true;
	$scaffold.addClass('show');
	// readAllMessages();
}

function closeScaffoldOrders() {
	var $scaffold = $('#courier-order-billet');
	$scaffold.removeClass('show');
	$scaffold.find('.order-billet__container').empty();
	window.setTimeout(function(){
		scaffoldShow = false;
	}, 120);
}

function readAllMessages() {
	Requests.asyncPut('notifications/read', {
		"time": (new Date()).format('yyyy-mm-dd HH:MM:ss')
	}).done(function() {});
}
var modalRequstShow = true;

function showExecRequest(notification) {
	// console.log(notification);
	if (!modalRequstShow) return;
	modalRequstShow = false;
	var $modal = $('#ExecRequest');
	$modal.find('.modal-body').empty();

	Requests.asyncGeting('orders/' + notification.order.id).done(function(order) {
		// console.log(order);
		$modal.find('.modal-body').append(
			RenderTabs.render($modal.find('script').html(), order[0])
		);
		$modal.modal('show');
	});
}

function ordersOffer(id) {
	Requests.asyncPost('orders/offer/' + id, {}).done(function() {
		$('#ExecRequest').modal('hide');
		$('#ExecRequest').find('.modal-body').empty();
		modalRequstShow = true;
	});
}

function orderDecline() {
	$('#ExecRequest').modal('hide');
	$('#ExecRequest').find('.modal-body').empty();
	modalRequstShow = true;
}