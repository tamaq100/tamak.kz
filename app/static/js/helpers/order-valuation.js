function orderValuation(orderId) {
	var meRole = User.getMe().role;
	if (meRole === "User") {
		userValuationToExecutor(orderId);
	} else {
		courierValuationToExecutor(orderId);
	}
}
function restValuation(orderId){
	Requests.getOrder(orderId, function(order) {
		// console.log(order);
		$('#rateCourierModal .modal-body').empty().append(
			RenderTabs.render($('#rateRestoModal script').html(), order[0])
		);
		boolValuation();
		ratesJs();
		$('#rateCourierModal').modal('show');
	});
}

// Default fields
var formRate = {
	"valuation": true,
	"msg_valuation": "",
	"claims":[]
};
function boolValuation(){
	$('.modal-courier__bigbtn').click(function(event) {
		$('.modal-courier__bigbtn').removeClass('active');
		$(this).addClass('active');
	});
}

function ratesJs() {
	$('.js-rate').find('.courier-review__stars .rate-star').click(function(e) {
		var percent = $(this).attr('data-percent');
		var $input = $(this).closest('.js-rate').find('input');
		$input.val(percent);

		var value = $input.val();
		$(this).closest('.js-rate').find('.front-rate').css('width', value + '%');
	});
}

function rateToCourier(orderId){
	var $modal = $('#rateCourierModal');
	var $thisForm = formRate;
	var FIVE_PERCENT = 0.05;

	$thisForm['msg_valuation'] = $modal.find('.basket__area-text').val();
	$thisForm['state_rate'] = FIVE_PERCENT * $modal.find('input[name=state_rate]').val();
	$thisForm['civility_rate'] = FIVE_PERCENT * $modal.find('input[name=civility_rate]').val();
	$thisForm['speed_rate'] = FIVE_PERCENT * $modal.find('input[name=speed_rate]').val();
	$thisForm['valuation'] = !$modal.find('.modal-courier__bigbtn.active').find('input').val();
  	$thisForm["typerate"] = {"key": "rate_executor"};

	// console.log($thisForm);
	Requests.asyncPost('order/rates/'+orderId,$thisForm).done(function(){
		$("rateOrderExec").text('Вы уже оставили отзыв');
		rateExecSucc = true;
		$modal.modal('hide');
		document.location.reload(true);
	});
}
function rateToRestourant(orderId){
	var $modal = $('#rateRestoModal');
	var $thisForm = formRate;

	$thisForm['msg_valuation'] = $modal.find('.basket__area-text').val();
	$thisForm['valuation'] = !$modal.find('.modal-courier__bigbtn.active').find('input').val();
  	$thisForm["typerate"] = {"key": "rate_restaurant"};

	Requests.asyncPost('order/rates/'+orderId,$thisForm).done(function(){
		rateExecSucc = true;
		$('#rateRestoModal').modal('hide');
		document.location.reload(true);
	});
}

function userValuationToExecutor(orderId) {
	Requests.getOrder(orderId, function(order) {
		// console.log(order);
		$('#rateCourierModal .modal-body').empty().append(
			RenderTabs.render($('#rateCourierModal script').html(), order[0])
		);
		boolValuation();
		ratesJs();
		$('#rateCourierModal').modal('show');
	});
	//POST https://tamaq.kz/order/rates/040b7a16-cf16-412c-8dcb-1560d2065889
	/* BODY
	{
		"msg_valuation": "Great executor!!",
		"valuation": true,
		"typerate": {
			"key": "rate_executor"
		},
		"speed_rate": 5,
		"civility_rate": 5,
		"state_rate": 5,
		"claims": []
	}
	*/
	// $()
}

function courierValuationToExecutor(orderId) {

}