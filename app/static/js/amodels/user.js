// Разработка Timenty
var User = (function() {
	// 0 никто
	// 1 User
	// 2 Courier
	// 3 Admin
	// 4 RestourantManager - Если это он то редирект на внутреннее приложение
	// https://admin.tamaq.kz/ru/main/orders/new
	var LocalDatabase = LDB();
	var AuthStatus = 0;
	var Me = {};
	var AuthData = {};
	var Adresses = [];
	var favoriteadr = {};
	var currenturl = window.location.href.toString().split(window.location.host)[1];
	// Получить информацию обо мне, использовать только когда мы авторизовали юзера
	// Записывает данные о юзере в переменную
	function assemblyNormalData(callback) {
		console.log('assemblyNormalData Start')
		AuthData.uid = Me.id;
		AuthData.role = Me.role;
		AuthData.name = (typeof Me.name !== 'undefined') ? Me.name : '';
		callback();
	}

	function changeStatus() {
		console.log('User changeStatus', Me.role.toLowerCase());
		if (typeof Me.role === 'undefined') return;
		switch (Me.role.toLowerCase()) {
			case 'user':
				AuthStatus = 1;
				LocalDatabase.setTemporal('AuthStatus', 1, 1);
				break;
			case 'executor':
				AuthStatus = 2;
				window.setTimeout(function() {
					User.startMakeExecNotify();
				}, 1 * 1000);
				LocalDatabase.setTemporal('AuthStatus', 2, 1);
				break;
			case 'admin':
				LocalDatabase.setTemporal('AuthStatus', 3, 1);
				AuthStatus = 3;
				break;
			default:
				AuthStatus = 0;
				break;
		}
		// LocalDatabase.changeStatus();
	}
	return { // public interface
		login: function(data, formcallback) {
			data.user_provider_id = data.user_provider_id.replace(/[()-\s]/g, '');
			Requests.login(data, function(response){
				switch (response) {
					case 200:
						Requests.me(function(data) {
							Me = data;
							assemblyNormalData(function() {
								changeStatus();
								Requests.acceptLogin({
										uid: AuthData.uid,
										r: AuthData.role
									},
									function() {
										AuthButtons.refresh();
										$(authForms.login)
											.closest('.modal')
											.modal('hide');
										// console.log('=== accept login ===');
										if (currenturl === '/basket')
											document.location.reload(false);
									}
								);
							});
						});
						formcallback(1);
						break;
					default:
						formcallback(0);
						break;
				}
			});
		},
		getMe: function() {
			return Me;
		},
		getMyAdresses: function() {
			return Adresses;
		},
		getFavoriteAddress: function() {
			return favoriteadr;
		},
		getMeUid: function() {
			return Me.id;
		},
		load: function() {},
		register: function(data, callback) {
			data.user_provider_id = data.user_provider_id.replace(/[()-\s]/g, '');
			Requests.register(data, callback);
		},
		exit: function() {
			LocalDatabase.removeItem('AuthStatus');
			Requests.logout(function() {
				localStorage.removeItem('stat');
				localStorage.removeItem('bucket');
				window.location.href = window.location.protocol + '//' + window.location.hostname;
			});
		},
		recovery: function() {},
		view: function() {},
		getStatus: function() {
			var authStatusT = LocalDatabase.getTemporal('AuthStatus');
			if(!LocalDatabase.isExpired(authStatusT.expiredAt)){
				return authStatusT.data;
			}

			return AuthStatus;
		},
		getFavoriteServices: function(callback) {
			if(Me.favorite) {
				Requests.getFavoriteServices(Me.favorite, function(data) {
					callback({
						restoraunts: data
					});
				});
			} else {
				callback({
					restoraunts: []
				});
			}
		},
		issetFavoriteServices: function(serviceID) {
			var reslt = false;
			(Me.favorite ? Me.favorite : []).find(function(elem) {
				if (elem.id === serviceID)
					reslt = true;
			});

			return reslt;
		},
		refreshAddresses: function(callback) {

			Requests.myAdresses(function(data) {
				favoriteadr = data.filter(function(object) {
					if (object.is_delete) return false;
					return object.favorite;
				})[0];

				unfavorite = data.filter(function(object) {
					if (object.is_delete) return false;
					return !object.favorite;
				});
				Adresses = ([].concat(favoriteadr, unfavorite)).filter(function(el) {
					return el != null;
				});

				localStorage.removeItem('UserAdresses');

				localStorage.setItem('UserAdresses', JSON.stringify(Adresses));
				if (!!callback)
					callback();
			});
		},
		upload: function(refresh) {
			console.log('User upload start function');
			Requests.me(
				function(data, status) {
				// Здесь сделать реджект на главную
				// var currenturl = window.location.href.toString().split(window.location.host)[1];

				if (!status) return 0;
				if (status == 2) return 0;
				if (data == 0) {
					if (!refresh) Router.event(currenturl);
					return 0;
				}
				Me = data;
				changeStatus();
				AuthButtons.refresh();
				User.refreshAddresses(function() {

					if (!is_undefined(User.getFavoriteAddress())) {
						if (Array.isArray(User.getFavoriteAddress()))
							$('.firstAddress')
								.text(User.getFavoriteAddress()[0].street)
								.removeClass('invisible');
						else
							$('.firstAddress')
								.text(User.getFavoriteAddress().street)
								.removeClass('invisible');
					} else {
						if (!is_undefined(User.getMyAdresses()[0]))
							$('.firstAddress')
								.text(User.getMyAdresses()[0].street)
								.removeClass('invisible');
						else
							return 0;
					}
					if ($('.account-dropdown').length && (User.getMyAdresses().length > 1)) {
						var Adresses = User.getMyAdresses();

						$('.account-dropdown .dropdown-toggle .nnn')
							.text(Adresses.length - 1);

						var result = '';
						for (var i = 1; i < Adresses.length; i++) {
							var adress = Adresses[i];
							result += '<li><a class="adress" href="' + adress.id + '">';
							result += adress.street + ' ,' + adress.town.key;
							result += '</a></li>';
						}
						$('.account-dropdown .drop-menu.menu-2')
							.append(result);
						$('.account-dropdown')
							.removeClass('invisible');
					}
				});
				if (Me.hasOwnProperty("executing_order"))
					Requests.asyncGeting('orders/'+Me.executing_order.id)
						.done(function(data){
						// TODO
							notificationsYourSelectedFunc(data[0]);
							// notificationsMakeExec(data);
						});
				// refresh
				console.log('User upload refresh', refresh);
				if (refresh) return 0;
				console.log('RenderTabs execute');
				RenderTabs.templRender();
				console.log('Router fire event in USER');
				Router.event(currenturl);

				if (!Router.issetRoute(currenturl))
					$.get(currenturl + '?full-aj', function(data) {
						$('.js-wrap-container')
							.append(RenderTabs.render(data.html));

						ChangeUrl('Личный Кабинет', currenturl);
						RemovePreload();
					}, "json");

				if ($('.courier__btn-options').length)
					courierSettingsBind();

				Requests.acceptLogin({
					uid: Me.id,
					r: Me.role
				}, function() {
					// if ((Me !== 0) && !Me.phone_approved)
						// phone_approve();
				});
			});
		},
		newAdress: function(addrData, maybe_notSave) {
			var fieldsReq = {
				"town": {
					"key": addrData.town
				},
				"country": {
					"key": addrData.country
				},
				"street": addrData.street,
				"latitude": addrData.lat,
				"longitude": addrData.lng
			};
			// validate
			var validateRes = true;

			for (var variable in fieldsReq) {
				if (variable.hasOwnProperty('key')) {
					if (!!!variable.key)
						validateRes = false;
				} else {
					if (!!!variable)
						validateRes = false;
				}
			}

			if (!validateRes)
				return false;

			var otherFields = {
				"flat": addrData.hasOwnProperty('flat') ? addrData.flat : '',
				"floor": addrData.hasOwnProperty('floor') ? addrData.floor : '',
				"favorite": false,
				"house": addrData.hasOwnProperty('house') ? addrData.house : '',
			};
			var result = Object.assign(fieldsReq, otherFields);
			if (maybe_notSave)
				return result;

			return Requests.asyncPost(
				'user_address',
				JSON.parse(JSON.stringify(result))
			);
		},
		startMakeExecNotify: function() {
				Requests.asyncGeting('notifications').done(function(data) {

					var notifications = notifyFilterCanMake(data);
					var notificationsYourSelected = notifyFilterCanMake(data, "you_selected_executor");
					if (notificationsYourSelected.length)
						notificationsYourSelectedFunc(notificationsYourSelected[0]);

					if (notifications.length)
						notificationsMakeExec(notifications);
				});
			window.setTimeout(function() {
				User.startMakeExecNotify();
			}, 12 * 1000);
		}
	};
})();
User.upload();

function upld() {
	window.setTimeout(function() {
		User.upload(1);
		window.setTimeout(function() {
			upld();
		}, 4500);
	}, 200000);
}
upld();