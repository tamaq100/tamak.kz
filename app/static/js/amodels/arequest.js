// Разработка Timenty
// email: sporretimur@gmail.com
// workEmail: y.timur@4dclick.kz
// https://vk.com/timenty
// Перед вами модуль авторизации через апи, задача которого
// авторизовать пользователя как в API так и на нашем сервере
// для лучшей читаемости и расширяемости ( не уверен насколько было оправдано )
// здесь применен паттерн: синглтон;
// У объекта есть приватные переменные и функции и публичные
// 30.11.2018 Есть два модуля
// Requests = Это модуль запросов
// User = в нём хранится состояние юзера и совершаются запросы
var Requests = (function() {
	// const API = 'https://tamaq-auth.4dclick.com';
	// Нексколько констант передающих адрес
	var API = '/api';
	var ServerName = window.location.protocol + '//' + document.domain;
	// приватные функции
	// Функция генерирования URL
	function generateURL(domain, path) {
		return domain + '/' + path;
	}
	// Обработчик входящих данных преобразующий в JSON
	function handlerToJSON(data) {
		var formdata = {};
		$(data).each(function(index, obj) {
			formdata[obj.name] = obj.value;
		});
		return formdata;
	}
	// Публичные функции
	return {
		// GET запрос
		getServerName: function() {
			return ServerName;
		},
		getProductList: function(params, callback) {
			var objParam = {
				"from": params.from,
				"count": params.count,
				"ands": {}
			};
			objParam.ands = {
				"overlap": []
			};
			objParam.ands.overlap.push({
				"field": "tags",
				"type": "ArrayString",
				"value": params.tag
			});
			objParam.ands.eq = [];
			objParam.ands.eq.push({
				"field": "service.id",
				"type": "UUID",
				"value": params.id
			});
			$.ajax({
				url: generateURL(API, 'services/products/list'),
				data: JSON.stringify(objParam),
				type: "POST",
				contentType: 'application/json',
				success: function(json) {
					callback(json);
				},
				error: function(err) {
					console.log('Error sendGET');
				}
			});
		},
		asyncDel(url, where) {
			return $.ajax({
				url: generateURL(API, url),
				type: "DELETE",
				contentType: 'application/json',
				data: JSON.stringify(where)
			});
		},
		asyncPost(url, where) {
			return $.ajax({
				url: generateURL(API, url),
				type: "POST",
				contentType: 'application/json',
				data: JSON.stringify(where)
			});
		},
		asyncGeting(url, where) {
			return $.ajax({
				url: generateURL(API, url),
				type: "GET",
				contentType: 'application/json',
				data: JSON.stringify(where)
			});
		},
		asyncPut(url, where) {
			return $.ajax({
				url: generateURL(API, url),
				type: "PUT",
				contentType: 'application/json',
				data: JSON.stringify(where)
			});
		},
		sendGET: function(where, callback) {
			$.ajax({
				url: generateURL(API, where),
				type: "GET",
				crossDomain: true,
				success: function(json) {
					callback(json);
				},
				error: function(err) {
					console.log('Error sendGET');
				}
			});
		},
		me: function(callback) {
			$.get(generateURL(API, 'me'), function(data, status) {
				if (status === 'success') {
					callback(data, 1);
					// console.clear();
				} else {
					callback(data, 2);
					// console.clear();
				}
				data = null;
				delete data;
			}).fail(function() {
				callback(0, 1);
				// console.clear();
			});
		},
		myAdresses: function(callback) {
			$.get(generateURL(API, 'user_address'), function(data, status) {
				if (status === 'success') {
					callback(data, 1);
				} else {
					callback(data, 2);
				}
				data = null;
				delete data;
			}).fail(function() {
				callback(0, 1);
			});
		},
		putNewAdress: function(callback) {},
		delAdress: function(data, callback) {

		},
		searchRestoProducts: function(nameInput, callback) {
			// Доделать
			Requests.getFavoriteServices(idsService, function(restorauntData) {

				if (!idsProducts.length) {
					callback({
						"order_products": [],
						"restorauntData": restorauntData
					});

				} else {

					$.get(generateURL(API, '/products'), {
						"ids": idsProducts
					}).done(function(productsData) {

						callback({
							"order_products": productsData,
							"restorauntData": restorauntData
						})

					})
				}
			})
		},
		register: function(data, callback) {
			$.ajax({
				type: "POST",
				url: generateURL(API, 'registrate'),
				data: data,
				dataType: '',
				success: function(response) {
					callback(response);
				},
			});
		},
		login: function(data, callback) {
			$.ajax({
				url: generateURL(API, 'login'),
				data: JSON.stringify(data),
				type: "POST",
				contentType: 'application/json',
				success: function(json) {},
				statusCode: {
					400: function() {
						callback(400)
					},
					200: function() {
						callback(200)
					},
				},
				error: function() {
					console.log('loginFailed')
				},
			});
		},
		logout: function(callback) {
			$.ajax({
				url: generateURL(API, 'logout'),
				type: "PUT",
				statusCode: {
					401: function() {
						try {
							$.get(generateURL(ServerName, 'auth/logout')).done(function() {
								callback(401);
							});
						} catch(e){
							callback(401);
						}
					},
					200: function() {
						try {
							$.get(generateURL(ServerName, 'auth/logout')).done(function() {
								callback(200);
							});
						} catch(e){
							callback(200);
						}	
					}
				}
			});
		},
		acceptLogin: function(data, callback) {
			// Для авторизации на нашем бэкэнде
			if (typeof data.uid === 'undefined') return 0;
			$.get(generateURL(ServerName, 'auth/login'), {
				uid: data.uid,
				r: data.r
			}).done(function(data) {
				callback();
			}).fail(function() {
				User.exit();
			});
		},
		getHistoryOrders: function(uid, exe, callback) {
			$.ajax({
				url: generateURL(API, 'orders/list'),
				type: "POST",
				contentType: 'application/json',
				// "from": 0,
				// "count": 0,
				data: JSON.stringify({
					"ors": {
						"eq": [{
							"field": exe,
							"type": "UUID",
							"value": uid
						}]
					},
					"sort": [{
						"field": "created",
						"type": "Timestamp",
						"asc": false
					}]
				}),
			}).done(function(data) {
				callback(data);
			}).fail(function() {
				callback(0);
			});
		},
		getCReviews: function(uid, callback) {
			$.get(generateURL(API, 'users/rates/' + uid)).done(function(data) {
				callback(data);
			}).fail(function() {
				console.log('fail request in reviews');
			});
		},
		getReviews: function(uid, callback) {
			$.get(generateURL(API, 'users/left_rates/' + uid)).done(function(data) {
				callback(data);
			}).fail(function() {
				console.log('fail request in reviews');
			});
		},
		getOrder: function(id, callback) {
			$.get(generateURL(API, 'orders'), {
				ids: id
			}).done(function(data) {
				callback(data);
			}).fail(function() {
				console.log('fail request in orders');
			});
		},
		getFavoriteServices: function(ids, callback) {
			var massive = [];
			ids.forEach(function(elem, index) {
				massive[index] = elem.id;
			});

			massive = massive.join(',');

			$.get(generateURL(API, 'services'), {
				ids: massive
			}).done(function(data) {
				callback(data);
			}).fail(function() {
				console.log('fail request in getFavoriteServices');
			});
		},
		getServices: function(params, callback) {
			$.ajax({
				url: generateURL(API, 'services/list'),
				type: "POST",
				contentType: 'application/json',
				data: JSON.stringify(params)
			}).done(function(data) {
				callback(data);
			}).fail(function() {
				callback({
					"fail": 1
				});
			});
		},
		deleteFavoriteService: function(id, callback) {
			var newfavoriteServices = User.getMe().favorite.filter(function(currentValue) {
				if (!(currentValue.id === id)) return currentValue;
			});
			User.getMe().favorite = newfavoriteServices;
			$.ajax({
				url: generateURL(API, 'me'),
				type: "PUT",
				contentType: 'application/json',
				data: JSON.stringify({
					"favorite": newfavoriteServices
				})
			}).done(function(data) {
				callback(id);
			}).fail(function() {
				callback(0);
			});
		},
		addFavoriteService: function(idr, callback) {
			User.getMe().favorite.push({
				"id": idr
			});
			var newfavoriteServices = User.getMe().favorite;
			$.ajax({
				url: generateURL(API, 'me'),
				type: "PUT",
				contentType: 'application/json',
				data: JSON.stringify({
					"favorite": newfavoriteServices
				})
			}).done(function(data) {
				callback(1);
			}).fail(function() {
				callback(0);
			});
		},
		putMeName: function(name, callback) {
			$.ajax({
				url: generateURL(API, 'me'),
				type: "PUT",
				contentType: 'application/json',
				data: JSON.stringify({
					"name": name
				})
			}).done(function(data) {
				callback(data);
			}).fail(function() {
				callback(0);
			});
		},
		putCourierSettings: function($param, callback) {
			$.ajax({
				url: generateURL(API, 'me'),
				type: "PUT",
				contentType: 'application/json',
				data: JSON.stringify($param)
			}).done(function(data) {
				callback(data, 1);
			}).fail(function() {
				callback(0, 0);
			});
		},
		getDictionary: function(who, callback) {
			$.get(generateURL(API, 'dictionary/' + who)).done(function(data) {
				callback(data);
			})
		},
		putMePhone: function(name, callback) {
			$.ajax({
				url: generateURL(API, 'changelogin'),
				type: "PUT",
				contentType: 'application/json',
				data: JSON.stringify({
					"name": name
				})
			}).done(function(data) {
				callback(data);
			}).fail(function() {
				callback(0);
			});
		},
		putMeEmail: function(email, callback) {
			$.ajax({
				url: generateURL(API, 'me'),
				type: "PUT",
				contentType: 'application/json',
				data: JSON.stringify({
					"email": email
				})
			}).done(function(data) {
				callback(data);
			}).fail(function() {
				callback(0);
			});
		},
		sendApprove: function(dataParam, callback) {
			var DATA = JSON.stringify({
				"user_provider_id": User.getMe().phone,
				"provider_key": "PHONE_PASSWORD",
				"password": dataParam.password
			});
			$.ajax({
				url: generateURL(API, 'approve'),
				type: 'PUT',
				contentType: 'application/json',
				data: DATA
			}).done(function(data) {
				callback(data, true);
			}).fail(function(data) {
				callback(data, false);
			});
		},
		requestApprove: function(callback) {
			var DATA = JSON.stringify({
				"user_provider_id": User.getMe().phone,
				"provider_key": "PHONE_PASSWORD"
			});
			$.ajax({
				url: generateURL(API, 'askapprove'),
				type: 'POST',
				contentType: 'application/json',
				data: DATA
			}).done(function(data) {
				callback(data, true);
				// data = null;
			}).fail(function(data) {
				callback(data, false);
			});
		},
		sendLastLoc: function() {
			var data = JSON.stringify({
				"time": (new Date()).format('yyyy-mm-dd HH:MM:ss'),
				"latitude": localStorage.getItem('lat'),
				"longitude": localStorage.getItem('lng'),
				"os": "Web"
			});
			$.ajax({
				url: generateURL(API, 'location'),
				type: 'POST',
				contentType: 'application/json',
				data: data
			}).done(function(data) {});
		},
		/*
			для корзины
		*/
		getServiceAndProducts(callback, preBucket) {
			console.log("getServiceAndProducts", preBucket);
			var idsService = [{
				"id": preBucket.service.id
			}];
			var idsProducts = preBucket.order_products.map(function(element) {
				return typeof element.product_id === "undefined" ?  element.id : element.product_id;
			});

			Requests.getFavoriteServices(idsService, function(restorauntData) {
				console.log("SPECIAL Requests.getFavoriteServices", idsProducts.length)
				if (!idsProducts.length) {
					callback({
						"order_products": [],
						"restorauntData": restorauntData
					});
				} else {
					var kek = [];
					idsProducts.forEach(function(id) {
						kek.push($.get(generateURL(API, 'products'), {
							"ids": id 
						}));
					})
					Promise.all(kek).then(function(values) {
						console.log("PROMISE VALUES", values);
						var response = {
							order_products: values,
							restorauntData: restorauntData,
							orderProducts: values
						};
						console.log("response promise all", response);
						callback(response);
					});

					// $.idsProducts
					// $.get(generateURL(API, 'products'), {
					// 	"ids": idsProducts
					// }).done(function(productsData) {
					// 	callback({
					// 		"order_products": productsData,
					// 		"restorauntData": restorauntData
					// 	})
					// })
				}
			})
		}
	}
})();