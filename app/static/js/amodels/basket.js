/*
	Model basket
*/
var Basket = (function () {
	var bucket, orderID;
	var readyOrder = false;
	var $uri = new URI();
	var orderedStatus = false;
	var orderPage = localStorage.getItem('orderPage');
	var localDB = LDB();
	var hooks = [];
	/**
	 * @returns
	 */
	function sync(oeo) {
		if (!orderID) {
			return '';
		}
		Requests.asyncGeting('orders/' + orderID).done(
			function (data) {
				if (data.length) {
					localStorage.setItem(
						'bucket',
						JSON.stringify(data[0])
					);
					setBucket(data[0]);
					if (oeo)
						localStorage.setItem('bucket', JSON.stringify(bucket));
					else
						Basket.save();
				}
			}
		);
	}
	function setBucket(EthernalBucket) {
		console.log('setBucket EthernalBucket', EthernalBucket);
		bucket = EthernalBucket;
		fireHooks(bucket.status);
	}
	function initialize() {
		if (localStorage.getItem('orderedStatus')) {

			var objectOrder = JSON
				.parse(
					localStorage.getItem('orderedStatus')
				);
			console.log('orderedStatus = objectOrder.sta === ', objectOrder.sta);
			orderedStatus = objectOrder.sta;
			orderID = objectOrder.id;
			sync();
		}
		if (localStorage.getItem('bucket'))
			bucket = JSON.parse(localStorage.getItem('bucket'));
		else
			templateFill();
	}

	/**
	 *
	 *
	 */
	function templateFill() {
		localDB.removeItem('orderPage');
		localStorage.setItem('bucket', JSON.stringify({
			"address": {},
			"service": {
				"id": ""
			},
			"pay_bonus": 0,
			"to_time": "",
			"need_change_from": "",
			"comment_to_executor": "",
			"order_products": [],
			"description": "",
			"tamaq_executor": false,
			"is_payed_online": false
		}));
		bucket = localDB.get('bucket');
	}
	function fireHooks(event) {
		hooks.forEach(function(hook){
			if(hook.event === 'any' || hook.event === event) {
				hook.callback();
			}
		});
	}
	return {
		registerHook: function(callback, event) {
			callback = typeof callback != 'function'? function(){} : callback;
			if(typeof event != 'string'){
				event = 'any';
			}

			hooks.push({
				'event': event,
				'callback': callback
			});
		},
		/**
		 *
		 *
		 * @returns
		 */
		currentPay: function () {
			if (!localStorage.getItem('currentPay')) {
				return 'cash';
			}
			return localStorage.getItem('currentPay');
		},

		init: function () {
			initialize();
		},
		/**
		 *
		 *
		 * @param {*} params
		 * @returns
		 */
		moveOrder: function (params) {
			if (params.next)
				return Requests.asyncPut('orders/move', {
					"id": bucket.id,
					"to_status": params.to_status,
					"msg": "Need move",
				});
		},
		is_cash: function () {
			switch ($('input[name=type_pay]:checked').val()) {
				case 'cash':
					return true;
				case 'card':
					return false;
				default:
					return true;
			}
		},
		refreshStatus: function (status) {
			if (is_undefined(status)) return;
			readyOrder = status;
		},
		getStatus: function () {
			return readyOrder;
		},
		getFull: function () {
			return bucket;
		},
		getOrderedStat: function () {
			if (localStorage.getItem('orderedStatus'))
				return {
					"id": orderID,
					"status": orderedStatus,
					"state": JSON.parse(localStorage.getItem('orderedStatus')).state
				};
			return false;
		},
		/**
		 *
		 *
		 * @param {*} data
		 */
		saveAdress: function (data) {
			var street = getStreet(data.ctx);
			var house = getHouse(data.ctx);

			var baseTown = data.ctx.getLocalities()[0];
			console.log('baseTown', baseTown);
			// console.log(baseTown);
			var baseCountry = data.ctx.getCountry();
			var newTown = baseTown;
			// TODO Astana это тот самый город
			AllCities.find(function (element) {
				// TODO FIXXX
				if ((baseTown === 'Нур-Султан (Астана)') && (element.key === 'Astana')) {
					newTown = element;
					return 0;
				}

				if (element.value_ru === baseTown) {
					newTown = element;
					return 0;
				}
			});
			var country = AllCountries.find(function (element) {
				if (element.value_ru === baseCountry)
					return element;
			});
			// console.log('newTown.key', newTown.key);
			localStorage.setItem("newTown.key", newTown.key);
			result = User.newAdress({
				"town": newTown.key,
				"country": country.key,
				"lat": data.coord[0],
				"lng": data.coord[1],
				"street": getStreet(data.ctx),
				"house": getHouse(data.ctx)
			}, true);

			var NewBucket = Basket.getFull();

			NewBucket.address = result;
			Basket.editBucket(NewBucket);

		},
		setOrderedStat: function (state, noreload) {
			if (!state)
				return void 0;
			// console.log('=== setOrderedStat ===');
			// console.log(bucket.id);
			if (bucket.id) {
				localStorage.setItem('orderedStatus', JSON.stringify({
					"sta": true,
					"id": bucket.id,
					"state": state
				}));
				if (!noreload)
					window.location.reload(false);
			}
		},
		editBucket: function (nbucket, callback) {
			console.log('editBucket nbucket', nbucket);
			if (!nbucket.order_products.length) {
				Basket.refresh();
				// console.log('== nbucket.order_products.length')
				// window.location.reload(true);
			}
			bucket = nbucket;
			localStorage.setItem('bucket', JSON.stringify(nbucket));

			(getCallback(callback))();
		},
		/**
		 *
		 *
		 */
		clear: function () {
			localDB.removeItems(['currentPay', 'orderPage']);
			localStorage.setItem('orderedStatus', JSON.stringify({}));
			templateFill();
		},
		/**
		 *
		 *
		 * @param {*} $productObject
		 */
		recalculationProducts: function ($productObject) {

			bucket.order_products.forEach(function (element) {

				if ($productObject.id === (element.product_id || element.id)) {
					element.amt = $productObject.count;
				}
			});

			Basket.save();
			window.setTimeout(function () {
				fillPreBasket();
			}, 45);
		},
		/**
		 *
		 *
		 * @param {*} $id
		 */
		removeProduct: function ($id) {

			bucket.order_products = bucket.order_products.filter(function (element) {
				if (element.id !== $id) {
					return true;
				}
			});
			Basket.resave_sender();

			window.setTimeout(function () {
				fillPreBasket();
			}, 45);
		},
		/**
		 *
		 *
		 */
		paramsHandler: function () {

			var tamaq_executor = $("#orderDataBag").data('tamaq_executor');
			tamaq_executor = typeof tamaq_executor != 'undefined' ? !tamaq_executor : true;
			bucket.tamaq_executor = tamaq_executor;

			var datetimepicker = $('.basket-input#datetimepicker');
			datetimepicker.on('change', function (e) {
				bucket.to_time = $(this).val();
				Basket.save();
			});

			var basketArea = $('.basket__area-text');
			basketArea.on('change', function (e) {
				bucket.description = $(this).val();
				Basket.save();
			});

			$("span[data-paytype=" + $('input[name=type_pay]:checked').val() + "]")
				.removeClass('hidden');

			$("#pays input[name=type_pay]").on('change', function () {
				var value = $('input[name=type_pay]:checked', '#pays').val();
				$(".type_pay")
					.addClass('hidden');
				$("span[data-paytype=" + value + "]")
					.removeClass('hidden');
				localStorage.setItem('currentPay', value);
				// var bbb = Basket.getFull();
				// if(value === 'card') {
				// 	bbb.is_payed_online = true;
				// } else {
				// 	bbb.is_payed_online = false;
				// }
				// Basket.setBucket(bbb);
				$('.js-order-accept').find('small').addClass('hidden');
				$('.js-order-accept').find('small[data-c=' + value + ']').removeClass('hidden');
			});

			$("input[name=type_to_time]").on('change', function () {
				// TODO
				var inptv = $(this).val();
				var $dtmContainer = $('.type_to_time--toTime').closest('.form-group');
				$dtmContainer.removeClass('hidden');
				if (inptv === 'toFast') {
					$dtmContainer.addClass('hidden');
					$('#datetimepicker').val((new Date()).format('yyyy-mm-dd HH:MM:ss'));
					bucket.to_time = $('#datetimepicker').val();
					Basket.save();
				}
			});
			$("input[name=type_pay]").on('change', function(){
				bucket.is_payed_online = !Basket.is_cash();
				Basket.save();
			});
			$("input[name=type_pay]").change();

			if (Basket.getOrderedStat().state === 'Draft' || 
			typeof Basket.getOrderedStat().state === 'undefined') {
				$("input[name=type_to_time]").trigger('change');
			}


			var payCash = $(".type_pay--cash input");
			payCash.on('change', function (e) {
				bucket.need_change_from = $(this).val();
				Basket.save();
			});
		},
		/**
		 *
		 *
		 * @returns
		 */
		addTo: function () {
			var $uriParam = $uri.search(true);
			if (readyOrder && ($uriParam.id !== orderPage)) {
				$('.js-addToBasket')
					.addClass('disabled')
					.attr("data-placement", "auto top")
					.attr("data-toggle", "popover")
					.attr("data-content", "Один заказ один ресторан")
					.popover();
				return;
			}

			$('.js-addToBasket').unbind('click').click(function () {

				var $self = $(this);
				var $uriParam = $uri.search(true);
				var $id = $uriParam.id;
				var searcRes = false;

				if (!orderPage) {
					localStorage.setItem('orderPage', $uriParam.id);
					orderPage = $uriParam.id;
				}

				bucket.order_products.forEach(function (elem) {
					if ((elem.product_id || elem.id) === $self.data('prodid'))
						searcRes = true;
				});

				if (searcRes)
					bucket.order_products.forEach(function (elem) {
						if ((elem.product_id || elem.id) === $self.data('prodid'))
							elem.amt = ++elem.amt;
					});

				var ordered_status = false;

				if (localStorage.getItem('orderedStatus') && Basket.getOrderedStat().status) {
					ordered_status = true;
					// TODO логика добавки
					// #resave
					Basket.resave({
						"id": $self.data('prodid'),
						"amt": 1,
						"price": $self.data('price')
					});
				} else if (!searcRes) {
					bucket.service.id = $id;
					bucket.order_products.push({
						"id": $self.data('prodid'),
						"amt": 1,
						"price": $self.data('price')
					});
				}

				// $hero = $("#welcome-hero");
				// if (!($hero.hasClass('buck')) && localStorage.getItem('orderPage')) {
				// 	$('#welcome_hero').css("bottom", "70px");
				// 	showHeroes("statusPending");
				// 	$hero.addClass("buck");
				// }
				if (!ordered_status) {
					Basket.save();
					Basket.refresh();
				}

			});
		},
		/**
		 *
		 *
		 * @param {*} product
		 */
		resave: function (product) {

			var yest = false;
			// Ищем есть ли такой же продукт и если нет то пушим
			bucket.order_products.forEach(function (element) {
				if ((element.product_id || element.id) === product.id)
					yest = true;
			});
			if (!yest)
				bucket.order_products.push({
					"id": product.id,
					"amt": 1
				});
			Basket.resave_sender();
		},
		resave_sender: function () {
			var products = [];
			// выдираем уже существующие продукты
			bucket.order_products.forEach(function (element) {
				products.push({
					"id": element.product_id || element.id,
					"amt": element.amt
				});
			});
			Requests.asyncPost('order/products', {
				"id": bucket.id,
				"order_products": products
			}).done(function (result) {
				Basket.editBucket(result, function () {
					Basket.refresh();
					Basket.save();
				});
			});
		},
		refresh: function () {
			var $orderBillet = $('#order-billet');
			var $uriParam = $uri.search(true);
			var uriIdEqualsOrderPage = $uriParam.id === orderPage;
			if (!uriIdEqualsOrderPage) {
				if (localStorage.getItem('orderPage'))
					Basket.refreshStatus(true);
				else
					$orderBillet.removeClass('show');
			}
			if (bucket.order_products.length && ($uri.path() !== '/basket')) {

				var bucket_price = 0;

				bucket.order_products.forEach(function (element, index) {
					if ((element.amt != 0) && (element.price !== 0))
						bucket_price += element.amt * element.price;
				});

				$('#basketSumCost').text(bucket_price.toFixed(2) + ' тг.');

				if ($orderBillet.not('show')) {
					$('footer').css('margin-bottom', '110px');
					$orderBillet.addClass('show');
				}
				Basket.refreshStatus(true);
			} else {
				$orderBillet.removeClass('show');
			}
			if (!bucket.order_products.length && bucket.service.id) {
				localStorage.removeItem('orderPage');
				Basket.clear();
			}
		},
		/**
		 *
		 *
		 * @returns
		 */
		isValid: function () {
			return Basket.validate();
		},
		/**
		 *
		 *
		 * @returns
		 */
		validate: function () {
			var required = {
				"address": true,
				"service": true,
				"order_products": true
			};
			var passed = true;
			try {
				for (element in bucket) {
					if (required.hasOwnProperty(element)) {
						if (element === 'address') {
							passed = !!(
								bucket[element].street.length &&
								bucket[element].latitude &&
								bucket[element].longitude
							);
						} else if (element === 'service') {
							passed = !!bucket[element].id.length;
						} else {
							passed = !!bucket[element].length;
						}
					}
					if (!passed) {
						return false;
					}
				}
			} catch (e) {
				console.log('elements in bucket failed');
			}
			return passed;
		},
		/**
		 *
		 *
		 * @param {*} data
		 * @returns
		 */
		checkProductsInOrder: function (data) {
			return data.order_products.length;
		},
		/**
		 *
		 *
		 * @returns {}
		 */
		checkout: function () {
			var part = {};
			var order_products;
			for (element in bucket) {
				if (element == 'order_products') {
					order_products = bucket[element];
				} else {
					part[element] = bucket[element];
				}
			}
			return {
				"requestSave": Requests.asyncPost('orders', part),
				"order_products": order_products
			};
		},
		save: function () {
			if (!bucket.order_products.length) {
				Basket.refresh();
				if ($uri.path() === '/basket')
					window.location.href = window.location.protocol + '//' + window.location.hostname;
			}

			if (typeof orderedStatus != 'undefined' && orderedStatus === true) {
				delete bucket.statistics;
				bucket.is_payed_online = !this.is_cash();

				console.log('beforeSavePut bucket state', bucket);
				Requests.asyncPut('orders/' + bucket.id, bucket).done(function (data) {
					// console.log("=== put basket ===");
					if (data.length) {
						bucket = data[0];
						console.log('afterSavePut', bucket);
						fillPreBasket();
						console.log('afterFillPreBasket', bucket);
						localStorage.setItem('bucket', JSON.stringify(data[0]));
					}
				});
			}
			localStorage.setItem('bucket', JSON.stringify(bucket));
			// console.log('=== basket save ===');
		},
		refreshSync: function() {
			sync(true);
		}
	};
})();

Basket.init();