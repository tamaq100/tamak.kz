function slickPromo() {

if ($('.slick-promo').find('.promo-item').length > 4) {
	$('.slick-promo').slick({
		slidesToShow: 2,
		slidesToScroll: 1,
		dots: true,
		infinite: true,
		prevArrow: '#promo-slider-control-left',
		nextArrow: '#promo-slider-control-right',
		arrows: true,
		draggable: true,
		responsive: [{
			breakpoint: 1260,
			settings: {
				centerMode: true,
				slidesToScroll: 1,
				slidesToShow: 3,
			}
		}, {
			breakpoint: 991,
			settings: {
				arrows: false,
				centerMode: true,
				slidesToScroll: 1,
				slidesToShow: 2,
			}
		}, {
			breakpoint: 768,
			settings: {
				centerMode: true,
				slidesToShow: 2,
			}
		}, {
			breakpoint: 639,
			settings: {
				centerMode: true,
				// variableWidth: true,
				slidesToScroll: 1,
				slidesToShow: 1,
			}
		}, {
			breakpoint: 500,
			settings: {
				centerMode: true,
				slidesToScroll: 1,
				slidesToShow: 1,
			}
		}, {
			breakpoint: 360,
			settings: {
				centerMode: false,
				slidesToScroll: 1,
				slidesToShow: 1,
			}
		}, ]
	});
} else if (widthScreen <= 767) {
	$('.slick-promo').slick({
		slidesToShow: 2,
		slidesToScroll: 2,
		dots: true,
		prevArrow: '#promo-slider-control-left',
		nextArrow: '#promo-slider-control-right',
		arrows: true,
		infinite: true,
		draggable: true,
		responsive: [{
			breakpoint: 875,
			settings: {
				arrows: false,
			}
		}, {
			breakpoint: 768,
			settings: {
				slidesToShow: 2,
			}
		}, {
			breakpoint: 605,
			settings: {
				centerMode: true,
				slidesToScroll: 1,
				slidesToShow: 1,
			}
		}, {
			breakpoint: 421,
			settings: {
				centerMode: true,
				slidesToScroll: 1,
				slidesToShow: 1,
			}
		}, {
			breakpoint: 360,
			settings: {
				centerMode: false,
				slidesToScroll: 1,
				slidesToShow: 1,
			}
		}, ]
	});
}

}

var restaurants__widgetSliderConfig = {
	slidesToShow: 2,
	slidesToScroll: 1,
	dots: true,
	infinite: true,
	prevArrow: '#resto-slider-control-left',
	nextArrow: '#resto-slider-control-right',
	responsive: [{
		breakpoint: 680,
		settings: {
			slidesToShow: 1,
		}
	}],
};

(function () {
	var restorauntsWidget = $('.restaurants__widget').find('.slick-in-modile');
	if (restorauntsWidget.length && (widthScreen <= 990)) {
		restorauntsWidget.slick(restaurants__widgetSliderConfig);
	} else if (restorauntsWidget.length) {
		restorauntsWidget.slick(restaurants__widgetSliderConfig);
	}
})();

if ($('.restoraunt-big__slider').not('.restoraunt-big__slider--arrows').find('.restoraunt-big__slide').length > 1) {
	var restaurants__widgetSliderConfig = {
		slidesToShow: 1,
		slidesToScroll: 1,
		lazyLoad: 'progressive',
		autoplay: false,
		autoplaySpeed: 7000,
		infinite: true,
		dots: true,
		arrows: false,
	};
	$('.restoraunt-big__slider').not('.restoraunt-big__slider--arrows').slick(restaurants__widgetSliderConfig);
}

function restorauntBig__slider() {
	var $arrowsSlider = $('.restoraunt-big__slider.restoraunt-big__slider--arrows');
	if (!($arrowsSlider.length)) {
		return 1;
	}
	if ($('.restoraunt-big__slider .restoraunt-big__slider--arrows.slick-initialized').length) {
		$('.restoraunt-big__slider .restoraunt-big__slider--arrows.slick-initialized').slick('unslick');
	}
	if ($arrowsSlider.not('.slick-initialized').find('.restoraunt-big__slide').length > 1) {
		$arrowsSlider.not('.slick-initialized').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			lazyLoad: 'progressive',
			autoplay: false,
			autoplaySpeed: 7000,
			infinite: true,
			dots: true,
			arrows: true,
			responsive: [{
				breakpoint: 767,
				settings: {
					arrows: false,
				}
			}, ],
		});
	}
}

function restorauntBig__sliderParams(parent, callback) {
	callback = getCallback();
	// Добился стабильной работы скрипта
	$(parent).find('.restoraunt-big__slider.restoraunt-big__slider--arrows').unbind('touchstart').unbind('touchmove');

	$(parent).find('.restoraunt-big__slider.restoraunt-big__slider--arrows.slick-initialized').slick('unslick');
	$(parent).find('.restoraunt-big__slider.restoraunt-big__slider--arrows').not('.slick-initialized').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		lazyLoad: 'progressive',
		autoplay: false,
		autoplaySpeed: 7000,
		infinite: true,
		dots: true,
		arrows: true,
		responsive: [{
			breakpoint: 767,
			settings: {
				arrows: false,
			}
		}],
	});
	callback();
}
restorauntBig__slider();

function mainSlider(block, callback) {
	callback = getCallback(callback);

	var sliderConfig = {
		slidesToShow: 1,
		slidesToScroll: 1,
		prevArrow: '#main-slider-control-left',
		nextArrow: '#main-slider-control-right',
		lazyLoad: 'progressive',
		autoplay: false,
		autoplaySpeed: 7000,
		infinite: true,
		dots: true,
		fade: true,
		arrows: true,
		speed: 750,
		responsive: [{
			breakpoint: 991,
			settings: {
				arrows: false,
				infinite: true,
				dots: true
			}
		}]
	};
	callback();
	$(block).not('.slick-initialized').slick(sliderConfig);
}

function dishesSliderOnMain(block, callback) {
	callback = getCallback(callback);
	var sliderConfig = {
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 1,
		lazyLoad: 'progressive',
		autoplay: false,
		autoplaySpeed: 7000,
		prevArrow: '#popular-slider-control-left',
		nextArrow: '#popular-slider-control-right',
		arrows: true,
		dots: false,
		speed: 750,
		draggable: true,
		variableWidth: true,
		responsive: [{
			breakpoint: 991,
			settings: {
				autoplay: false,
				slidesToShow: 3,
				draggable: true,
				dots: true
			}
		}, {
			breakpoint: 767,
			settings: {
				autoplay: false,
				draggable: true,
				slidesToShow: 2,
				dots: true
			}
		}],
	};
	if ((widthScreen >= 768) && ($(block).find('.dish').length <= 3)) {
		sliderConfig.responsive = [{
			breakpoint: 991,
			settings: {
				infinite: true,
				slidesToShow: 3,
				dots: true,
			}
		}, {
			breakpoint: 767,
			settings: {
				infinite: true,
				slidesToShow: 2,
				dots: true,
			}
		}];
	}
	callback();
	$(block).not('.slick-initialized').slick(sliderConfig);
}

function promotionSliderOnMain(block, callback) {
	callback = getCallback(callback);

	var sliderConfig = {
		slidesToShow: 1,
		slidesToScroll: 1,
		lazyLoad: 'progressive',
		autoplay: false,
		autoplaySpeed: 7000,
		prevArrow: '#promo-slider-control-left',
		nextArrow: '#promo-slider-control-right',
		arrows: true,
		speed: 750,
		draggable: false,
		vertical: true,
	};
	callback();
	$(block).not('.slick-initialized').slick(sliderConfig);
}

function restaurantsSliderOnMain(block, callback) {
	callback = getCallback(callback);

	var sliderConfig = {
		slidesToShow: 1,
		slidesToScroll: 1,
		lazyLoad: 'progressive',
		autoplay: false,
		autoplaySpeed: 7000,
		arrows: true,
		prevArrow: '#reviews-slider-up',
		nextArrow: '#reviews-slider-down',
		speed: 750,
		draggable: false,
		vertical: true,
	};
	callback();
	$(block).not('.slick-initialized').slick(sliderConfig);
}

function courierSliderOnMain(block, callback) {
	callback = getCallback(callback);

	var sliderConfig = {
		slidesToShow: 3,
		slidesToScroll: 1,
		lazyLoad: 'progressive',
		autoplay: false,
		autoplaySpeed: 7000,
		arrows: true,
		prevArrow: '#couriers-slider-up',
		nextArrow: '#couriers-slider-down',
		speed: 750,
		draggable: true,
		vertical: true,
	};
	callback();
	$(block).not('.slick-initialized').slick(sliderConfig);
}
function dishPartners(block, callback) {
	callback = getCallback(callback);

	var sliderConfig = {
		slidesToShow: 6,
		slidesToScroll: 1,
		lazyLoad: 'progressive',
		autoplay: false,
		autoplaySpeed: 7000,
		arrows: false,
		speed: 750,
		dots: true,
		draggable: true,
		responsive: [{
			breakpoint: 1200,
			settings: {
				slidesToShow: 6,
				dots: true
			}
		}, {
			breakpoint: 991,
			settings: {
				slidesToShow: 6,
				dots: true
			}
		}, {
			breakpoint: 767,
			settings: {
				slidesToShow: 3,
				dots: true
			}
		}, ],
	};
	callback();
	$(block).not('.slick-initialized').slick(sliderConfig);
}
if (document.querySelectorAll('.partners').length) {
	dishPartners($('.partners'));
}

function sliderAutoOurCourses(block, callback) {
	callback = getCallback(callback);

	var sliderConfig = {
		slidesToShow: 2,
		slidesToScroll: 1,
		lazyLoad: 'progressive',
		autoplay: false,
		autoplaySpeed: 7000,
		arrows: true,
		speed: 750,
		dots: false,
		draggable: true,
		prevArrow: '#couriers-slider-prev',
		nextArrow: '#couriers-slider-next',
		responsive: [{
			breakpoint: 1200,
			settings: {
				slidesToShow: 2,
				dots: false
			}
		}, {
			breakpoint: 991,
			settings: {
				slidesToShow: 2,
				dots: false
			}
		}, {
			breakpoint: 767,
			settings: {
				slidesToShow: 1,
				dots: false
			}
		}],
	};
	callback();
	$(block).not('.slick-initialized').slick(sliderConfig);
}
sliderAutoOurCourses($('.js-slider-our-couriers'));