$(function() {
	function ajax_form() {
		$('.ajax-form').submit(function(e) {
			e.preventDefault();
			var form = $(this);
			var button = form.find('button').attr("disabled", 'disabled');
			form.find('button').text('Отправляется...');
			$.ajax({
				url: form.attr('action'),
				method: form.attr('method'),
				data: form.serialize(),
				dataType: 'json',
				success: function(data) {
					button.text('Отправить');
					button.attr("disabled", 'enabled');
					if (data.redirect) {
						window.location.href = data.redirect;
					} else {
						form.html(data.html);
					}
				}
			});
		});
	}
	if ($('.ajax-form').length)
		ajax_form();
});