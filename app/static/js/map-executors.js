var myMapExecutors;

function initialExecutors() {
	if (!$('#executors').length) return;
	
	var toExactMinute = 300000 - (new Date().getTime() % 300000);
	getExecutors(initialExecutorsMap);
	setInterval(function() {
		getExecutors(initialExecutorsMap, true);
	}, toExactMinute);
}

function initialExecutorsMap(data, destroy) {
	var managerObject;
	if (destroy) myMapExecutors.destroy();

	myMapExecutors = new ymaps.Map('mapExecutors', {
		center: [CookieLat, CookieLng],
		zoom: 9
	}, {
		searchControlProvider: 'yandex#search'
	});
	// Создаём менеджера для иконок
	managerObject = new ymaps.ObjectManager({
		clusterize: false
	});
	managerObject.objects.options.set('preset', 'islands#darkOrangeDotIcon');

	// Добавляем его на карту
	myMapExecutors.geoObjects.add(managerObject);

	var executorsArray = {
		"type": "FeatureCollection",
		"features": []
	};

	var currentId = 0;
	data.executors.forEach(function (executor){
		if (!executor.hasOwnProperty('last_location')) return;
		executorsArray.features.push({
			type: 'Feature',
			id: currentId++,
			geometry: {
				type: 'Point',
				coordinates: [
					executor.last_location.latitude,
					executor.last_location.longitude
				]
			},
			properties: {
				hintContent: executor.name
			}
		});
	});
	managerObject.add(executorsArray);
}

function getExecutors(callback, destroy) {
	$.ajax({
		url: '/users/executors',
		method: 'get',
		success: function(data) {
			data = JSON.parse(data);
			callback(data, destroy);
			$('.couriers-list').html(data.html);
		}
	});
}