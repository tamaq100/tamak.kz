var mainMap, modalMap, myPlacemark, staticPlacemark, geolocation, findRestoModal, restoobjectManager, megaAdress, CookieLat, CookieLng, trash;
// astana coordinates

var settingsAdress;
var default_long = 71.490091;
var default_lat = 51.140708;

var objectManagerStatus = 0;
var dragendPoint = false;
var currentCityOnDropDown;

if (!coordIsExpired()) {
	if ((typeof localStorage.getItem('lat') !== void 0) && (typeof localStorage.getItem('lng') !== void 0)) {
		CookieLat = +localStorage.getItem('lat');
		CookieLng = +localStorage.getItem('lng');
	}
}

if ($('.get-order-widget').length != 0) {
	// Сделать функцию вставления города
	$('.get-order-form.posr').submit(function(e){
		e.preventDefault();
	});
	var cityDropdown = localStorage.getItem('cityDropdown'); 
	if (cityDropdown) {
		$('.city_list_current .city').text(cityDropdown);
	}
	currentCityOnDropDown = $('.city_list_current .city').text().replace(/\s/g,'');
	$('a.city_list').click(function($this) {
		// Сделать функцию запоминания города
		$('.city_list_current .city').text($(this).text());
		currentCityOnDropDown = $(this).text().replace(/\s/g,'');
		localStorage.setItem('cityDropdown', currentCityOnDropDown);

		var lat = $(this).attr('data-lat');
		var lng = $(this).attr('data-lng');

		modalMap.panTo([+lat, +lng]);
		myMapExecutors.panTo([+lat, +lng]);
		findRestoModal.panTo([+lat, +lng]);

	});

	function mapM() {
		function localStorageCoordNotExists(result) {
				if ((localStorage.getItem('lat') && localStorage.getItem('lat')) == void 0) {
					if (coordIsExpired()) {
						localStorage.setItem('lat', result.geoObjects.position[0]);
						localStorage.setItem('lng', result.geoObjects.position[1]);

						localStorage.setItem("coordExpires", (new Date().getTime() + (1000 * 60 * 60 * 2)));
					}
				}
		}

		if (!(CookieLat && CookieLng)) {

			CookieLat = default_lat;
			CookieLng = default_long;

		}

		geolocation = ymaps.geolocation;
		modalMap = new ymaps.Map('mapModal', {
			center: [CookieLat, CookieLng],
			zoom: 15,
		}, {
			searchControlProvider: 'yandex#search'
		});

		initialExecutors();

		findRestoModal = new ymaps.Map('mapResto', {
			center: [CookieLat, CookieLng],
			zoom: 15
		}, {
			searchControlProvider: 'yandex#search'
		});
		// Создаём объект мэнеджера, для дофигалиарда placemarks
		restoobjectManager = new ymaps.ObjectManager({
			clusterize: true,
			gridSize: 32,
			clusterDisableClickZoom: true
		});

		// Составляем пресеты для объект менеджера
		restoobjectManager.objects.options.set('preset', 'islands#greenDotIcon');
		restoobjectManager.clusters.options.set('preset', 'islands#greenClusterIcons');
		// Привязываем его к карте
		findRestoModal.geoObjects.add(restoobjectManager);

		// 	// renderRestoraunts(restoobjectManager);
		// 	return void 0;
		// });

		geolocation.get({
			// Запрос на вычисление координат браузером
			provider: 'auto',
			mapStateAutoApply: true
		}).then(function(result) {
			localStorageCoordNotExists(result);
			// ============================================================
			// СОЗДАТЬ ФУНКЦИЮ КОТОРАЯ ПОДДЕЛАЕТ КООРДИНАТЫ ЕСЛИ У НАС ЕЩЁ НЕ ИСТЁК СРОК ГОДНОСТИ
			// UPD уже создал
			var fakeResult = fakeResultCoord(result);
			initMyPlacemark({
					"result": fakeResult,
					"map": modalMap
				}, function(obj) {
					if (!renderAdress(obj.ctx)) {
						window.setTimeout(function(obj) {
							renderAdress(obj.ctx);
						}, 200, obj);
					}
				},
				function(data) {
					//initialize
					window.setTimeout(function() {
						modalMap.setCenter(
							myPlacemark.geometry.getCoordinates()
						);
					}, 700);

				},
				function(coords) {
					//on click and drag placemark
					modalMap.panTo(coords);
					if (!renderAdress(coords)) {
						window.setTimeout(function() {
							renderAdress(megaAdress);
						}, 200);
					}
				});
			// ============================================================
			staticPlacemark = createStaticPlacemark(result.geoObjects.position);
			findRestoModal.geoObjects.add(staticPlacemark);
			findRestoModal.panTo(result.geoObjects.position);
			Requests.sendLastLoc();
		}, function(e) {
			// console.log('Увы не получилось получить ваши координаты');
		});
	}


	// 'islands#blueFoodIcon'	
	function requestCouriers() {
		$.get('api/getcouriers', function(data) {
			// console.log(data);
		}, "json");
	}

	function requestRestoraunts(callback) {
		if (!!typeof orderCoords) {
			return void 0;
		}
		if (!!typeof orderCoords.adress || !!typeof orderCoords.coord[1] || !!typeof orderCoords.coord[0]) {
			return void 0;
		}
		$.get('apio/restoaround?coord=' + orderCoords.coord[0] + ',' + orderCoords.coord[1] + '&adress=' + orderCoords.adress, function(data) {
			callback(data);
		}, "json");
	}
	// Величайший скрипт для вывода всех ресторанов
	function renderRestoraunts(objManager) {
		// Принимаем объект менеджера
		// objectManagerStatus
		if (objectManagerStatus === 0) {
			objectManagerStatus = 1;
			requestRestoraunts(function(response) {
				var restoraunts = response;
				trash = response;
				var newPlacemarks = {};
				newPlacemarks.type = 'FeatureCollection';
				newPlacemarks.features = [];

				for (var i = restoraunts.length - 1; i >= 0; i--) {
					if (restoraunts[i].departments) {
						var dep = Object.keys(restoraunts[i].departments);
						for (var a = dep.length - 1; a >= 0; a--) {
							var htmlM = "<font size=3><b><a style='color:#fe7d00;' href='" + restoraunts[i].departments[dep[a]].id + "'>" + restoraunts[i].name + "</a></b></font>";
							var htmk = "<p class='ballonDesc'>" + restoraunts[i].description + "</p>";
							if (restoraunts[i].photo) {
								htmk = "<img style='width:65%; display:inline-block' src='https://tamaq.kz" + restoraunts[i].photo + "'><span style='margin-top:5px;margin-bottom:5px; display:inline-block; font-style:italic;'>" + restoraunts[i].categories + "</span><p class='ballonDesc'>" + restoraunts[i].description + "</p>";
							}
							// chlusterCaption
							var cluster = "<p style='color:#fe7d00;'><b>" + restoraunts[i].name + "</b></p>";
							var someone = {
								"type": "Feature",
								"id": i,
								"geometry": {
									"type": "Point",
									"coordinates": [restoraunts[i].departments[dep[a]].latitude, restoraunts[i].departments[dep[a]].longitude]
								},
								"properties": {
									"balloonContentHeader": htmlM,
									"balloonContentBody": htmk,
									"clusterCaption": cluster
								}
							};
							// console.log(someone);

							newPlacemarks.features.push(someone);
						}
					}
				}
				// console.log(newPlacemarks);
				objManager.add(newPlacemarks);
			});
		}
	}
	function failmap(err) {
		// console.log(err);
	}

	function renderAdress(geo) {

		if ((typeof geo === void 0) || (typeof geo == void 0)) {
			return false;
		}
		// UNSAFE
		settingsAdress = geo;
		// UNSAFE
		// console.log('GEO', geo);
		var geoAddressLine = geo.getAddressLine();
		$('#adressInput').removeClass('alert-danger');
		$('.modal-map-title').text(geoAddressLine);

		console.log("geoAddressLine", geoAddressLine);
		console.log('getCountryWithTown()',getCountryWithTown());

		$('#adressInput').attr('value', geoAddressLine.replace(getCountryWithTown(), ""));
		$('#InputAdress').attr('value', geoAddressLine);
		$('.search-bar_text').text(geoAddressLine);

		var orderCoords = {
			adress: geoAddressLine,
			coord: geo.geometry._coordinates
		};
		try {
			Basket.saveAdress({
				"ctx": geo,
				"coord": geo.geometry._coordinates
			});
		} catch(e) {
			console.log(e);
		}
		try {
			// statements
			if (Basket.isValid()) {
				console.log('Valid basket');
				try {
					Basket.saveAdress({
						"ctx": geo,
						"coord": geo.geometry._coordinates
					});
				} catch(e) {
					// console.log('saveAdress() fail');
					console.log({
						"ctx": geo,
						"coord": geo.geometry._coordinates
					});
				}
			}
		} catch(e) {
			// statements
			console.log('Basket.isValid() failed');
		}
		////////////////////////////////////////////////////////
		// console.log("geo.geometry._coordinates");
		// console.log(geo.geometry._coordinates);
		localStorage.setItem('lat', geo.geometry._coordinates[0]);
		localStorage.setItem('lng', geo.geometry._coordinates[1]);

		localStorage.setItem("coordExpires", (new Date().getTime() + (1000 * 60 * 60 * 2)));
		//////////////////////////////////////////////////////////////////////////
		try {
			// statements
			// #place
		staticPlacemark.geometry.setCoordinates(geo.geometry._coordinates);
		findRestoModal.panTo(staticPlacemark.geometry._coordinates, {
			fluing: false,
			safe: true,
			checkZoomRange: true
		});
		} catch(e) {
			// console.log('#place');
		}
		$("#addressList").hide();
		$('#adressBar').slideUp();
		$('#searchDoneBar').slideDown();
		return true;
	}
	$('.search-bar__button').on('click', function() {
		$('#searchDoneBar').slideUp();
		$('#adressBar').slideDown();
	});
	var modal = $('#find-me-modal');
	var modalPanelSelect = $(modal).find('.modal-panel-select');
	var modalAdress = $(modal).find('.modal-adress');
	//
	$(modal).find('.control-okay').on('click', function() {
		$('#adressBar').slideUp();
		$('#searchDoneBar').slideDown();
		$(modal).find('button.close').click();
	});

	var btnStat = 0;
	// Единая процедура обработки инпута
	function elementGeoBind($element) {
		if (!$element.length) return false;
		function myfunc(value) {
			geocodeYandex(value, 1);
		}
		var input = $element,
			timeOut;
		input.on('keyup', function() {
			clearTimeout(timeOut);
			timeOut = setTimeout(myfunc, 1400, $(this).val());
		});
		input.on('keydown', function() {
			clearTimeout(timeOut);
		});
	}

	$("#addressList").click(function(event){
		var $targetEl = $(event.target); 
		if($targetEl.is("li")) {
			geocodeYandexCallSearch($targetEl.data('search'));
		}
	});

	$(document).ready(function() {

		elementGeoBind($('#InputAdress'), 1);
		elementGeoBind($('#adressInput'), 1);

	});
	// Закрывается
	$(modal).find('.control-no').on('click', function() {

		$(modalAdress).toggleClass('hide fade');
		$(modalPanelSelect).toggleClass('hide fade');
		if (btnStat === 0) {
			$('.control-okay').text('Всё верно');
			btnStat = 1;
		}

	});
}

function createPlacemark(coords) {
	return new ymaps.Placemark(coords, {
		iconCaption: 'поиск...'
	}, {
		preset: 'islands#orangeGovernmentIcon',
		draggable: true
	});
}

function createStaticPlacemark(coords) {
	return new ymaps.Placemark(coords, {
	}, {
		preset: 'islands#geolocationIcon',
		draggable: false
	});
}
/**
 * @returns {string} country with city
 */
function getCountryWithTown() {
	var country = "Казахстан";
	if (currentCityOnDropDown && (currentCityOnDropDown.toLowerCase() == 'нур-султан')) {
		currentCityOnDropDown = 'Нур-Султан (Астана)';
	}
	var countryWithTown = country + ", " + (currentCityOnDropDown ? currentCityOnDropDown + "," : "");
	return countryWithTown;
}

// var TEST_GEO;
function geocodeYandex(address, rendr) {
	$('#adressInput').removeClass("alert-danger");
	$("#addressList ul").empty();
	var countryWithTown = getCountryWithTown();

	var temp = getCountryWithTown() + address;
	console.log("current adress " + temp);
	
	/**
	 * @param {string} adressValue
	 */
	function adressFilter(adressValue) {
		return adressValue.replace(countryWithTown, "");
	}

	var count = 3;
	ymaps.suggest(temp, {
		results: count
	}).then(function(items) {
		if (items.length > 0) {
			items.forEach(function(el) {
				console.log(el.value, currentCityOnDropDown, el.value.indexOf(currentCityOnDropDown.trim()) > -1);
				if(el.value.indexOf(currentCityOnDropDown.trim()) > -1) {
					var dataSearch = adressFilter(el.value);
					$('<li>', {
						text: dataSearch,
						'data-search': el.value
					}).appendTo('#addressList ul');
				}
			});
			mouseupSearchRes();
			$("#addressList").show();
			console.log("yamaps suggest ", res1);
			// countryWithTown

		}else{
			console.log('not found');
			searchNotFound();
		}
	});
}
function mouseupSearchRes() {
	$(document).unbind('mouseup').mouseup(function (e){
		var div2 = $('.input-wrapper');
		if (!(!div2.is(e.target) && div2.has(e.target).length === 0)) {
		}else{
			$('#addressList').hide();
			$('#addressList ul').empty();
		}
	});
}
/**
 * @param  {string} fullAdress полный адрес Вида "Country, Town, Street, house"
 */
function geocodeYandexCallSearch(fullAdress) {
	$('#adressInput').removeClass("alert-danger");
	ymaps.geocode(fullAdress, {
		results: 1
	}).then(function(res) {
		console.log("geocode call response", res);
		var firstGeoObject = res.geoObjects.get(0),
			coords = firstGeoObject.geometry.getCoordinates();
		renderAdress(firstGeoObject);

		myPlacemark.geometry.setCoordinates(coords);
		modalMap.panTo(coords);

		myPlacemark.properties
			.set({
				iconCaption: firstGeoObject.properties._data.name,
				balloonContent: firstGeoObject.properties._data.text
			});

	}).catch(function(err) {
		// вывести сообщение об ошибке
	});
}
			// ymaps.geocode(temp, {
		// 	results: 1
		// }).then(function(res) {
	
		// 	console.log("geocode call response", res);
	
		// 	// var firstGeoObject = res.geoObjects.get(0),
		// 	// 	coords = firstGeoObject.geometry.getCoordinates();
		// 	// if (typeof rendr !== 'undefined') {
		// 	// 	renderAdress(firstGeoObject);
		// 	// }
		// 	// myPlacemark.geometry.setCoordinates(coords);
		// 	// modalMap.panTo(coords);
		// 	// myPlacemark.properties
		// 	// 	.set({
		// 	// 		iconCaption: firstGeoObject.properties._data.name,
		// 	// 		balloonContent: firstGeoObject.properties._data.text
		// 	// 	});
		// });

function searchNotFound(){
	$("#addressList ul").empty();
	$('<span>', {
		text: "Нет результатов по поиску",
		class: 'err'
	}).appendTo('#addressList ul');

	$('#adressInput').addClass('alert-danger');
}
/**
 * @description geocodeYandexCall
 * @params objectMap | map object + searchControlProvider: 'yandex#search'
 * @params address | string
 * @params callback | function
*/
function geocodeYandexCall(objectMap, address, callback) {
	// console.log("address");
	// console.log(currentCityOnDropDown);
	var countryWithTown = getCountryWithTown();

	var temp = countryWithTown + address;

	ymaps.geocode(temp, {
		results: 5
	}).then(function(res) {
		console.log(res);
		if (callback) {
			callback({
				"firstGeoObject": res.geoObjects.get(0),
			});
		}
	});
}

function getAddress(coords, callback) {
	myPlacemark.properties.set('iconCaption', 'поиск...');
	ymaps.geocode([+coords[0], +coords[1]]).then(function(res) {
		var firstGeoObject = res.geoObjects.get(0);
		myPlacemark.properties
			.set({
				// Формируем строку с данными об объекте.
				iconCaption: [
					// Название населенного пункта или вышестоящее административно-территориальное образование.
					firstGeoObject.getLocalities().length ? firstGeoObject.getLocalities() : firstGeoObject.getAdministrativeAreas(),
					// Получаем путь до топонима, если метод вернул null, запрашиваем наименование здания.
					firstGeoObject.getThoroughfare() || firstGeoObject.getPremise()
				].filter(Boolean).join(', '),
				// В качестве контента балуна задаем строку с адресом объекта.
				balloonContent: firstGeoObject.getAddressLine()
			});

		localStorage.removeItem('lat');
		localStorage.removeItem('lng');
		localStorage.setItem('lat', coords[0]);
		localStorage.setItem('lng', coords[1]);

		megaAdress = firstGeoObject;
		if (callback) {
			callback(firstGeoObject);
		}
	});
}

function map() {
	window.setTimeout(function() {
		ymaps.ready(function() {
			myMap = new ymaps.Map("map", {
				center: [default_lat, default_long],
				zoom: 12
			}, {
				searchControlProvider: 'yandex#search'
			});
			var newPlacemark = new ymaps.Placemark([default_lat, default_long]);
			myMap.geoObjects.add(newPlacemark);
			myMap.panTo([default_lat, default_long]);
		});
	}, 50);
}

function failmap(err) {
	// console.log(err);
}
function fakeResultCoord (result) {
	if (coordIsExpired()) {
		return result;
	}else{
		// console.log("fakeResultCoord");
		// console.log(result);
		// console.log(result.geoObjects.position);

		result.geoObjects.position[0] = localStorage.getItem('lat');
		result.geoObjects.position[1] = localStorage.getItem('lng');

		return result;
	}
}
function coordIsExpired() {
	console.log('coordIsExpired');

	var expiresCORD = localStorage.getItem('coordExpires') || 0;
	if (expiresCORD < new Date().getTime()) {
		return true;
	}else{
		// console.log('coordIsExpired true');
		localStorage.setItem("coordExpires", (new Date().getTime() + (1000 * 60 * 60 * 2)));
		return false;
	}
}