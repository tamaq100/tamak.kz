function bindElem($element, callback, timing) {
	var time = timing ? timing : 1000;
	if (!$element.length) return false;
	var input = $element,
		timeOut;
	input.on('keyup', function() {
		clearTimeout(timeOut);
		timeOut = setTimeout(callback, time, $(this).val());
	});
	input.on('keydown', function() {
		clearTimeout(timeOut);
	});
}
$(function() {
	// Обработчик поиска
	$(".search-wrap").on('submit', function(e) {
		e.preventDefault();
		searchInput($('#input-search').val());
		// Сделал кривой тогглер
		$(document).unbind('mouseup').mouseup(function (e){
			var div2 = $('.search-results');
			if (!(!div2.is(e.target) && div2.has(e.target).length === 0)) {
			}else{
				searchToggle();
			}
		});
	});

	bindElem(
		$('#input-search'), 
		function() {
			$(".search-wrap").submit();
		}, 
		500);
	$('.search-button').on('click', function(){
		window.location.href = window.location.protocol + '//' + document.domain + '/search?value=' + $('#input-search').val();
	});
});

function generateSearchParams(fields, count) {
	var response = {
		"from": 0,
		"ors": {
			"like": fields
		}
	};
	if (count) response.count = count;
	return response;
}

function searchRender(data) {
	data.more = false;

	data.all_count = data.services.length + data.products.length;
	if (!(data.services || []).length && !(data.products || []).length)
		data = {
			"services": [],
			"products": [],
			"isset": false
		};
	// Условие для обрезания массивов
	if ((data.services.length > 5) || (data.products.length > 5)) {
		data.more = !0;
		data.products.length = (!!data.products.length) ? 5 : 0;
		data.services.length = (!!data.services.length) ? 5 : 0;
	}

	$(".search-results ul").append(
		RenderTabs.render(
			$("#search-results")
			.html(), data
		)
	);

	$('.search-results.hidden').removeClass('hidden');
	$('.form-control').addClass('result');
	$('.form-control.search-input.result').on('input', function(){
		$(this).siblings('.popover').remove();
	});

}

function searchToggle() {
	$(document).unbind('mouseup');
	$('.search-results').not('hidden').addClass('hidden');
	$('.form-control').removeClass('result');
	$('.search-results li').remove();

}

function searchInput(value) {
	searchToggle();
	$.when(
		Requests.asyncPost("services/list", generateSearchParams([{
			"field": "name",
			"type": "String",
			"value": value
		}])),
		Requests.asyncPost("services/products/list", generateSearchParams([{
			"field": "name",
			"type": "String",
			"value": value
		}]))
	).then(function(services, products) {
		searchRender({
			"services": services[2].responseJSON,
			"products": products[2].responseJSON,
			"isset": true,
			"search": value
		});
		services = null;
		products = null;
	});
}