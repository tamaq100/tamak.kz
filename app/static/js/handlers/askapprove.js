// Функция для подтверждения юзером его аккаунта
function phone_approve() {

	$('#askapprove_form')
		.find('input[name="user_provider_id"]')
		.attr('value', User.getMe().phone);
	$('#phone_approve').modal('show');

	$('#askapprove_button').click(function() {

		$(this).attr('disabled', true);

		Requests.requestApprove(function(data, status) {

			if (status) {

				$('#askapprove_button')
					.parent('div')
					.slideToggle(200);
				$('#askapprove_form')
					.slideToggle(200);

			} else {

				$('#askapprove_button')
					.siblings('small.alert-danger')
					.removeClass('invisible');
				window.setTimeout(function(self) {
					$(self)
						.siblings('small.alert-danger')
						.addClass('invisible');
					$(self).attr('disabled', false);
				}, 2000, $('#askapprove_button'));

			}

		});

	});

	$('.reload_askapprove').click(function(e) {

		e.preventDefault();
		phone_approve();

	});

	$('#send_approve').unbind('click').click(function(e) {

		e.preventDefault();
		$(this).attr('disabled', true);
		var data = getFormData($('#askapprove_form'));

		Requests.sendApprove(data, function(data, status) {
			if (status) {
				$('#phone_approve').modal('hide');
			} else {

				$('#askapprove_form')
					.find('.settings-table__input[name="password"]')
					.addClass('settings-table__input--error');
				$('small.alert-danger.invisible')
					.removeClass('invisible');

				window.setTimeout(function() {
					$('#askapprove_form')
						.find('.settings-table__input[name="password"]')
						.removeClass('.settings-table__input--error');
					$('#askapprove_form small.alert-danger')
						.addClass('invisible');
				}, 1500);

				window.setTimeout(function() {
					$('#send_approve').attr('disabled', false);
				}, 780);

			}
		});

	});

}