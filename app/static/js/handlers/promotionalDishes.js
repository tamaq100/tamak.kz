function promotionalDishes(){
    function render(products) {
        $("#promotional_dishes_container")
        .append(
            RenderTabs.render(
                $("#promotional_dishes_template")
                .html(),
                products
            )
        );
        return function(callback){ callback(); }
    }
    $.when(
        Requests.asyncPost("services/products/list", generateSearchParams([{
            "field": "tags",
            "type": "String",
            "value": "Акция"
        }], 10))
    ).then(function(promos) {
        render(promos)(slickPromo());
        promos = null;
    });
}