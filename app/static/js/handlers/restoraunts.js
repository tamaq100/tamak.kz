$(function() {
	if ($('#restoraunt_items').length) {

		var filterURI = new URI();
		$(".text.text--gray").click(function(e){
			if ($(this).hasClass('active')) {
				e.preventDefault();
				var $uri = new URI();
				var parsedQuery = URI.parseQuery($uri.query());
				delete parsedQuery[$(this).data('type')];
				$uri.query(parsedQuery); 
				window.location.href = $uri.href();
			}
		});
		var allrest;
		var pages;
		var restourauntsFired = false;
		Router.route('/restoraunts', function(url, params) {
			if(restourauntsFired) return;
			restourauntsFired = true;

			console.log('start execute Router route /restoraunts');
			if (isEmpty(params)) 
				return 0;
			params.from = (void 0 === params.from) ? 0 : params.from;
			params.page = (void 0 === params.page) ? 1 : params.page;
			console.log("params.type", params.type);
			params.type = (void 0 === params.type) ? null : params.type;
			params.area = (void 0 === params.area) ? 0 : params.area;
			params.sort = (void 0 === params.sort) ? "distanceASC" : params.sort;

			if (params.sort && params.sort.length) {

				var $text = $('.menu-2.dre a[data-filter="' + params.sort + '"]').text();
				$('#sortBy .textF').text($text || '--- ---');
				$('.menu-2.dre a[data-filter="' + params.sort + '"]').addClass('hidden');

			}
			if (params.filter && params.filter.length) {
				var $filtr = URI.parseQuery(filterURI.query());
				$filtr = $filtr.filter.split(',');
				$filtr.forEach(function(elem) {
					$('input[name="' + elem + '"]').prop('checked', true);
				});
			}

			getAllRestoraunts(
				addLocationToParams(generateParam(0, AndsType(params), params.sort, 0)),
				function(){
					if (parseInt(params.page) === 1) {
						params.from = 0;
					} else {
						params.from = (params.page * 10 ) - 10;
					}
					restorauntsRender(params);
				}
			);


			console.log('end execute Router route /restoraunts');
		});
		Router.event(window.location.href.toString().split(window.location.host)[1]);

		function addLocationToParams(params) {
			var town = localStorage.getItem("newTown.key");
			town = typeof town === 'undefined'? "NurSultan" : town;
			params.city = town;
			params.country = "Kazakhstan";
			return params;
		}
		function getAllRestoraunts(params, callback) {
			console.log('getAllRestoraunts', params);
			if(typeof callback === 'undefined') callback = function(){};

			Requests.getServices(params, function(data) {
				allrest = data.count ? data.count : 0 ;
				console.log('getServices => ', allrest);

				$('#resto_count')
					.text('(' + allrest + ')');
				params.from = params.from ? params.from : 1;
				pages = Math.round(allrest / 10);
				$('.js-wrap-container-rest').removeClass('preload');

				callback();
			});
		}

		function addFilter(e) {
			e.preventDefault();
		}

		function AndsType(params) {
			if (!params.type) return void 0;

			if ((params.type === "all_restoraunts") || (params.type === "all_restaurants")) return void 0;
			
			// if (params.type === "all_restaurants") return void 0;
			console.log("params.type", params.type);
			var result = {
				"eq_ands": [{
					"field": "categories.key",
					"type": "String",
					"value": params.type
				}]
			};

			var addedParams = {};
			if (params.filter) {
				var filterParams = params.filter.split(',');
				// Со сплитом получается массив
				filterParams.forEach(function(element) {
					var nElment = element;
					if (filters.hasOwnProperty(element)) {
						if (!Array.isArray(addedParams[filters[element].type])) {
							// console.log('в массиве такого элемента нет и мы создаём его');
							addedParams[filters[element].type] = [];
							filters[element].data.forEach(function(element) {
								addedParams[filters[nElment].type].push(element);
							});
						} else {
							// console.log('в массив пушим');
							filters[element].data.forEach(function(element) {
								addedParams[filters[nElment].type].push(element);
							});
						}
						// Особый случай
						if (element === 'work_now') {
							addedParams["le_ands"] = [];
							addedParams["le_ands"].push({
								"field":"openfrom",
								"type": "Integer",
								"value": new Date().getHours() * 60
							});
						}
					}
				});
			}

			result.eq_ands.forEach(function(element, index) {
				if (addedParams.hasOwnProperty('eq_ands')) {
					addedParams.eq_ands.push(element);
				} else {
					addedParams.eq_ands = [element];
				}
			});
			if (params.area) {
				if (!addedParams.hasOwnProperty('overlap')) { addedParams.overlap = []; }
				addedParams.overlap.push({
					"field": "areas.key",
					"type": "String",
					"value": params.area
				});
			}

			return addedParams;
		}

		function restorauntsRender(params) {
			// params.sort
			Requests.getServices(addLocationToParams(generateParam(params.from, AndsType(params), params.sort, 10)), function(data) {
				var htmlResult = RenderTabs.render(document.getElementById('restoraunt_items').innerHTML, data);
				$('#restoraunts_container').append(htmlResult);
				// Рендер пагинации
				console.log('====  Pagination render =====', {
					"current": params.page,
					"onPage": 10,
					"pages": pages,
				});
				if(typeof pages === 'undefined') {
					getAllRestoraunts(params, function(){
						console.log('====  Pagination render 2 =====', {
							"current": params.page,
							"onPage": 10,
							"pages": pages,
						});

						$(".page-content")
						.append(RenderTabs.render(document.getElementById('restoraunt_nav').innerHTML, {
							"current": params.page,
							"onPage": 10,
							"pages": pages,
						}));
						onLike();
					});
					
				} else {
					$(".page-content")
						.append(RenderTabs.render(document.getElementById('restoraunt_nav').innerHTML, {
							"current": params.page,
							"onPage": 10,
							"pages": pages,
						}));
					onLike();
				}
			});

		}

		function generateParam(from, ands, sort, count) {
			// console.log("generateParam");
			var dataObject = {};
			// console.log("!!!!!!!!!!ands");
			// console.log(ands);
			if(typeof ands !== "undefined") {
				if (ands.hasOwnProperty('like')) {
					dataObject.like = ands.like;
					delete ands.like;
				}
			}
			// if (ands.hasOwnProperty('like_ands')) {
			// 	dataObject.like_ands = ands.like_ands;
			// 	delete ands.like_ands;
			// }
			dataObject.from = from;
			dataObject.count = count;
			if (ands) {
				// console.log("+++++++++++++++");
				dataObject.ands = ands;
				// console.log(dataObject.ands);
			}

			if (sortE.hasOwnProperty(sort)) {

				dataObject.sort = [sortE[sort]];
			}
			if (localStorage.getItem('lat') && localStorage.getItem('lng')) {
				dataObject.latitude = localStorage.getItem('lat');
				dataObject.longitude = localStorage.getItem('lng');
			}
			return dataObject;
		}
		//console.timeLog('restoraunts_page');
		/*
			Обработчик фильтров с задержкой
		*/
		$('.js-menu-rest').on('change', function(e) {
			// console.log('change');
			if (this.changeForm) clearTimeout(this.changeForm);
			this.changeForm = setTimeout(function() {
				$(this).trigger('changeEnd');
			}, 1200);
		});
		$(window).bind('changeEnd', function() {
			var $uri = new URI();
			var $filtr = getFormData($('.js-menu-rest'));
			var parsedQuery = URI.parseQuery($uri.query());

			var $keys = Object.keys($filtr);

			var newParsed;

			if (parsedQuery.hasOwnProperty('filter')) {
				newParsed = parsedQuery.filter.split(",");
				newParsed = newParsed.filter(function(element) {
					return $keys.includes(element);
				});
				parsedQuery.filter = newParsed.join(',');
			}

			$keys.forEach(function(elem) {

				if (parsedQuery.hasOwnProperty('filter')) {

					if (filters.hasOwnProperty(elem)) {
						console.log("newParsed");
						console.log(newParsed);
						var $addQuery = newParsed.includes(elem) ? '' : ',' + elem;
						parsedQuery.filter += $addQuery;

					}
				} else {

					$uri.addSearch('filter', elem);
					parsedQuery = URI.parseQuery($uri.query());

				}

			});

			var filterData = $(this).data('filter');

			$uri.query(parsedQuery);
			// console.log(URI.parseQuery($uri.query()));
			window.location.href = $uri.href();

		});
		// для того, что бы форму не отправили
		$('.js-menu-rest').on('submit', function(e) {
			e.preventDefault();
		});
		// конец обработчика 
		// Нужно доделать нормально
		$('.filter-drop .dropdown-menu a').on('click', function(e) {
			e.preventDefault();

			var $self = $(this);
			$('.filter-drop .dropdown-menu a').removeClass('hidden');
			$self.addClass('hidden');

			$('#sortBy .textF').text($self.text());

			var filterData = $(this).data('filter');
			var $uri = new URI();
			$uri.setSearch('sort', filterData);
			window.location.href = $uri.href();
		});
	}
});

function onLike() {

	$('.restoraunt__like')
		.unbind('click')
		.on('click', function() {

			if (User.issetFavoriteServices($(this).data('id'))) {

				Requests.deleteFavoriteService($(this).data('id'), function(resp) {});
				$(this)
					.find('span')
					.text('В избранное');
				$(this)
					.find('.fa-heart')
					.removeClass('like--active');
			} else {

				$(this)
					.find('span')
					.text('Убрать из избранных');
				$(this)
					.find('.fa-heart')
					.addClass('like--active');

				Requests.addFavoriteService($(this).data('id'), function(resp) {});
			}

		});

}