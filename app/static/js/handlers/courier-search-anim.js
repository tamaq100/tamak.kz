function courierSearchAnim(orderID) {
	// var $courierModal = $getCourierSearchModal();
	// $courierModal.modal('show');
	searchCourierOrder(orderID);
}

var timeSearch = 0;
var countMove = 0;
function searchCourierOrder(orderID) {
	window.setTimeout(function(orderID) {
		Requests.getOrder(orderID, function(orders) {
			var order = orders[0];
			timeSearch = 15 * 1000;
			if (order.hasOwnProperty('executor') && order.executor) {
				Basket.refreshSync();
				if (order.executor.name !== 'no_executor') {
					// Даём выбрать курьера
					$getCourierSearchModal().find('.tab-pane[data-pane=' + 2 + '] .pane-courier').html(
						RenderTabs.render($('#ourCourier').html(), order)
					);
					// $getCourierSearchModal().find('.tab-pane').removeClass('active');
					// $getCourierSearchModal().find('.tab-pane[data-pane=' + 2 + ']').addClass('active');
					// $("#modalBasketDraft").modal('show');
					return 0;
				}
				if (countMove < 2) {
					Requests.asyncPut('orders/move', {
						"id": order.id,
						"to_status": 'FindExecutor',
						"msg": "Need move",
					}).done(function(data) {
						timeSearch = 15 * 1000;
						searchCourierOrder(order.id);
						Basket.refreshSync();
					}).fail(function(data){
						Basket.refreshSync();
						// $('#modalBasketDraft')
						// 	.find('.tab-pane.active')
						// 	.html('<h5>Курьер будет выбран системой автоматически</h5>');
							var animateBuck = JSON.parse(localStorage.getItem('animateBuck'));
							if (animateBuck && animateBuck.hasOwnProperty("pagesList")) {
								animateBuck.pagesList.forEach(function(element, index) {
									if ((element.order === order.id)) {
										animateBuck.pagesList.splice(index, 1);
									}
								});
							}
						localStorage.setItem('animateBuck', JSON.stringify(animateBuck));
						selectThisCourier(order.id);
						// window.setTimeout(function() {
						// 	$('#modalBasketDraft').modal('hide');
						// }, 700);
						return '';
					});
				} else {
					countMove++;
					selectThisCourier(order.id);
					timeSearch = 300 * 1000;
				}
			} else {
				countMove++;
				timeSearch = 15 * 1000;
				searchCourierOrder(order.id);
			}
		});
	}, timeSearch, orderID);

}

function $getCourierSearchModal() {
	return $("#modalBasketDraft");
}

function searchNewCourier(id) {
	if (id) {

		$getCourierSearchModal().find('.tab-pane[data-pane=' + 2 + ']').removeClass('active');
		$getCourierSearchModal().find('.tab-pane[data-pane=' + 1 + ']').addClass('active');
		Requests.asyncPut('orders/move', {
			"id": id,
			"to_status": "findExecutor",
			"msg": "Need move",
		}).done(function(data) {
			// console.log("done");
		}).error(function(data) {
			// console.log("error move to find");
		});

		window.setTimeout(function(bucketId){
			searchCourierOrder(bucketId);
		}, 150, id);
	}
}

function selectThisCourier(id) {
	// var returned = confirm('selectThisCourier :' + id);
	if (localStorage.getItem('currentPay') === 'card') {
		// alert('nice')
		$("#modalBasketDraft").modal('hide');
		payOnline();
		return;
	}
	// alert('ifOnlinePay false');
	if (id) {
		Requests.asyncPut('orders/move', {
			"id": id,
			"to_status": "Published",
			"msg": "Need move",
		}).done(function(data) {
			$getCourierSearchModal().find('.tab-pane').removeClass('active');
			$getCourierSearchModal().find('.tab-pane[data-pane=' + 3 + ']').addClass('active');
			if (localStorage.getItem('animateBuck')) {
				var newAnimateBuck = JSON.parse(localStorage.getItem('animateBuck')).pagesList.forEach( function(element, index) {
					if (element.order !== id) {
						return element;
					}
				});
			}
			
			localStorage.setItem('animateBuck', JSON.stringify({ "pagesList":newAnimateBuck }));

			window.setTimeout(function(){
				window.location.reload(false);
			}, 850);

		});
	}
}