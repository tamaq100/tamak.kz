var RemovePreload = function () {
	$('.js-wrap-container').removeClass('preload');
};
var StartPreload = function () {
	$('.js-wrap-container').addClass('preload');
};

function restoLike() {
	$('.restoraunt__like').on('click', function (e) {

		$(this).find('.fa-heart').toggleClass('like--active');
		Requests.deleteFavoriteService($(this).data('id'), function (res) {
			if (res)
				$(".restoraunt__like[data-id='" + res + "']")
				.parents('.white__block')
				.remove();
		});

	});
}

function hiddenSlides() {
	$(".js-wrap-slide").addClass('hidden');
}

function navClicker() {
	var $jsNavLk = $('.js-nav-lk');
	$jsNavLk.unbind('click').on('click', function (e) {

		e.preventDefault();
		$jsNavLk
			.parent('li')
			.removeClass('active');

		var hrefElement = $(this).attr('href');
		var slideIsset = $(this).data('goto');
		var $jsWrapSlide = $(".js-wrap-slide");
		var $jsWrapSlideIssetSlide = $(".js-wrap-slide[data-wrap=" + slideIsset + "]");

		$(this)
			.parent('li')
			.addClass('active');

		if ($jsWrapSlideIssetSlide.length) {
			$jsWrapSlide
				.not('hidden')
				.addClass('hidden');
			$jsWrapSlideIssetSlide
				.removeClass('hidden');

			ChangeUrl('Личный Кабинет', hrefElement);
			return 0;
		}
		var $wrapContainer = $('.js-wrap-container');
		/*
			procedure
		*/
		$wrapContainer
			.addClass('preload');
		$jsWrapSlide
			.not('hidden')
			.addClass('hidden');

		Router.event(hrefElement);

		if (!Router.issetRoute(hrefElement))
			$.get(hrefElement + '?full-aj', function (data) {
				$('.js-wrap-container')
					.append(RenderTabs.render(data.html));
				ChangeUrl('Личный Кабинет', hrefElement);
				RemovePreload();
			}, "json");
	});
}

var superTempTest;
$(function () {
	$('.js-wrap-container').addClass('preload');
	
	if (!$('.js-nav-lk').length) return;

	Router.route('/cabinet/restolike', function (url) {

		$.get(url + '?full-aj', function (dataHTML) {
			User.getFavoriteServices(function (restoraunts) {
				$('.js-wrap-container').append(
					RenderTabs.render(dataHTML.html, restoraunts)
				);
				navClicker();
				RemovePreload();
				restoLike();
			});
			ChangeUrl('Личный Кабинет', url);
		}, "json");

	});
	Router.route('/cabinet/history', function (url) {

		$.get(url + '?full-aj', function (dataHTML) {
			Requests.getHistoryOrders(User.getMeUid(), "customer.id", function (orders) {
				$('.js-wrap-container').append(
					RenderTabs.render(dataHTML.html, orders)
				);
				navClicker();
				RemovePreload();
			});
			ChangeUrl('Личный Кабинет', url);
		}, "json");

	});
	Router.route('/cabinet/khistory', function (url) {

		$.get(url + '?full-aj', function (dataHTML) {
			Requests.getHistoryOrders(User.getMeUid(), "executor.id", function (orders) {
				var newObj = {
					lastOrders: [],
					newOrders: []
				};
				// Фильтрем массив на две части
				newObj.newOrders = orders.filter(function (elem) {
					if (elem.status === 'FindExecutor')
						return true;
					newObj.lastOrders.push(elem);
				});
				$('.js-wrap-container').append(
					RenderTabs.render(dataHTML.html, newObj)
				);
				navClicker();
				RemovePreload();
				orders = null;
			});
			ChangeUrl('Личный Кабинет', url);
		}, "json");

	});
	Router.route('/cabinet/reviews', function (url) {

		$.get(url + '?full-aj', function (dataHTML) {
			Requests.getReviews(User.getMeUid(), function (reviews) {
				$('.js-wrap-container').append(
					RenderTabs.render(dataHTML.html, reviews)
				);
				navClicker();
				RemovePreload();
			});
			ChangeUrl('Личный Кабинет', url);
		}, "json");

	});
	Router.route('/cabinet/creviews', function (url) {

		$.get(url + '?full-aj', function (dataHTML) {
			Requests.asyncPost('rates/list', {
				"from": 0,
				"count": 100,
				"sort": [{
					"field": "valuation",
					"type": "Boolean",
					"asc": true
				}],

				"ands": {
					"eq": [{
						"field": "user.id",
						"type": "UUID",
						"value": User.getMeUid()
					}, {
						"field": "typerate_key",
						"type": "String",
						"value": "rate_executor"
					}]
				}
			}).done(function (data) {
				$('.js-wrap-container').append(
					RenderTabs.render(dataHTML.html, data)
				);
				navClicker();
				RemovePreload();
				data = null;
			});
			ChangeUrl('Личный Кабинет', url);
		}, "json");

	});
	Router.route('/cabinet/korder', function (url, params) {
		// TODO
		if (isEmpty(params)) return;
		$.get(url + '?full-aj', function (dataHTML) {
			Requests.getOrder(latticeFilter(params.id), function (order) {
				$('.js-wrap-container').append(
					RenderTabs.render(dataHTML.html, order)
				);
				orderActivateMap(params.id, order[0].address);
				navClicker();
				$('.navbar-cabinet').find('.js-nav-lk[data-goto="khistory"]').parent('li').addClass('active');
				statusTooltip(order);
				RemovePreload();
			});
			ChangeUrl('Личный Кабинет', url + '?id=' + params.id);
		}, "json");

	});
	Router.route('/cabinet/order', function (url, params) {
		// TODO
		if (isEmpty(params)) return;
		$.get(url + '?full-aj', function (dataHTML) {
			Requests.getOrder(latticeFilter(params.id), function (order) {
				order[0].own_executor = false;
				if(typeof order[0].executor === 'undefined') {
					order[0].executor = {
						name: 'no_executor'
					};
				}
				if(order[0].executor.name === 'no_executor' || order[0].executor.name === 'no_executer') order[0].executor.name = 'Курьер ресторана';

				var restoUrl = Requests.getServerName() + '/api/services';
				$.get(restoUrl ,{
					'ids': order[0].service.id + ""
				}).done(function(data){
					if(typeof data[0].own_executors !== 'undefined'){
						order[0].own_executors = data[0].own_executors;
						order[0].executor.name = 'Курьер ресторана';
					}
					$('.js-wrap-container').append(
						RenderTabs.render(dataHTML.html, order)
					);
					if(order[0].own_executors !== true) {
						orderActivateMap(params.id, order[0].address);
					}
					
					navClicker();
					$('.navbar-cabinet').find('.js-nav-lk[data-goto="history"]').parent('li').addClass('active');
					RemovePreload();
					statusTooltip(order);

				}).fail(function(data){

					$('.js-wrap-container').append(
						RenderTabs.render(dataHTML.html, order)
					);
					if(order[0].own_executors !== true) {
						orderActivateMap(params.id, order[0].address);
					}
	
					navClicker();
					$('.navbar-cabinet').find('.js-nav-lk[data-goto="history"]').parent('li').addClass('active');
					RemovePreload();
					// var animateBuck = JSON.parse(localStorage.getItem('animateBuck'));
					// var successAnSearch = false;
	
					// if (animateBuck && animateBuck.hasOwnProperty("pagesList"))
					// 	animateBuck.pagesList.forEach(function (element) {
					// 		if (element.order === params.id)
					// 			successAnSearch = true;
					// 	});
	
					// if (successAnSearch)
					// 	courierSearchAnim(params.id);
	
					statusTooltip(order);
				});
			});
			ChangeUrl('Личный Кабинет', url + '?id=' + params.id);
		}, "json");

	});
	Router.route('/cabinet/bonus', function (url) {
		$.get(url + '?full-aj', function (dataHTML) {
			function bonusFilter(data) {
				var resp = [];
				data.forEach(function (element) {
					if (element.paytype == 'bonus')
						resp.push(element);
				});
				return resp;
			}
			var data = {
				"bonusBalance": User.getMe().bonus_balance,
				"payments": bonusFilter(User.getMe().payments),
			};
			$('.js-wrap-container').append(
				RenderTabs.render(dataHTML.html, data)
			);
			navClicker();
			RemovePreload();
			ChangeUrl('Личный Кабинет', url);
		}, "json");

	});
	Router.route('/cabinet/redact', function (url) {

		$.get(url + '?full-aj', function (dataHTML) {

			// Костыль с таймаутом для того, что бы мы успели получить и отфильтровать данные
			function recursiveTimeout(time) {

				var addRess = JSON.parse(localStorage.getItem('UserAdresses'));
				var firstadress = (addRess.splice(0, 1))[0];

				var data = {
					"addRess": addRess,
					"firstadress": firstadress
				};
				window.setTimeout(function (data) {
					$('.js-wrap-container').append(
						RenderTabs.render(dataHTML.html, {
							"userdata": User.getMe(),
							"firstadress": data.firstadress,
							"addresses": data.addRess,
						})
					);
					RemovePreload();
					navClicker();
					redactHandlers();
					return 0;
				}, time, data);
			}

			recursiveTimeout(500);
			ChangeUrl('Личный Кабинет', url);

		}, "json");

	});
	Router.route('/cabinet/kredact', function (url) {

		$.get(url + '?full-aj', function (dataHTML) {
			hiddenSlides();
			$('.js-wrap-container').append(
				RenderTabs.render(dataHTML.html, User.getMe())
			);
			kredactHandler();
			redactHandlers();
			navClicker();
			RemovePreload();
			ChangeUrl('Личный Кабинет', url);

		}, "json");

	});
	Router.route('/cabinet/payed', function (url) {

		$.get(url + '?full-aj', function (dataHTML) {
			hiddenSlides();
			$('.js-wrap-container').append(
				RenderTabs.render(dataHTML.html, User.getMe())
			);
			navClicker();
			periodFilter();
			RemovePreload();
			ChangeUrl('Личный Кабинет', url);

		}, "json");

	});
	Router.route('/cabinet/kchat', function (url) {
		$.get(url + '?full-aj', function (data) {
			hiddenSlides();
			chatHTMLStorage.data = data.html;
			chatRender();

		}, "json");
	});
	Router.route('/cabinet/chat', function (url) {
		$.get(url + '?full-aj', function (data) {
			hiddenSlides();
			chatHTMLStorage.data = data.html;
			chatRender();

		}, "json");
	});
	navClicker();
});
/*
	tooltipContainerInfo
	history | array
	Функция предназначена для гененирования истории для тултипа
*/
function tooltipContainerInfo(history) {

	var statuses = orderStatus;
	var result = '<div class="info__tooltip-container">';
	var item = '<div class="info__tooltip-item">';
	var itemActive = '<div class="info__tooltip-item info__tooltip-item--active">';

	for (var i = 0, length1 = history.length; i < length1; i++) {
		// генерируем тултип айтем со статусом

		if (history[i].last)
			result += '<div class="info__tooltip-row info__tooltip-row--off">';
		else
			result += '<div class="info__tooltip-row">';

		result += history[i].current ? itemActive : item;

		result += statuses[history[i].status];
		result += '</div>';

		// Генерируем тултип тайм
		result += '<div class="info__tooltip-time">';

		result += (history[i].time === void 0) ? '' : history[i].time;

		result += '</div></div>';

	}

	result += '</div>';
	return result;

}

function parseOrderFromHistory(order) {
	function parseTime(time) {
		var date = new Date(Date.parse(time));
		var hours = date.getHours();
		var minutes = "0" + date.getMinutes();
		return hours + '.' + minutes.substr(-2);
	}
	var orders = [{
		"status": "Draft",
		"time": parseTime(order.created)
	}];
	order.statistics.forEach(function (element, index) {
		if (element.hasOwnProperty('move_from')) {
			var stat = {
				"status": element.move_from,
				"time": parseTime(element.created)
			};
			if (index == (order.statistics.length - 1))
				stat.current = true;
			orders.push(stat);
			stat = null;
		}
	});
	return orders;
}

function statusTooltip(order) {
	$('.js-wrap-slide[data-wrap="order' + order[0].id + '"]')
		.find('.info__body--info')
		.popover({
			"html": "true",
			"trigger": "hover",
			"placement": "bottom",
			"title": "статус заказа",
			"content": tooltipContainerInfo(parseOrderFromHistory(order[0])),
			"template": '<div class="popover info__tooltip" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
		});
}