$(function() {
	/*
		Большой хэндлер для обработки страницы корзины
	*/
	Router.route('/basket', function() {
		function successStory(){
			var basketId = Basket.getFull().id;
			window.setTimeout(function(){
				window.location.href = window.location.protocol + '//' + window.location.hostname + '/cabinet/order?id=' + basketId;
			}, 100);
			Basket.clear();
		}
		Basket.registerHook(successStory, "ExecutorSelected");
		Basket.registerHook(successStory, "Published");


		var LocalDatabase = LDB();
		// Получаем предварительную корзину
		var preBucket = LocalDatabase.get('bucket');
		console.log('routerFired');
		// Выводим карту
		var yamapt = yandexMapScript("mapBasket");
		$("body").append(yamapt);

		// получаем корзину и преобразуем к правильному виду 
		// и рендерим контейнер
		$("#basket_menu_container").append(
			RenderTabs.render(
				$("#basket_menu")
				.html(), {
					"mock": ['1', '2']
				}
			)
		);

		/**
		 * 
		 */
		// Получаем все сервисы и продукты
		Requests.getServiceAndProducts(function(data, preBucket) {

			if (data.restorauntData.length > 1)
				data.restorauntData = [];
			// Рендерим контент
			console.log('Получаем все сервисы и продукты', data);
			console.log("preBucket", preBucket);
			if(typeof Basket.getFull().order_products[0].product !== 'undefined'){
				data.order_products = Basket.getFull().order_products;
			} else {
				data.order_products = data.orderProducts;
			}
			console.log('Данные корзины', data);

			$("#basket_content_container").append(
				RenderTabs.render(
					$("#basket_content")
					.html(),
					data
				)
			);

			fillPreBasket(preBucket);
			inputsCounters();
			bucketInputHandlers();
			Basket.paramsHandler();
			/*
				Функция оформления заказа void CheckoutForm
			*/
			CheckoutForm();
			initAddressGeoBinding();
			// orderCustomAdressSelect();
			$('#adressSelect').on('change', function(e){
				$('textarea[name=newAddr]').val($(this).val());
				$('textarea[name=newAddr]').trigger('input');
			});

			$('.js-order-close').on('click', function(e) {
				e.preventDefault();
				localStorage.setItem('closeOrder', true);
				LocalDatabase.removeItems(['bucket', 'orderedStatus']);

				location.href = getUrlDomainAndProtocol();
			});

			window.setTimeout(function() {
				$('.preload').removeClass('preload');
			}, 600);

			$('.order__delete--small').click(function() {
				var prodID = $(this).data('productid');
				Basket.removeProduct(prodID);
				$('.row.order-content[data-productid="' + prodID + '"]').remove();
			});
		}, preBucket);
	});
});

function checkBasketIsNotOrdered() {
	return (!Basket.getOrderedStat() ||
		Basket.getOrderedStat().hasOwnProperty('id') &&
		!Basket.getOrderedStat().id)
}


var countFind = 0;
function ifOnlinePay() {
	return ((Basket.getFull().executor) && (localStorage.getItem('currentPay') === 'card'));
}
function Published() {
	Basket.moveOrder({
		"next": true,
		"to_status": "Published"
	}).done(function (data) {
		// Очищаем
		console.log('moveOrder Published', data);
		Basket.setOrderedStat("Published", true);
		var basketId = Basket.getFull().id;
		// Перенаправляем
		++countFind;
		window.setTimeout(function (basketId) {
			window.location.href = window.location.protocol + '//' + window.location.hostname + '/cabinet/order?id=' + basketId;
		}, 450, basketId);
		Basket.clear();
	}).fail(function (err) {
		++countFind;
		if (countFind < 2) {
			Published();
		} else {
			findExecutorFN();
		}
		console.log('Basket.moveOrder error', err);
	});
}
function findExecutorFN(){
	var LocalDatabase = LDB();
	Basket.moveOrder({
		"next": true,
		"to_status": "FindExecutor"
	}).done(function () {
		Basket.setOrderedStat("FindExecutor", true);

		var basket = Basket.getFull();
		basket.status = 'FindExecutor';

		var defPagesList = {
			"pagesList": []
		};
		var animateBuck = LocalDatabase.get('animateBuck', defPagesList);
		if (JSON.stringify(animateBuck) === '{}') animateBuck = defPagesList;
		animateBuck.pagesList.push({
			"order": Basket.getFull().id,
			"animate": true
		});

		LocalDatabase.set('animateBuck', animateBuck);
		Basket.editBucket(basket); // появляется ошибка
		// Uncaught TypeError: Cannot read property 'order_products' of undefined

		// courierSearchAnim(basket.id);
		// TODO restore
		// window.location.reload(false);
		Basket.refreshSync();
	}).fail(function (data) {
		Basket.refreshSync();
		window.setTimeout(function() {
			saveOrderLogic();
		}, 400);
	});
}
function payOnline() {
	$("#modalBasketDraft").modal('hide');
	Requests
		.asyncGeting('bill/' + Basket.getFull().id, '')
		.done(function (data) {
			console.log(data);
			localStorage.removeItem('currentPay');
			localStorage.setItem('payOnlineChecked', false);
			modalPay(data.redirect_url);
			baskRefresh();
		});
}

function baskRefresh() {
	Basket.refreshSync();
	window.setTimeout(function(){
		window.setTimeout(baskRefresh(), 400);
	}, 1500);
	if (Basket.getFull().is_payed_online === true) {
		$('#modalPay').modal('hide');
		saveOrderLogic();
	}
}

function saveOrderLogic() {
	if (!User.getStatus()) {
		console.log('Не Прошёл');
		return;
	}

	if (checkBasketIsNotOrdered()) {
		Basket.getFull()
		
		var req = Basket.checkout();
		req.requestSave.done(function (data) {
			var newPart = {
				"id": data.id,
				"order_products": req.order_products,
			};
			Requests.asyncPost('order/products', newPart)
				.done(function (data) {
					if (Basket.checkProductsInOrder(data)) {
						console.log("=== order/products ===");
						Basket.editBucket(data, function () {
							Basket.setOrderedStat("Draft");
						});
					} else {
						console.log('Basket.checkProductsInOrder не Прошёл');
					}
				}).fail(function () {});
		});
		return;
	}
	var BasketStatus = Basket.getFull().status;
	if (BasketStatus === "Draft") {
		findExecutorFN(); // Обрабатываем курьера
	} else if (BasketStatus === 'FindExecutor') {
		// Если онлайн пей и не оплачено
		Basket.refreshSync()
		if (ifOnlinePay()) {
			alert('FindExecutor payed online');
			payOnline();
			// https://tamaq.kz/bill/62466098-60f5-45fa-ae0f-96516df394a3
			return;
		}
		// Оплачено
		if (
			(localStorage.getItem('payOnlineChecked') === 'false') &&
			Basket.getFull().is_payed_online
		) {
			alert('is payed online');
			console.log('Не оплатил ты ещёёё');
			Basket.refreshSync();
			return;
		}
		alert('prePublished');
		Published();
	} else if (BasketStatus === 'Published') {
		alert('published')
		window.setTimeout(function (basketId) {
			window.location.href = window.location.protocol + '//' + window.location.hostname + '/cabinet/order?id=' + basketId;
		}, 590, Basket.getFull().id);
		Basket.clear();
	}
}




function CheckoutForm() { // ОЧЕНЬ ВАЖНАЯ ФУНКЦИЯ
	var $ordersAccept = $('.js-order-accept');

	if (!User.getStatus())
		$ordersAccept.popover({
			"trigger": "hover",
			"placement": "top",
			"content": "Необходимо зарегестрироваться что-бы сделать заказ"
		});
	else if (Basket.isValid())
		$ordersAccept.attr('disabled', false);
	if((Basket.getFull().status === 'Published') || 
	(Basket.getFull().status === 'FindExecutor')){
		saveOrderLogic();
	}
	$ordersAccept.on('click', function(e) {
		e.preventDefault();
		// TODO Логика сохранения заказа TODO
		saveOrderLogic();
	});
	dateTimePickerInit();
}

function bucketInputHandlers() {
	$("#userdata input").on('change', function() {
		var data = {};
		$($("#userdata").serializeArray()).each(function(obj) {
			data[obj.name] = obj.value;
		});
	});
}

var basketMap;
/*
	void mapBasket
	служит для инициализации карты на странице корзины
*/
function mapBasket() {
	console.log('mapBasket');
	var cor = {
		"lat": default_lat,
		"lng": default_long
	};

	function is_basketCoord() {
		var adr = Basket.getFull().address;
		return !!(adr && (adr.hasOwnProperty('latitude') && adr.hasOwnProperty('longitude')));
	}
	if (is_basketCoord()) {
		cor.lat = Basket.getFull().address.latitude;
		cor.lng = Basket.getFull().address.longitude;
	}
	geolocation = ymaps.geolocation;
	basketMap = new ymaps.Map('map', {
		center: [cor.lat, cor.lng],
		zoom: 15,
	}, {
		searchControlProvider: 'yandex#search'
	});

	initAddressGeoBinding();
	if (!is_basketCoord()) {

		geolocation.get({
			// Запрос на вычисление координат браузером
			provider: 'auto',
			mapStateAutoApply: true

		}).then(function(result) {
			if (!coordIsExpired()) {
				result.geoObjects.position = [
					localStorage.getItem('lat'),
					localStorage.getItem('lng')
				];
			}
			initMyPlacemark({
				"result": result,
				"map": basketMap
			}, function(obj) {
				renderAddressForm(obj, Basket.saveAdress);
			});
			window.setTimeout(function() {
				basketMap.panTo([
					localStorage.getItem('lat'),
					localStorage.getItem('lng')
				]);
			}, 20);
		}, function(e) {
			console.log('Увы не получилось получить ваши координаты');
		});
	} else {
		initMyPlacemark({
			"result": {
				"geoObjects": {
					"position": [cor.lat, cor.lng]
				}
			},
			"map": basketMap
		}, function(obj) {
			renderAddressForm(obj, Basket.saveAdress);
		});
	}
	window.setTimeout(function(cor) {
		basketMap.panTo([cor.lat, cor.lng]);
	}, 10, cor);

}
/*
	void initMyPlacemark
	callback | function
	data | object?array?
*/
function initMyPlacemark(data, callback2, callback, callCursor) {
	localStorage.setItem('lat', data.result.geoObjects.position[0]);
	localStorage.setItem('lng', data.result.geoObjects.position[1]);
	// console.log("=== INIT MY PLACEMARK ===");
	myPlacemark = createPlacemark(data.result.geoObjects.position);
	data.map.geoObjects.add(myPlacemark);

	var coordinates = myPlacemark.geometry.getCoordinates();

	function eventHandler(coordinates, data, optionCallback) {
		getAddress(coordinates, function(firstGeoObject) {
			callback2({
				"text": firstGeoObject.getAddressLine(),
				"coord": JSON.stringify(firstGeoObject.geometry._coordinates),
				"ctx": firstGeoObject
			});
			if (typeof optionCallback !== void 0) {
				optionCallback(firstGeoObject);
			}
			// TODO
			// data.map.panTo(firstGeoObject.geometry._coordinates);
		});
	}

	function setCoordinates(firstGeoObject) {
		myPlacemark.geometry.setCoordinates(firstGeoObject.geometry._coordinates);
	}
	// Это срабатывает при инициализации плейсмарка
	eventHandler(data.result.geoObjects.position, data, setCoordinates);
	// Это срабатывает при драгэнде
	myPlacemark.events.add('dragend', function() {
		coordinates = myPlacemark.geometry.getCoordinates();
		eventHandler(coordinates, data);
		if (typeof callCursor !== 'undefined') {
			callCursor(myPlacemark.geometry.getCoordinates());
		}
	});
	// при клике
	data.map.events.add('click', function(e) {
		myPlacemark.geometry.setCoordinates(e.get('coords'));
		coordinates = myPlacemark.geometry.getCoordinates();
		eventHandler(coordinates, data);
		if (typeof callCursor !== 'undefined') {
			callCursor(myPlacemark.geometry.getCoordinates());
		}
	});
	if (typeof callback != 'undefined') {
		callback(data);
	}
}
/*
	void renderAddressForm
	data | object
*/
var geoTest;

function renderAddressForm(data, callback) {
	geoTest = data;

	$('textarea[name="newAddr"]').val(data.text);
	$('textarea[name="position"]').val(data.coord);

	$("#addrTotal").find("span").text(data.text);

	if (typeof data.coord === 'string') {
		data.coord = JSON.parse(data.coord);
	}
	console.log('renderAddressForm coords = ', data.coord);

	window.setTimeout(function(data) {

		localStorage.setItem('lng', data.coord[1]);
		localStorage.setItem('lat', data.coord[0]);
		
	}, 40, data);

	Basket.saveAdress(data);

	if (typeof callback == 'function') {
		callback(data);
	}
	setTimeout(function(obj) {
		basketMap.panTo(data.coord);
		// console.log(obj.ctx.getAddressLine());
		// obj.ctx.geometry.getAddressLine()
		// User.newAdress({
		// }).done();
	}, 200, {
		"ctx": data.ctx
	});

}

/*
	void initAddressGeoBinding
*/
function initAddressGeoBinding() {
	var $inpt = $('textarea[name="newAddr"]');

	$inpt.unbind('input').on('input', function() {
		if (this.delay)
			clearTimeout(this.delay);

		this.delay = setTimeout(function(e) {
			$inpt.trigger('inputEnd');
		}, 750);
	});

	$inpt.bind('inputEnd', function(e) {

		geocodeYandexCall(basketMap, $(this).val(), function(obj) {
			$("#addrTotal").find("span").text('');
			// console.log(obj);
			var coords = obj.firstGeoObject.geometry.getCoordinates();
			// console.log("inputEnd");
			myPlacemark.geometry.setCoordinates(coords);
			basketMap.panTo(coords);
			renderAddressForm({
				"text": $('textarea[name="newAddr"]').val(),
				"coord": coords,
				"ctx": obj.firstGeoObject
			}, Basket.saveAdress);
			coords = null;
			myPlacemark.events.fire('dragend');
			$("#addrTotal").find("span").text($('textarea[name="newAddr"]').val());
		});
	});
}

/*
	void fillPreBasket
	preBasket | object | basket template
*/
function fillPreBasket(preBasket) {

	// console.log("fillPreBasket");
	// console.log(preBasket);

	var totalAmt = 0;
	var totalCost = 0;
	var preBasket = typeof preBasket === "undefined" ? Basket.getFull() : preBasket;

	preBasket.order_products.forEach(function(element) {

		var row = $('.order-content[data-productid="' + element.id + '"]');

		console.log("preBasket.order_products");
		console.log(element);

		row.find('.product-count').text(element.amt);
		row.find('.product-portions').val(element.amt);
		row.find('.order-content__order-price span').text(element.price);
		row.find('.product-price').text((element.amt * element.price).toFixed(2));

		totalAmt += element.amt;
		totalCost += +(element.amt * element.price).toFixed(2);

	});

	$('.order__order-amt span')
		.text(totalAmt);

	$('.order_sum__cost-value.prefatory')
		.text(totalCost.toFixed(2) + ' тг.');

	$('.order__order-count span')
		.text(totalCost.toFixed(2));

	$('.restoraunt-side__description span')
		.text(totalAmt);
	// console.log("preBucket");
	if (preBasket.esum) {
		/// TODO 
		console.log("=== preBasket.esum");
		console.log(preBasket);

		var $deliverySum = $(".order_sum_delivery");
		$deliverySum.find('.order_sum__delivery-value').text(preBasket.esum + ' тг.');
		$deliverySum.removeClass('hidden');
		var sum = preBasket.esum + preBasket.sum;

		var $order_sum_total = $(".order_sum_total");
		$order_sum_total.find('.order_sum__total-value').text(sum + ' тг.');
		$order_sum_total.removeClass('hidden');
		$(".order__order-cost span").text(preBasket.esum);

	}
	var datetimepicker = $('.basket-input#datetimepicker');
	datetimepicker.val(preBasket.to_time);

	var basketArea = $('.basket__area-text');
	basketArea.val(preBasket.description);

	var payCash = $(".type_pay--cash input");
	payCash.val(preBasket.need_change_from);

}

function inputsCounters() {

	$('.basket__button-counter--elem.basket__button-counter--minus').click(function(e) {
		var $self = $(this);
		var $counter = $self.siblings('.basket__button-counter.product-portions');
		var valueCount = $counter.val();
		if (valueCount > 1) {
			--valueCount;
			$counter.val(valueCount);
			Basket.recalculationProducts({
				"id": $self.closest('.row.order-content').data('productid'),
				"count": valueCount
			});
		}
	});

	$('.basket__button-counter--elem.basket__button-counter--plus').click(function(e) {
		var $self = $(this);
		var $counter = $self.siblings('.basket__button-counter.product-portions');
		var valueCount = $counter.val();
		++valueCount;
		$counter.val(valueCount);

		Basket.recalculationProducts({
			"id": $self.closest('.row.order-content').data('productid'),
			"count": valueCount
		});
	});

}
// 0:
// amt: 6
// id: "8497a669-a67f-417e-9b74-4627570ad864"
// price: 600.32
function getHouse(ctx) {
	if (ctx.getPremiseNumber() !== void 0)
		return ctx.getPremiseNumber();
	return null;
}

function getStreet(ctx) {
	if (ctx.getThoroughfare() !== void 0)
		return ctx.getThoroughfare();

	if (ctx.getPremise() !== void 0)
		return ctx.getPremise();

	if (ctx.getLocalities()[ctx.getLocalities().length-1])
		return ctx.getLocalities()[ctx.getLocalities().length-1];

	return null;
}