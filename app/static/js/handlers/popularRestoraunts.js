function popularRestorauntsRender() {
	function render(products, callback) {
		if (!products.length) return;
		$("#restoraunt_items_container")
		.append(
			RenderTabs.render(
				$("#restoraunt_items")
				.html(),
				products
			)
		);
		callback();
	}
	$.when(
		Requests.asyncPost("services/products/list", {
			"from": 0,
			// "ands": {
			// 	"like_ands": [
			// 		{
			// 			"field": "tags",
			// 			"type": "String",
			// 			"value": "Популярное"
			// 		}
			// 	]
			// },
			"count": 12
		})
	).then(function(popular) {
		render(popular, function() {
			console.log('dishesSliderOnMain', popular);
			dishesSliderOnMain($('#restoraunt_items_container .dishes-list__popular__widget'));
		});
	});
}