// "joy":{"hero":"03","msg":"03"},
// "training":{"hero":"06","msg":"02"},
// "deliveryStatus":{"hero":"07","msg":"07"},
var heroesData = {
	"hello": {
		"hero": "01",
		"msg": "01"
	},
	"help": {
		"hero": "06",
		"special": true
	},
	"anticipation": {
		"hero": "02",
		"msg": "02"
	},
	"sadness": {
		"hero": "04",
		"msg": "04"
	},
	"forchat": {
		"hero": "05",
		"msg": "05"
	},
	"orderStatus": {
		"hero": "08",
		"msg": "08"
	},
	"statusPending": {
		"hero": "09",
		"msg": "09"
	}
};
var heroesAction = false;

function showHeroes(stage) {
	if (stage === 'undefined') stage = '';
	if (!(stage + '').length) stage = "hello";
	// console.log('hello! Show heroes')
	var helpStart = localStorage.getItem('helpStart');
	var widthScreenIsDesktopAndHeroesAction = (widthScreen >= 990) || heroesAction;
	if (!widthScreenIsDesktopAndHeroesAction) return 0;
	
	var heroes = $('#welcome_hero');
	var path = "/img/heroes/";

	if ($('#order-billet').hasClass('show'))
		heroes.css("bottom", "70px");

	var $uri = new URI();
	if (localStorage.getItem('orderPage') === $uri.search(true).id) return 0;
	if (window.location.pathname === '/basket') return 0;

	if ((helpStart === null) && (stage === "hello")) {
		// $(".welcome-hero__text").hide();
		// $("#welcome_hero_close").remove();
		stage = "help";
		// stageHelp({
		// 	"heroes" : heroes,
		// 	"path": path
		// });
		return 0;
	}


	// heroes.find('.welcome-hero__ico').attr("src", path + 'hero' + heroesData[stage].hero + '.png');
	// heroes.find('.welcome-hero__text').attr("src", path + "msg" + heroesData[stage].msg + '.png');
	// heroes.delay(5000).addClass('shown');
	heroesAction = true;
	if (stage === 'statusPending') {
		heroes.find('.welcome-hero__ico').css('cursor', 'pointer').unbind('click').click(function(event) {
			event.preventDefault();
			window.location.href = window.location.protocol + '//' + window.location.hostname + '/basket';
		});
		heroes.find('.welcome-hero__text').css('cursor', 'pointer').unbind('click').click(function(event) {
			event.preventDefault();
			window.location.href = window.location.protocol + '//' + window.location.hostname + '/basket';
		});
		$('.welcome-hero_close').click(function(event) {
			event.preventDefault();
			heroes.removeClass('shown');
		});
	} else {
		heroes.unbind('click').click(function(event) {
			event.preventDefault();
			heroes.removeClass('shown');
		});
	}
	window.setTimeout(function(){
		$('.welcome-hero_close').click();
	}, 3006);
}

function stageHelp(data) {

	// data.heroes.find('.welcome-hero__ico').attr("src", data.path + 'hero' + heroesData.help.hero + '.png');
	// data.heroes.find('.welcome-hero__text').attr("src", data.path + "msg" + heroesData.help.msg + '.png');

	// $("#helpModal").modal("show");


	// $("#helpModal").on("hide.bs.modal",function() {
		
	// 	window.setTimeout(function(){
	// 		if (!$("#driver-page-overlay").length) {
	// 			data.heroes.removeClass('shown');
	// 		}
	// 	}, 220);

	// });

	// data.heroes.delay(5000).addClass('shown');
	// heroesAction = true;

	// data.heroes.unbind('click').click(function(event) {
	// 	event.preventDefault();
	// 	data.heroes.removeClass('shown');
	// });

}
function dismissHelp(){
	$("#helpModal").modal("hide");
	localStorage.setItem('helpStart', false);
	$('#welcome_hero').removeClass('shown');

	// window.setTimeout(function(){
	// 	window.setTimeout(function(){
	// 		var $heroes = $('#welcome_hero');
	// 		var path = "/img/heroes/";
	// 		$heroes.find('.welcome-hero__ico').attr("src", path + 'hero' + heroesData.hello.hero + '.png');
	// 		$heroes.find('.welcome-hero__text').attr("src", path + "msg" + heroesData.hello.msg + '.png');
	// 		$(".welcome-hero__text").show();
	// 		$heroes.addClass('shown');
	// 	}, 150);
	// }, 150);
}

function helpActions(data) {
	if (data.action === "dismis")
		dismissHelp();
	$("#helpModal").modal("hide");
	// startHelp();
}

function startHelp() {
	var driver = new Driver({
		allowClose: false,
		doneBtnText: 'Завершить', // Text on the final button
		closeBtnText: '✖', // Text on the close button for this step
		nextBtnText: 'Далее ▶', // Next button text for this step
		prevBtnText: '◀ Назад',
		onReset: function(){
			// запускается по окончанию
			dismissHelp();
		}
	});
	driver.defineSteps(helpSteps);
	driver.steps.forEach(function(element) {
		// console.log(element);
		$(element.node).on('click', function(e) {
			driver.reset();
		});
	});
	driver.start();
}
$(function() {
	setTimeout(function() {
			// body...
		if (localStorage.getItem('closeOrder') === 'true') {
			localStorage.setItem('closeOrder', false);
			showHeroes("sadness");
		}
		if (Basket.getStatus()) {
			// $('#welcome_hero').css("bottom","70px");
			showHeroes("statusPending");
		}
		Router.route('/', function(url) {
			showHeroes("hello");
		});
		Router.route('/restoraunts', function(url) {
			showHeroes("anticipation");
		});
		Router.route('/basket', function(url) {
			showHeroes("statusPending");
		});
		Router.route('/cabinet/chat', function(url) {
			showHeroes("forchat");
		});
		Router.route('/restoraunts/restoraunt_info', function(url) {
			showHeroes("anticipation");
		});
		Router.route('/restoraunts/restoraunt_reviews', function(url) {
			showHeroes("anticipation");
		});
		Router.route('/restoraunts/restoraunt_menu', function(url) {
			showHeroes("anticipation");
		});
		Router.route('/shares', function(url) {
			showHeroes("anticipation");
		});
		Router.route('/shares/show', function(url) {
			showHeroes("anticipation");
		});
	}, 320);
});