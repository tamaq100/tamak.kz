var yamapt;
var ADRESSESRESTO = [];
var restor;
$(function() {
	var restoraunt_menue = $('#restoraunt_menue');
	if (restoraunt_menue.length) {

		var container = $('.js-wrap-container-rest');
		container.addClass('preload');

		var $uri = new URI();
		var $uriParam = $uri.search(true);
		var $id = $uriParam.id;

		var url = Requests.getServerName() + '/api/services';
		$.get(url, {
			'ids': $id
		}, function(data) {
			if (data[0].name) {
				$('.restoname').text(data[0].name);
			} else {
				$('.restoname').text(data[0].id);
			}
			$('.menu-main').append(
				RenderTabs.render(restoraunt_menue.html(), data[0])
			);
			$('.restoraunt__wrap-hlist a[data-nav="' + $uri.pathname() + '"]').addClass('active');
			container.removeClass('preload');
			onLike();
		}).done(function(data) {
			var url;
			var oldData;
			switch ($uri.pathname()) {
				case '/restoraunts/restoraunt_menu':

					oldData = data[0] ? data[0] : {};
					console.log("===== restoraunt_menu =====", oldData);

					oldData.tags = oldData.product_tags ? oldData.product_tags.split('|') : [];
					oldData.tags = oldData.tags.sort(function(a,b){
						return a === "Акция"? 1 : -1;
					}).reverse();
					url = Requests.getServerName() + '' + $id;

					$('.resto-slides').append(
						RenderTabs.render(
							$('.rest-slide-data[data-slide="menu"]').html(),
							oldData
						)
					);
					console.log('1');
					/* 
					#@!POINT
					===========
					*/
					console.log('2');
					navProducts();
					console.log("==== baseRestSlideCategories ===", oldData);
					baseRestSlideCategories(oldData);
					/* 
						#@!POINT
						===========
					*/
					break;
				case '/restoraunts/restoraunt_reviews':
					oldData = data ? data : [];

					url = Requests.getServerName() + '/api/services/rates/' + $id;
					$.get(url).done(function(data) {
						oldData[0].reviews = data;
						$('.resto-slides').append(
							RenderTabs.render(
								$('.rest-slide-data[data-slide="reviews"]').html(),
								oldData[0]
							)
						);
						$('.thumb').click(function(e) {
							var $dataCheck = $(this).data('check');
							$('.thumb').removeClass('check');
							if (String($dataCheck) === $('#valuation').val())
								$('#valuation').val('');
							else
								$(this).addClass('check');
								$('#valuation').val($dataCheck);
						});
					}).fail(function(){
						$('.resto-slides').append(
							RenderTabs.render(
								$('.rest-slide-data[data-slide="reviews"]').html(),[]
							)
						);
						$('.thumb').click(function(e) {
							var $dataCheck = $(this).data('check');
							$('.thumb').removeClass('check');
							if (String($dataCheck) === $('#valuation').val())
								$('#valuation').val('');
							else
								$(this).addClass('check');
								$('#valuation').val($dataCheck);
						});
					});
					break;
				case '/restoraunts/restoraunt_info':
					oldData = data[0] ? data[0] : {};

					var cities = JSON.parse(localStorage.getItem('cities'));
					console.log('cities', cities);
					console.log('oldData.addresses', oldData);
					
					ADRESSESRESTO = [{ latitude: oldData.latitude, longitude: oldData.longitude }];
					restor = oldData;
					yamapt = yandexMapScript("initMapInfo");
					$("body").append(yamapt);
					$('.resto-slides').append(
						RenderTabs.render(
							$('.rest-slide-data[data-slide="info"]').html(),
							oldData
						)
					);
					break;
				default:
					break;
			}
			data = null;
		});

	}
});

/**
 * @param  {} toggling
 * @param  {jQuery} $this
 */
function reviewsNav(toggling, $this) {
	$('.micro-nav__text').removeClass('micro-nav__text--active');
	$($this).addClass('micro-nav__text--active');
	var reviewValuationFalse = $('.review[data-valuation="false"]');
	var reviewValuationTrue = $('.review[data-valuation="true"]');
	
	switch (toggling) {
		case 'all':
			$('.review').slideDown();
			break;
		case 'positive':
			reviewValuationTrue.slideDown();
			reviewValuationFalse.slideUp();
			break;
		case 'negative':
			reviewValuationTrue.slideUp();
			reviewValuationFalse.slideDown();
			break;
		default:
			return;
	}
}

function navProducts() {
	// addToBasket();
	window.setTimeout(function() {
		Basket.addTo();
	}, 20);

	$('.restoraunt-sidebar__title').unbind('click').click(function(e) {
		e.preventDefault();
		// Инициируем хэндлеры для добавления товаров в корзинку
		Basket.addTo();
		// Инициализируем служебные переменные
		var $self = {
			"key": $(this).data('key')
		};
		var $id = (new URI()).search(true).id;
		// делаем нашу навку активной
		$('.restoraunt-sidebar__title').removeClass('restoraunt-sidebar__title--active');
		$(this).addClass('restoraunt-sidebar__title--active');
		//если нет нашего слайда то запрашиваем его
		if (!($('.product-slides__slide[data-slide="' + $self.key + '"]').length)) {
			console.log('=== product slides key ===');
			// Добавляем прелоадер на слайды
			console.log('click add preload');
			$('.product-slides__container').addClass('preload');

			Requests.getProductList({
					"id": $id,
					"from": 0,
					"count": 100,
					"tag": $self.key
				},
				function(data) {
					console.log('!!!! productList !!!!!', data);
					var neData = tagsSort({
						"products": data,
						"slideKey": $self.key
					});

					data = null;
					$('.product-slides__container')
						.append(
							RenderTabs.render(
								$('.rest-slide-category').html(),
								neData
							)
						);
					neData = null;
					procedureBigSlider($self);
				}
			);

		} else {
			$('.product-slides__slide[data-slide="' + $self.key + '"]').show();
			procedureBigSlider($self);
		}

	});
}

function baseRestSlideCategories(restoData) {
	console.log('!!! baseRestSlideCategories', restoData);
	if ($('.product-slides__slide[data-slide="baseCategory"]').length) {
		procedureBigSlider({"key":"baseCategory"});
		return;
	}

	console.log('baseRestSlideCategories add preload');
	$('.product-slides__container').addClass('preload');
	var lenTagsCounter = restoData.tags.length;
	var genTag = (function() {
		var counter = 1;
		var memory = [];
		return {
			getParamsFromTags: function(tagsArr) {
				genTag.setCounter(--counter);
				var dublicate = memory.findIndex(function(tag) {
					return tagsArr[counter] === tag;
				});

				if (dublicate + 1) {
					memory.push('dublicate');
				} else {
					memory.push(tagsArr[counter]);
				}
				console.log("el tag " + counter, tagsArr[counter]);

				return {
					"from": 0,
					"count": 100,
					"ands": {
						"overlap": [{
							"field": "tags",
							"type": "ArrayString",
							"value": tagsArr[counter]
						}],
						"eq": [{
							"field": "service.id",
							"type": "UUID",
							"value": restoData.id
						}]
					}
				};
			},
			getCategories: function() {
				return memory;
			},
			setCounter: function(count) {

				if (lenTagsCounter < count) {
					count = lenTagsCounter;
				}
				if (count < 0) {
					count = 0;
				}

				counter = count;
			}
		};
	})();

	/**
	 * @param  {products[]} array
	 * @param  {} category
	 */
	function renderBaseCategory(array, category) {
		console.log('render base category', category);
		console.log('render base array', array);

		var newAArray = [];
		for (var i = 0, length1 = array.length; i < length1; i++) {
			newAArray[i] = {
				"data": array[i],
				"category": category[i]
			};
		}
		newAArray = newAArray.reverse();
		newAArray.sort(function(a, b) {
			console.log("sort" ,a.category);
			return a.category === "Акция"? 1 : -1;
		});
		newAArray = newAArray.reverse();

		newAArray[0].slide = true;
		$('.product-slides__container')
			.append(
				RenderTabs.render(
					$('.rest-slide-categories').html(),
					newAArray
				)
			);
		procedureBigSlider({"key":"baseCategory"});
	}
	
	// genTag.setCounter(restoData.tags.length);
	// $.when(
	// 	Requests.asyncPost("services/products/list", genTag.getParamsFromTags(restoData.tags)),
	// 	Requests.asyncPost("services/products/list", genTag.getParamsFromTags(restoData.tags)),
	// 	Requests.asyncPost("services/products/list", genTag.getParamsFromTags(restoData.tags))
	// ).then(function(first, second, third) {
	// 	console.log('dataTags', restoData.tags);
	// 	// Здесь может и будет дофига ошибок
	// 	renderBaseCategory([first[0],second[0],third[0]], genTag.getCategories());
	// });

	genTag.setCounter(restoData.tags.length);
	var kek = [];
	for (var i = 0; restoData.tags.length > i; i++) {
		kek.push(Requests.asyncPost("services/products/list", genTag.getParamsFromTags(restoData.tags)));
	}

	Promise.all(kek).then(function(values) {
		// console.log("Promise.all", values);
		var result = values;
		renderBaseCategory(result, genTag.getCategories());
		// values.forEach(function(el, idx) {
			// console.log("el:" + idx, el);
		// })
	});
}

function procedureBigSlider($self) {

	$('.product-slides__slide')
		.not('[data-slide="' + $self.key + '"]')
		.hide();

	var $prodSlide = $('.product-slides__slide[data-slide="' + $self.key + '"]');
	if ($prodSlide.find('.restoraunt-big__slide').length > 1) {
		restorauntBig__sliderParams(
			$prodSlide,
			function() {
				$('.product-slides__container').removeClass('preload');
			}
		);
	} else {
		$('.product-slides__container').removeClass('preload');
	}
	Basket.addTo();
	$('.product-slides__container').removeClass('preload');
}

/*
	сортировка по тегам
	смысл в отделении популярных блюд от остальных
	object
*/
function tagsSort(data) {
	function onlyTag(tags, tag) {
		return !!tags.includes(tag);
	}
	// var mainCategory = data.slideKey; //- опорный элемент например Супы
	var newProducts = {
		popular: [],
		onlyKey: []
	};
	console.log("tagsSort = ", data);
	data.products.forEach(function(element, index) {
		if (onlyTag(element.tags, 'Акция'))
			newProducts.popular.push(element);
		else
			newProducts.onlyKey.push(element);
	});
	if (!newProducts.onlyKey.length) {
		newProducts.onlyKey = newProducts.popular;
		newProducts.popular = [];
	}
	data.products = newProducts;
	newProducts = null;
	return data;
}