var chatRoles = {
	"Executor": "kchat",
	"User": "chat"
};

function toChat(id) {
	window.location.href = window.location.protocol + '//' + window.location.hostname + '/cabinet/' + chatRoles[User.getMe().role] + '?id=' + id;
}

function getChatId() {
	var $uri = new URI();
	return $uri.search(true).id;
}
var chatHTMLStorage = {
	"data": '',
};

function chatRender() {
	var $id = getChatId();
	var chat = $(".js-wrap-slide[data-chatid=" + getChatId() + "]");
	if ($id) {
		Requests.asyncGeting('orders/' + $id).done(function (data) {
			if (chat.length)
				chat.remove();

			var chatData = {
				data: data[0].hasOwnProperty('chats') ? data[0].chats.reverse() : false,
				courier: data[0].executor,
				user: User.getMe().name,
				chatId: $id
			};

			$('.js-wrap-container')
				.append(RenderTabs.render(chatHTMLStorage.data, chatData));
			RemovePreload();
			sendingHandler();
		});
	} else {
		$('.js-wrap-container')
			.append(RenderTabs.render(chatHTMLStorage.data, lastChats()));
		RemovePreload();
	}
	sendingHandler();
	window.setTimeout(function () {
		chatRender();
	}, 1000 * 18);
}

function lastChats() {
	var lastChats = localStorage.getItem('lastChats');
	if (lastChats) return {
		"lastChats": JSON.parse(lastChats)
	};
	return {
		"lastChats": []
	};
}

function sendingHandler() {
	$('.white__block.message__area').unbind('submit').on('submit', function (e) {
		e.preventDefault();
		var $field = $('.message__field');
		Requests.asyncPost('order/chats/' + getChatId(), {
			"msg": $field.val()
		}).done(function (data) {
			$field.val('');
			window.setTimeout(function () {
				//TODO навесить адекватный обработчик
				chatRender();
			}, 25);
		});
	});
}