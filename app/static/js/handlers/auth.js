function handlerAuth() {
	if (!authForms.login.length) return '';
	$(authForms.login).on('submit', function(e) {
		e.preventDefault();
		// удаляем из логина все пробелы
		var $login = $('#login').find('input[name="user_provider_id"]');
		$login.val($login.val().replace(/\s/g, ''));
		if (typeof $(this).prop("disabled") === 'undefined' || $(this).prop("disabled") === false) {
			$(authForms.login).find('input').removeClass('form-control-alert');

			User.login(
				getFormData(authForms.login),
				function(status) {
					statusLoginFormsHandler(status, authForms.login);
					window.setTimeout(function() {
						$(authForms.login)
							.find('input')
							.removeClass('form-control-alert');
					}, 250);
				});

			$(authForms.login).removeAttr('disabled');
		}
	});
	$(authForms.login).change(function() {

		$(authForms.login)
			.find('input')
			.removeClass('form-control-alert')
			.removeClass('form-error');

	});
}

function handlerRegistration() {
	if (!authForms.register.length) return '';
	authForms.register.on('submit', function(e) {
		e.preventDefault();
		User.register(
			getFormData(authForms.register),
			function(status) {
				statusLoginFormsHandler(status, authForms.register);
				window.setTimeout(function() {
					$(authForms.register)
						.find('input')
						.removeClass('form-control-alert');
				}, 250);
				window.setTimeout(function() {
					$('#main-modal').find('a[href="#login"]').click();
				}, 400);
			});
	});
}
function statusLoginFormsHandler(status, $form) {
	$($form)
		.find('.form-error__desc')
		.toggleClass('hidden fade', true);
	if (status === 1) {
		$($form)
			.find('.form-control')
			.toggleClass('form-error', false);
		return '';
	}
	$($form)
		.find('.form-control')
		.toggleClass('form-error', true);
	$($form)
		.find('input')
		.addClass('form-control-alert');
}
function handlerRecovery() {
	if (authForms.recovery.length)
		authForms.recovery.on('submit', function() {
			// Для восстановления пароля
			window.setTimeout(function() {
				authForms.recovery.find('.back-chevron-round').click();
			}, 400);
		});
}