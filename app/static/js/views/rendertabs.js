var HandlebarsLog = false;
Handlebars.registerHelper('counterInArray', function (object, how) {
	var totalCount = 0;
	object.forEach(function (currentValue) {
		totalCount += currentValue[how];
	});
	return totalCount;
});

var orderStatus = {
	"Draft": "Черновик",
	"FindExecutor": "Поиск курьера",
	"Deleted": "Удалён",
	"Published": "Опубликован",
	"ExecutorSelected": "Назначен курьер",
	"GoingToProvider": "Курьер в пути к ресторану",
	"ArrivedToProvider": "Курьер прибыл в ресторан",
	"GoingToCustomer": "Курьер на пути к заказчику",
	"Confirmed": "Заказ подтвержден",
	"Overdue": "Заказ просрочен",
	"CancelCustomer": "Отменён пользователем",
	"ArrivedToCustomer": "Доставляется курьером",
	"CancelExecutor": "Отклонён Курьёром",
	"Done": "Завершен"
};
var courierStatuses = {
	"ExecutorSelected": "Назначен курьер",
	"GoingToProvider": "Курьер в пути к ресторану",
	"ArrivedToProvider": "Курьер прибыл в ресторан",
	"Confirmed": "Заказ подтвержден",
	"GoingToCustomer": "Курьер на пути к заказчику",
	"ArrivedToCustomer": "Доставляется курьером"
};
Handlebars.registerHelper('readyCourierChat', function (status) {
	return courierStatuses.hasOwnProperty(status);
});
Handlebars.registerHelper('stringWithDelimiters', function (array) {
	var k = Array.isArray(array) ? array : [];
	function reducer(str, el) {
		return str + el.value_ru + '|';
	}
	return k.reduce(reducer, '').slice(0, -1);
});

Handlebars.registerHelper('prod_issetShares', function (list) {
	var result = list.split('|');
	return result.includes('Акции');
});
Handlebars.registerHelper('prod_removeShares', function (list) {
	var result = list ? list.split('|') : [];
	if (result.indexOf('Акции') !== -1)
		result.splice(result.indexOf('Акции'), 1);
	if (result.indexOf('Акция') !== -1)
		result.splice(result.indexOf('Акция'), 1);

	return result.join(' | ');
});

function timeOffsetPlus(str, format) {
	var DateK = new Date(str).getTime();
	DateK = (new Date(DateK + timeOffsetMs)).format(format);
	return DateK;
}
Handlebars.registerHelper('timeOffsetPlus', function (timeString) {
	if (timeString && timeString.length)
		return timeOffsetPlus(timeString, 'yyyy-mm-dd HH:MM:ss');

	return timeString;
});
Handlebars.registerHelper('timeOffsetUnixPlus', function (unix) {
	unix += timeOffsetMs;
	return unix;
});
Handlebars.registerHelper('timeOffsetPlusYMD', function (timeString) {
	if (timeString && timeString.length)
		return timeOffsetPlus(timeString, 'yyyy-mm-dd');

	return timeString;
});
Handlebars.registerHelper('readyCourierChat', function (status) {
	return courierStatuses.hasOwnProperty(status);
});
Handlebars.registerHelper('basketOrderedStat', function () {
	return !!(localStorage.getItem('orderedStatus') && (Basket.getOrderedStat() && Basket.getOrderedStat().status));
});
Handlebars.registerHelper('isCard', function () {
	return Basket.currentPay() === 'card';
});
Handlebars.registerHelper('isCash', function () {
	return Basket.currentPay() === 'cash';
});

function typeToTime(_to) {
	var temp = window.localStorage.getItem('type_to_time');
	return !!(temp && (temp === _to));
}
Handlebars.registerHelper('toTime', function () {
	return typeToTime("toTime");
});
Handlebars.registerHelper('toFast', function () {
	var temp = window.localStorage.getItem('type_to_time');
	if (!temp) return true;

	return typeToTime("toFast");
});
Handlebars.registerHelper('orderStatus', function (object) {
	if (orderStatus[object]) return orderStatus[object];

	return 'В обработке';
});

Handlebars.registerHelper('isDeleted', function (object) {
	return 'Deleted' !== object;
});
Handlebars.registerHelper('removeAllCategory', function ($array, $removed_element) {
	$newarray = [];
	$array.forEach(function (elem) {
		var expression = elem.key === $removed_element;
		if (!expression) $newarray.push(elem);
	});
	return $newarray;
});
Handlebars.registerHelper('declOfNumDishes', function (products, how) {
	var totalCount = 0;
	products.forEach(function (currentValue) {
		totalCount += currentValue[how];
	});
	return declOfNum(totalCount, ['блюдо', 'блюда', 'блюд']);
});

Handlebars.registerHelper('declOfNumOVAV', function ($Massive) {
	return declOfNum($Massive.length, ['отзыв', 'отзывов', 'отзыва', 'отзыва', ]);
});
Handlebars.registerHelper('typerate_executor', function (element) {
	return element === 'rate_executor';
});

Handlebars.registerHelper('declOfNumOVAVSUM', function (a, b) {
	return declOfNum(a + b, ['отзыв', 'отзыва', 'отзывов']);
});

Handlebars.registerHelper('sum2', function (a, b) {
	return a + b;
});

Handlebars.registerHelper('Prev_nav', function (a) {
	return a > 1;
});

Handlebars.registerHelper('Next_nav', function (a, pages) {
	return a < pages;
});

Handlebars.registerHelper('Next_navFull', function (a, pages) {
	// a = current page
	a = parseInt(a);
	var page = (a + 4 < pages) ? a + 4 : pages;
	var uri = new URI().setSearch(
		'page',
		page
	);

	return uri.href();
});

Handlebars.registerHelper('Prev_navFull', function (a) {
	var page = (a - 4 > 1) ? a - 4 : 1;
	var uri = new URI().setSearch(
		'page',
		page
	);
	return uri.href();
});

Handlebars.registerHelper('urlBuilder', function (param1, param2) {
	var uri = new URI().setSearch(param1, param2);
	return uri.href();
});

Handlebars.registerHelper('ratestar', function (object) {
	var output = '';

	var allPercent = (object.positive + object.negative) / 100;
	var step = allPercent * 20;
	var positivePercent = object.positive / allPercent;

	return new Handlebars.SafeString("<li>asaaa<li>");
});
Handlebars.registerHelper('isDublicate', function (element) {
	return element === 'dublicate';
});
Handlebars.registerHelper('days_sort', function (days, options) {
	var daysUnix = [];
	for (var i = days.length - 1; i >= 0; i--) {
		daysUnix[i] = Date.parse((days[i].created).split(' ')[0]);
	}
	daysUnix = daysUnix.filter(function (item, pos, self) {
		return self.indexOf(item) === pos;
	});
	daysUnix.sort();
	var NormalDate = [];
	for (var j = daysUnix.length - 1; j >= 0; j--) {
		NormalDate[j] = {};
		NormalDate[j].unix = daysUnix[j];
		var day = new Date(daysUnix[j]);
		var month = (day.getMonth() === 0) ? '01' : (day.getMonth() < 10) ? '0' + (day.getMonth() + 1).toString() : day.getMonth() + 1;
		var date = (day.getDate() === 0) ? '01' : (day.getDate() < 10) ? '0' + (day.getDate()).toString() : day.getDate();
		NormalDate[j].date = day.getFullYear() + '-' + month + '-' + date;
	}
	return options.fn(NormalDate);
});

Handlebars.registerHelper("normTime", function (time) {
	if(HandlebarsLog) console.log("Handlebars normTime", time);
	// var hours = (time / 60);
	// var rhours = Math.floor(hours);
	// var minutes = (hours - rhours) * 60;
	// var rminutes = Math.round(minutes);
	// if (rminutes == 0) 
	// 	rminutes += '0';
	// console.log(`RenderTabs ${rhours} - ${rminutes}`);

	// return rhours + "." + rminutes;

	return time;
});

Handlebars.registerHelper('to_nix', function (day) {
	return Date.parse((day).split(' ')[0]);
});

Handlebars.registerHelper('user_is_auth', function () {
	if(HandlebarsLog) {
		console.log('UserIsAuthSTATUS', !!User.getStatus());
		console.log('UserIsAuthSTATUS', User.getStatus());
	}

	return !!User.getStatus();
});

Handlebars.registerHelper('Basket_is_ordered', function (idr) {
	return Basket.getOrderedStat().status;
});

Handlebars.registerHelper('is_favorite', function (idr) {
	return User.issetFavoriteServices(idr);
});

Handlebars.registerHelper('times', function (n, current, block) {
	var accum = '';
	for (var i = 0; i < n; ++i)
		accum += block.fn({
			times: i,
			current: current
		});
	return accum;
});
Handlebars.registerHelper('for', function (from, to, incr, block) {
	var accum = '';
	for (var i = from; i < to; i += incr)
		accum += block.fn(i);
	return accum;
});
// функция для вывода навбара
Handlebars.registerHelper('times2', function (n, current, limit, block) {
	if(HandlebarsLog) console.log('Times2', n, current, limit);
	var accum = '';
	// n - All
	// current - currentPage
	limit = parseInt(limit);
	current = parseInt(current);
	n = parseInt(n);
	var lowLim = ((current - limit) > 0) ? current - limit : 1;
	if (lowLim === 1)
		limit += lowLim;

	var maxLim = ((current + limit) < n) ? current + limit : n;
	for (var i = lowLim; i <= maxLim; i++) {
		accum += block.fn({
			times: i,
			current: current
		});
	}
	return accum;
});
Handlebars.registerHelper("xif", function (expression, options) {
	return Handlebars.helpers["x"].apply(this, [expression, options]) ? options.fn(this) : options.inverse(this);
});
Handlebars.registerHelper('if_eq', function (a, b, opts) {
	if (a === b) // Or === depending on your needs
		return opts.fn(this);
	else
		return opts.inverse(this);
});
Handlebars.registerHelper("x", function (expression, options) {
	var result;
	// you can change the context, or merge it with options.data, options.hash
	var context = this;

	// yup, i use 'with' here to expose the context's properties as block variables
	// you don't need to do {{x 'this.age + 2'}}
	// but you can also do {{x 'age + 2'}}
	// HOWEVER including an UNINITIALIZED var in a expression will return undefined as the result.
	with(context) {
		result = (function () {
			try {
				return eval(expression);
			} catch (e) {
				console.warn('•Expression: {{x \'' + expression + '\'}}\n•JS-Error: ', e, '\n•Context: ', context);
			}
		}).call(context); // to make eval's lexical this=context
	}
	return result;
});

Handlebars.registerHelper('cond', function (expression, options) {
	var fn = function () {},
		result;
	try {
		fn = Function.apply(this, ['return ' + expression + ' ;']);
	} catch (e) {}
	try {
		result = fn.bind(this)();
	} catch (e) {}

	return result ? options.fn(this) : options.inverse(this);
});

Handlebars.registerHelper('useraddressesOption', function () {
	var result = false;
	result = JSON.parse(localStorage.getItem('UserAdresses'));
	result = result ? result.length : false;
	return result;
});
Handlebars.registerHelper('useraddressesOptionsss', function (id) {
	upAllCities();
	console.log("useraddressesOptionsss", id);
	var result = '';
	var addrs = [];
	var td = '';
	var td2 = '';

	addrs = JSON.parse(localStorage.getItem('UserAdresses'));
	if (addrs === null)
		return new Handlebars.SafeString(result);

	addrsLen = addrs.length;
	var i = 0;
	if (addrsLen) result += '<select class="form-control" id="' + id + '">';
	function allCitiesFunc(element, i) {
		if (element.key === addrs[i].town.key)
			return element;
	}
	console.log(AllCities, addrs);
	console.log(allCitiesFunc);
	while (addrsLen > i) {
		// statement
		var town = addrs[i].town.key;
		town = AllCities.find(function(element) {
			return allCitiesFunc(element, i);
		});

		/// УСЛОВИЕ ДЛЯ КОРРЕКТНОГО АДРЕСА
		result += '<option data-id="' + addrs[i].id + '" data-lat="' + addrs[i].latitude + '" data-lng="' + addrs[i].longitude + '">' + town.value_ru + ', ' + addrs[i].street + ', ' + addrs[i].house + '</option>';
		++i;
	}
	if (addrsLen) result += '</select>';

	return new Handlebars.SafeString(result);
});


Handlebars.registerHelper('useraddresses', function () {
	var result = '';
	var addrs;
	var td = '<td>';
	var td2 = '</td>';
	addrs = User.getMyAdresses();
	addrsLen = addrs.length;
	if (!addrsLen) {
		result = '<tr><td><span class="settings-table__label">Адрес:</span></td>';
		result += '<td><input name="" class="settings-table__input" value=""/></td>';
		return new Handlebars.SafeString(result);
	}
	result = '<tr><td><span class="settings-table__label">Адрес:</span></td>';
	result += '<td><input name="" class="settings-table__input" value="' + addrs[0].town + ', ' + addrs[0].street + '"/></td>';
	if (addrsLen > 1) {
		result += td;
		result += '<span class="settings-table__delete d-xs-none">Удалить</span>';
		result += '<span class="settings-table__delete settings-table__delete--small">×</span>';
		result += td2;
	} else {
		result += td;
		result += td2;
	}
	return new Handlebars.SafeString(result);
});
Handlebars.registerHelper('split', function (str, params) {
	return str.split(' ')[params];
});
Handlebars.registerHelper('FullDateToDate', function (date) {
	date = date.split(' ');
	date = date[0].replace(/-/g, '.');
	return date;
});
Handlebars.registerHelper('hasUserValuationrev', function (orderStatus, isValuation, valuations) {
	$returned = false;
	valuations.forEach(function (element) {
		if (element.typerate.key === isValuation)
			$returned = true;
	});
	return $returned;
});
Handlebars.registerHelper('hasUserValuation', function (orderStatus, isValuation, valuations) {
	$returned = false;
	valuations.forEach(function (element) {
		if (element.typerate.key === isValuation)
			$returned = true;
	});
	if ($returned) {
		return false;
	}
	return (orderStatus === 'Done');
});

Handlebars.registerHelper('percentagePercent', function (positive_valuations, negative_valuations) {
	if (typeof positive_valuations === 'undefined')
		positive_valuations = 0;
	if (typeof negative_valuations === 'undefined')
		negative_valuations = 0;

	var allPercent = (positive_valuations + negative_valuations) / 100;
	var positivePercent = positive_valuations / allPercent;
	if (isNaN(positivePercent)) positivePercent = 50;

	return positivePercent;
});
Handlebars.registerHelper('percentagePercentFive', function (valuations) {
	if (typeof valuations === 'undefined')
		valuations = 0;

	var allPercent = 5 / 100;
	var positivePercent = valuations / allPercent;
	if (isNaN(positivePercent)) positivePercent = 0;

	return positivePercent;
});

var RenderTabs = (function () {
	return {
		render: function (templhtml, dataJSON) {
			var template = Handlebars.compile(templhtml);
			return template(dataJSON);
		},
		templRender: function (param) {
			if(HandlebarsLog) console.log("RenderTabs = ", param);
			// console.log(getLastUrl());
		}
	};
})();