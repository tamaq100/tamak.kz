var AuthButtons = (function() {

	var nonAuthorized = $('.defaultLogin');
	var uAuthorized = $('.authLogin');
	var userName = $('.user-name__title');
	var userNumber = $('.account__info-block__number.tel').length ? $('.account__info-block__number.tel') : $('.courier__caption--tel');
	var userBonus = $('.account__info-block__number.bon').length ? $('.account__info-block__number.bon') : $('.courier__caption.bon');
	return {

		refresh: function() {
			if (User.getStatus() === 0) {
				nonAuthorized.removeClass('hidden fade');
				uAuthorized.addClass('hidden fade');
			} else {
				var uData = User.getMe();
				uAuthorized.find('.uname').text(uData.name);
				userName.text(uData.name).removeClass('invisible');
				userNumber.text(uData.phone).removeClass('invisible');
				if ($('.courier__caption.bon').length) {
					var userData = User.getMe();
					var allPercent = (userData.positive_valuations + userData.negative_valuations) / 100;
					var positivePercent = userData.positive_valuations / allPercent;
					$('.front-rate').css('width', positivePercent + '%');
					$('.courier__caption.transport').text(userData.transport.value_ru);
					userBonus.text(uData.balance).removeClass('invisible');
				} else {
					userBonus.text(uData.bonus_balance).removeClass('invisible');
				}
				nonAuthorized.addClass('hidden fade');
				uAuthorized.find('button').removeClass('smooth');
				uAuthorized.removeClass('hidden fade');
			}
		}

	}
}());