var authForms = (function() {
	var $login = $('.login-ajax');
	var $register = $('#register');
	var $recovery = $('.recovery-ajax');
	return {
		login: $login,
		register: $register,
		recovery: $recovery,
		modalClose: function(Modal) {
			$(Modal).closest('.modal').modal('hide');
		}
	};
})();