// Глобальные переменные и функции
// Берём ширину экрана
var timeOffsetMs;
var timeOffsetSec;

function getDateOffset() {
	Requests.asyncGeting('/servertime').done(function ($date) {
		var localDate = new Date().getTime();
		var serverDate = new Date($date).getTime();
		timeOffsetMs = Math.abs(localDate - serverDate); //ms
		timeOffsetSec = timeOffsetMs / 1000; //=> sec
	});
}

var widthScreen = document.documentElement.clientWidth;
// Проверяем существует ли
var isMain = !!document.querySelector('.main-slider-wrap');
// // Скрипты не требующие полной загрузки всех ресурсов
window.addEventListener("load", function () {
	Basket.refresh();

	$('#adressInput').submit(function (event) {
		event.preventDefault();
		event.stopPropagation();
	});
	upAllCities();

	getDateOffset();
///
	if (isMain) {
		popularRestorauntsRender();
		promotionalDishes();
		window.setTimeout(function () {
			$('.slider').each(function () {
				var $this = $(this);
				var sliderData = $this.data('slider');
				if (!sliderData) return;
				window[sliderData]($this, function () {
					$this.css({
						'overflow': 'visible',
						'max-height': 'none',
						'opacity': '1',
					});
				});
			});
		}, 50);

		Requests.getServices({
			"from": 0,
			"count": 10,
			"ands": {
				// "eq_ands": [{
				// 	"field": "categories.key",
				// 	"type": "String",
				// 	"value": "all_restaurants"
				// }]
			},
			"like": [{
				"field": "tags",
				"type": "String",
				"value": "Популярное"
			}],
			"sort": [{
				"field": "distance",
				"type": "Double",
				"asc": true
			}],
			"latitude": CookieLat,
			"longitude": CookieLng
		}, function (data) {
			$('.restaurants .container .row')
				.append(RenderTabs.render(document.getElementById('restaurants').innerHTML, data));
			$('.row.slick-in-modile').slick(restaurants__widgetSliderConfig);
		});
		window.setTimeout(function () {
			var yamapt = yandexMapScript("mapM");
			$("body").append(yamapt);
		}, 2000);

		if (widthScreen <= 767)
			$('[data-toggle="popover"]').popover('destroy');
		$('#category-toggle').click(function (event) {
			event.preventDefault();
			$(this).children('.category-toggle-arrow').toggleClass('arrow-up');
			$('.main-categories-wrap').css("height", "100%");
			$('#hidden-main-categories').stop().slideToggle();
		});

		$(window).resize(function () {
			if (this.resizeTO) clearTimeout(this.resizeTO);
			this.resizeTO = setTimeout(function () {
				$(this).trigger('resizeEnd');
			}, 600);
		});

		$(window).bind('resizeEnd', function () {
			if (windowIsCriticallyResized())
				ResizeAuto();
		});
		ResizeAuto();
	}

	var maskList = $.masksSort(
		$.masksLoad("/vendor/phone-codes.json"), 
		['#'],
		/[0-9]|#/,
		"mask"
	);
	$('[data-phone]').inputmasks({
		inputmask: {
			definitions: {
				'#': {
					validator: "[0-9]",
					cardinality: 1
				}
			},
			clearMaskOnLostFocus: true,
			showMaskOnHover: true,
			autoUnmask: false
		},
		match: /[0-9]/,
		replace: '#',
		list: maskList,
		listKey: "mask"
	});
	/*
	SERVICE WORKER INITIALIZE
	*/
	try {
		if ('serviceWorker' in navigator) {
			// Регистрация service worker-а, расположенного в корне сайта
			// за счет использования дефолтного scope (не указывая его)
			navigator.serviceWorker.register('/sw.js', {
				scope: '/'
			}).then(function (registration) {
				console.log('Service worker зарегистрирован:', registration);
			}).catch(function (error) {
				console.log('Ошибка при регистрации service worker-а:', error);
			});
		} else {
			// Текущий браузер не поддерживает service worker-ы.
			console.log('Текущий браузер не поддерживает service worker-ы');
		}
	} catch (error) {}

	window.setTimeout(function () {
		var $btnUP = $('#button-up');
		$(window).scroll(function () {
			if ($(this).scrollTop() > 100) {
				// Функция для тоггл бара
				if ($btnUP.is(':hidden'))
					$btnUP.css({
						opacity: 1
					}).fadeIn('slow');
			} else {
				$btnUP.stop(true, false).fadeOut('fast');
			}
		});
		$('#button-up').click(function () {
			$('html, body').stop().animate({
				scrollTop: 0
			}, 300);
		});
		$('.link').click(function () {
			$('.block').css("height", "200px");
		});
	}, 200);

	if ($('.faq').length)
		$('.faq-item').click(function (event) {
			event.preventDefault();
			$this = $(this);
			$this.toggleClass('opened').siblings('.faq-full-text').stop().slideToggle();
		});

	// вставляет слова из популярных запросов в поиск
	$('[data-toggle="popover"]').popover();
	$('.popular-query').click(function () {
		var text = $(this).text();
		$('#input-search').val(text);
		$(".search-wrap").submit();
	});

	$("#input-search").on('keypress', function (e) {
		if (e.which === 13) {
			window.location.href = window.location.protocol + '//' + document.domain + '/search?value=' + $(this).val();
		}
	});
	


	$('.dropdown-toggle').dropdown();

	// Главная функция
	getMobileOperatingSystem(function (userAgent) {
		if (/android/i.test(userAgent)) {
			$('.apple').remove();
		}
		if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
			$('.android').remove();
		}
	});

	(function(){
		var $mobileApp = $('.mobile-app');
		var $pageInner = $('.page-inner');
		if (localStorage.getItem('appStatus') != 'disable')
			$mobileApp.removeClass('disable');
		else
			$pageInner.removeClass('addTopApp');
		$('.mobile-app-close').on('click',
			function () {
				$mobileApp.addClass('disable');
				$pageInner.removeClass('addTopApp');
			});
	})();

	(function(){
		var dropdownLi = $('.drop-menu').find('li');
		var dropLen = dropdownLi.length;
		var delayAnim = -300;
		for (var i = 0; i < dropLen; i++) {
			$(dropdownLi[i]).css({
				'animation-delay': delayAnim + 'ms'
			});
			delayAnim += 150;
		}
	})();
});
// скрипты при ресайзе
var windowIsCriticallyResized = (function() {
	var windowSize = document.documentElement.clientWidth + 70;
	var pxChangeLimit = 50;
	return function() {
		if (
			((windowSize + pxChangeLimit) < document.documentElement.clientWidth) ||
			((windowSize - pxChangeLimit) > document.documentElement.clientWidth)
		) {
			windowSize = document.documentElement.clientWidth;
			return true;
		}
		return false;
	}
})();

function ResizeAuto() {
	window.setTimeout(
		AutoHeightPure(document.getElementsByClassName('restaurant')),
		0);
	window.setTimeout(
		AutoHeightPure(document.querySelectorAll('.promo-item-info')),
		0);
	window.setTimeout(
		AutoHeightPure(document.querySelectorAll('.promo-item-pic > img')),
		0);
}
//Обработчики ресайза