<?php
$lang = array(
    'yes' => 'Да',
    'no'  => 'Нет',
    'validation' => array(
        'empty_field'                           => 'Поле должно быть заполнено',
        'captcha'                               => 'Вы не прошли проверку',
        'incorrect_phone'                       => 'Некорректный телефон',
        'incorrect_email'                       => 'Некорректный Email',
        'incorrect_specialist_letter'           => 'Произошел сбой, попробуйте перезагрузить страницу и попробовать снова',
    ),
    '404' => array(
    	'text' => '404 Страница не найдена', 
    ),
);

$messages = include_once 'messages.php';
$lang = $lang + $messages;
return $lang;

