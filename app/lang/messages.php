<?php
$messages = array(
    'mails' => [
    	'admin' => [
    		'after_feedback_send' => [
    			'subject' => 'Новое сообщение в обратной связи',
    		],
    	],
    ],
);

return $messages;