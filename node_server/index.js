"use strict"
const startUpTime = (new Date()).getTime();
const config = require('./config.json');
const port = process.env.PORT || 3036;
const app = require('express')();

const request = require('request');
const protocol = require('http');

const logger = require('./helpers/logger.js');
logger.info('Start');

const server = protocol.Server(app);

let cookieAdmin;

const bodyParser = require('body-parser');
app.use(bodyParser.json()); // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({ // to support URL-encoded bodies
    extended: true
}));


app.post('/user/list', function (req, responsiveServer) {
    console.log(req.body);
    if (req.body.key != config['homeKey']) {
        responsiveServer.status(403).end()
    }
    var data;
    if (!cookieAdmin) {
        data = adminAuth(getUsersList, responsiveServer)
    } else {
        logger.info('getUsersList');
        data = getUsersList(responsiveServer)
    }
});
app.get('/executors', function (req, res) {
    var data;
    if (!cookieAdmin) {
        data = adminAuth(
            getExecutorsList,
            res
        )
    } else {
        logger.info('getUsersList');
        data = getExecutorsList(res);
    }
});
app.get('/userall', function (req, responsiveServer) {
    var data;
    logger.info('userall ept');
    if (!cookieAdmin) {
        logger.info('adminAuth');

        data = adminAuth(getUsersAllList, responsiveServer)
    } else {
        logger.info('getUsersAllList');
        data = getUsersAllList(responsiveServer)
    }
});

app.get('/lifecheck', function (req, res) {
    res.json({
        'life': true,
        'uptime': `${Math.ceil((
            ((new Date()).getTime() - startUpTime) / 1000
        ))} sec`,
        'start': new Date(startUpTime)
    });
});

app.get('/express', function (req, responsiveServer) {
    var data;
    logger.info('userall ept');
    if (!cookieAdmin) {
        logger.info('adminAuth');
        data = adminAuth(getUsersAllList, responsiveServer)
    } else {
        logger.info('getUsersAllList');
        data = getUsersAllList(responsiveServer)
    }
});

function adminAuth(callback, responsiveServer) {
    logger.info('adminAuth');
    var adminAuthOptions = {
        url: config['apiServerTamaq'] + 'login',
        body: JSON.stringify(config['admin']),
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        }
    }
    request(adminAuthOptions, function (error, response) {
        cookieAdmin = response.headers['set-cookie'];
        if (typeof callback == "function") {
            callback(responsiveServer)
        }
        return;
    });
}
// initialization
(function () {
    var adminAuthOptions = {
        url: config['apiServerTamaq'] + 'login',
        body: JSON.stringify(config['admin']),
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        }
    }
    request(adminAuthOptions, function (error, response) {
        if (response.body.length) {
            var resp = JSON.parse(response.body);
            if (resp.user_provider_id) {

                console.error('\x1b[31m%s\x1b[25m', resp.user_provider_id[0]);

                logger.info('FAIL login');
                process.exit(-1);
            }
        } else {
            logger.info('OKLOG login');

            console.log('\x1b[32m%s\x1b[0m', 'Okay');
        }
    })
})();

function getUsersAllList(responsiveServer) {
    logger.info('getUsersAllList');

    var usersListOptions = {
        url: config['apiServerTamaq'] + 'users/list',
        body: JSON.stringify({
            "from": 0,
        }),
        method: 'POST',
        headers: {
            'cookie': cookieAdmin,
            'Content-Type': 'application/json'
        }
    }
    request(usersListOptions, function (error, response) {
        if (response.status == "403") {
            adminAuth(getUsersAllList);
        } else {
            responsiveServer.end(response.body);
        }
    });
}

function getUsersList(responsiveServer) {
    logger.info('getUsersAllList');
    var usersListOptions = {
        url: config['apiServerTamaq'] + 'users/list',
        body: JSON.stringify(config['optionsListUsers']),
        method: 'POST',
        headers: {
            'cookie': cookieAdmin,
            'Content-Type': 'application/json'
        }
    }
    request(usersListOptions, function (error, response) {
        if (response.status == "403") {
            adminAuth(getUsersList)
        } else {
            responsiveServer.end(response.body);
        }
    });

}

function getExecutorsList(res) {
    logger.info('getExecutorsList');
    const usersListOptions = {
        url: config['apiServerTamaq'] + 'users/list',
        body: JSON.stringify({
            "from": 0,
            "ands": {
                "eq": [{
                    "field": "role",
                    "type": "String",
                    "value": "executor"
                }, {
                    "field": "ask_for_executing",
                    "type": "Boolean",
                    "value": "true"
                }],
            },
            "sort": [{
                "field": "name",
                "type": "String",
                "asc": true
            }]
        }),
        method: 'POST',
        headers: {
            'cookie': cookieAdmin,
            'Content-Type': 'application/json'
        }
    }
    request(usersListOptions, function (error, response) {
        if (response.status == "403") {
            adminAuth(getExecutorsList);
        } else {
            res.end(response.body);
        }
    });
}

server.listen(port, function () {
    console.log('listening on *:' + port);
    logger.info('listening on *:' + port);
    console.log('listen function');
});