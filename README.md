# TAMAQ
**[⇓ вниз ⇓](#глава-1-front-end)** 

**[⇓ Всякое разное](#всякое-разное)** 

И так tamaq это сервис доставки еды который состоит из нескольких частей


### Сторонний API + Админка
[Ссылка на полную документацию к API](https://documenter.getpostman.com/view/13147/6YsWcJb#301c6563-fd4c-987a-2609-2509d8201ea3)  
предположительно написано на Java

[Ссылка на их админку](https://admin.tamaq.kz/)  
написано на Ember.js

### Наша часть (micro-backend [PHP](#Глава-2---Back-end) + [front-end](#Глава-1---Front-end))
[Ссылка на наш сайт](https://tamaq.online/)

стек и требования к нашей части:
- php 5.6+
- mysql 5.6+
- node.js 10

Настройки прокси nginx для Tamaq

```nginx
server {
    server_name tamaq.loc;


    access_log /var/log/nginx/tamaq-access.log;
    error_log /var/log/nginx/tamaq-error.log;

    proxy_connect_timeout 600;
    proxy_send_timeout 600;
    proxy_read_timeout 600;
    send_timeout 600;

    location /api/ {
        proxy_pass https://tamaq.kz/;
        proxy_intercept_errors on;
        recursive_error_pages on;
        proxy_buffering off;
        proxy_set_header        X-Real-IP $remote_addr;
        proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header        Host $http_host;
    }

    include templates/base.conf;
    include templates/php70.conf;
}
```

## Глава 1 Front-end
**[⇓ вниз ⇓](#глава-2-back-end) ⇅ [⇑ вверх ⇑](#tamaq)**  

Весь фронтенд завязан по большей части на [API](#Сторонний-API-+-Админка) и всё обращается на /api/ который проксируется к tamaq.kz/api  
  

Что желательно понимать\читать\скачать для работы с фронтом 
- [Insomnia](https://insomnia.rest/download/) инструмент для лёгкого взаимодействия с api
- [Handlebars.js](https://handlebarsjs.com/) шаблонизатор для фронтенда

фронтенд частично рендерится на php и частично рендерится у клиента с помощью [Handlebars.js](https://handlebarsjs.com/)

#### **О static/js**
Здесь была попытка реализовать частичную mvc модель  
Важные вещи  
**Router** -> helpers/router.js
Основной объект для роутинга, т.е подгружения и запуска определённых ресурсов и скриптов после смены **url**  
Реализовано это всё с помощью паттерна Observer  

пользоваться можно вот так
```javascript
Router.route('/cabinet/kredact', function (url) {
    console.log('Мы тут')
    ChangeUrl('Личный Кабинет', url);
});
```
немного о папках и их сути:
- `js/handlers` -> расположены различные обработчики в т.ч роуты
- `js/helpers` -> В целом здесь расположены различные вспомогательные функции\объекты
- `js/views` -> для хэлперов к handlebars.js
- `js/amodels` -> здесь содержаться модели которые реализуют большую часть бизнес логики и запросов
- `js/*.js` -> здесь различные файлы в том числе и common.js в котором содержится основная загрузка приложения


## Глава 2 Back-end
**[~~⇓ вниз ⇓~~](#) [⇑ вверх ⇑](#глава-1-front-end)**

PHP + Node.js  
в PHP достойным внимания будет только модуль `app/helpers/apio.php` в нём происходит подгрузка каких бы то ни было данных из API

Теперь о горячем и наболевшем **Node.js**
находится эта штука в папке `/node_server/`  
Роль этого сервиса заключается в "обходе" API для php
т.к не все методы в API являются безопасными и разрешёнными для посторонних людей Nodejs логинится на сервере tamaq.kz и работает в качестве демона. Затем PHP либо Front-JS могут запрашивать у этого сервиса данные которые Nodejs им будет предоставлять. Для работоспособности этого сервиса необходимо иметь `логин и пароль администратора tamaq` и вставленный в `config.json` .


## Всякое разное
Логины\пароли ssh ключи доступ к серверу просить у Андрея Зейбеля

настройки nginx для tamaq front

```nginx
server {
    server_name app.tamaq.localhost;
    listen *:80;

    location / {
         proxy_pass http://172.22.0.1:4200;
         proxy_redirect off;
         proxy_set_header Host $host;
         proxy_set_header X-Real-IP $remote_addr;
         proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
         proxy_connect_timeout 90;
         proxy_buffer_size   2m;
         proxy_buffers   4 4m;
         proxy_busy_buffers_size   4m;
    }


    location /api_v1/ {
        proxy_pass https://tamaq.kz/;
        proxy_intercept_errors on;
        recursive_error_pages on;
        proxy_buffering off;
        proxy_set_header        X-Real-IP $remote_addr;
        proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header        Host $http_host;
    }

}
```


**[~~⇓ вниз ⇓~~](#) [⇑ вверх ⇑](#глава-2-back-end)**