<?php
$time_start = microtime(true);

define('WORK_DIR', __DIR__);
define('SITE_DIR', WORK_DIR . '/../');

if (isset($_GET['is_admin']) && $_GET['is_admin'] === 'true') {
	define('ADMIN_FOLDER', 'admin/');
} else {
	define('ADMIN_FOLDER', '');
}

$GLOBALS['enviropment'] = getenv('ENV') == 'docker_dev' ? 'dev' : 'prod';

session_start();
if (strstr($_SERVER['REQUEST_URI'], 'admin') && ! strstr($_SERVER['REQUEST_URI'], 'admin/auth/login')) {
    if (!isset($_SESSION['u_id']) || $_SESSION['u_id'] <= 0) {
        header('Location: /admin/auth/login?returnpath='.urlencode($_SERVER['REQUEST_URI']));
        exit;
    }
}

header('Cache-Control: no-cache');
header('Pragma: no-cache');

$is_ajax = ! empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';

if ($_SERVER["REMOTE_ADDR"] == '127.0.0.1') {
    $GLOBALS['enviropment'] = 'dev';
}

if ($GLOBALS['enviropment'] == 'dev') {
	define('PROTOCOL',  'http');
    error_reporting(E_ALL & ~E_DEPRECATED);
	ini_set('display_errors', '1');
} else {
    error_reporting(0);
}
error_reporting(0);

define('KB', pow(2, 10));
define('MB', pow(2, 20));
define('GB', pow(2, 30));
define('TB', pow(2, 40));
require_once SITE_DIR . 'vendor/autoload.php';
require_once SITE_DIR . 'app/helpers/functions.php';

// Автолоадер для загрузки классов
spl_autoload_register(function ($class) {
	if (strpos($class, 'Controller') !== FALSE) {
		$class = str_replace('Controller', '', $class);
		$class = strtolower($class);

		$file = SITE_DIR . ADMIN_FOLDER . 'app/controllers/' . $class . '.php';
		if (file_exists($file)) {
			require_once $file;
		}
	} elseif (file_exists(SITE_DIR . 'app/models/'.strtolower($class).'.php')) {
		require_once SITE_DIR . 'app/models/' . strtolower($class) . '.php';
	} elseif (file_exists(SITE_DIR . 'app/helpers/'.strtolower($class).'.php')) {
		require_once SITE_DIR . 'app/helpers/' . strtolower($class) . '.php';
	}
});

if ($_SERVER['SERVER_NAME'][2] == '.' && in_array(substr($_SERVER['SERVER_NAME'], 0, 2), BaseModel::$lang_versions)) {
	define('SITE_DOMAIN', substr($_SERVER['SERVER_NAME'], 3));

} else {
	define('SITE_DOMAIN', $_SERVER['SERVER_NAME']);
}

$time_start = getmicrotime();

if (isset($_GET['debug']) && $_GET['debug'] == '5e4h3dcfz80924505') {
	$_SESSION['debug_mode'] = true;
}

if (isset($_SESSION['debug_mode']) && $_SESSION['debug_mode']) {
	$GLOBALS['enviropment'] = 'dev';
}

$default_lang = 'ru';

$lang_subdomain = substr($_SERVER['SERVER_NAME'], 0, 2);

if (isset(BaseModel::$lang_versions[$lang_subdomain])) {
	$_SESSION['lang'] = $lang_subdomain;
} else {
	$_SESSION['lang'] = $default_lang;
}
if (in_array($lang_subdomain, BaseModel::$lang_versions)) {
    $_SESSION['lang'] = $lang_subdomain;
    define('SERVER_DOMAIN', substr($_SERVER['SERVER_NAME'], 3));
} else {
    $_SESSION['lang'] = current(BaseModel::$lang_versions);
    define('SERVER_DOMAIN', $_SERVER['SERVER_NAME']);
}



if (isset($_GET['is_admin']) && $_GET['is_admin'] === 'true') {
	$lang = require_once SITE_DIR . 'app/lang/admin.php';
} else {
	$default_lang_file = require_once SITE_DIR . 'app/lang/'.$default_lang.'.php';
	if (file_exists(SITE_DIR . 'app/lang/'.$_SESSION['lang'].'.php') && $_SESSION['lang'] != $default_lang) {
		$lang = require_once SITE_DIR . 'app/lang/'.$_SESSION['lang'].'.php';
	} else {
		$lang = $default_lang_file;
	}
}

/**
 * Функция для вывода языковых переменных
 * __('yes') - Да
 * __('city.field.country_id') - Страна
 */
function __($path, $params = array(), $force_lang = false, $need_false_if_not_found = false) {
    global $lang, $default_lang_file;

    $result = $lang;

    $keys = explode('.', $path);

    while ($key = array_shift($keys)) {
        if (isset($result[$key])) $result = $result[$key];
    }
    
    if (($result == $path) || is_array($result)) {
    	if ($need_false_if_not_found) {
    		return false;
    	}
    	return $path;
    }

    foreach ($params as $key => $value) {
        $result = str_replace('%'.$key.'%', $value, $result);
    }

    return $result;
}

function force__($path, $params, $force_lang) {
    if ($force_lang) {
    	$lang = require_once SITE_DIR . 'app/lang/'.$force_lang.'.php';
    }
    return __($path, $params, $force_lang);
}

date_default_timezone_set("Asia/Almaty");
setlocale(LC_ALL, "ru_RU.UTF-8");
DB::$TABLE_PREFIX = $_SESSION['lang'] . '_';

DB::connect();

// Объявляем переменные
$controller = isset($_GET['controller']) && $_GET['controller'] ? $_GET['controller'] : 'default';
$action     = isset($_GET['action'])     && $_GET['action']     ? $_GET['action']     : 'index';
$id 		= isset($_GET['id'])         && $_GET['id']         ? $_GET['id']         : false;


$controller = ucfirst($controller).'Controller';
$action = $action.'Action';

// Возможно, если метод не найден используется сокращенная нотация.
// Т.е. контроллер опускается, и вместо него сразу пишется экшн.
// Например http://example.com/error_404 это экшн error_404 контроллера DefaultController

// Проверяем существование нужного контроллера
if (! class_exists($controller, $autoload = true)) {
	// Проверяем на сокращенную нотацию
	if (method_exists(new DefaultController, $_GET['controller'] . 'Action')) {
		$controller = 'DefaultController';
		$action = $_GET['controller'].'Action';
		$id = $_GET['action'];
	} else {
		abort('404');
	}
}

$controller = new $controller;

// Проверяем существование нужного метода
if (! method_exists($controller, $action)) abort('502');


if (isset($_GET['is_admin']) && $_GET['is_admin'] === 'true') {
	BaseModel::$is_admin = true;
}

echo $controller->{$action}($id);
$is_ajax = !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
if (!$is_ajax) {
	$is_ajax = !empty($_SERVER['HTTP_ACCEPT']) && strtolower($_SERVER['HTTP_ACCEPT']) == 'application/json';
}

// Execution time of the script
if ($GLOBALS['enviropment'] == 'dev' && !$is_ajax) {
	show_memory_usage($time_start);
}
