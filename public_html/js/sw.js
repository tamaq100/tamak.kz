// self.addEventListener('install', function(event) {
// 	event.waitUntil(
// 		caches.open('v2').then(function(cache) {
// 			return cache.addAll([
// 				'/img/search-icon.png'
// 			]);
// 		})
// 	);
// });

// self.addEventListener('fetch', function(event) {
// 	// console.log('fetch');
// 	if (!(event.request.url.includes('image'))) {
// 		if (event.request.url.includes('api') || event.request.url.includes('/executors') || event.request.url.includes("maps")) {
// 			return fetch(event.request);
// 		}
// 	}
// 	event.respondWith(caches.match(event.request).then(function(response) {
// 		// caches.match() always resolves
// 		// but in case of success response will have value
// 		if (response !== undefined) {
// 			return response;
// 		} else {

// 			return fetch(event.request).then(function(response) {
// 				// response may be used only once
// 				// we need to save clone to put one copy in cache
// 				// and serve second one
// 				let responseClone = response.clone();

// 				caches.open('v2').then(function(cache) {
// 					cache.put(event.request, responseClone);
// 				});
// 				return response;
// 			}).catch(function() {
// 				return fetch(event.request);
// 			});
// 		}
// 	}));
// });
