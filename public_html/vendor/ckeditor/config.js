/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	config.filebrowserUploadUrl = '/admin/editor/upload';
    config.contentsCss = '/css/default.css';
    config.allowedContent = true;
    config.extraPlugins = 'panelbutton';
    config.extraPlugins = 'colorbutton';
    // config.colorButton_enableAutomatic = true;
	config.toolbar = 'Full';
	// config.colorButton_colors = 'CF5D4E,454545,FFF,CCC,DDD,CCEAEE,66AB16';

	config.toolbar_Full =
	[
		{ name: 'document', items : [ 'Source','-','Save','NewPage','DocProps','Preview','Print','-','Templates' ] },
		{ name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
		{ name: 'editing', items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
		{ name: 'forms', items : [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 
	        'HiddenField' ] },
		'/',
		{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
		{ name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv',
		'-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
		{ name: 'links', items : [ 'Link','Unlink','Anchor' ] },
		{ name: 'insert', items : [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe' ] },
		'/',
		// { name: 'colors', items : [ ] },
		{ name: 'styles', items : [ 'Styles','Format','Font','FontSize',  'TextColor','BGColor' ] },
		{ name: 'tools', items : [ 'Maximize', 'ShowBlocks','-','About' ] },
	];

	config.toolbar_Default =
	[
		{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','-','RemoveFormat' ] },
		{ name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-',
		'-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock' ] },
		{ name: 'insert', items : [ 'Table','Link','Unlink','Image','Iframe' ] },
		{ name: 'styles', items : [ 'Format','FontSize' ] },
		{ name: 'document', items : [ 'Source' ] },
		{ name: 'colors', items : [ 'TextColor','BGColor' ] },
	];

	config.toolbar_CompanyEdit =
	[
		{ name: 'basicstyles', items : [ 'Bold','Italic'] },
		{ name: 'paragraph', items : ['BulletedList'] },
		{ name: 'insert', items : [ 'Link','Unlink' ] },
	];

	config.entities = false;
	config.removePlugins = 'elementspath';

	config.pasteFilter = 'h1 h2 p ul ol table tr td li; img[!src, alt]; a[!href]';
	config.removeDialogTabs = 'image:advanced;link:advanced;link:upload';


	config.stylesSet = 'my_styles';
};

CKEDITOR.stylesSet.add( 'my_styles', [
    // Block-level styles.
    { name: 'Orange li', element: 'ol', styles: { color: 'Blue' } },
    { name: 'Red Title',  element: 'span', styles: { color: 'Red' } },

    // Inline styles.
    { name: 'CSS Style', element: 'span', attributes: { 'class': 'my_style' } },
    { name: 'Marker: Yellow', element: 'span', styles: { 'background-color': 'Yellow' } }
]);